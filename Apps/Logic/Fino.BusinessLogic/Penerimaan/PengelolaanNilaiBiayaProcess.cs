﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;

namespace Fino.BusinessLogic
{
    public class PengelolaanNilaiBiayaProcess : IPengelolaanNilaiBiayaProcess
    {
        public IPotonganRepository PotonganRepo { get; set; }
        public IBiayaNilaiOptionRepository BiayaNilaiRepo { get; set; }

        private void ValidateData(PengelolaanNilaiBiayaModel model)
        {
            if (model.Nilai == 0)
            {
                throw new ApplicationException("Nilai harus lebih besar dari 0");
            }
        }

        public ProcessResult GetBiayaList()
        {
            ProcessResult result = new ProcessResult();
            var listValue = new List<ValueList>();
            try
            {
                var model = PotonganRepo.GetRefBiaya();
                if (model != null)
                {
                    foreach (var b in model)
                    {
                        listValue.Add(new ValueList
                        {
                            Id = b.biaya_id,
                            Value = b.nama
                        });
                    }
                    result.DataResult = listValue;
                }
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetTingkatList()
        {
            ProcessResult result = new ProcessResult();
            var listValue = new List<ValueList>();

            try
            {
                foreach (KelasTingkatEnum tingkat in Enum.GetValues(typeof(KelasTingkatEnum)))
                {
                    listValue.Add(new ValueList
                        {
                            Id = (int)tingkat,
                            Value = (int)tingkat
                        });

                    result.DataResult = listValue;
                }

                result.IsSucess = true;
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetNilaiBiayaOption(int biaya_id, int option)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                var nilaiBiaya = BiayaNilaiRepo.GetNilaiBiaya(new Datalib.Entity.RefBiaya { biaya_id = biaya_id }, option);

                result.DataResult = nilaiBiaya;
                result.IsSucess = true;
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult UpdateNilaiBiayaOption(PengelolaanNilaiBiayaModel nilaiBiaya)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                ValidateData(nilaiBiaya);

                BiayaNilaiRepo.UpdateNilaiBiaya(new Datalib.Entity.BiayaNilaiOption
                    {
                        biayanilaioption_id = nilaiBiaya.BiayaNilaiOption_Id,
                        biaya_id = nilaiBiaya.Biaya_Id,
                        option = nilaiBiaya.Option,
                        nilai = nilaiBiaya.Nilai
                    });

                result.IsSucess = true;
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult DeleteNilaiBiayaOption(PengelolaanNilaiBiayaModel nilaiBiaya)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                BiayaNilaiRepo.DeleteNilaiBiaya(new Datalib.Entity.BiayaNilaiOption
                {
                    biayanilaioption_id = nilaiBiaya.BiayaNilaiOption_Id,
                    biaya_id = nilaiBiaya.Biaya_Id,
                    option = nilaiBiaya.Option,
                    nilai = nilaiBiaya.Nilai
                });

                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
