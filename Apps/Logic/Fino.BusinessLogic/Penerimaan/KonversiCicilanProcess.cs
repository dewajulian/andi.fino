﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using Fino.Datalib.Dbmodel;

namespace Fino.BusinessLogic
{
    public class KonversiCicilanProcess : IKonversiCicilanProcess
    {
        public IPosBiayaRepository PosBiayaRepo { get; set; }
        public IKonversiCicilanRepository KonversiCicilanRepo { get; set; }

        public ProcessResult GetJenisBiayaWithExistingPosBiaya(int p_SiswaId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                var resultModel = KonversiCicilanRepo.GetJenisBiayaWithExistingPosBiaya(p_SiswaId);

                result.DataResult = resultModel;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult CreateInstallment(List<KonversiCicilanHeaderModel> p_Model)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                var resultModel = KonversiCicilanRepo.CreateInstallment(p_Model);

                result.DataResult = resultModel;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GenerateInstallamentSimulation(KonversiCicilanHeaderModel p_Model)
        {
            ProcessResult result = new ProcessResult();

            try
            {                
                var posBiaya = PosBiayaRepo.GetPosBiaya(p_Model.PosBiaya_Id);
                var present = DateTime.Now;
                var firstCicilanDate = p_Model.MulaiTanggal;
                var lastCicilanDate = p_Model.MulaiTanggal.AddMonths(p_Model.Tenor - 1);
                p_Model.Details = new List<KonversiCicilanDetailModel>();

                var currCicilanDate = firstCicilanDate;
                var totalCicilan = posBiaya.Nilai - posBiaya.NilaiPotongan;
                double sumCicilan = 0;
                double cicilan = p_Model.NilaiCicilan;
                while (currCicilanDate <= lastCicilanDate)
                {
                    if ((sumCicilan + cicilan) > totalCicilan)
                    {
                        cicilan = totalCicilan - sumCicilan;
                    }
                    else
                    {
                        sumCicilan += cicilan;
                    }

                    p_Model.Details.Add(new KonversiCicilanDetailModel
                    {
                        JTempo = currCicilanDate,
                        NamaBiaya = string.Format("Kredit {0} {1}", posBiaya.Biaya.nama, currCicilanDate.ToString("MMM yy")),
                        NilaiBiaya = cicilan,
                        RefBiayaId = posBiaya.Biaya_Id
                    });
                    currCicilanDate = currCicilanDate.AddMonths(1);

                }
                result.DataResult = p_Model;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }


        public ProcessResult GetSiswaByNoInduk(string pNoInduk)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var model = KonversiCicilanRepo.GetSiswaByCode(pNoInduk);
                if (model != null)
                {
                    result.IsSucess = true;
                    result.DataResult = model;
                }
                
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
