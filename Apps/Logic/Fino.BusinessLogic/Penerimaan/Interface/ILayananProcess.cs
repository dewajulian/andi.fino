﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;

namespace Fino.BusinessLogic
{
    public interface ILayananProcess
    {
        ILayananRepository LayananRepo { get; set; }
        ProcessResult GetAllLayanan();
        ProcessResult GetAllLayananAsValueList();
        ProcessResult SimpanDaftarLayanan(DaftarLayananModel p_model);
    }
}
