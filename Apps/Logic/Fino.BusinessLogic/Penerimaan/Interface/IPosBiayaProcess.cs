﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;

namespace Fino.BusinessLogic
{
    public interface IPosBiayaProcess
    {
        IPosBiayaRepository PosBiayaRepo { get; set; }
        ProcessResult GetRekapitulasiPiutangDanPenerimaan(DateTime p_FromDate, DateTime p_ToDate);
        ProcessResult GetPosBiaya(int p_SiswaId, bool p_ShowAllBiaya);
        ProcessResult UpdatePembayaranPosBiaya(List<PosBiayaModel> p_PosBiayaModel, int p_SiswaId);
        ProcessResult GetLaporanKasDetail(DateTime p_ToDate, int p_KelasId, int p_SiswaId);
        ProcessResult SimpanCetakPembayaran(CetakPembayaranModel p_CetakPembayaranModel, List<PosBiayaPaidModel> p_PosBiayaPaidModel);
        ProcessResult GetAllKelas();
        ProcessResult GetSiswaUntukKelas(int p_KelasId);
        ProcessResult GetLaporanPenerimaanDetail(DateTime p_ToDate, int p_KelasId, int p_SiswaId);
    }
}
