﻿using Fino.Lib.Core;
using Fino.Repository;
using System;
using System.Collections.Generic;

namespace Fino.BusinessLogic
{
    public class RefAccountProcess : IRefAccountProcess
    {
        public IRefAccountRepository RefAccountRepo { get; set; }

        public ProcessResult UpdateRefAccount(Model.RefAccountModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                RefAccountRepo.UpdateRefAccount(entity);

                result.IsSucess = true;

                RefAccountRepo.DisposeDBContext();
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetRefAccount(int account_id)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = RefAccountRepo.GetRefAccount(account_id);
                result.IsSucess = true;
                RefAccountRepo.DisposeDBContext();
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAllRefAccount()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = RefAccountRepo.GetAllRefAccount();
                result.IsSucess = true;
                RefAccountRepo.DisposeDBContext();
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult DeleteRefAccount(Model.RefAccountModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                RefAccountRepo.DeleteRefAccount(entity);

                result.IsSucess = true;
                RefAccountRepo.DisposeDBContext();
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
