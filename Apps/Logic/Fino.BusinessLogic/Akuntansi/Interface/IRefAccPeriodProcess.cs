﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface IRefAccPeriodProcess
    {
        IRefAccPeriodRepository RefAccPeriodRepo { get; set; }
        ProcessResult UpdateRefAccPeriod(RefAccPeriodModel entity);
        ProcessResult UpdateRefAccPeriodStatus(int period_id, int status);
        ProcessResult GetRefAccPeriodModel(int period_id);
        ProcessResult GetAllRefAccPeriodModel();
    }
}
