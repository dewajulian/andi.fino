﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public class PostTransactionToJournalProcess : IPostTransactionToJournalProcess
    {        
        private List<AccountClassSetting> _settings;

        public ITransactionToJournalRepository TransactionToJournalRepo { get; set; }

        public ProcessResult GetActiveTransactionForJournal(DateTime p_Start, DateTime p_End)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = TransactionToJournalRepo.GetTransactionToPost(p_Start, p_End);
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult PostTransactionForJournal(List<TransaksiToJurnalModel> p_Model)
        {
            ProcessResult result = new ProcessResult();
            var JournalList = new List<JournalHeader>();
            
            int postedCount = 0;
            try
            {
                _settings = TransactionToJournalRepo.GetAllAccountClassSetting();
                if (_settings.Count.Equals(0))
                {
                    throw new ApplicationException("No account setting specified");
                }

                foreach (var model in p_Model)
                {
                    if (model.IsSelected)
                    {
                        // Create Receivables
                        if (model.AccountClassificationId.Equals((int)AccountClassificationEnum.PIUTANG))
                        {
                            JournalList.Add(GenerateReceivableJournal(model));
                            postedCount++;
                        }

                        // Create Canceled Receivables
                        if (model.AccountClassificationId.Equals((int)AccountClassificationEnum.CANCELED_PIUTANG))
                        {
                            JournalList.Add(GenerateCanceledReceivableJournal(model));
                            postedCount++;
                        }

                        // Create Income
                        if (model.AccountClassificationId.Equals((int)AccountClassificationEnum.PENDAPATAN))
                        {
                            JournalList.Add(GenerateIncomeJournal(model));
                            postedCount++;
                        }

                        // Create Cost
                        if (model.AccountClassificationId.Equals((int)AccountClassificationEnum.BEBAN))
                        {
                            JournalList.Add(GenerateCostJournal(model));
                            postedCount++;
                        }
                    }
                }

                TransactionToJournalRepo.PostTransaction(JournalList);

                result.DataResult = postedCount;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        private JournalHeader GenerateCostJournal(TransaksiToJurnalModel p_Model)
        {
            var journalH = new JournalHeader
            {
                is_generated = true,
                journal_date = p_Model.TransactionDate,
                journal_name = p_Model.TransactionName,
                period_id = p_Model.AccountingPeriodId,
                sourceid = p_Model.SourceId,
                status = 0,
                Details = new List<JournalDetail>(),
                AccountClassificationId = p_Model.AccountClassificationId
            };

            // Generate Debit Credit
            GenerateCostDebet(p_Model, journalH);
            GenerateCostCredit(p_Model, journalH);

            return journalH;
        }

        private void GenerateCostCredit(TransaksiToJurnalModel p_Model, JournalHeader p_Header)
        {
            var accSettings = _settings.Where(e => e.acc_class_id.Equals((int)AccountClassificationEnum.KAS)).FirstOrDefault();
            if (accSettings == null)
            {
                throw new ApplicationException(AppResource.MSG_ACCOUNT_KAS_NOT_SPECIFIED);
            }

            p_Header.Details.Add(new JournalDetail
            {
                account_id = accSettings.account_id,
                amount = p_Model.TransactionValue,
                detail_name = accSettings.Account.account_name,
                factor = -1, // Credit
                status = 0
            });
        }

        private void GenerateCostDebet(TransaksiToJurnalModel p_Model, JournalHeader p_Header)
        {
            var accSettings = _settings.Where(e => e.acc_class_id.Equals((int)AccountClassificationEnum.BEBAN) &&
                                        e.param_id.Equals(p_Model.ParamId)).FirstOrDefault();
            if (accSettings == null)
            {
                var msg = string.Format(AppResource.MSG_ACCOUNT_SETTING_NOT_SPECIFIED,
                                Enum.GetName(typeof(AccountClassificationEnum), (int)AccountClassificationEnum.BEBAN),
                                p_Model.TransactionName);
                throw new ApplicationException(msg);
            }

            p_Header.Details.Add(new JournalDetail
            {
                account_id = accSettings.account_id,
                amount = p_Model.TransactionValue,
                detail_name = accSettings.Account.account_name,
                factor = 1, // Debet
                status = 0
            });
        }

        private JournalHeader GenerateCanceledReceivableJournal(TransaksiToJurnalModel p_Model)
        {
            var journalH = new JournalHeader
            {
                is_generated = true,
                journal_date = p_Model.TransactionDate,
                journal_name = p_Model.TransactionName,
                period_id = p_Model.AccountingPeriodId,
                sourceid = p_Model.SourceId,
                status = 0,
                Details = new List<JournalDetail>(),
                AccountClassificationId = p_Model.AccountClassificationId
            };

            // Generate Debit Credit
            GenerateCanceledReceivableDebet(p_Model, journalH);
            GenerateCanceledReceivableCredit(p_Model, journalH);

            return journalH;
        }

        private void GenerateCanceledReceivableDebet(TransaksiToJurnalModel p_Model, JournalHeader p_Header)
        {
            var accSettings = _settings.Where(e => e.acc_class_id.Equals((int)AccountClassificationEnum.PENDAPATAN) &&
                                        e.param_id.Equals(p_Model.ParamId)).FirstOrDefault();
            if (accSettings == null)
            {
                ThrowSettingError(Enum.GetName(typeof(AccountClassificationEnum), p_Model.AccountClassificationId),
                                  p_Model.TransactionName);
            }

            p_Header.Details.Add(new JournalDetail
            {
                account_id = accSettings.account_id,
                amount = p_Model.TransactionValue,
                detail_name = accSettings.Account.account_name,
                factor = 1, // Debet
                status = 0
            });
        }

        private void GenerateCanceledReceivableCredit(TransaksiToJurnalModel p_Model, JournalHeader p_Header)
        {
            var accSettings = _settings.Where(e => e.acc_class_id.Equals((int)AccountClassificationEnum.PIUTANG) &&
                                        e.param_id.Equals(p_Model.ParamId)).FirstOrDefault();
            if (accSettings == null)
            {
                ThrowSettingError(Enum.GetName(typeof(AccountClassificationEnum), p_Model.AccountClassificationId),
                                  p_Model.TransactionName);
            }

            p_Header.Details.Add(new JournalDetail
            {
                account_id = accSettings.account_id,
                amount = p_Model.TransactionValue,
                detail_name = accSettings.Account.account_name,
                factor = -1, // Credit
                status = 0
            });
        }

        private JournalHeader GenerateIncomeJournal(TransaksiToJurnalModel p_Model)
        {
            var journalH = new JournalHeader
            {
                is_generated = true,
                journal_date = p_Model.TransactionDate,
                journal_name = p_Model.TransactionName,
                period_id = p_Model.AccountingPeriodId,
                sourceid = p_Model.SourceId,
                status = 0,
                Details = new List<JournalDetail>(),
                AccountClassificationId = p_Model.AccountClassificationId
            };

            // Generate Debit Credit
            GenerateIncomeDebet(p_Model, journalH);
            GenerateIncomeCredit(p_Model, journalH);

            return journalH;
        }

        private JournalHeader GenerateReceivableJournal(TransaksiToJurnalModel p_Model)
        {
            var journalH = new JournalHeader
            {
                is_generated = true,
                journal_date = p_Model.TransactionDate,
                journal_name = p_Model.TransactionName,
                period_id = p_Model.AccountingPeriodId,
                sourceid = p_Model.SourceId,
                status = 0,
                Details = new List<JournalDetail>(),
                AccountClassificationId = p_Model.AccountClassificationId
            };

            // Generate Debit Credit
            GenerateReceivableDebet(p_Model, journalH);
            GenerateReceivableCredit(p_Model, journalH);

            return journalH;
        }

        private void GenerateReceivableDebet(TransaksiToJurnalModel p_Model, JournalHeader p_Header)
        {
            var accSettings = _settings.Where(e => e.acc_class_id.Equals((int)AccountClassificationEnum.PIUTANG) &&
                                        e.param_id.Equals(p_Model.ParamId)).FirstOrDefault();
            if (accSettings == null)
            {
                ThrowSettingError(Enum.GetName(typeof(AccountClassificationEnum), p_Model.AccountClassificationId),
                                  p_Model.TransactionName);
            }

            p_Header.Details.Add(new JournalDetail
            {
                account_id = accSettings.account_id,
                amount = p_Model.TransactionValue,
                detail_name = accSettings.Account.account_name,
                factor = 1, // Debet
                status = 0
            });
        }

        private void GenerateReceivableCredit(TransaksiToJurnalModel p_Model, JournalHeader p_Header)
        {
            var accSettings = _settings.Where(e => e.acc_class_id.Equals((int)AccountClassificationEnum.PENDAPATAN) &&
                                        e.param_id.Equals(p_Model.ParamId)).FirstOrDefault();
            if (accSettings == null)
            {
                ThrowSettingError(Enum.GetName(typeof(AccountClassificationEnum), p_Model.AccountClassificationId),
                                  p_Model.TransactionName);
            }

            p_Header.Details.Add(new JournalDetail
            {
                account_id = accSettings.account_id,
                amount = p_Model.TransactionValue,
                detail_name = accSettings.Account.account_name,
                factor = -1, // Credit
                status = 0
            });
        }

        private void GenerateIncomeDebet(TransaksiToJurnalModel p_Model, JournalHeader p_Header)
        {
            var accSettings = _settings.Where(e => e.acc_class_id.Equals((int)AccountClassificationEnum.KAS)).FirstOrDefault();
            if (accSettings == null)
            {
                ThrowSettingError(Enum.GetName(typeof(AccountClassificationEnum), p_Model.AccountClassificationId),
                                  p_Model.TransactionName);
            }

            p_Header.Details.Add(new JournalDetail
            {
                account_id = accSettings.account_id,
                amount = p_Model.TransactionValue,
                detail_name = accSettings.Account.account_name,
                factor = 1, // Debet
                status = 0
            });
        }

        private void GenerateIncomeCredit(TransaksiToJurnalModel p_Model, JournalHeader p_Header)
        {
            var accSettings = _settings.Where(e => e.acc_class_id.Equals((int)AccountClassificationEnum.PIUTANG) &&
                                        e.param_id.Equals(p_Model.ParamId)).FirstOrDefault();
            if (accSettings == null)
            {
                ThrowSettingError(Enum.GetName(typeof(AccountClassificationEnum), p_Model.AccountClassificationId),
                                  p_Model.TransactionName);
            }

            p_Header.Details.Add(new JournalDetail
            {
                account_id = accSettings.account_id,
                amount = p_Model.TransactionValue,
                detail_name = accSettings.Account.account_name,
                factor = -1, // Credit
                status = 0
            });
        }



        public ProcessResult GetNeracaSaldo(int p_PeriodId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<NeracaSaldoModel> dataResult = TransactionToJournalRepo.GetNeracaSaldo(p_PeriodId);
                foreach(var saldo in dataResult)
                {
                    saldo.AccountClassification = Enum.GetName(typeof(AccountClassificationEnum), saldo.AccountClassificationId);
                }

                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetDailyJournal(DateTime p_Start, DateTime p_End)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                var ap = TransactionToJournalRepo.GetActiveAccountingPeriod();
                List<DailyJournalModel> dataResult = TransactionToJournalRepo.GetDailyJournal(p_Start, p_End, ap.Period_id);
                foreach(var d in dataResult)
                {
                    d.journal_date = d.journal_date.Date;
                }
                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        void ThrowSettingError(string pAccountClass, string pName)
        {
            var msg = string.Format(AppResource.MSG_ACCOUNT_SETTING_NOT_SPECIFIED,
                                pAccountClass, pName);
            throw new ApplicationException(msg);
        }

        public ProcessResult GetActiveAccountingPeriod()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                RefAccPeriodModel dataResult = TransactionToJournalRepo.GetActiveAccountingPeriod();
                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAvailablePeriod()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<ValueList> dataResult = TransactionToJournalRepo.GetAvailablePeriod();
                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
