﻿using AutoMapper;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class SiswaProcess : ISiswaProcess
    {
        IRefSiswaRepository SiswaRepository { get; set; }

        public ProcessResult GetSiswa(string p_stringSiswa)
        {
            ProcessResult result = new ProcessResult();

            CheckSiswaRepository();

            try
            {
                List<SiswaModel> dataResult = SiswaRepository.GetSiswa(p_stringSiswa);

                //List<SiswaModel> modelResult = dataResult.Select(x => Mapper.Map<SiswaModel>(x)).ToList();

                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }                                

            return result;
        }

        private void CheckSiswaRepository()
        {
            if (SiswaRepository == null)
            {
                SiswaRepository = new RefSiswaRepository();
            }
        }
    }
}
