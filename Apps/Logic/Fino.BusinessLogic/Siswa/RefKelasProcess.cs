﻿using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Lib.Core.Util;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class RefKelasProcess : IRefKelasProcess
    {
        public IRefKelasRepository RefKelasRepository { get; set; }

        public ProcessResult GetAllRefKelas()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                var allKelas = this.RefKelasRepository.SelectAllKelas();

                List<RefKelasModel> dataResult = new List<RefKelasModel>();

                foreach(var kelas in allKelas)
                {
                    dataResult.Add(new RefKelasModel
                        {
                            KelasId = kelas.kelas_id,
                            KelasCode = kelas.kelas_code,
                            Nama = kelas.nama,
                            Tingkat = kelas.tingkat
                        });
                }

                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch(Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            RefKelasRepository.DisposeDBContext();

            return result;
        }

        public ProcessResult GetAllRefKelasByTingkat(int tingkat)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                var allKelas = this.RefKelasRepository.SelectAllKelas().Where(x => x.tingkat == tingkat).ToList();
                List<RefKelasModel> dataResult = new List<RefKelasModel>();

                foreach (var kelas in allKelas)
                {
                    dataResult.Add(new RefKelasModel
                    {
                        KelasId = kelas.kelas_id,
                        KelasCode = kelas.kelas_code,
                        Nama = kelas.nama,
                        Tingkat = kelas.tingkat
                    });
                }

                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            RefKelasRepository.DisposeDBContext();

            return result;
        }

        public ProcessResult GetRefKelas(int kelasId)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                var kelas = this.RefKelasRepository.GetRefKelas(kelasId);

                result.DataResult = new RefKelasModel
                    {
                        KelasId = kelas.kelas_id,
                        KelasCode = kelas.kelas_code,
                        Nama = kelas.nama,
                        Tingkat = kelas.tingkat
                    };
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            RefKelasRepository.DisposeDBContext();

            return result;
        }

        public ProcessResult UpdateRefKelas(RefKelasModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                this.RefKelasRepository.UpdateRefKelas(new RefKelas
                    {
                        kelas_id = entity.KelasId,
                        kelas_code = entity.KelasCode,
                        nama = entity.Nama,
                        tingkat = entity.Tingkat
                    });
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            RefKelasRepository.DisposeDBContext();

            return result;
        }

        public ProcessResult DeleteRefKelas(RefKelasModel entity)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                this.RefKelasRepository.DeleteRefKelas(new RefKelas
                {
                    kelas_id = entity.KelasId,
                    kelas_code = entity.KelasCode,
                    nama = entity.Nama,
                    tingkat = entity.Tingkat
                });
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            RefKelasRepository.DisposeDBContext();

            return result;
        }


        public ProcessResult GetTingkatDataSource()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                result.DataResult = EnumHelper.Instance.ConvertToValueValueDataSource<KelasTingkatEnum>();
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            RefKelasRepository.DisposeDBContext();

            return result;
        }
    }
}
