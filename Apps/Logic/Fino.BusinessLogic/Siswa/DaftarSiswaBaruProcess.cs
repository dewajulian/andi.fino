﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public class DaftarSiswaBaruProcess : IDaftarSiswaBaruProcess
    {
        public ITahunAjaranRepository TahunAjaranRepo { get; set; }
        public IDaftarSiswaBaruRepository DaftarSiswaBaruRepo { get; set; }
        public IRefKelasRepository RefKelasRepo { get; set; }

        void ValidateData(DaftarSiswaBaruModel model)
        {
            if (string.IsNullOrEmpty(model.Nama)
            || string.IsNullOrEmpty(model.Code)
            || !(model.Kelas_Id > 0)
            || (!(model.OptionTingkat_Id >0) && model.IsPindahan)
            || !(model.TahunAjaran_Id > 0))
            {
                throw new ApplicationException(AppResource.MSG_INCOMPLETE_DATA);
            }
        }

        public ProcessResult TambahSiswaBaru(DaftarSiswaBaruModel model)
        {
            ProcessResult result = new ProcessResult();
            var data = model;
            try
            {
                ValidateData(data);

                data = DaftarSiswaBaruRepo.TambahSiswaBaru(model);

                result.IsSucess = true;
                result.DataResult = data;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAllKelas()
        {
            ProcessResult result = new ProcessResult();
            try
            {
                result.DataResult = RefKelasRepo.SelectAllKelas().Select(e => new ValueList { Id = e.kelas_id, Value = e.nama }).ToList();

                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetTahunAjaran(DateTime currentDate)
        {
            ProcessResult result = new ProcessResult();
            try
            {                
                var refTA = TahunAjaranRepo.GetValidTahunAjaran(currentDate.Year, currentDate.Month);
                if (refTA != null)
                {
                    result.DataResult = new ValueList
                    {
                        Id = refTA.tahunajaran_id,
                        Value = refTA.nama
                    };
                }
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
