﻿using System;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public interface IDaftarUlangSiswaProcess
    {
        ITahunAjaranRepository TahunAjaranRepo { get; set; }
        IDaftarUlangSiswaRepository DaftarUlangSiswaRepo { get; set; }
        IRefKelasRepository RefKelasRepo { get; set; }
        ISiswaKelasRepository SiswaKelasRepo { get; set; }
        ProcessResult TambahDaftarUlang(DaftarUlangSiswaModel model);
        ProcessResult GetTahunAjaran(DateTime currentDate);
        ProcessResult GetAllKelas();
        ProcessResult GetSiswaByNoInduk(string pNoInduk);
    }
}
