﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public class PendaftaranTabunganProcess : IPendaftaranTabunganProcess
    {
        public IPendaftaranTabunganRepository PendaftaranTabunganRepo { get; set; }
        public IRefSiswaRepository RefSiswaRepo { get; set; }

        public ProcessResult BukaRekening(Model.PendaftaranTabunganModel p_Model)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                var resultModel = PendaftaranTabunganRepo.NewRekening(p_Model);
                result.DataResult = resultModel;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetSiswaByNoInduk(string pNoInduk)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var model = RefSiswaRepo.GetSiswaByCode(pNoInduk);
                if (model != null)
                {
                    result.DataResult = new PendaftaranTabunganModel
                    {
                        jkelamin = model.jkelamin,
                        siswa_code = model.siswa_code,
                        siswa_id = model.siswa_id,
                        siswa_nama = model.nama
                    };
                }
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
