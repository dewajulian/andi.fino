﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class LaporanPengeluaranProcess : ILaporanPengeluaranProcess
    {
        public ILaporanPengeluaranRepository LaporanPengeluaranRepo { get; set; }

        public ProcessResult GetLaporanPengeluaranData(DateTime p_FromDate, DateTime p_ToDate)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<LaporanPengeluaranModel> dataResult = LaporanPengeluaranRepo.GetDataPengeluaran(p_FromDate, p_ToDate);

                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
