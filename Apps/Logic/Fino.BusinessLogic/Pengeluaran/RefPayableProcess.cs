﻿using AutoMapper;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class RefPayableProcess : IRefPayableProcess
    {
        public IRefPayableRepository RefPayableRepo { get; set; }

        public ProcessResult GetRefPayable()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<RefPayable> resultData = RefPayableRepo.RefPayableData();
                List<RefPayableModel> resultModel = new List<RefPayableModel>(resultData.Count);

                foreach (RefPayable item in resultData)
                {
                    resultModel.Add(Mapper.Map<RefPayableModel>(item));
                }

                result.DataResult = resultModel;
                result.IsSucess = true;
                RefPayableRepo.DisposeDBContext();
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetRefPayable(int p_Id)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                RefPayable resultData = RefPayableRepo.RefPayableData(p_Id);
                RefPayableModel resultModel = new RefPayableModel();

                resultModel = Mapper.Map<RefPayableModel>(resultData);
                
                result.DataResult = resultModel;
                result.IsSucess = true;
                RefPayableRepo.DisposeDBContext();
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult TambahRefPayable(RefPayableModel p_RefPayableModel)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                RefPayable refPayable = AutoMapper.Mapper.Map<RefPayable>(p_RefPayableModel);
                RefPayableRepo.AddRefPayableData(refPayable);
                RefPayableRepo.SaveChanges();

                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult UpdateRefPayable(RefPayableModel p_RefPayableModel)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                RefPayable refPayable = new RefPayable();
                refPayable.refpayable_id = p_RefPayableModel.RefpayableId;
                RefPayableRepo.UpdateRefPayableData(refPayable);

                refPayable.name = p_RefPayableModel.Nama;
                refPayable.code = p_RefPayableModel.Code;
                refPayable.aktif = p_RefPayableModel.Aktif;                

                RefPayableRepo.SaveChanges();

                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult DeleteRefPayable(Model.RefPayableModel p_RefPayableModel)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                RefPayable refPayable = AutoMapper.Mapper.Map<RefPayable>(p_RefPayableModel);
                RefPayableRepo.DeleteRefPayableData(refPayable);
                RefPayableRepo.SaveChanges();

                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
