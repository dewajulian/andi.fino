﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public class PayableProcess : IPayableProcess
    {
        public IPayableRepository PayableRepo { get; set; }

        public ProcessResult GetRekapitulasiPengeluaran(DateTime p_FromDate, DateTime p_ToDate)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                List<RekapitulasiPengeluaranModel> dataResult = PayableRepo.GetRekapitulasiPengeluaran(p_FromDate, p_ToDate);

                result.DataResult = dataResult;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
