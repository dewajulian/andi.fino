﻿using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Fino.BusinessLogic
{
    public class UserLogonProcess : IUserLogonProcess
    {
        public IUserRepository UserRepo { get; set; }
        public ITahunAjaranRepository TahunAjaranRepo { get; set; }

        public ProcessResult UserLogon(string p_UserName, string p_Password)
        {
            ProcessResult result = new ProcessResult();

            SysUser user = UserRepo.UserLogon(p_UserName, p_Password);

            if (user != null)
            {
                UserModel model = new UserModel()
                {
                    UserId = user.user_id,
                    Username = user.name,
                    FullName = user.fullname,
                    Password = user.password
                };

                result.IsSucess = true;
                result.DataResult = model;
            }
            else
            {
                result.IsSucess = false;
                result.ProcessException = new Exception("Username atau password salah");
            }            

            return result;
        }

        public ProcessResult UserLogoff(string p_UserName)
        {
            throw new NotImplementedException();
        }

        public ProcessResult GetTahunAjaranAktif()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                RefTahunAjaran tahunAjaran = TahunAjaranRepo.GetValidTahunAjaran(DateTime.Today.Year, DateTime.Today.Month);

                TahunAjaranModel model = AutoMapper.Mapper.Map<TahunAjaranModel>(tahunAjaran);

                result.DataResult = model;
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.ProcessException = ex;
                result.IsSucess = false;
            }

            return result;
        }
    }
}
