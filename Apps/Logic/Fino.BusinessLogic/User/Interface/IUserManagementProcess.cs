﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface IUserManagementProcess
    {
        IUserManagementRepository UserManagementRepo { get; set; }
        ProcessResult GetUserForManagement(int userid);
        ProcessResult GetAllUserForManagement();
        ProcessResult UpdateUser(UserManagementModel entity);
        ProcessResult DeleteUser(UserManagementModel entity);
    }
}
