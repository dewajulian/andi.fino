﻿using Fino.Lib.Core.Util;
using Fino.View;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class SaveMachineFingerPrintPresenter : BasePresenter
    {
        ISaveMachineFingerPrintView _saveMachineFingerPrintView;

        public SaveMachineFingerPrintPresenter(ISaveMachineFingerPrintView p_View) : base(p_View)
        {
            this._saveMachineFingerPrintView = p_View;

            this._saveMachineFingerPrintView.ButtonSaveClicked += _saveMachineFingerPrintView_ButtonSaveClicked;
        }

        private void _saveMachineFingerPrintView_ButtonSaveClicked()
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = "Simpan File Kunci";
            dialog.FileName = "FileKunci.fino";
            dialog.Filter = "All Files(*.*)|*.*";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(dialog.FileName))
                {
                    string machineFingerPrint = EnvironmentHelper.Instance.GetEnvironmentHashID();
                    File.WriteAllText(dialog.FileName, machineFingerPrint);

                    _saveMachineFingerPrintView.UpdateFirstRun(false);
                    _saveMachineFingerPrintView.Close();
                }
            }
        }
    }
}
