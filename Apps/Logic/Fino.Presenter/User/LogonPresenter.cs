﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Core.Settings;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class LogonPresenter : BasePresenter
    {
        private readonly IUserLogonProcess _logonProcess;
        private readonly ILogonView _logonView;

        public LogonPresenter(ILogonView p_LogonView, IUserLogonProcess p_UserLogonProcess) : base(p_LogonView)
        {
            _logonProcess = p_UserLogonProcess;
            _logonView = p_LogonView;

            _logonView.OkButtonClicked += OnOkButtonClicked;
            _logonView.CancelButtonClicked += OnCancelButtonClicked;
            _logonView.Load += OnLoad;
            _logonView.LinkForgotPasswordClicked += _logonView_LinkForgotPasswordClicked;
        }

        public void OnLoad(object sender, EventArgs e)
        {
            _logonView.User = new UserModel();

            _logonView.Username = _logonView.User.Username;
            _logonView.Password = _logonView.User.Password;

            _logonView.User.ErrorValidation += User_ErrorValidation;
            _logonView.SetBinding();
        }

        void User_ErrorValidation(object sender, CustomEventArgs<string> e)
        {
            WinApi.ShowErrorMessage(e.Data, "Validasi Error");
        }

        public void OnCancelButtonClicked()
        {
            _logonView.Close();
        }

        public void OnOkButtonClicked()
        {
            var context = new ValidationContext(_logonView.User, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(_logonView.User, context, results, true);

            if (isValid)
            {
                _logonView.Hide();
                
                ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                    () => _logonProcess.UserLogon(_logonView.Username, _logonView.Password)),
                    "Processing Login");

                if (result.IsSucess)
                {
                    bool tahunAjaran = LoadTahunAjaran();

                    if (tahunAjaran)
                    {
                        _logonView.LogonResult = true;

                        UserModel dataResult = result.DataResult as UserModel;
                        AppVariable.Instance.AppGlobal.Add(AppResource.APP_VARIABEL_LOGGEDONUSER, dataResult);
                        _logonView.Close();
                    }
                    else
                    {
                        _logonView.LogonResult = false;                        
                    }
                }
                else
                {
                    _logonView.LogonResult = false;
                    WinApi.ShowErrorMessage(result.ProcessException.Message, "Validasi Error");
                    _logonView.Show();
                }
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                WinApi.ShowErrorMessage(sb.ToString(), "Validasi Error");
            }
        }

        private void _logonView_LinkForgotPasswordClicked()
        {
            this._logonView.ShowForgotPasswordView();
        }

        private bool LoadTahunAjaran()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                    () => _logonProcess.GetTahunAjaranAktif()),
                    "Konfigurasi Tahun Ajaran");

            if (result.IsSucess)
            {
                TahunAjaranModel model = result.DataResult as TahunAjaranModel;

                if (model != null)
                {
                    AppVariable.Instance.AppGlobal.Add(AppResource.APP_VARIABEL_TAHUNAJARANAKTIF, model);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                WinApi.ShowErrorMessage(result.ProcessException.Message, "Tahun Ajaran Error");
                return false;   
            }
        }
    }
}
