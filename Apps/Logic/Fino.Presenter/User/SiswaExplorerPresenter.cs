﻿using Fino.BusinessLogic;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Presenter
{
    public class SiswaExplorerPresenter : BasePresenter
    {
        private readonly ISiswaExplorerProcess _siswaExplorerProcess;
        private readonly ISiswaExplorerView _siswaExplorerView;

        public SiswaExplorerPresenter(ISiswaExplorerView p_View, ISiswaExplorerProcess p_Process) : base(p_View)
        {
            _siswaExplorerProcess = p_Process;
            _siswaExplorerView = p_View;
        }
    }
}
