﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class EditUserManagementPresenter : BasePresenter
    {
        IEditUserManagementView _editUserManagementView;
        IUserManagementProcess _userManagementProcess;

        #region Contructors

        public EditUserManagementPresenter(IUserManagementProcess p_Process, IEditUserManagementView p_View) : base(p_View)
        {
            this._userManagementProcess = p_Process;
            this._editUserManagementView = p_View;

            this._editUserManagementView.Load += _editUserManagementView_Load;
            this._editUserManagementView.BtnSimpanClicked += _editUserManagementView_BtnSimpanClicked;
            this._editUserManagementView.BtnBatalClicked += _editUserManagementView_BtnBatalClicked;
        }

        #endregion Constructors

        #region Private Methods

        public void Load()
        {
            if (this._editUserManagementView.SelectedUser != null)
            {
                this._editUserManagementView.Fullname = this._editUserManagementView.SelectedUser.Fullname;
                this._editUserManagementView.Username = this._editUserManagementView.SelectedUser.Username;
                this._editUserManagementView.Password = this._editUserManagementView.SelectedUser.Password;
                this._editUserManagementView.Phone = this._editUserManagementView.SelectedUser.Phone;
                this._editUserManagementView.Email = this._editUserManagementView.SelectedUser.Email;
            }

            this._editUserManagementView.SelectedUser.ErrorValidation += SelectedUser_ErrorValidation;
            this._editUserManagementView.SetBinding();
        }

        private void Save()
        {
            // Call save process here
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => this._userManagementProcess.UpdateUser(this._editUserManagementView.SelectedUser)),
                        AppResource.MSG_SAVING_USER_MANAGEMENT);

            if (result.IsSucess)
            {
                WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                this._editUserManagementView.UserManagementView.ReloadDataEvent();
                this._editUserManagementView.Close();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        #endregion

        #region Control Events

        private void _editUserManagementView_Load(object sender, EventArgs e)
        {
            Load();
        }

        private void SelectedUser_ErrorValidation(object sender, Lib.Core.CustomEventArgs<string> e)
        {
            WinApi.ShowErrorMessage(e.Data, AppResource.CAP_VALIDASI_ERROR);
        }

        private void _editUserManagementView_BtnSimpanClicked()
        {
            var context = new ValidationContext(this._editUserManagementView.SelectedUser, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(this._editUserManagementView.SelectedUser, context, results, true);

            if (isValid)
            {
                Save();

                this._editUserManagementView.SelectedUser.ValidationMessage = string.Empty;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                this._editUserManagementView.SelectedUser.ValidationMessage = sb.ToString();
                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }

        private void _editUserManagementView_BtnBatalClicked()
        {
            this._editUserManagementView.Close();
        }

        #endregion Control Events
    }
}
