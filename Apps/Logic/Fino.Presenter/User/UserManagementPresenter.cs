﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class UserManagementPresenter : BasePresenter
    {
        IUserManagementView _userManagementView;
        IUserManagementProcess _userManagementProcess;

        public UserManagementPresenter(IUserManagementProcess p_Process, IUserManagementView p_View) : base(p_View)
        {
            try
            {
                this._userManagementProcess = p_Process;
                this._userManagementView = p_View;

                this._userManagementView.BtnHapusClick += _userManagementView_BtnHapusClick;
                this._userManagementView.BtnTambahClick += _userManagementView_BtnTambahClick;
                this._userManagementView.BtnTutupClick += _userManagementView_BtnTutupClick;
                this._userManagementView.GvUserManagementDoubleClick += _userManagementView_GvUserManagementDoubleClick;
                this._userManagementView.Load += _userManagementView_Load;
                this._userManagementView.ReloadData += _userManagementView_ReloadData;
            }
            catch (Exception ex)
            {
                _userManagementView.ShowErrorMessage(ex.Message, AppResource.CAP_PROCESS_ERROR);
            }
        }

        #region Private Methods

        private void LoadData()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => this._userManagementProcess.GetAllUserForManagement()),
                AppResource.MSG_LOADING_USER_MANAGEMENT);

            if (result.IsSucess)
            {
                List<UserManagementModel> data = result.DataResult as List<UserManagementModel>;

                this._userManagementView.SetData(data);
                this._userManagementView.RefreshDataGrid();
            }
            else
            {
                MessageBox.Show(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion Private Methods

        #region Control Events

        private void _userManagementView_ReloadData()
        {
            LoadData();
        }

        private void _userManagementView_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void _userManagementView_GvUserManagementDoubleClick()
        {
            _userManagementView.ShowEditUserView();
        }

        private void _userManagementView_BtnTutupClick()
        {
            this._userManagementView.Close();
        }

        private void _userManagementView_BtnTambahClick()
        {
            _userManagementView.ShowTambahUserView();
        }

        private void _userManagementView_BtnHapusClick()
        {
            UserManagementModel selectedUser = this._userManagementView.GetSelectedUser();

            if (MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_DLT_REFBIAYA, selectedUser.Username), "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                                () => this._userManagementProcess.DeleteUser(selectedUser)),
                                AppResource.MSG_DELETE_REF_ACCOUNT);

                if (result.IsSucess)
                {
                    LoadData();
                }
                else
                {
                    if (result.ProcessException != null && result.ProcessException.GetType() == typeof(UpdateException))
                    {
                        MessageBox.Show(string.Format(AppResource.MSG_DATA_TDKBISADIHAPUS, selectedUser.Username, Environment.NewLine), AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        #endregion Control Events
    }
}
