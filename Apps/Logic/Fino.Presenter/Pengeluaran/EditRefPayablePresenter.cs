﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{    
    public class EditRefPayablePresenter : BasePresenter
    {
        IRefPayableProcess _process;
        IEditRefPayableView _view;

        public EditRefPayablePresenter(IEditRefPayableView p_view, IRefPayableProcess p_process)
            : base(p_view)
        {
            _view = p_view;
            _process = p_process;

            _view.Load += Load;
            _view.BtnBatalClicked += BtnBatalClicked;
            _view.BtnSimpanClicked += BtnSimpanClicked;
        }

        public void BtnSimpanClicked()
        {
            var context = new ValidationContext(_view.SelectedRefPayable, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(_view.SelectedRefPayable, context, results, true);

            if (isValid)
            {
                if (_view.SelectedRefPayable.RefpayableId == 0)
                {
                    Save();
                }
                else
                {
                    Update();
                }

                _view.SelectedRefPayable.ValidationMessage = string.Empty;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                _view.SelectedRefPayable.ValidationMessage = sb.ToString();
                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }

        public void BtnBatalClicked()
        {
            _view.RefPayableView.ReloadDataEvent();
            _view.Close();
        }

        public void Load(object sender, EventArgs e)
        {
            if (_view.SelectedRefPayable != null)
            {
                _view.Nama = _view.SelectedRefPayable.Nama;
                _view.Kode = _view.SelectedRefPayable.Code;                
            }

            _view.SelectedRefPayable.ErrorValidation += SelectedRefPayable_ErrorValidation;
            _view.SetBinding();
        }

        private void SelectedRefPayable_ErrorValidation(object sender, Lib.Core.CustomEventArgs<string> e)
        {
            WinApi.ShowErrorMessage(e.Data, AppResource.CAP_VALIDASI_ERROR);
        }

        private void Update()
        {
            // Call save process here
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => _process.UpdateRefPayable(_view.SelectedRefPayable)),
                        AppResource.MSG_SAVING_REFPAYABLE);

            if (result.IsSucess)
            {
                WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                _view.RefPayableView.ReloadDataEvent();
                _view.Close();

                //MessageData messageData = new MessageData(MessageTypeEnum.WithKey, _view.SelectedRefPayable);
                //Mediator.Instance.NotifyColleagues(MessageKey.UPDATE_CURRENT_SELECTED_DATA, messageData);
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        private void Save()
        {
            // Call save process here
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => _process.TambahRefPayable(_view.SelectedRefPayable)),
                        AppResource.MSG_SAVING_REFPAYABLE);

            if (result.IsSucess)
            {
                WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                _view.RefPayableView.ReloadDataEvent();
                _view.Close();

                //MessageData messageData = new MessageData(MessageTypeEnum.WithKey, result.DataResult);
                //Mediator.Instance.NotifyColleagues(MessageKey.RELOAD_DATA, messageData);

            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }
    }
}
