﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Core.Messaging;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class RefPayablePresenter : BasePresenter
    {
        IRefPayableView _refPayableView;
        IRefPayableProcess _refPayableProcess;

        public RefPayablePresenter(IRefPayableView p_view, IRefPayableProcess p_process)
            : base(p_view)
        {
            _refPayableView = p_view;
            _refPayableProcess = p_process;

            _refPayableView.BtnTambahClick += BtnTambahClick;
            _refPayableView.BtnHapusClick += BtnHapusClick;
            _refPayableView.BtnTutupClick += BtnTutupClick;
            _refPayableView.GvRefPayableDoubleClick += GvRefPayableDoubleClick;
            _refPayableView.Load += Load;
            _refPayableView.ReloadData += ReloadData;
        }

        public void ReloadData()
        {
            LoadData();
        }

        public void Load(object sender, EventArgs e)
        {
            LoadData();
        }

        public void GvRefPayableDoubleClick()
        {
            _refPayableView.ShowEditRefPayableView();
        }

        public void BtnTutupClick()
        {
            _refPayableView.Close();
        }

        public void BtnHapusClick()
        {
            RefPayableModel selectedModel = _refPayableView.GetSelectedRefPayable();

            if (MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_DLT_REFPAYABLE, selectedModel.Nama), "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                    () => _refPayableProcess.DeleteRefPayable(selectedModel)),
                    AppResource.MSG_DELETE_REFPAYABLE);

                if (result.IsSucess)
                {
                    LoadData();
                }
                else
                {
                    if (result.ProcessException != null && result.ProcessException.GetType() == typeof(UpdateException))
                    {
                        MessageBox.Show(string.Format(AppResource.MSG_DATA_TDKBISADIHAPUS, selectedModel.Nama, Environment.NewLine), AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
                    }
                }
            }
        }

        public void BtnTambahClick()
        {
            _refPayableView.ShowTambahRefPayableView();
        }

        private void LoadData()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _refPayableProcess.GetRefPayable()),
                AppResource.MSG_LOADING_REFPAYABLE);

            if (result.IsSucess)
            {
                List<RefPayableModel> data = result.DataResult as List<RefPayableModel>;

                _refPayableView.SetData(data);
                _refPayableView.RefreshDataGrid();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        [MediatorMessageAttribute(MessageKey.RELOAD_DATA)]
        public void SelesaiSimpanGrupBiaya(object p_MessageData)
        {
            //MessageData mData = p_MessageData as MessageData;

            //if (mData.MessageType == MessageTypeEnum.WithKey)
            //{
            //    GrupBiayaModel t = mData.Data as GrupBiayaModel;
            //    _grupBiayaView.GetGVGrupBiayaDataSource().Add(t);
            //    _grupBiayaView.RefreshDataGrid();
            //}
        }

        [MediatorMessageAttribute(MessageKey.UPDATE_CURRENT_SELECTED_DATA)]
        public void SelesaiUpdateGrupBiaya(object p_MessageData)
        {
            //MessageData mData = p_MessageData as MessageData;

            //if (mData.MessageType == MessageTypeEnum.WithKey)
            //{
            //    _grupBiayaView.RefreshDataGrid();
            //}
        }

    }
}
