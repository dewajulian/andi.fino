﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class PayablePaidPresenter : BasePresenter
    {
        IPayablePaidProcess _payablePaidProcess;
        IPayablePaidView _payablePaidView;

        public PayablePaidPresenter(IPayablePaidView p_View, IPayablePaidProcess p_Process)
            : base (p_View)
        {
            this._payablePaidView = p_View;
            this._payablePaidProcess = p_Process;

            this._payablePaidView.AddNewVendorButtonClicked += _payablePaidView_AddNewVendorButtonClicked;
            this._payablePaidView.CloseButtonClicked += _payablePaidView_CloseButtonClicked;
            this._payablePaidView.NewDataButtonClicked += _payablePaidView_NewDataButtonClicked;
            this._payablePaidView.SaveButtonClicked += _payablePaidView_SaveButtonClicked;
            this._payablePaidView.SaveNextButtonClicked += _payablePaidView_SaveNextButtonClicked;
            this._payablePaidView.ReloadDataEvent += _payablePaidView_ReloadDataEvent;
            this._payablePaidView.Load += _payablePaidView_Load;
        }

        #region Control Events

        void _payablePaidView_ReloadDataEvent()
        {
            this._payablePaidView.RefVendorDataSource = GetRefVendorDataSource();
            this._payablePaidView.SetBinding();
        }

        void _payablePaidView_Load(object sender, EventArgs e)
        {
            this._payablePaidView.RefVendorDataSource = GetRefVendorDataSource();
            this._payablePaidView.RefPayableDataSource = GetRefPayableDataSource();
            AssignNewViewModel();
            this._payablePaidView.ClearView();
            this._payablePaidView.SetBinding();
        }

        void _payablePaidView_SaveNextButtonClicked()
        {
            if (SaveData())
            {
                AssignNewViewModel();
                this._payablePaidView.ChangeEditable(true);
                this._payablePaidView.ClearView();
            }
        }

        void _payablePaidView_SaveButtonClicked()
        {
            if (SaveData())
            {
                this._payablePaidView.ChangeEditable(false);
            }
        }

        void _payablePaidView_NewDataButtonClicked()
        {
            AssignNewViewModel();
            this._payablePaidView.ClearView();
            this._payablePaidView.ChangeEditable(true);
        }

        void _payablePaidView_CloseButtonClicked()
        {
            this._payablePaidView.Close();
        }

        void _payablePaidView_AddNewVendorButtonClicked()
        {
            this._payablePaidView.ShowCreateNewRefVendorView();
        }

        #endregion Control Events

        #region Private Methods

        private void AssignNewViewModel()
        {
            this._payablePaidView.ViewModel = new PayablePaidModel
            {
                JatuhTempo = DateTime.Today
            };
        }

        private object GetRefVendorDataSource()
        {
            return this._payablePaidProcess.GetAllRefVendor().DataResult;
        }

        private object GetRefPayableDataSource()
        {
            return this._payablePaidProcess.GetAllRefPayable().DataResult;
        }

        private bool Save()
        {
            var saved = false;

            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => this._payablePaidProcess.UpdatePayablePaid(this._payablePaidView.ViewModel)),
                        AppResource.MSG_SAVING_PAYABLEPAID);

            if (result.ProcessException != null)
            {
                this._payablePaidView.ShowErrorMessage(result.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
            }
            else
            {
                saved = true;
                this._payablePaidView.ShowInformation(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.CAP_PROCESS_INFORMATION);
            }

            return saved;
        }

        private bool SaveData()
        {
            var saved = false;
            var context = new ValidationContext(this._payablePaidView.ViewModel, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            // for some reasons the text property is unexpectedly not functioning
            // need to assign the value explicitly, here:
            _payablePaidView.ViewModel.RefPayableName = _payablePaidView.RefPayableName;

            var isValid = Validator.TryValidateObject(this._payablePaidView.ViewModel, context, results, true);

            // Later, this should be done in data annotation
            isValid = isValid && (this._payablePaidView.ViewModel.RefVendorId > 0);
            if (isValid)
            {
                saved = Save();

                this._payablePaidView.ViewModel.ValidationMessage = string.Empty;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }
                if (this._payablePaidView.ViewModel.RefVendorId <= 0)
                {
                    sb.Append("Vendor is required");
                }
                this._payablePaidView.ViewModel.ValidationMessage = sb.ToString();
                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }

            return saved;
        }

        #endregion Private Methods
    }
}
