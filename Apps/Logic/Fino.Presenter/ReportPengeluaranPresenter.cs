﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class ReportPengeluaranPresenter : BasePresenter
    {
        IReportPengeluaranView _view;
        ILaporanPengeluaranProcess _process;

        public ReportPengeluaranPresenter(IReportPengeluaranView p_view, ILaporanPengeluaranProcess p_process)
            : base(p_view)
        {
            _view = p_view;
            _process = p_process;

            _view.BtnOkClicked += OkBtnClicked;
            _view.BtnTutupClicked += BtnTutupClicked;
        }

        public void BtnTutupClicked()
        {
            _view.Close();
        }

        public void OkBtnClicked()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _process.GetLaporanPengeluaranData(_view.FromDate, _view.ToDate)),
                AppResource.MSG_REPORT_PENGELUARAN);

            if (result.IsSucess)
            {
                _view.ShowReport(result.DataResult as List<LaporanPengeluaranModel>);
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            } 
        }
    }
}
