﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class EditRefKelasPresenter : BasePresenter
    {
        IEditRefKelasView _editRefKelasView;
        IRefKelasProcess _refKelasProcess;

        public EditRefKelasPresenter(IEditRefKelasView p_View, IRefKelasProcess p_Process)
            : base(p_View)
        {
            this._editRefKelasView = p_View;
            this._refKelasProcess = p_Process;

            this._editRefKelasView.BtnSimpanClicked += _editRefKelasView_BtnSimpanClicked;
            this._editRefKelasView.BtnBatalClicked += _editRefKelasView_BtnBatalClicked;
            this._editRefKelasView.Load += _editRefKelasView_Load;
        }

        #region Control Events

        void _editRefKelasView_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        void _editRefKelasView_BtnBatalClicked()
        {
            this._editRefKelasView.Close();
        }

        void _editRefKelasView_BtnSimpanClicked()
        {
            var context = new ValidationContext(this._editRefKelasView.SelectedRefKelas, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(this._editRefKelasView.SelectedRefKelas, context, results, true);

            if (isValid)
            {
                Save();

                this._editRefKelasView.SelectedRefKelas.ValidationMessage = string.Empty;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                this._editRefKelasView.SelectedRefKelas.ValidationMessage = sb.ToString();
                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }

        #endregion Control Events

        #region Private Methods

        private void LoadData()
        {
            this._editRefKelasView.TingkatDataSource = GetTingkatDataSource();
            this._editRefKelasView.SetBinding();

        }

        private object GetTingkatDataSource()
        {
            return this._refKelasProcess.GetTingkatDataSource().DataResult;
        }
        
        private void Save()
        {
            // Call save process here
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => this._refKelasProcess.UpdateRefKelas(this._editRefKelasView.SelectedRefKelas)),
                        AppResource.MSG_SAVING_REF_KELAS);

            if (result.IsSucess)
            {
                WinApi.ShowInformationMessage(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.APP_TITLE);
                this._editRefKelasView.RefKelasView.ReloadDataEvent();
                this._editRefKelasView.Close();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        #endregion Private Methods
    }
}
