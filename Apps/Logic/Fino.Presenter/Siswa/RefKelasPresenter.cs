﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View.Siswa;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class RefKelasPresenter : BasePresenter
    {
        IRefKelasView _refKelasView;
        IRefKelasProcess _refKelasProcess;
        private int _selectedTingkat = 0;

        public RefKelasPresenter(IRefKelasView p_View, IRefKelasProcess p_Process)
            : base(p_View)
        {
            this._refKelasView = p_View;
            this._refKelasProcess = p_Process;

            this._refKelasView.ViewModel = new RefKelasModel();

            this._refKelasView.Load += _refKelasView_Load;
            this._refKelasView.BtnTambahClick += _refKelasView_BtnTambahClick;
            this._refKelasView.BtnHapusClick += _refKelasView_BtnHapusClick;
            this._refKelasView.BtnTutupClick += _refKelasView_BtnTutupClick;
            this._refKelasView.GvRefKelasDoubleClick += _refKelasView_GvRefKelasDoubleClick;
            this._refKelasView.ViewModel.PropertyChanged += ViewModel_PropertyChanged;
            this._refKelasView.ReloadData += _refKelasView_ReloadData;
        }

        #region Control Events

        void _refKelasView_ReloadData()
        {
            LoadData();
        }

        void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == RefKelasModel.TingkatPropertyName
                && _selectedTingkat != this._refKelasView.ViewModel.Tingkat)
            {
                SetGvRefKelasDataSource();
                _selectedTingkat = this._refKelasView.ViewModel.Tingkat;
            }
        }

        void _refKelasView_GvRefKelasDoubleClick()
        {
            this._refKelasView.ShowEditRefKelasView();
        }

        void _refKelasView_BtnTutupClick()
        {
            this._refKelasView.Close();
        }

        void _refKelasView_BtnHapusClick()
        {
            RefKelasModel selectedRefKelas = this._refKelasView.GetSelectedRefKelas();

            if (MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_DLT_REFKELAS, selectedRefKelas.Nama), "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                                () => this._refKelasProcess.DeleteRefKelas(selectedRefKelas)),
                                AppResource.MSG_DELETE_REFKELAS);

                if (result.IsSucess)
                {
                    LoadData();
                }
                else
                {
                    if (result.ProcessException != null && result.ProcessException.GetType() == typeof(UpdateException))
                    {
                        MessageBox.Show(string.Format(AppResource.MSG_DATA_TDKBISADIHAPUS, selectedRefKelas.Nama, Environment.NewLine), AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        void _refKelasView_BtnTambahClick()
        {
            this._refKelasView.ShowTambahRefKelasView();
        }

        void _refKelasView_Load(object sender, EventArgs e)
        {
            this._refKelasView.TingkatDataObject = GetTingkatDataSource();

            LoadData();

            this._refKelasView.SetBinding();
        }

        #endregion Control Events

        #region Private Methods

        private object GetTingkatDataSource()
        {
            return this._refKelasProcess.GetTingkatDataSource().DataResult;
        }

        private void LoadData()
        {
            SetGvRefKelasDataSource();
        }

        private void SetGvRefKelasDataSource()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _refKelasProcess.GetAllRefKelasByTingkat(this._refKelasView.ViewModel.Tingkat)),
                AppResource.MSG_LOADING_REF_KELAS);

            if (result.IsSucess)
            {
                List<RefKelasModel> data = result.DataResult as List<RefKelasModel>;

                _refKelasView.SetData(data);
                _refKelasView.RefreshDataGrid();
            }
            else
            {
                MessageBox.Show(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion Private Methods
    }
}
