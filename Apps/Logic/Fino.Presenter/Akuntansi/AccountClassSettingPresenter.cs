﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class AccountClassSettingPresenter: BasePresenter
    {
        IAccountClassSettingView _accountClassSettingView;
        IAccountClassSettingProcess _accountClassSettingProcess;
        private int _selectedAccClassId = 0;

        public AccountClassSettingPresenter(IAccountClassSettingView p_View, IAccountClassSettingProcess p_Process)
            : base (p_View)
        {
            try
            {
                this._accountClassSettingView = p_View;
                this._accountClassSettingProcess = p_Process;

                this._accountClassSettingView.ViewModel = new AccountClassSettingModel();

                this._accountClassSettingView.Load += _accountClassSettingView_Load;
                this._accountClassSettingView.BtnTambahClick += _accountClassSettingView_BtnTambahClick;
                this._accountClassSettingView.BtnHapusClick += _accountClassSettingView_BtnHapusClick;
                this._accountClassSettingView.BtnTutupClick += _accountClassSettingView_BtnTutupClick;
                this._accountClassSettingView.ReloadData += _accountClassSettingView_ReloadData;
                this._accountClassSettingView.GvAccountClassSettingDoubleClick += _accountClassSettingView_GvAccountClassSettingDoubleClick;
                this._accountClassSettingView.ViewModel.PropertyChanged += ViewModel_PropertyChanged;
            }
            catch (Exception ex)
            {
                this._accountClassSettingView.ShowErrorMessage(ex.Message, AppResource.CAP_PROCESS_ERROR);
            }
        }

        void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == AccountClassSettingModel.AccClassIdPropertyName
                && _selectedAccClassId != this._accountClassSettingView.ViewModel.AccClassId)
            {
                SetGvAccountClassSettingDataSource();
                _selectedAccClassId = this._accountClassSettingView.ViewModel.AccClassId;
            }
        }

        #region Private Methods

        private void LoadData()
        {
            SetGvAccountClassSettingDataSource();
        }

        private object GetAccClassIdDataSource()
        {
            return Enum.GetValues(typeof(AccountClassificationEnum)).Cast<AccountClassificationEnum>().Where(v => v != AccountClassificationEnum.CANCELED_PIUTANG).Select(v => new 
            {
                Id = (int)v,
                Value = v.ToString()
            }).ToList();
        }

        private void SetGvAccountClassSettingDataSource()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _accountClassSettingProcess.GetAllAccountClassSettingByAccClassId(this._accountClassSettingView.ViewModel.AccClassId)),
                AppResource.MSG_LOADING_ACCOUNT_CLASS_SETTING);

            if (result.IsSucess)
            {
                List<AccountClassSettingModel> data = result.DataResult as List<AccountClassSettingModel>;

                _accountClassSettingView.SetData(data);
                _accountClassSettingView.RefreshDataGrid();

                this._accountClassSettingView.SetDataGridViewParamHeaderText();
            }
            else
            {
                MessageBox.Show(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion Private Methods

        #region Control Events

        void _accountClassSettingView_Load(object sender, EventArgs e)
        {
            LoadData();

            this._accountClassSettingView.AccClassDataObject = GetAccClassIdDataSource();
            this._accountClassSettingView.SetBinding();

            if (this._accountClassSettingView.ViewModel.AccClassId == 0)
            {
                this._accountClassSettingView.ViewModel.AccClassId = (int)AccountClassificationEnum.KAS;
            }
        }

        void _accountClassSettingView_GvAccountClassSettingDoubleClick()
        {
            this._accountClassSettingView.ShowEditAccountClassSettingView();
        }

        void _accountClassSettingView_ReloadData()
        {
            LoadData();
        }

        void _accountClassSettingView_BtnTutupClick()
        {
            this._accountClassSettingView.Close();
        }

        void _accountClassSettingView_BtnHapusClick()
        {
            AccountClassSettingModel selectedAccountClassSetting = _accountClassSettingView.GetSelectedAccountClassSetting();

            if (MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_DLT_ACCOUNTCLASSSETTING, selectedAccountClassSetting.AccountName), "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                                () => _accountClassSettingProcess.DeleteAccountClassSetting(selectedAccountClassSetting)),
                                AppResource.MSG_DELETE_ACCOUNTCLASSSETTING);

                if (result.IsSucess)
                {
                    LoadData();
                }
                else
                {
                    if (result.ProcessException != null && result.ProcessException.GetType() == typeof(UpdateException))
                    {
                        MessageBox.Show(string.Format(AppResource.MSG_DATA_TDKBISADIHAPUS, selectedAccountClassSetting.AccountName, Environment.NewLine), AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        void _accountClassSettingView_BtnTambahClick()
        {
            this._accountClassSettingView.ShowTambahAccountClassSettingView();
        }

        #endregion Control Events
    }
}
