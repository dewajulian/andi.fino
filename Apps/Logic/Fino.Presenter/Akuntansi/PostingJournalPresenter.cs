﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Model;
using Fino.View;

namespace Fino.Presenter
{
    public class PostingJournalPresenter : BasePresenter
    {
        private readonly IPostTransactionToJournalProcess _postTransactionToJournalProcess;
        private readonly IPostingJournalView _postingJournalView;

        public PostingJournalPresenter(IPostTransactionToJournalProcess p_Process, IPostingJournalView p_View) : base(p_View)
        {
            _postTransactionToJournalProcess = p_Process;
            _postingJournalView = p_View;
            _postingJournalView.Load += _postingJournalView_Load;
            _postingJournalView.CloseButtonClicked += _postingJournalView_CloseButtonClicked;
            _postingJournalView.SearchTransactionButtonClicked += _postingJournalView_SearchTransactionButtonClicked;
            _postingJournalView.PostingButtonClicked += _postingJournalView_PostingButtonClicked;
            _postingJournalView.SelectAllClicked += _postingJournalView_SelectAllClicked;
            _postingJournalView.UnselectAllClicked += _postingJournalView_UnselectAllClicked;
        }

        void _postingJournalView_UnselectAllClicked()
        {
            _postingJournalView.UnselectAllItem();
        }

        void _postingJournalView_SelectAllClicked()
        {
            _postingJournalView.SelectAllItem();
        }

        void _postingJournalView_PostingButtonClicked()
        {
            if (_postingJournalView.ViewModel != null)
            {
                var transactions = _postingJournalView.ViewModel;

                var result = _postTransactionToJournalProcess.PostTransactionForJournal(transactions);
                if (result.IsSucess)
                {
                    var msg = string.Format("Berhasil melakukan posting {0} transaksi", (int)result.DataResult);
                    Lib.Win32.WinApi.ShowInformationMessage(msg, AppResource.CAP_PROCESS_INFORMATION);
                    _postingJournalView_SearchTransactionButtonClicked();
                }
                else
                {
                    Lib.Win32.WinApi.ShowErrorMessage(result.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
                }
            }
        }

        void _postingJournalView_SearchTransactionButtonClicked()
        {
            // Override the start date with minimum value
            // This approach will reduce human error, but keep the option for flexibility in later use
            DateTime dtmStart = new DateTime(2000,1,1);
            var result = _postTransactionToJournalProcess.GetActiveTransactionForJournal(dtmStart, _postingJournalView.EndDate);
            if (result.IsSucess)
            {
                _postingJournalView.ViewModel = (List<TransaksiToJurnalModel>)result.DataResult;
            }
            else
            {
                Lib.Win32.WinApi.ShowErrorMessage(result.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
            }
        }


        void _postingJournalView_CloseButtonClicked()
        {
            _postingJournalView.Close();
        }

        void _postingJournalView_Load(object sender, EventArgs e)
        {
            var result = _postTransactionToJournalProcess.GetActiveAccountingPeriod();
            if (result.IsSucess)
            {
                _postingJournalView.AccountPeriod = (RefAccPeriodModel)result.DataResult;
            }
            else
            {
                Lib.Win32.WinApi.ShowErrorMessage(result.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
            }
        }
    }
}
