﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class RefAccountPresenter : BasePresenter
    {
        private readonly IRefAccountProcess _refAccountProcess;
        private readonly IRefAccountView _refAccountView;

        #region Contructors

        public RefAccountPresenter(IRefAccountProcess p_Process, IRefAccountView p_View) : base(p_View)
        {
            try
            {
                this._refAccountProcess = p_Process;
                this._refAccountView = p_View;

                this._refAccountView.Load += _refAccountView_Load;
                this._refAccountView.BtnTambahClick += _refAccountView_BtnTambahClick;
                this._refAccountView.BtnHapusClick += _refAccountView_BtnHapusClick;
                this._refAccountView.BtnTutupClick += _refAccountView_BtnTutupClick;
                this._refAccountView.ReloadData += _refAccountView_ReloadData;
                this._refAccountView.GvRefAccountDoubleClick += _refAccountView_GvRefAccountDoubleClick;
            }
            catch(Exception ex)
            {
                _refAccountView.ShowErrorMessage(ex.Message, AppResource.CAP_PROCESS_ERROR);
            }
        }

        #endregion Contructors

        #region Private Methods

        private void LoadData()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _refAccountProcess.GetAllRefAccount()),
                AppResource.MSG_LOADING_REF_ACCOUNT);

            if (result.IsSucess)
            {
                List<RefAccountModel> data = result.DataResult as List<RefAccountModel>;

                _refAccountView.SetData(data);
                _refAccountView.RefreshDataGrid();
            }
            else
            {
                MessageBox.Show(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion Private Methods

        #region Control Events

        private void _refAccountView_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void _refAccountView_BtnTambahClick()
        {
            _refAccountView.ShowTambahRefBiayaView();
        }

        private void _refAccountView_BtnHapusClick()
        {
            RefAccountModel selectedRefAccount = _refAccountView.GetSelectedRefAccount();

            if (MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_DLT_REFBIAYA, selectedRefAccount.Account_name), "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                                () => _refAccountProcess.DeleteRefAccount(selectedRefAccount)),
                                AppResource.MSG_DELETE_REF_ACCOUNT);

                if (result.IsSucess)
                {
                    LoadData();
                }
                else
                {
                    if (result.ProcessException != null && result.ProcessException.GetType() == typeof(UpdateException))
                    {
                        MessageBox.Show(string.Format(AppResource.MSG_DATA_TDKBISADIHAPUS, selectedRefAccount.Account_name, Environment.NewLine), AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void _refAccountView_BtnTutupClick()
        {
            this._refAccountView.Close();
        }

        private void _refAccountView_GvRefAccountDoubleClick()
        {
            _refAccountView.ShowEditRefBiayaView();
        }

        private void _refAccountView_ReloadData()
        {
            LoadData();
        }

        #endregion Control Events
    }
}
