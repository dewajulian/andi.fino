﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class KonfigurasiBiayaPresenter : BasePresenter
    {
        IKonfigurasiBiayaView _view;
        IKonfigurasiBiayaProcess _process;

        public KonfigurasiBiayaPresenter(IKonfigurasiBiayaView p_View, IKonfigurasiBiayaProcess p_Process) : base(p_View)
        {
            _view = p_View;
            _process = p_Process;

            _view.BtnHapusRefBiayaClick += BtnHapusPosBiayaClick;
            _view.BtnHapusSemuaRefBiayaData += BtnHapusSemuaPosBiayaData;
            _view.BtnSimpanDataClick += BtnSimpanDataClick;
            _view.BtnTambahRefBiayaClick += BtnTambahPosBiayaClick;
            _view.BtnTambahSemuaRefBiayaData += BtnTambahSemuaPosBiayaData;
            _view.BtnTutupClick += BtnTutupClick;
            _view.Load += Load;            
        }

        public void Load(object sender, EventArgs e)
        {
            LoadRefData();
            LoadSelectedRefData();
        }

        public void BtnTutupClick()
        {
            _view.Close();
        }

        public void BtnTambahSemuaPosBiayaData()
        {
            List<RefBiayaModel> masterSource = _view.GetGVRefBiayaMasterDataSource();
            List<RefBiayaModel> selectedSource = _view.GetGVRefBiayaSelectedDataSource();

            if (selectedSource == null)
            {
                selectedSource = new List<RefBiayaModel>();
            }

            selectedSource.AddRange(masterSource);

            masterSource.Clear();

            _view.SetRefBiayaSelectedGrid(selectedSource);

            _view.SetRefBiayaMasterGrid(masterSource);
        }

        public void BtnTambahPosBiayaClick()
        {
            if (_view.GetSelectedRefBiayaMaster() != null)
            {
                List<RefBiayaModel> masterSource = _view.GetGVRefBiayaMasterDataSource();
                List<RefBiayaModel> selectedSource = _view.GetGVRefBiayaSelectedDataSource();

                if (selectedSource == null)
                {
                    selectedSource = new List<RefBiayaModel>();
                }

                if (masterSource != null && _view.GetSelectedRefBiayaMaster() != null)
                {
                    selectedSource.Add(_view.GetSelectedRefBiayaMaster());
                    masterSource.Remove(_view.GetSelectedRefBiayaMaster());

                    _view.SetRefBiayaSelectedGrid(selectedSource);                    

                    _view.SetRefBiayaMasterGrid(masterSource);                    
                }
            }
        }

        public void BtnSimpanDataClick()
        {
            GrupBiayaModel data = _view.SelectedGrupBiaya;
            data.RefBiayas = _view.GetGVRefBiayaSelectedDataSource();

            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                        () => _process.UpdateMappingGrupBiaya(data)),
                        AppResource.MSG_SAVING_MAPPING_GRUP_BIAYA);

            if (result.IsSucess)
            {
                LoadRefData();
                LoadSelectedRefData();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        public void BtnHapusSemuaPosBiayaData()
        {
            List<RefBiayaModel> masterSource = _view.GetGVRefBiayaMasterDataSource();
            List<RefBiayaModel> selectedSource = _view.GetGVRefBiayaSelectedDataSource();

            masterSource.AddRange(selectedSource);

            selectedSource.Clear();

            _view.SetRefBiayaSelectedGrid(selectedSource);

            _view.SetRefBiayaMasterGrid(masterSource);
        }

        public void BtnHapusPosBiayaClick()
        {
            if (_view.GetSelectedRefBiayaSelected() != null)
            {
                List<RefBiayaModel> masterSource = _view.GetGVRefBiayaMasterDataSource();
                List<RefBiayaModel> selectedSource = _view.GetGVRefBiayaSelectedDataSource();

                if (masterSource != null && selectedSource != null && _view.GetSelectedRefBiayaSelected() != null)
                {
                    masterSource.Add(_view.GetSelectedRefBiayaSelected());
                    selectedSource.Remove(_view.GetSelectedRefBiayaSelected());

                    _view.SetRefBiayaSelectedGrid(selectedSource);

                    _view.SetRefBiayaMasterGrid(masterSource);
                }
            }
        }

        private void LoadRefData()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                    () => _process.GetRefBiaya()),
                    AppResource.MSG_LOADING_REF_BIAYA);

            if (result.IsSucess)
            {
                List<RefBiayaModel> data = result.DataResult as List<RefBiayaModel>;

                _view.SetRefBiayaMasterGrid(data);
                _view.RefreshRefBiayaMasterDataGrid();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        private void LoadSelectedRefData()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                    () => _process.GetMappingBiaya(_view.SelectedGrupBiaya.GrupBiayaId)),
                    AppResource.MSG_LOADING_MAPPING_GRUP_BIAYA);

            if (result.IsSucess)
            {
                List<RefBiayaModel> data = result.DataResult as List<RefBiayaModel>;

                _view.SetRefBiayaSelectedGrid(data);
                _view.RefreshRefBiayaSelectedDataGrid();
                CompareMasterAndSelectedDataSource();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        private void CompareMasterAndSelectedDataSource()
        {
            List<RefBiayaModel> masterData = _view.GetGVRefBiayaMasterDataSource();
            List<RefBiayaModel> selectedData = _view.GetGVRefBiayaSelectedDataSource();

            if (selectedData != null)
            {
                foreach (RefBiayaModel item in selectedData)
                {
                    RefBiayaModel deletedItem = masterData.Where(x => x.Biaya_id == item.Biaya_id).FirstOrDefault();

                    masterData.Remove(deletedItem);
                }
            }

            _view.SetRefBiayaMasterGrid(masterData);
        }
    }
}
