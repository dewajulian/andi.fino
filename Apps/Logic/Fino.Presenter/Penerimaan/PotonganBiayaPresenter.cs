﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Model;
using Fino.View;

namespace Fino.Presenter
{
    public class PotonganBiayaPresenter : BasePresenter
    {
        private readonly IPotonganBiayaProcess _potonganBiayaProcess;
        private readonly IPotonganBiayaView _potonganBiayaView;

        public PotonganBiayaPresenter(IPotonganBiayaProcess p_Process, IPotonganBiayaView p_View) : base(p_View)
        {
            try
            {
                _potonganBiayaProcess = p_Process;
                _potonganBiayaView = p_View;

                _potonganBiayaView.CloseButtonClicked += _potonganBiayaView_CloseButtonClicked;
                _potonganBiayaView.SearchSiswaButtonClicked += _potonganBiayaView_SearchSiswaButtonClicked;
                
                _potonganBiayaView.NewDataButtonClicked += _potonganBiayaView_NewDataButtonClicked;
                _potonganBiayaView.SaveButtonClicked += _potonganBiayaView_SaveButtonClicked;
                _potonganBiayaView.DeleteButtonClicked += _potonganBiayaView_DeleteButtonClicked;

                AssignNewViewModel();
                _potonganBiayaView.BiayaDataSource = GetBiayaDataSource();
                _potonganBiayaView.ShowHeader();
                _potonganBiayaView.ShowPotonganDetail(false);

                _potonganBiayaView.SetSearchMode();

                _potonganBiayaView.BiayaListSelectionChanged += _potonganBiayaView_BiayaListSelectionChanged;
                _potonganBiayaView.CariSiswaTextKeypressed += _potonganBiayaView_CariSiswaTextKeypressed;
                _potonganBiayaView.SelectingSiswaDone += _potonganBiayaView_SelectingSiswaDone;
            }
            catch(Exception ex)
            {
                _potonganBiayaView.ShowErrorMessage(ex.Message, AppResource.CAP_PROCESS_ERROR);
            }
        }

        void _potonganBiayaView_SelectingSiswaDone()
        {
            _potonganBiayaView.NoInduk = _potonganBiayaView.PencarianSiswaView.SelectedSiswa.Code;
            _potonganBiayaView_SearchSiswaButtonClicked();
        }

        void _potonganBiayaView_CariSiswaTextKeypressed()
        {
            _potonganBiayaView.PencarianSiswaView.Visible = true;
            _potonganBiayaView.PencarianSiswaView.StringSiswa = _potonganBiayaView.Keyword;

            _potonganBiayaView.PencarianSiswaView.Cari();
        }

        void _potonganBiayaView_DeleteButtonClicked()
        {
            if (_potonganBiayaView.PotonganId > 0)
            {
                // Marked as DELETED by changing to negative value
                _potonganBiayaView.CurrentDetail.Potongan_Id = _potonganBiayaView.CurrentDetail.Potongan_Id * -1;
            }
            else if (_potonganBiayaView.PotonganId.Equals(0))
            {
                // PotonganID == 0 means it has not been saved to database
                _potonganBiayaView.ViewModel.PotonganBiayaDetails.Remove(_potonganBiayaView.CurrentDetail);
            }
            SetCurrentDetail();
            ShowDetailInformation();
        }

        
        void _potonganBiayaView_SaveButtonClicked()
        {
            SaveData();
        }

        void SaveData()
        {
            var model = _potonganBiayaView.ViewModel;
            ProcessResult procResult = _potonganBiayaProcess.ManagePotonganBiaya(model.PotonganBiayaDetails);

            if (procResult.ProcessException != null)
            {
                _potonganBiayaView.ShowErrorMessage(procResult.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
            }
            else
            {
                _potonganBiayaView.ViewModel.PotonganBiayaDetails = (List<PotonganBiayaDetailModel>)procResult.DataResult;
                SetCurrentDetail();
                
                _potonganBiayaView.ShowInformation(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.CAP_PROCESS_INFORMATION);
            }
        }

        void _potonganBiayaView_NewDataButtonClicked()
        {
            _potonganBiayaView.CurrentDetail = new PotonganBiayaDetailModel
            {
                Biaya_Id = _potonganBiayaView.BiayaId,
                Nilai = 0,
                IsPersen = true,
                Mulai_Tanggal = DateTime.Now.Date,
                Hingga_Tanggal = DateTime.Now.Date,
                Siswa_Id = _potonganBiayaView.SiswaId,
                Potongan_Id = 0
            };

            _potonganBiayaView.ViewModel.PotonganBiayaDetails.Add(_potonganBiayaView.CurrentDetail);
            ShowDetailInformation();
        }

        void ShowDetailInformation()
        {
            var show = _potonganBiayaView.CurrentDetail != null;

            _potonganBiayaView.ShowPotonganDetail(show);
            if (_potonganBiayaView.CurrentDetail != null)
            {
                _potonganBiayaView.SetBinding();
            }
        }

        void SetCurrentDetail()
        {
            _potonganBiayaView.CurrentDetail = _potonganBiayaView.ViewModel.PotonganBiayaDetails
                .Where(e => e.Biaya_Id.Equals(_potonganBiayaView.BiayaId) && e.Potongan_Id >= 0).FirstOrDefault();
        }

        void _potonganBiayaView_BiayaListSelectionChanged()
        {
            SetCurrentDetail();
            ShowDetailInformation();
        }

        public object GetBiayaDataSource()
        {
            return _potonganBiayaProcess.GetBiayaList().DataResult;
        }

        void _potonganBiayaView_SearchSiswaButtonClicked()
        {
            if (_potonganBiayaView.IsSearchMode)
            {
                var procResult = _potonganBiayaProcess.GetSiswaByNoInduk(_potonganBiayaView.NoInduk);
                if (procResult != null && procResult.DataResult != null)
                {
                    AssignNewViewModel();
                    var returnModel = (PotonganBiayaHeaderModel)procResult.DataResult;
                    _potonganBiayaView.ViewModel = returnModel;

                    //_potonganBiayaView.ViewModel.Code = returnModel.Code;
                    //_potonganBiayaView.ViewModel.JKelamin = returnModel.JKelamin;
                    //_potonganBiayaView.ViewModel.Nama = returnModel.Nama;
                    //_potonganBiayaView.ViewModel.Siswa_Id = returnModel.Siswa_Id;

                    _potonganBiayaView.ShowHeader();
                    SetCurrentDetail();
                    ShowDetailInformation();
                    _potonganBiayaView.SetEntryMode();
                }
                else
                {
                    _potonganBiayaView.ShowInformation(AppResource.MSG_LOADING_SISWA_NOT_FOUND, AppResource.CAP_PROCESS_INFORMATION);
                }
            }
            else
            {
                AssignNewViewModel();
                _potonganBiayaView.ShowHeader();
                _potonganBiayaView.ClearView();
                _potonganBiayaView.SetSearchMode();
            }
        }

        private void AssignNewViewModel()
        {
            _potonganBiayaView.ViewModel = new PotonganBiayaHeaderModel
            {
                PotonganBiayaDetails = new List<PotonganBiayaDetailModel>()
            };
        }

        void _potonganBiayaView_CloseButtonClicked()
        {
            _potonganBiayaView.Close();
        }
    }
}
