﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Model;
using Fino.View;

namespace Fino.Presenter
{
    public class KonversiCicilanPresenter : BasePresenter
    {
        private readonly IKonversiCicilanProcess _konversiCicilanProcess;
        private readonly IKonversiCicilanView _konversiCicilanView;

        public KonversiCicilanPresenter(IKonversiCicilanProcess p_Process, IKonversiCicilanView p_View) : base(p_View)
        {
            try
            {
                _konversiCicilanProcess = p_Process;
                _konversiCicilanView = p_View;

                _konversiCicilanView.CloseButtonClicked += _konversiCicilanView_CloseButtonClicked;
                _konversiCicilanView.SearchSiswaButtonClicked += _konversiCicilanView_SearchSiswaButtonClicked;

                _konversiCicilanView.NewDataButtonClicked += _konversiCicilanView_NewDataButtonClicked;
                _konversiCicilanView.SaveButtonClicked += _konversiCicilanView_SaveButtonClicked;
                _konversiCicilanView.LamaRadioButtonSelected += _konversiCicilanView_LamaRadioButtonSelected;
                _konversiCicilanView.NilaiCicilanRadioButtonSelected += _konversiCicilanView_NilaiCicilanRadioButtonSelected;
                _konversiCicilanView.TabViewSelected += _konversiCicilanView_TabViewSelected;
                _konversiCicilanView.TenorCicilanChanged += _konversiCicilanView_TenorCicilanChanged;
                _konversiCicilanView.NilaiCicilanChanged += _konversiCicilanView_NilaiCicilanChanged;
                _konversiCicilanView.MulaiTanggalTenorChanged += _konversiCicilanView_MulaiTanggalTenorChanged;
                _konversiCicilanView.MulaiTanggalNilaiCicilanChanged += _konversiCicilanView_MulaiTanggalNilaiCicilanChanged;
                _konversiCicilanView.CariSiswaTextKeypressed += _konversiCicilanView_CariSiswaTextKeypressed;
                _konversiCicilanView.SelectingSiswaDone += _konversiCicilanView_SelectingSiswaDone;
                _konversiCicilanView.SetSearchMode();

            }
            catch (Exception ex)
            {
                _konversiCicilanView.ShowErrorMessage(ex.Message, AppResource.CAP_PROCESS_ERROR);
            }
        }

        void _konversiCicilanView_SelectingSiswaDone()
        {
            _konversiCicilanView.NoInduk = _konversiCicilanView.PencarianSiswaView.SelectedSiswa.Code;
            _konversiCicilanView_SearchSiswaButtonClicked();
        }
        
        private void _konversiCicilanView_CariSiswaTextKeypressed()
        {
            _konversiCicilanView.PencarianSiswaView.Visible = true;
            _konversiCicilanView.PencarianSiswaView.StringSiswa = _konversiCicilanView.Keyword;

            _konversiCicilanView.PencarianSiswaView.Cari();
        }

        void _konversiCicilanView_MulaiTanggalNilaiCicilanChanged()
        {
            _konversiCicilanView.TenorCicilan = CalculateTenor();
            GenerateInstallment();
        }

        void _konversiCicilanView_MulaiTanggalTenorChanged()
        {
            _konversiCicilanView.NilaiCicilan = CalculateCicilan();
            GenerateInstallment();
        }


        private int CalculateTenor()
        {
            decimal total = (decimal)_konversiCicilanView.TotalBiaya;
            decimal cicilan = (decimal)_konversiCicilanView.NilaiCicilan;

            if (cicilan.Equals(0))
            {
                return 0;
            }
            else
            {
                return (int)Math.Ceiling((total / cicilan));
            }
        }

        private decimal CalculateCicilan()
        {
            decimal total = (decimal)_konversiCicilanView.TotalBiaya;
            decimal tenor = (decimal)_konversiCicilanView.TenorCicilan;

            if (tenor.Equals(0))
            {
                return 0;
            }
            else
            {
                return Math.Round((total / tenor), MidpointRounding.AwayFromZero);
            }
        }
        void _konversiCicilanView_NilaiCicilanChanged()
        {
            _konversiCicilanView.TenorCicilan = CalculateTenor();
            GenerateInstallment();
        }

        void _konversiCicilanView_TenorCicilanChanged()
        {
            _konversiCicilanView.NilaiCicilan = CalculateCicilan();
            GenerateInstallment();
        }

        private void GenerateInstallment()
        {
            _konversiCicilanView.CurrentHeader.Details.Clear();
            var proc = _konversiCicilanProcess.GenerateInstallamentSimulation(_konversiCicilanView.CurrentHeader);
            if (proc.IsSucess)
            {
                _konversiCicilanView.CurrentHeader = (KonversiCicilanHeaderModel)proc.DataResult;
                _konversiCicilanView.CicilanDetailDatasource = _konversiCicilanView.CurrentHeader.Details;
            }
        }

        void _konversiCicilanView_TabViewSelected()
        {
            // DO nothing
        }

        void _konversiCicilanView_NilaiCicilanRadioButtonSelected()
        {
            _konversiCicilanView.CurrentHeader.IsTenor = false;
            _konversiCicilanView.ShowTenorAndNilai();
        }

        void _konversiCicilanView_LamaRadioButtonSelected()
        {
            _konversiCicilanView.CurrentHeader.IsTenor = true;
            _konversiCicilanView.ShowTenorAndNilai();
        }

        void ShowHeaderInformation()
        {
            var show = _konversiCicilanView.CurrentHeader != null;

            _konversiCicilanView.ShowKonversiHeader(show);
            _konversiCicilanView.SetBinding();
            if (show)
            {
                
                _konversiCicilanView.CicilanDetailDatasource = _konversiCicilanView.CurrentHeader.Details;

                if (_konversiCicilanView.CurrentHeader.IsSaved)
                {
                    _konversiCicilanView.SetViewMode();
                }
                else
                {
                    _konversiCicilanView.SetEntryMode();
                }
            }
            else
            {
                _konversiCicilanView.CicilanDetailDatasource = null;
            }

        }

        void SetCurrentHeader()
        {
            _konversiCicilanView.CurrentHeader = _konversiCicilanView.ViewModel.KonversiCicilanHeaders.Where(e => e.PosBiaya_Id.Equals(_konversiCicilanView.PosBiayaId)).FirstOrDefault();
            _konversiCicilanView.CurrentPosBiaya = _konversiCicilanView.ViewModel.PosBiayas.Where(e => e.PosBiayaId.Equals(_konversiCicilanView.PosBiayaId)).FirstOrDefault();

        }

        void _konversiCicilanView_BiayaListSelectionChanged()
        {
            SetCurrentHeader();
            ShowHeaderInformation();
        }

        private void _konversiCicilanView_SaveButtonClicked()
        {
            SaveData();
        }

        void SaveData()
        {
            var modelList = _konversiCicilanView.ViewModel.KonversiCicilanHeaders;
            
            if (modelList.Count > 0 && modelList.Any(e=>!e.IsSaved))
            {
                if (MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_CRT_CICILAN,
                string.Empty, Environment.NewLine),
                "Konversi Biaya ke Cicilan", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    ProcessResult procResult = _konversiCicilanProcess.CreateInstallment(modelList);

                    if (procResult.ProcessException != null)
                    {
                        _konversiCicilanView.ShowErrorMessage(procResult.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
                    }
                    else
                    {
                        _konversiCicilanView.ViewModel.KonversiCicilanHeaders = (List<KonversiCicilanHeaderModel>)procResult.DataResult;
                        SetCurrentHeader();
                        ShowHeaderInformation();
                        _konversiCicilanView.ShowInformation(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.CAP_PROCESS_INFORMATION);
                    }
                }
            }
        }

        private void _konversiCicilanView_NewDataButtonClicked()
        {
            if (MessageBox.Show(string.Format(AppResource.MSG_KONFIRMASI_CRT_CICILAN, 
                _konversiCicilanView.PosBiayaNama, Environment.NewLine), 
                "Konversi Biaya ke Cicilan", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var newHeader = new KonversiCicilanHeaderModel()
                {
                    PosBiaya_Id = _konversiCicilanView.PosBiayaId,
                    DateCreated = DateTime.Now,
                    IsActive = true,
                    MulaiTanggal = DateTime.Now.Date,
                    NilaiCicilan = 0,
                    Status_Id = 0,
                    Tenor = 0,
                    Details = new List<KonversiCicilanDetailModel>(),
                    TotalNilai = _konversiCicilanView.CurrentPosBiaya.NilaiBiaya - _konversiCicilanView.CurrentPosBiaya.Potongan,
                    IsTenor = true
                };

                _konversiCicilanView.CurrentHeader = newHeader;
                _konversiCicilanView.ViewModel.KonversiCicilanHeaders.Add(newHeader);
                SetCurrentHeader();
                ShowHeaderInformation();
            }
        }



        private List<ValueList> InitBiayaDatasource()
        {
            var list = new List<ValueList>();
            if (_konversiCicilanView.ViewModel != null && _konversiCicilanView.ViewModel.PosBiayas != null)
            {

                foreach (var h in _konversiCicilanView.ViewModel.PosBiayas)
                {
                    list.Add(new ValueList
                    {
                        Id = h.PosBiayaId,
                        Value = h.NamaBiaya
                    });
                }
            }

            return list;
        }

        void _konversiCicilanView_SearchSiswaButtonClicked()
        {
            //IKonversiCicilanProcess process = new KonversiCicilanProcess();

            if (_konversiCicilanView.IsSearchMode)
            {
                var procResult = _konversiCicilanProcess.GetSiswaByNoInduk(_konversiCicilanView.NoInduk);
                if (procResult != null && procResult.DataResult != null)
                {
                    AssignNewViewModel();
                    var returnModel = (KonversiCicilanSiswaModel)procResult.DataResult;
                    _konversiCicilanView.ViewModel = returnModel;
                    _konversiCicilanView.BiayaListSelectionChanged -= _konversiCicilanView_BiayaListSelectionChanged;
                    _konversiCicilanView.BiayaDataSource = InitBiayaDatasource();
                    _konversiCicilanView.BiayaListSelectionChanged += _konversiCicilanView_BiayaListSelectionChanged;
                    SetCurrentHeader();
                    ShowHeaderInformation();
                    _konversiCicilanView.SetEntryMode();
                }
                else
                {
                    _konversiCicilanView.ShowInformation(AppResource.MSG_LOADING_SISWA_NOT_FOUND, AppResource.CAP_PROCESS_INFORMATION);
                }
            }
            else
            {
                AssignNewViewModel();
                _konversiCicilanView.ClearView();
                _konversiCicilanView.SetSearchMode();
            }
        }

        private void AssignNewViewModel()
        {
            _konversiCicilanView.ViewModel = new KonversiCicilanSiswaModel
            {
                KonversiCicilanHeaders = new List<KonversiCicilanHeaderModel>()
            };
            foreach (var c in _konversiCicilanView.ViewModel.KonversiCicilanHeaders)
            {
                c.Details = new List<KonversiCicilanDetailModel>();
            }
        }

        void _konversiCicilanView_CloseButtonClicked()
        {
            _konversiCicilanView.Close();
        }
    }
}
