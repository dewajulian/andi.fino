using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class DaftarLayananPresenter : BasePresenter
    {
        private IDaftarLayananView _view;
        private ILayananProcess _process;

        public DaftarLayananPresenter(IDaftarLayananView p_view, ILayananProcess p_process) : base(p_view)
        {
            _view = p_view;
            _process = p_process;

            _view.btnTutupClicked += _view_btnTutupClicked;
            _view.btnSimpanClicked += _view_btnSimpanClicked;
            _view.txtSiswaKeyPressed += _view_txtSiswaKeyPressed;
            _view.SelectingSiswaDone += _view_SelectingSiswaDone;
            _view.btnClearClicked += _view_btnClearClicked;
            _view.Load += _view_Load;
            _view.cmbLayananValueChanged += _view_cmbLayananValueChanged;
        }

        void _view_btnClearClicked()
        {
            ClearView();
        }

        void _view_Load(object sender, EventArgs e)
        {
            LoadRefLayanan();

            _view.DaftarLayanan = new DaftarLayananModel();

            _view.DaftarLayanan.SiswaId = 0;
            _view.DaftarLayanan.LayananId = 0;
            _view.DaftarLayanan.KodeLayanan = string.Empty;
            _view.DaftarLayanan.Nilai = 0;
            _view.DaftarLayanan.MulaiTanggal = DateTime.Now;
            _view.DaftarLayanan.HinggaTanggal = DateTime.Now.AddMonths(1);
            
            _view.SetBinding();
            _view.DaftarLayanan.ErrorValidation += DaftarLayanan_ErrorValidation;
        }

        private void LoadRefLayanan()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
            () => _process.GetAllLayananAsValueList()),
            "Memuat data layanan");

            if (result.IsSucess)
            {
                List<ValueList> dataResult = result.DataResult as List<ValueList>;

                if (dataResult != null)
                {
                    _view.ListLayananItems = dataResult;                    
                }
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        void _view_cmbLayananValueChanged()
        {
            if (_view.DaftarLayanan != null)
            {
                _view.DaftarLayanan.LayananId = _view.LayananId;
                _view.DaftarLayanan.KodeLayanan = _view.KodeLayanan;
                _view.DaftarLayanan.BiayaId = _view.BiayaId;
            }
        }

        void DaftarLayanan_ErrorValidation(object sender, CustomEventArgs<string> e)
        {
            WinApi.ShowErrorMessage(e.Data, AppResource.CAP_VALIDASI_ERROR);
        }        

        void _view_SelectingSiswaDone()
        {
            _view.NamaSiswa = _view.PencarianSiswaView.SelectedSiswa.Nama;
            _view.NoInduk = _view.PencarianSiswaView.SelectedSiswa.Code;

            if (_view.DaftarLayanan != null)
            {
                _view.DaftarLayanan.SiswaId = _view.SiswaId;
            }
        }

        void _view_txtSiswaKeyPressed()
        {
            _view.PencarianSiswaView.Visible = true;
            _view.PencarianSiswaView.StringSiswa = _view.TextSearchSiswa;

            _view.PencarianSiswaView.Cari();
        }

        void _view_btnSimpanClicked()
        {
            var context = new ValidationContext(_view.DaftarLayanan, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(_view.DaftarLayanan, context, results, true);

            if (isValid)
            {
                // simpan
                ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                                                () => _process.SimpanDaftarLayanan(_view.DaftarLayanan)),
                                                "Menyimpan data layanan");

                if (result.IsSucess)
                {
                    ClearView();
                }
                else
                {
                    WinApi.ShowErrorMessage(result.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
                }
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (var item in results)
                {
                    sb.AppendLine(item.ErrorMessage);
                }

                WinApi.ShowErrorMessage(sb.ToString(), AppResource.CAP_VALIDASI_ERROR);
            }
        }

        void _view_btnTutupClicked()
        {
            _view.Close();
        }

        void ClearView()
        {
            _view.DaftarLayanan.ErrorValidation -= DaftarLayanan_ErrorValidation;
            _view.ClearBinding();

            _view.DaftarLayanan = new DaftarLayananModel();
            _view.NamaSiswa = string.Empty;
            _view.NoInduk = string.Empty;
            _view.KodeLayanan = string.Empty;
            _view.Nilai = 0;
            _view.DaftarLayanan.MulaiTanggal = DateTime.Now;
            _view.DaftarLayanan.HinggaTanggal = DateTime.Now.AddMonths(1);

            _view.PencarianSiswaView.ClearSelected();

            _view.SetBinding();
            _view.DaftarLayanan.ErrorValidation += DaftarLayanan_ErrorValidation;
        }
    }
}
