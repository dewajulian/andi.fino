﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class ReportMutasiPresenter : BasePresenter
    {
        IReportMutasiView _view;
        IMutasiTabunganProcess _process;

        public ReportMutasiPresenter(IReportMutasiView p_view, IMutasiTabunganProcess p_process) : base(p_view)
        {
            _view = p_view;
            _process = p_process;

            _view.BtnOkClicked += OkBtnClicked;
            _view.BtnTutupClicked += BtnTutupClicked;
        }

        public void BtnTutupClicked()
        {
            _view.Close();
        }

        public void OkBtnClicked()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _process.GetAllMutasiAsOn(_view.SelectedBulan)),
                AppResource.MSG_REPORT_MUTASI);

            if (result.IsSucess)
            {
                _view.ShowRPP(result.DataResult as List<LaporanMutasiModel>);
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            } 
        }
    }
}
