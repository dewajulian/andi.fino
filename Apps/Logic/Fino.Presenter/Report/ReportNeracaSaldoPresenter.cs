﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;

namespace Fino.Presenter
{
    public class ReportNeracaSaldoPresenter : BasePresenter
    {
        IReportNeracaSaldoView _view;
        IPostTransactionToJournalProcess _process;

        public ReportNeracaSaldoPresenter(IReportNeracaSaldoView p_view, IPostTransactionToJournalProcess p_process) : base(p_view)
        {
            _view = p_view;
            _process = p_process;

            _view.BtnOkClicked += OkBtnClicked;
            _view.BtnTutupClicked += BtnTutupClicked;
            _view.Load += _view_Load;
        }

        void _view_Load(object sender, EventArgs e)
        {
            var result = _process.GetAvailablePeriod();
            if (result.IsSucess)
            {
                _view.PeriodDataSource = result.DataResult;
            }
        }

        public void BtnTutupClicked()
        {
            _view.Close();
        }

        public void OkBtnClicked()
        {
            var periodId = _view.PeriodId;

            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _process.GetNeracaSaldo(periodId)),
                AppResource.MSG_REPORT_NERACA_SALDO);

            if (result.IsSucess)
            {
                _view.ShowRPP(result.DataResult as List<NeracaSaldoModel>);
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            } 
        }
    }
}
