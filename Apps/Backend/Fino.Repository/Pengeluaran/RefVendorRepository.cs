﻿using Fino.Datalib.Entity;
using Fino.Model;
using System.Collections.Generic;
using System.Linq;

namespace Fino.Repository.Pengeluaran
{
    public class RefVendorRepository : BaseRepository, IRefVendorRepository
    {
        private RefVendor GetExistingRefVendor(int refVendorId)
        {
            return this.Context.RefVendors.Where(x => x.refvendor_id == refVendorId).FirstOrDefault();
        }

        public Model.RefVendorModel GetVendor(int refVendorId)
        {
            var existingRefVendor = GetExistingRefVendor(refVendorId);

            if (existingRefVendor != null)
            {
                return new RefVendorModel
                {
                    VendorId = existingRefVendor.refvendor_id,
                    VendorName = existingRefVendor.vendorname,
                    VendorAddr = existingRefVendor.vendor_addr,
                    VendorEmail = existingRefVendor.vendor_email,
                    VendorPhone = existingRefVendor.vendor_phone
                };
            }

            return null;
        }

        public List<RefVendorModel> GetAllVendor()
        {
            List<RefVendorModel> result = new List<RefVendorModel>();

            var allVendor = this.Context.RefVendors.ToList();

            foreach(var vendor in allVendor)
            {
                result.Add(new RefVendorModel
                    {
                        VendorId = vendor.refvendor_id,
                        VendorName = vendor.vendorname,
                        VendorAddr = vendor.vendor_addr,
                        VendorEmail = vendor.vendor_email,
                        VendorPhone = vendor.vendor_phone
                    });
            }

            return result;
        }

        public void UpdateVendor(RefVendorModel entity)
        {
            var existingRefVendor = GetExistingRefVendor(entity.VendorId);

            if (existingRefVendor == null)
            {
                this.Context.RefVendors.Add(new RefVendor
                    {
                        vendorname = entity.VendorName,
                        vendor_addr = entity.VendorAddr,
                        vendor_email = entity.VendorEmail,
                        vendor_phone = entity.VendorPhone
                    });
            }
            else
            {
                existingRefVendor.vendorname = entity.VendorName;
                existingRefVendor.vendor_addr = entity.VendorAddr;
                existingRefVendor.vendor_email = entity.VendorEmail;
                existingRefVendor.vendor_phone = entity.VendorPhone;
            }

            this.Context.SaveChanges();
        }

        public void DeleteVendor(RefVendorModel entity)
        {
            var existingRefVendor = GetExistingRefVendor(entity.VendorId);

            if (existingRefVendor != null)
            {
                this.Context.RefVendors.Remove(existingRefVendor);
            }

            this.Context.SaveChanges();
        }
    }
}
