﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Model;
using Fino.Repository;

namespace Fino.Repository
{
    public class PayableRepository : BaseRepository, IPayableRepository
    {
        public List<RekapitulasiPengeluaranModel> GetRekapitulasiPengeluaran(DateTime p_FromDate, DateTime p_ToDate)
        {
            var startDate = p_FromDate.Date;
            var endDate = new DateTime(p_ToDate.Year, p_ToDate.Month, p_ToDate.Day, 23, 59, 59);
            var sqlCommand = new StringBuilder("select ")
                                   .Append("p.refpayable_id, ")
                                   .Append("r.name as payable_name, ")
                                   .Append("sum(p.nilai) as payable_value ")
                                   .Append("from payables p inner join ")
                                   .Append("     refpayables r on p.refpayable_id = r.refpayable_id ")
                                   .Append("where p.isactive = 1 ")
                                   .Append("      and p.created >= @p0 ")
                                   .Append("	  and p.created <= @p1 ")
                                   .Append("group by p.refpayable_id, r.name ");

            List<RekapitulasiPengeluaranModel> resultSql = Context.Database
                    .SqlQuery<RekapitulasiPengeluaranModel>(
                        sqlCommand.ToString(),
                        startDate.ToString("yyyy/MM/dd"),
                        endDate.ToString("yyyy/MM/dd HH:mm:ss")).ToList();

            return resultSql;
        }
    }
}
