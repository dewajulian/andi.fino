﻿using Fino.Datalib.Dbmodel;
using Fino.Model;
using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;
using Fino.Lib.Core.Settings;

namespace Fino.Repository
{
    public class PosBiayaRepository : BaseRepository, IPosBiayaRepository
    {
        public List<LaporanKasDetailModel> GetLaporanKasDetail(DateTime p_ToDate, int p_KelasId, int p_SiswaId)
        {
            var endDate = new DateTime(p_ToDate.Year, p_ToDate.Month, p_ToDate.Day, 23, 59, 59);
            var sqlCmd = new StringBuilder("select d.nama as kelas, b.nama as siswa, a.deskripsi as biaya, (a.nilai - a.nilaipotongan) as nilai ")
                            .Append("from posbiayas a ")
                            .Append("left join refsiswas b on a.siswa_id = b.siswa_id ")
                            .Append("left join siswakelas c on b.siswa_id = c.siswa_id ")
                            .Append("left join refkelas d on c.kelas_id = d.kelas_id ")
                            .Append("inner join ")
	                        .Append("( select siswa_id, ")
	                        .Append("max(mulai_tanggal) mdtm ")
		                    .Append("from SiswaKelas ")
                            .Append("group by siswa_id) g on c.siswa_id = g.siswa_id and c.mulai_tanggal = g.mdtm ")
                            .Append("where a.jtempo <= @p0 and a.ispaid = 0 and a.IsActive = 1 ");

            if (p_KelasId > 0 && p_SiswaId > 0)
            {
                sqlCmd.Append("and d.kelas_id = @p1 and b.siswa_id = @p2 ");
            }
            else if (p_KelasId > 0 && p_SiswaId == 0)
            {
                sqlCmd.Append("and d.kelas_id = @p1 ");
            }

            List<LaporanKasDetailModel> resultSql = null;

            if (p_KelasId == 0 && p_SiswaId == 0)
            {
                resultSql = Context.Database
                    .SqlQuery<LaporanKasDetailModel>(
                        sqlCmd.ToString(),
                        endDate.ToString("yyyy/MM/dd HH:mm:ss")).ToList();
            }
            else if (p_KelasId > 0 && p_SiswaId > 0)
            {
                resultSql = Context.Database
                    .SqlQuery<LaporanKasDetailModel>(
                        sqlCmd.ToString(),
                        endDate.ToString("yyyy/MM/dd HH:mm:ss"), p_KelasId, p_SiswaId).ToList();
            }
            else if (p_KelasId > 0 && p_SiswaId == 0)
            {
                resultSql = Context.Database
                    .SqlQuery<LaporanKasDetailModel>(
                        sqlCmd.ToString(),
                        endDate.ToString("yyyy/MM/dd HH:mm:ss"), p_KelasId).ToList();
            }

            return resultSql;
        }

        public List<RekapitulasiPiutangDanPenerimaanModel> GetRekapitulasiPiutangDanPenerimaan(DateTime p_FromDate, DateTime p_ToDate)
        {
            var startDate = p_FromDate.Date;
            var endDate = new DateTime(p_ToDate.Year, p_ToDate.Month, p_ToDate.Day, 23, 59, 59);
            var sqlCmd = new StringBuilder("select b.nama as NamaBiaya ")
                            .Append(", sum(case ispaid ")
                            .Append("when 0 then 0 ")
                            .Append("when 1 then p.nilai - p.nilaipotongan  ")
                            .Append("end) as Penerimaan ")
                            .Append(", sum(case ispaid ")
                            .Append("when 0 then p.nilai - p.nilaipotongan ")
                            .Append("when 1 then 0 ")
                            .Append("end) as Piutang ")
                            .Append("from PosBiayas p inner join ")
                            .Append("RefBiayas b on p.Biaya_Id = b.biaya_id ")
                            .Append("where p.jtempo >= @p0 ")
                            .Append("and ")
                            .Append("p.jtempo <= @p1 and p.IsActive = 1 ")
                            .Append("group by b.nama ");

            List<RekapitulasiPiutangDanPenerimaanModel> resultSql = Context.Database
                .SqlQuery<RekapitulasiPiutangDanPenerimaanModel>(
                    sqlCmd.ToString(),
                    startDate.ToString("yyyy/MM/dd"),
                    endDate.ToString("yyyy/MM/dd HH:mm:ss")).ToList();

            return resultSql;
        }

        public void GeneratePosBiaya(DateTime pStart, DateTime pEnd, int pOption, RefBiaya pBiaya, RefSiswa pSiswa)
        {
            var startDate = pStart.Date;
            var endDate = new DateTime(pEnd.Year, pEnd.Month, pEnd.Day, 23, 59, 59);

            switch (pBiaya.repetisi)
            {
                case (int)RepetisiEnum.BULANAN:
                    GenerateBulanan(startDate, endDate, pOption, pBiaya, pSiswa);
                    break;

                default:
                    GenerateDefault(startDate, pOption, pBiaya, pSiswa);
                    break;
            }
        }

        public void GeneratePosBiayaLayanan(DateTime pStart, DateTime pEnd, AgtLayanan pLayanan, RefBiaya pBiaya, RefSiswa pSiswa)
        {
            var startDate = pStart.Date;
            var endDate = new DateTime(pEnd.Year, pEnd.Month, pEnd.Day, 23, 59, 59);

            for (int i = 0; startDate.AddMonths(i) <= endDate; i++)
            {
                var currentDate = startDate.AddMonths(i);
                IBiayaNilaiOptionRepository biayaNilaiOption = new BiayaNilaiOptionRepository();
                double nilaiBiaya = pLayanan.nilai;
                var jTempo = new DateTime(currentDate.Year, currentDate.Month, pBiaya.jt_tanggal);
                var deskripsi = new StringBuilder(pBiaya.nama).Append(" ").Append(jTempo.ToString("MMMM-yyyy")).ToString();

                double potongan = 0;
                bool isPersen = false;
                double nilaiPotongan = 0;
                CalculatePotongan(nilaiBiaya, jTempo, pBiaya, pSiswa, out potongan, out isPersen, out nilaiPotongan);

                var posBiaya = new PosBiaya
                {
                    Siswa_Id = pSiswa.siswa_id,
                    Biaya_Id = pBiaya.biaya_id,
                    Deskripsi = deskripsi,
                    IsActive = true,
                    IsPaid = false,
                    JTempo = jTempo,
                    Nilai = nilaiBiaya,
                    Potongan = potongan,
                    IsPersen = isPersen,
                    NilaiPotongan = nilaiPotongan,
                    Status_Id = 0
                };
                pSiswa.PosBiayas.Add(posBiaya);
            }
        }

        private void CalculatePotongan(double pNilaiBiaya, 
            DateTime pJTempo, RefBiaya pBiaya, RefSiswa pSiswa,
            out double pPotongan, out bool pIsPersen, out double pNilaiPotongan)
        {
            
            double potongan = 0;
            bool isPersen = false;
            double NilaiPotongan = 0;
            

            if (pSiswa.Potongans != null && pSiswa.Potongans.Count > 0)
            {
                var potonganBiaya = pSiswa.Potongans.Where(e => e.biaya_id.Equals(pBiaya.biaya_id)).FirstOrDefault();
                if (potonganBiaya != null &&
                    (pJTempo.Date >= potonganBiaya.mulai_tanggal.Date
                     && pJTempo.Date.Date <= potonganBiaya.hingga_tanggal.Date))
                {
                    potongan = potonganBiaya.nilai;
                    isPersen = potonganBiaya.persen;
                    if (isPersen)
                    {
                        NilaiPotongan = pNilaiBiaya * (potongan / 100);
                    }
                    else
                    {
                        NilaiPotongan = potongan;
                    }
                }
            }
            pPotongan = potongan;
            pIsPersen = isPersen;
            pNilaiPotongan = NilaiPotongan;
        }

        private void GenerateDefault(DateTime currentDate, int pOption, RefBiaya pBiaya, RefSiswa pSiswa)
        {
            IBiayaNilaiOptionRepository biayaNilaiOption = new BiayaNilaiOptionRepository();
            biayaNilaiOption.Context = Context;
            double nilaiBiaya = (double)biayaNilaiOption.GetNilaiBiayaInt(pBiaya, pOption);
            var jTempo = new DateTime(currentDate.Year, currentDate.Month, pBiaya.jt_tanggal);
            var deskripsi = new StringBuilder(pBiaya.nama).ToString();

            double potongan = 0;
            bool isPersen = false;
            double nilaiPotongan = 0;
            CalculatePotongan(nilaiBiaya, jTempo, pBiaya, pSiswa, out potongan, out isPersen, out nilaiPotongan);

            var posBiaya = new PosBiaya
            {
                Siswa_Id = pSiswa.siswa_id,
                Biaya_Id = pBiaya.biaya_id,
                Deskripsi = deskripsi,
                IsActive = true,
                IsPaid = false,
                JTempo = jTempo,
                Nilai = nilaiBiaya,
                Potongan = potongan,
                IsPersen = isPersen,
                NilaiPotongan = nilaiPotongan,
                Status_Id = 0
            };
            pSiswa.PosBiayas.Add(posBiaya);
        }

        private void GenerateBulanan(DateTime pStart, DateTime pEnd, int pOption, RefBiaya pBiaya, RefSiswa pSiswa)
        {
            for (int i = 0; pStart.AddMonths(i) <= pEnd; i++)
            {
                var currentDate = pStart.AddMonths(i);
                IBiayaNilaiOptionRepository biayaNilaiOption = new BiayaNilaiOptionRepository();
                biayaNilaiOption.Context = Context;
                double nilaiBiaya = biayaNilaiOption.GetNilaiBiayaInt(pBiaya, pOption);
                var jTempo = new DateTime(currentDate.Year, currentDate.Month, pBiaya.jt_tanggal);
                var deskripsi = new StringBuilder(pBiaya.nama).Append(" ").Append(jTempo.ToString("MMMM-yyyy")).ToString();
                
                double potongan = 0;
                bool isPersen = false;
                double nilaiPotongan = 0;
                CalculatePotongan(nilaiBiaya, jTempo, pBiaya, pSiswa, out potongan, out isPersen, out nilaiPotongan);

                var posBiaya = new PosBiaya
                {
                    Siswa_Id = pSiswa.siswa_id,
                    Biaya_Id = pBiaya.biaya_id,
                    Deskripsi = deskripsi,
                    IsActive = true,
                    IsPaid = false,
                    JTempo = jTempo,
                    Nilai = nilaiBiaya,
                    Potongan = potongan,
                    IsPersen = isPersen,
                    NilaiPotongan = nilaiPotongan,
                    Status_Id = 0
                };
                pSiswa.PosBiayas.Add(posBiaya);
            }
        }

        public List<PosBiaya> GetNotPaidCurrentPosBiayaSiswa(int p_SiswaId)
        {
            var endOfThisMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1);
            List<PosBiaya> result = (from a in Context.PosBiayas
                                     where a.Siswa_Id == p_SiswaId && a.JTempo <= endOfThisMonth && !a.IsPaid && a.IsActive
                                     select a).ToList();

            return result;
        }

        public List<PosBiayaModel> GetNextNotPaidAllPosBiayaSiswa(int p_SiswaId)
        {
            var sqlCmd = new StringBuilder("select ")
                .Append("case when (c.posbiaya_id is NULL) then 0 else c.posbiaya_id end as posbiayaid,")
                .Append("case when (c.deskripsi is NULL) then a.nama else c.deskripsi end as namabiaya,")
                .Append("case when (c.jtempo is NULL) then getdate() else c.jtempo end as jtempo,")
                .Append("case when (c.nilai is NULL) then 0 else c.nilai end as nilaibiaya,")
                .Append("case when (c.nilaipotongan is NULL) then 0 else c.nilaipotongan end as potongan,")
                .Append("c.ispaid,")
                .Append("case when (b.layanan_id is NULL) then 0 else b.layanan_id end as layananid,")
                .Append("a.noposbiaya,")
                .Append("a.biaya_id as refbiayaid ")
                .Append("from refbiayas a ")
                .Append("left join biayalayanans b on b.biaya_id = a.biaya_id ")
                .Append("left join posbiayas c on a.biaya_id = c.biaya_id and a.biaya_id = c.biaya_id and ")
                .Append("c.siswa_id = @p0 and c.ispaid = 0 and c.isactive = 1 and c.jtempo > @p1");

            var endOfThisMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 23, 59, 59).AddMonths(1).AddDays(-1);

            List<PosBiayaModel> models = Context.Database
                .SqlQuery<PosBiayaModel>(
                    sqlCmd.ToString(),
                    p_SiswaId,
                    endOfThisMonth.ToString("yyyy/MM/dd HH:mm:ss")).ToList();
            
            //List<PosBiaya> result = (from a in Context.PosBiayas
            //                         where a.Siswa_Id == p_SiswaId &&
            //                         a.JTempo > endOfThisMonth && a.IsActive && !a.IsPaid 
            //                         select a).ToList();

            return models;
        }
		
		public List<PosBiayaPaid> UpdatePembayaranPosBiaya(List<PosBiayaModel> p_posBiayaModel, int p_SiswaId)
        {
            List<PosBiayaPaid> result = new List<PosBiayaPaid>();

            List<int> posBiayaIds = (from a in p_posBiayaModel
                                     select a.PosBiayaId).ToList();

            result = UpdatePosBiaya(p_posBiayaModel, p_SiswaId);

            Context.SaveChanges();

            return result;
        }

        public CetakPembayaranModel SimpanCetakPembayaran(CetakPembayaranModel p_CetakPembayaranModel, 
            List<PosBiayaPaidModel> p_PosBiayaPaidModel)
        {
            NotaPembayaran np = new NotaPembayaran();

            np.siswa_id = p_CetakPembayaranModel.SiswaId;
            np.tanggal_cetak = DateTime.Today;
            np.user_id = p_CetakPembayaranModel.User.UserId;

            Context.NotaPembayarans.Add(np);

            Context.SaveChanges();

            p_CetakPembayaranModel.NotaPembayaranId = np.nota_pembayaran_Id;

            foreach (PosBiayaPaidModel item in p_PosBiayaPaidModel)
            {
                PosBiayaPaid pbp = new PosBiayaPaid();
                pbp.PosBiaya_Id = item.PosBiayaId;

                Context.PosBiayaPaids.Attach(pbp);

                pbp.nota_pembayaran_id = np.nota_pembayaran_Id;
            }

            Context.SaveChanges();

            return p_CetakPembayaranModel;
        }

        private List<PosBiayaPaid> UpdatePosBiaya(List<PosBiayaModel> p_posBiayaModel, int p_SiswaId)
        {
            List<PosBiayaPaid> result = new List<PosBiayaPaid>();

            foreach (var item in p_posBiayaModel)
            {
                if (item.PosBiayaId == 0)
                {
                    PosBiaya pb = new PosBiaya();
                    pb.Deskripsi = item.NamaBiaya;
                    pb.IsActive = true;
                    pb.IsPaid = true;
                    pb.Biaya_Id = item.RefBiayaId;
                    pb.Siswa_Id = p_SiswaId;
                    pb.DatePaid = DateTime.Today;
                    pb.JTempo = item.JTempo;
                    pb.Nilai = item.NilaiBiaya;
                    pb.NilaiPotongan = item.Potongan;                   

                    Context.PosBiayas.Add(pb);

                    Context.SaveChanges();

                    item.PosBiayaId = pb.PosBiaya_Id;

                    result.Add(AddPosBiayaPaid(item));
                }
                else
                {
                    PosBiaya pb = new PosBiaya();

                    pb.PosBiaya_Id = item.PosBiayaId;

                    Context.PosBiayas.Attach(pb);

                    pb.IsPaid = true;
                    pb.DatePaid = DateTime.Today;

                    result.Add(AddPosBiayaPaid(item));
                }
            }

            return result;
        }

        private PosBiayaPaid AddPosBiayaPaid(PosBiayaModel item)
        {            
            PosBiayaPaid pbp = new PosBiayaPaid();
            pbp.paynilai = item.JumlahBiaya;
            pbp.PosBiaya_Id = item.PosBiayaId;
            pbp.datepaid = DateTime.Today;

            Context.PosBiayaPaids.Add(pbp);

            return pbp;
        }

        public PosBiaya GetPosBiaya(int p_PosBiayaId)
        {
            PosBiaya returnModel = null;

            var entity = Context.PosBiayas.Find(p_PosBiayaId);
            returnModel = entity;
            returnModel.Biaya = entity.Biaya;

            return returnModel;
        }

        public List<RefKelas> GetAllKelas()
        {
            return Context.RefKelases.ToList();
        }

        public List<SiswaKelas> GetSiswaByKelas(int p_KelasId)
        {
            int tahunAjaranId = AppVariable.Instance.GetTahunAjaran().TahunAjaranId;

            return Context.SiswaKelases.Where(x => x.kelas_id == p_KelasId && x.tahun_ajaran_id == tahunAjaranId).ToList();
        }

        public List<PenerimaanHistoryModel> GetLaporanPenerimaanDetail(DateTime p_ToDate, int p_KelasId, int p_SiswaId)
        {
            var endDate = new DateTime(p_ToDate.Year, p_ToDate.Month, p_ToDate.Day, 23, 59, 59);
            var sqlCmd = new StringBuilder("select ")
                        .Append("s.siswa_id, ")
                        .Append("s.siswa_code, ")
                        .Append("s.nama as siswa_nama, ")
                        .Append("k.kelas_id, ")
                        .Append("k.nama as kelas_nama, ")
                        .Append("d.posbiaya_id, ")
                        .Append("b.deskripsi, ")
                        .Append("d.datepaid as tgl_bayar, ")
                        .Append("d.paynilai as nilai_bayar, ")
                        .Append("b.jtempo, ")
                        .Append("b.nilai as nilai_biaya, ")
                        .Append("b.nilaipotongan as nilai_potongan, ")
                        .Append("n.nota_pembayaran_id as nota_pembayaran ")
                        .Append("from PosBiayaPaids d inner join ")
                        .Append("     PosBiayas b on d.posbiaya_id = b.posbiaya_id inner join ")
                        .Append("	 RefSiswas s on b.siswa_id = s.siswa_id inner join ")
                        .Append("	 ( ")
                        .Append("	 	select siswa_id, max(tgl_daftar) as tgl_daftar ")
                        .Append("		from SiswaKelas ")
                        .Append("		group by siswa_id ")
                        .Append("	 ) skm on b.siswa_id = skm.siswa_id inner join ")
                        .Append("	 SiswaKelas sk on skm.siswa_id = sk.siswa_id ")
                        .Append("	             and  skm.tgl_daftar = sk.tgl_daftar inner join ")
                        .Append("	 RefKelas k on sk.kelas_id = k.kelas_id left outer join ")
                        .Append("	 NotaPembayarans n on d.nota_pembayaran_id = d.nota_pembayaran_id ")
                        .Append("where d.datepaid <= @p0 ");

            if (p_KelasId > 0 && p_SiswaId > 0)
            {
                sqlCmd.Append("and k.kelas_id = @p1 and s.siswa_id = @p2 ");
            }
            else if (p_KelasId > 0 && p_SiswaId == 0)
            {
                sqlCmd.Append("and k.kelas_id = @p1 ");
            }

            List<PenerimaanHistoryModel> resultSql = null;

            if (p_KelasId == 0 && p_SiswaId == 0)
            {
                resultSql = Context.Database
                    .SqlQuery<PenerimaanHistoryModel>(
                        sqlCmd.ToString(),
                        endDate.ToString("yyyy/MM/dd HH:mm:ss")).ToList();
            }
            else if (p_KelasId > 0 && p_SiswaId > 0)
            {
                resultSql = Context.Database
                    .SqlQuery<PenerimaanHistoryModel>(
                        sqlCmd.ToString(),
                        endDate.ToString("yyyy/MM/dd HH:mm:ss"), p_KelasId, p_SiswaId).ToList();
            }
            else if (p_KelasId > 0 && p_SiswaId == 0)
            {
                resultSql = Context.Database
                    .SqlQuery<PenerimaanHistoryModel>(
                        sqlCmd.ToString(),
                        endDate.ToString("yyyy/MM/dd HH:mm:ss"), p_KelasId).ToList();
            }

            return resultSql;
        }
    }
}
