﻿using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public class KonfigurasiBiayaRepository : BaseRepository, IKonfigurasiBiayaRepository
    {
        public List<RefBiaya> KonfigurasiBiayaData()
        {
            return (from a in Context.RefBiayas
                    select a).ToList();
        }

        public void AddKonfigurasiBiayaData(RefBiaya p_RefBiaya)
        {
            Context.RefBiayas.Add(p_RefBiaya);
        }

        public void UpdateKonfigurasiBiayaData(RefBiaya p_RefBiaya)
        {
            RefBiaya updateObj = Context.RefBiayas.Where(x => x.biaya_id == p_RefBiaya.biaya_id).FirstOrDefault();

            updateObj.nama = p_RefBiaya.nama;
            updateObj.code = p_RefBiaya.code;
            updateObj.repetisi = p_RefBiaya.repetisi;
            updateObj.jt_tanggal = p_RefBiaya.jt_tanggal;
            updateObj.jt_bulan = p_RefBiaya.jt_bulan;
            updateObj.aktif = p_RefBiaya.aktif;
            updateObj.mulai_tanggal = p_RefBiaya.mulai_tanggal;
            updateObj.noposbiaya = p_RefBiaya.noposbiaya;
            updateObj.bisacicil = p_RefBiaya.bisacicil;
        }

        public void DeleteKonfigurasiBiayaData(RefBiaya p_RefBiaya)
        {
            Context.Entry<RefBiaya>(p_RefBiaya).State = System.Data.Entity.EntityState.Deleted;
        }

        public List<RefGrupBiaya> GrupBiayaData()
        {
            return (from a in Context.RefGrupBiayas
                    select a).ToList();
        }

        public void AddGrupBiayaData(RefGrupBiaya p_RefGrupBiaya)
        {
            Context.RefGrupBiayas.Add(p_RefGrupBiaya);
        }

        public void DeleteGrupBiayaData(RefGrupBiaya p_RefGrupBiaya)
        {
            Context.Entry<RefGrupBiaya>(p_RefGrupBiaya).State = System.Data.Entity.EntityState.Deleted;
        }

        public void UpdateGrupBiayaData(RefGrupBiaya p_RefGrupBiaya)
        {
            RefGrupBiaya updateObj = Context.RefGrupBiayas.Where(x => x.Grupbiaya_id == p_RefGrupBiaya.Grupbiaya_id).FirstOrDefault();

            updateObj.nama = p_RefGrupBiaya.nama;
            updateObj.code = p_RefGrupBiaya.code;
        }

        public void UpdateMappingGrupBiayaData(List<GrupBiaya> grupBiayas, RefGrupBiaya p_refGrupBiaya)
        {
            RemoveAllExistingGrupBiaya(p_refGrupBiaya);

            if (grupBiayas.Count > 0)
            {                
                foreach (GrupBiaya item in grupBiayas)
                {
                    GrupBiaya existing = (from a in Context.GrupBiayas
                                          where a.GrupBiaya_Id == item.GrupBiaya_Id && a.Biaya_Id == item.Biaya_Id
                                          select a).FirstOrDefault();

                    if (existing != null)
                    {
                        Context.GrupBiayas.Remove(existing);
                        Context.GrupBiayas.Add(item);
                    }
                    else
                    {
                        Context.GrupBiayas.Add(item);
                    }
                }
            }
        }

        private void RemoveAllExistingGrupBiaya(RefGrupBiaya p_refGrupBiaya)
        {
            List<GrupBiaya> existing = (from a in Context.GrupBiayas
                                        where a.GrupBiaya_Id == p_refGrupBiaya.Grupbiaya_id
                                        select a).ToList();

            Context.GrupBiayas.RemoveRange(existing);
        }

        public List<GrupBiaya> GetMappingGrupBiayaData(int p_GrupBiayaId)
        {
            return (from a in Context.GrupBiayas
                    where a.GrupBiaya_Id == p_GrupBiayaId
                    select a).ToList();
        }

        public void SaveChanges()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                throw new UpdateException(e.Message, e);
            }
        }
    }
}