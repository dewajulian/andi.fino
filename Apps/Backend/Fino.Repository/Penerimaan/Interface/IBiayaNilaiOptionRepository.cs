﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Model;

namespace Fino.Repository
{
    public interface IBiayaNilaiOptionRepository : IRepository
    {
        PengelolaanNilaiBiayaModel GetNilaiBiaya(Datalib.Entity.RefBiaya pBiaya, int pOption);
        int GetNilaiBiayaInt(RefBiaya pBiaya, int pOption);
        void DeleteNilaiBiaya(BiayaNilaiOption nilaiBiaya);
        void UpdateNilaiBiaya(BiayaNilaiOption nilaiBiaya);
    }
}
