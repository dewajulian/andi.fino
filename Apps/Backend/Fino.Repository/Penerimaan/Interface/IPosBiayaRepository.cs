﻿using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public interface IPosBiayaRepository : IRepository
    {        
        List<RekapitulasiPiutangDanPenerimaanModel> GetRekapitulasiPiutangDanPenerimaan(DateTime p_FromDate, DateTime p_ToDate);
        void GeneratePosBiaya(DateTime pStart, DateTime pEnd, int pOption, RefBiaya pBiaya, RefSiswa pSiswa);
        List<PosBiaya> GetNotPaidCurrentPosBiayaSiswa(int p_SiswaId);
        List<PosBiayaModel> GetNextNotPaidAllPosBiayaSiswa(int p_SiswaId);
        void GeneratePosBiayaLayanan(DateTime pStart, DateTime pEnd, AgtLayanan pLayanan, RefBiaya pBiaya, RefSiswa pSiswa);
        List<LaporanKasDetailModel> GetLaporanKasDetail(DateTime p_ToDate, int p_KelasId, int p_SiswaId);
        List<PosBiayaPaid> UpdatePembayaranPosBiaya(List<PosBiayaModel> p_posBiayaModel, int p_SiswaId);
        CetakPembayaranModel SimpanCetakPembayaran(CetakPembayaranModel p_CetakPembayaranModel, List<PosBiayaPaidModel> p_PosBiayaPaidModel);
        PosBiaya GetPosBiaya(int p_PosBiayaId);
        List<RefKelas> GetAllKelas();
        List<SiswaKelas> GetSiswaByKelas(int p_KelasId);
        List<PenerimaanHistoryModel> GetLaporanPenerimaanDetail(DateTime p_ToDate, int p_KelasId, int p_SiswaId);
    }
}
