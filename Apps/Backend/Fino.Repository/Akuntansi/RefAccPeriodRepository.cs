﻿using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;

namespace Fino.Repository
{
    public class RefAccPeriodRepository : BaseRepository, IRefAccPeriodRepository
    {
        private RefAccPeriod GetExistingRefAccPeriod(int period_id)
        {
            return Context.RefAccPeriods.Where(x => x.period_id == period_id).FirstOrDefault();
        }

        private void _UpdateRefAccPeriod(RefAccPeriodModel entity)
        {
            var existingRefAccPeriod = GetExistingRefAccPeriod(entity.Period_id);

            if (existingRefAccPeriod == null)
            {
                Context.RefAccPeriods.Add(new RefAccPeriod
                {
                    period_name = entity.Period_name,
                    period_start = entity.Period_start,
                    period_end = entity.Period_end,
                    status = entity.Status
                });
            }
            else
            {
                existingRefAccPeriod.period_name = entity.Period_name;
                existingRefAccPeriod.period_start = entity.Period_start;
                existingRefAccPeriod.period_end = entity.Period_end;
                existingRefAccPeriod.status = entity.Status;
            }

            Context.SaveChanges();
        }

        public void UpdateRefAccPeriod(RefAccPeriodModel entity)
        {
            _UpdateRefAccPeriod(entity);
        }

        public void UpdateStatusAccPeriodAndImportBalance(RefAccPeriodModel entity)
        {
            var existingRefAccPeriod = GetExistingRefAccPeriod(entity.Period_id);

            if (existingRefAccPeriod == null)
            {
                throw new ApplicationException("Periode akuntansi tidak ditemukan");
            }
            else
            {
                // Update status
                existingRefAccPeriod.status = entity.Status;

                // Import balance
                var previousPeriod = Context.RefAccPeriods.Where(e => e.period_end < entity.Period_start && e.GeneralLedgers.Any()).OrderByDescending(o => o.period_end).FirstOrDefault();
                if (previousPeriod != null)
                {
                    if (previousPeriod.status.Equals((int)RefAccPeriodStatusEnum.Opened))
                    {
                        throw new ApplicationException("Periode sebelumnya belum di tutup.");
                    }

                    var prevNeraca = Context.RefGLs.Where(e => e.period_id.Equals(previousPeriod.period_id)).FirstOrDefault();
                    var alreadyExist = Context.RefGLs.Any(e => e.period_id.Equals(entity.Period_id));
                    if (alreadyExist)
                    {
                        throw new ApplicationException("Neraca untuk periode tersebut sudah ada. Pembukaan periode tidak valid");
                    }
                    else
                    {
                        if (prevNeraca != null)
                        {
                            var newNeraca = new RefGL
                            {
                                gl_name = string.Format("Neraca {0}", entity.Period_name),
                                period_id = entity.Period_id,
                                GLAccounts = new List<GLAccount>()
                            };

                            foreach (var gc in prevNeraca.GLAccounts)
                            {
                                newNeraca.GLAccounts.Add(new GLAccount
                                {
                                    account_id = gc.account_id,
                                    accountclassification_id = gc.accountclassification_id,
                                    amount = gc.amount,
                                    factor = gc.factor
                                });
                            }

                            Context.RefGLs.Add(newNeraca);
                        }
                    }
                }
            }

            Context.SaveChanges();
        }

        public RefAccPeriodModel GetRefAccPeriod(int period_id)
        {
            var existingRefAccPeriod = GetExistingRefAccPeriod(period_id);

            if (existingRefAccPeriod != null)
            {
                return new RefAccPeriodModel
                {
                    Period_id = existingRefAccPeriod.period_id,
                    Period_name = existingRefAccPeriod.period_name,
                    Period_start = existingRefAccPeriod.period_start,
                    Period_end = existingRefAccPeriod.period_end,
                    Status = existingRefAccPeriod.status
                };
            }

            return null;
        }

        public List<RefAccPeriodModel> GetAllRefAccPeriod()
        {
            List<RefAccPeriodModel> result = new List<RefAccPeriodModel>();

            var allRefAccPeriod = Context.RefAccPeriods.ToList();

            foreach (RefAccPeriod refAccPeriod in allRefAccPeriod)
            {
                result.Add(new RefAccPeriodModel
                    {
                        Period_id = refAccPeriod.period_id,
                        Period_name = refAccPeriod.period_name,
                        Period_start = refAccPeriod.period_start,
                        Period_end = refAccPeriod.period_end,
                        Status = refAccPeriod.status
                    });
            }

            return result;
        }
    }
}
