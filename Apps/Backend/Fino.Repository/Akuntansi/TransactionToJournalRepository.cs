﻿using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public class TransactionToJournalRepository : BaseRepository, ITransactionToJournalRepository
    {
        private int _GetActiveAccountingPeriod()
        {
            var activePeriod = GetActiveAccountingPeriod();
            return activePeriod == null ? 0 : activePeriod.Period_id;
        }

        public RefAccPeriodModel GetActiveAccountingPeriod()
        {
            RefAccPeriodModel model = null;
            var entity = Context.RefAccPeriods.Where(e => e.status.Equals((int)RefAccPeriodStatusEnum.Opened)).FirstOrDefault();

            if (entity != null)
            {
                model = new RefAccPeriodModel
                {
                    Period_id = entity.period_id,
                    Period_name = entity.period_name,
                    Period_start = entity.period_start,
                    Period_end = entity.period_end,
                    Status = entity.status
                };
            }
            else
            {
                throw new ApplicationException(AppResource.MSG_LOADING_NO_ACTIVE_ACCPERIODE);
            }

            return model;
        }

        public List<TransaksiToJurnalModel> GetTransactionToPost(DateTime p_Start, DateTime p_End)
        {
            var transactionList = new List<TransaksiToJurnalModel>();
            var startDate = p_Start.Date;
            var endDate = new DateTime(p_End.Year, p_End.Month, p_End.Day, 23, 59, 59);
            var activePeriod = _GetActiveAccountingPeriod();

            // Retrieve Receivables which includes unpaid or paid in advance
            RetrieveReceivablesIncludeAdvancePaid(transactionList, startDate, endDate, activePeriod);
            // Retrieve Canceled Receivables
            RetrieveCanceledReceivables(transactionList, startDate, endDate, activePeriod);
            // Retrieve Income
            RetrieveIncome(transactionList, startDate, endDate, activePeriod);
            // Retrieve Payable
            RetrievePayables(transactionList, startDate, endDate, activePeriod);

            return transactionList;
        }

        private void RetrievePayables(List<TransaksiToJurnalModel> p_TrxList, DateTime p_Start, DateTime p_End, int activePeriodId)
        {
            var selectedTrx = Context.Payables.Where(e => e.created >= p_Start &&
                                    e.created <= p_End &&
                                    e.isactive &&
                                    e.status_id.Equals((int)TransactionStatusEnum.OPEN));
            foreach (var trx in selectedTrx)
            {
                p_TrxList.Add(new TransaksiToJurnalModel
                {
                    AccountingPeriodId = activePeriodId,
                    ParamId = trx.refpayable_id,
                    IsSelected = true,
                    AccountClassificationId = (int)AccountClassificationEnum.BEBAN,
                    SourceId = trx.payable_id,
                    TransactionDate = trx.created,
                    TransactionName = trx.deskripsi,
                    TransactionValue = trx.nilai 
                });
            }
        }

        private void RetrieveReceivables(List<TransaksiToJurnalModel> p_TrxList, DateTime p_Start, DateTime p_End, int activePeriodId)
        {
            var selectedTrx = Context.PosBiayas.Where(e => e.JTempo >= p_Start &&
                                    e.JTempo <= p_End &&
                                    e.IsActive &&
                                    e.Status_Id.Equals((int)TransactionStatusEnum.OPEN));
            foreach (var trx in selectedTrx)
            {
                p_TrxList.Add(new TransaksiToJurnalModel
                {
                    AccountingPeriodId = activePeriodId,
                    ParamId = trx.Biaya_Id,
                    IsSelected = true,
                    AccountClassificationId = (int)AccountClassificationEnum.PIUTANG,
                    SourceId = trx.PosBiaya_Id,
                    TransactionDate = trx.JTempo,
                    TransactionName = "Piutang " + trx.Deskripsi,
                    TransactionValue = trx.Nilai - trx.NilaiPotongan // TODO: potongan should be in particular account
                });
            }
        }

        private void RetrieveReceivablesIncludeAdvancePaid(List<TransaksiToJurnalModel> p_TrxList, DateTime p_Start, DateTime p_End, int activePeriodId)
        {
            var selectedTrx = Context.PosBiayas.Where(e => ((e.IsPaid &&
                                    e.Paid.datepaid >= p_Start &&
                                    e.Paid.datepaid <= p_End) ||
                                    (!e.IsPaid && 
                                    e.JTempo >= p_Start &&
                                    e.JTempo <= p_End)) &&
                                    e.IsActive &&
                                    e.Status_Id.Equals((int)TransactionStatusEnum.OPEN));
            foreach (var trx in selectedTrx)
            {
                p_TrxList.Add(new TransaksiToJurnalModel
                {
                    AccountingPeriodId = activePeriodId,
                    ParamId = trx.Biaya_Id,
                    IsSelected = true,
                    AccountClassificationId = (int)AccountClassificationEnum.PIUTANG,
                    SourceId = trx.PosBiaya_Id,
                    TransactionDate = trx.JTempo,
                    TransactionName = "Piutang " + trx.Deskripsi,
                    TransactionValue = trx.Nilai - trx.NilaiPotongan // TODO: potongan should be in particular account
                });
            }
        }

        private void RetrieveCanceledReceivables(List<TransaksiToJurnalModel> p_TrxList, DateTime p_Start, DateTime p_End, int activePeriodId)
        {
            var selectedTrx = Context.PosBiayas.Where(e => e.JTempo >= p_Start &&
                                    e.JTempo <= p_End &&
                                    !e.IsActive &&
                                    e.Status_Id.Equals((int)TransactionStatusEnum.CANCELED));
            foreach (var trx in selectedTrx)
            {
                p_TrxList.Add(new TransaksiToJurnalModel
                {
                    AccountingPeriodId = activePeriodId,
                    ParamId = trx.Biaya_Id,
                    IsSelected = true,
                    AccountClassificationId = (int)AccountClassificationEnum.CANCELED_PIUTANG,
                    SourceId = trx.PosBiaya_Id,
                    TransactionDate = trx.JTempo,
                    TransactionName = "Ubah cicilan " + trx.Deskripsi,
                    TransactionValue = trx.Nilai - trx.NilaiPotongan // TODO: potongan should be in particular account
                });
            }
        }

        private void RetrieveIncome(List<TransaksiToJurnalModel> p_TrxList, DateTime p_Start, DateTime p_End, int activePeriodId)
        {
            var selectedTrx = Context.PosBiayaPaids.Where(e => e.datepaid >= p_Start &&
                                    e.datepaid <= p_End &&
                                    e.status.Equals((int)TransactionStatusEnum.OPEN));
            foreach (var trx in selectedTrx)
            {
                p_TrxList.Add(new TransaksiToJurnalModel
                {
                    AccountingPeriodId = activePeriodId,
                    ParamId = trx.PosOfBiaya.Biaya_Id,
                    IsSelected = true,
                    AccountClassificationId = (int)AccountClassificationEnum.PENDAPATAN,
                    SourceId = trx.PosBiaya_Id,
                    TransactionDate = trx.datepaid,
                    TransactionName = "Penerimaan " + trx.PosOfBiaya.Deskripsi,
                    TransactionValue = trx.paynilai
                });
            }
        }

        public void PostTransaction(List<JournalHeader> p_Model)
        {
            foreach (var model in p_Model)
            {
                Context.JournalHeaders.Add(model);

                // Update receivable transaction
                if (model.AccountClassificationId.Equals((int)AccountClassificationEnum.PIUTANG))
                {
                    var source = Context.PosBiayas.Find(model.sourceid);
                    source.Status_Id = (int)TransactionStatusEnum.POSTED;
                }

                // Update canceled receivable transaction
                if (model.AccountClassificationId.Equals((int)AccountClassificationEnum.CANCELED_PIUTANG))
                {
                    model.AccountClassificationId = (int)AccountClassificationEnum.PIUTANG;
                    var source = Context.PosBiayas.Find(model.sourceid);
                    source.Status_Id = (int)TransactionStatusEnum.POST_CANCELED;
                }
                // Update income transaction
                if (model.AccountClassificationId.Equals((int)AccountClassificationEnum.PENDAPATAN))
                {
                    var source = Context.PosBiayaPaids.Find(model.sourceid);
                    source.status = (int)TransactionStatusEnum.POSTED;
                }
                // Update cost transaction
                if (model.AccountClassificationId.Equals((int)AccountClassificationEnum.BEBAN))
                {
                    var source = Context.Payables.Find(model.sourceid);
                    source.status_id = (int)TransactionStatusEnum.POSTED;

                    var paidSource = Context.PayablePaids.Find(model.sourceid);
                    paidSource.status = (int)TransactionStatusEnum.POSTED;
                }
            }
            Context.SaveChanges();

            // Post to balance sheet            
            IGeneralLedgerRepository glRepo = new GeneralLedgerRepository();
            glRepo.Context = Context;
            glRepo.GenerateBalance(p_Model);
            Context.SaveChanges();
        }


        public List<AccountClassSetting> GetAllAccountClassSetting()
        {
            List<AccountClassSetting> settingList = new List<AccountClassSetting>();

            foreach (var s in Context.AccClassificationSettings)
            {
                var item = new AccountClassSetting();
                item = s;
                item.Account = s.Account;

                settingList.Add(item);
            }

            return settingList;
        }


        public List<NeracaSaldoModel> GetNeracaSaldo(int p_PeriodId)
        {
            var allSaldo = new List<NeracaSaldoModel>();
            var sqlSb = new StringBuilder("SELECT ")
                       .Append("gla.accountclassification_id as AccountClassificationId ")
                       .Append(",a.account_code as AccountCode ")
                       .Append(",a.account_name as AccountName ")
                       .Append(",p.period_name as PeriodName ")
                       .Append(",case factor ")
                       .Append("      when 1 then amount ")
                       .Append("      else 0 ")
                       .Append(" end as DebetValue ")
                       .Append(",case factor ")
                       .Append("      when -1 then amount ")
                       .Append("      else 0 ")
                       .Append(" end as CreditValue ")
                       .Append("from GLAccounts gla inner join ")
                       .Append("RefAccounts a on gla.account_id = a.account_id inner join ")
                       .Append("RefGLs g on gla.gl_id = g.gl_id inner join ")
                       .Append("RefAccPeriods p on g.period_id = p.period_id ")
                       .Append("where p.period_id = @p0 ")
                       .Append("Order by gla.accountclassification_id, a.account_code ");

            using (Context)
            {
                allSaldo = Context.Database.SqlQuery<NeracaSaldoModel>(sqlSb.ToString(), p_PeriodId).ToList();
            }
            return allSaldo;
        }

        public List<DailyJournalModel> GetDailyJournal(DateTime p_Start, DateTime p_End, int p_PeriodId)
        {
            var startDate = p_Start.Date;
            var endDate = new DateTime(p_End.Year, p_End.Month, p_End.Day, 23, 59, 59);
            var allSaldo = new List<DailyJournalModel>();
            var sqlSb = new StringBuilder("SELECT ")
                      .Append("p.period_id, ")
                      .Append("p.period_name, ")
                      .Append("h.AccountClassificationId, ")
                      .Append("h.journal_id, ")
                      .Append("h.journal_date, ")
                      .Append("h.journal_name, ")
                      .Append("d.journaldetail_id, ")
                      .Append("a.account_id, ")
                      .Append("a.account_code, ")
                      .Append("d.detail_name, ")
                      .Append("d.factor, ")
                      .Append("case d.factor ")
                      .Append("	 when 1 then d.amount ")
                      .Append("  else 0 ")
                      .Append("end as debet, ")
                      .Append("case d.factor ")
                      .Append("  when -1 then d.amount ")
                      .Append("	 else 0 ")
                      .Append("end as credit ")
                      .Append("from journalheaders h inner join ")
                      .Append("	 refaccperiods p on h.period_id = p.period_id inner join ")
                      .Append("     journaldetails d on h.journal_id = d.journal_id inner join ")
                      .Append("	 refaccounts a on d.account_id = a.account_id ")
                      .Append("where h.status = 0 ")
                      .Append("  and h.journal_date >= @p0 ")
                      .Append("  and h.journal_date <= @p1 ")
                      .Append("  and h.period_id = @p2 ")
                      .Append("order by h.period_id, h.journal_id, d.journaldetail_id ");

            using (Context)
            {
                allSaldo = Context.Database.SqlQuery<DailyJournalModel>(sqlSb.ToString(), 
                                                    startDate.ToString("yyyy-MM-dd"),
                                                    endDate.ToString("yyyy-MM-dd HH:mm:ss"),
                                                    p_PeriodId).ToList();
            }
            return allSaldo;
        }

        public List<ValueList> GetAvailablePeriod()
        {
            var allPeriod = new List<ValueList>();
            var sqlSb = new StringBuilder("SELECT DISTINCT ")
                      .Append("p.period_id, ")
                      .Append("p.period_name ")
                      .Append("from RefGLs h inner join ")
                      .Append("	 refaccperiods p on h.period_id = p.period_id ")
                      .Append("order by p.period_id ");

            using (Context)
            {
                var periods = Context.Database.SqlQuery<RefAccPeriodModel>(sqlSb.ToString());
                foreach(var p in periods)
                {
                    allPeriod.Add(new ValueList
                    {
                        Id = p.Period_id,
                        Value = p.Period_name
                    });
                }
            }
            return allPeriod;
        }
    }
}
