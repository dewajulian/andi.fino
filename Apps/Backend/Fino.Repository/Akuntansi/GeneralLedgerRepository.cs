﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Lib.Core;

namespace Fino.Repository
{
    public class GeneralLedgerRepository : BaseRepository, IGeneralLedgerRepository
    {
        public void GenerateBalance(List<JournalHeader> p_JournalHeader)
        {
            var currentPeriodId = GetActiveAccountingPeriod();

            // Init to GL
            var currentGL = Context.RefGLs.Where(e => e.period_id.Equals(currentPeriodId)).FirstOrDefault();
            if (currentGL == null)
            {
                currentGL = new RefGL
                {
                    gl_name = "GL " + Context.RefAccPeriods.Find(currentPeriodId).period_name,
                    period_id = currentPeriodId,
                    GLAccounts = new List<GLAccount>()
                };
                Context.RefGLs.Add(currentGL);
            }

            // Get unposted journal
            //var journalDetails = GetUnpostedJournal(context);
            foreach(var header in p_JournalHeader)
            {
                foreach (var detail in header.Details)
                {
                    var glAccount = currentGL.GLAccounts.Where(e => e.account_id.Equals(detail.account_id)).FirstOrDefault();
                    if (glAccount == null)
                    {
                        currentGL.GLAccounts.Add(new GLAccount
                        {
                            account_id = detail.account_id,
                            accountclassification_id = Context.AccClassificationSettings.Find(detail.account_id).acc_class_id,
                            amount = detail.amount,
                            factor = detail.factor
                        });
                    }
                    else
                    {
                        if (glAccount.factor.Equals(detail.factor))
                        {
                            glAccount.amount += detail.amount;
                        }
                        else
                        {
                            glAccount.amount -= detail.amount;
                        }
                    }
                }
            }

        }

        private int GetActiveAccountingPeriod()
        {
            var activePeriod = Context.RefAccPeriods.Where(e => e.status.Equals((int)RefAccPeriodStatusEnum.Opened)).FirstOrDefault();
            return activePeriod == null ? 0 : activePeriod.period_id;
        }

        private List<JournalDetail> GetUnpostedJournal()
        {
            return Context.JournalDetails.Where(e => e.status.Equals((int)TransactionStatusEnum.OPEN)).ToList();
        }
    }
}
