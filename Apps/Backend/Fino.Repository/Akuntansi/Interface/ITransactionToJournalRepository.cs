﻿using System;
using System.Collections.Generic;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;

namespace Fino.Repository
{
    public interface ITransactionToJournalRepository : IRepository
    {
        List<TransaksiToJurnalModel> GetTransactionToPost(DateTime p_Start, DateTime p_End);
        void PostTransaction(List<JournalHeader> p_Model);
        List<AccountClassSetting> GetAllAccountClassSetting();
        List<NeracaSaldoModel> GetNeracaSaldo(int p_PeriodId); 
        RefAccPeriodModel GetActiveAccountingPeriod();
        List<DailyJournalModel> GetDailyJournal(DateTime p_Start, DateTime p_End, int p_PeriodId);
        List<ValueList> GetAvailablePeriod();
    }
}
