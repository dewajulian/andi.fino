﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public interface IGeneralLedgerRepository : IRepository
    {
        void GenerateBalance(List<JournalHeader> p_JournalHeader);
    }
}
