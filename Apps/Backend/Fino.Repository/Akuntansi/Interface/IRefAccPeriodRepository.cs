﻿using Fino.Model;
using System.Collections.Generic;

namespace Fino.Repository
{
    public interface IRefAccPeriodRepository : IRepository
    {
        void UpdateRefAccPeriod(RefAccPeriodModel entity);
        RefAccPeriodModel GetRefAccPeriod(int period_id);
        List<RefAccPeriodModel> GetAllRefAccPeriod();
        void UpdateStatusAccPeriodAndImportBalance(RefAccPeriodModel entity);
    }
}
