﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;

namespace Fino.Repository
{
    public class DaftarUlangSiswaRepository : BaseRepository, IDaftarUlangSiswaRepository
    {
        public DaftarUlangSiswaModel TambahDaftarUlang(Model.DaftarUlangSiswaModel model)
        {
            var returnModel = model;
            var siswaKelasList = new List<SiswaKelas>();
            IPosBiayaRepository posBiayaRepo = new PosBiayaRepository();
            IGrupBiayaRepository grupBiayaRepo = new GrupBiayaRepository();
            var siswaKelasEntity = new SiswaKelas
            {
                siswa_id = model.Siswa_Id,
                kelas_id = model.Kelas_Id,
                is_siswa_baru = false,
                mulai_tanggal = model.Mulai_Tanggal.Date,
                tahun_ajaran_id = model.TahunAjaran_Id,
                tgl_daftar = DateTime.Now,
                option_tingkat = model.OptionTingkat_Id
            };
            siswaKelasList.Add(siswaKelasEntity);

            var siswaEntity = new RefSiswa
            {
                siswa_id = model.Siswa_Id,
                nama = model.Nama,
                siswa_code = model.Code,
                jkelamin = model._JKelamin,
                SiswaKelas = siswaKelasList,
                PosBiayas = new List<PosBiaya>(),
                ispindahan = model.IsPindahan
            };

            // Get Tahun Ajaran range
            var ta = Context.RefTahunAjarans.Find(model.TahunAjaran_Id);
            var kelas = Context.RefKelases.Find(model.Kelas_Id);

            // Generate pos biaya here
            DateTime endOfTA = model.Mulai_Tanggal;

            
            grupBiayaRepo.Context = Context;
            List<RefBiaya> biayaList = null;
            if (ta.semcawu.Equals(1))
            {
                biayaList = grupBiayaRepo.GetBiayaOfGrupCode(AppResource.SYS_GRUPBIAYA_DAFTAR_ULANG);
                var taNext = Context.RefTahunAjarans.Where(e => e.mulai_tahun.Equals(ta.mulai_tahun + 1) && e.hingga_tahun.Equals(ta.hingga_tahun + 1)).FirstOrDefault();
                if (taNext != null)
                {
                    endOfTA = new DateTime(taNext.hingga_tahun, taNext.hingga_bulan, model.Mulai_Tanggal.Day);
                }
                else
                {
                    throw new ApplicationException(AppResource.MSG_SAVING_SEMESTER_2_NOTFOUND);
                }
            }
            else
            {
                throw new ApplicationException(AppResource.MSG_SAVING_DAFTARULANG_INVALID_SEMESTER);
            }

            foreach (var b in biayaList)
            {
                posBiayaRepo.Context = Context;
                int tingkat = 0;

                if (siswaEntity.ispindahan && siswaKelasEntity.option_tingkat > 0)
                {
                    tingkat = siswaKelasEntity.option_tingkat;
                }
                else
                {
                    tingkat = kelas.tingkat;
                }
                posBiayaRepo.GeneratePosBiaya(model.Mulai_Tanggal.Date, endOfTA, kelas.tingkat, b, siswaEntity);
            }

            Context.SiswaKelases.Add(siswaKelasEntity);
            foreach (var p in siswaEntity.PosBiayas)
            {
                Context.PosBiayas.Add(p);
            }

            Context.SaveChanges();

            return returnModel;
        }


        public DaftarUlangSiswaModel SelectSiswaByCode(string pCode)
        {
            DaftarUlangSiswaModel returnModel = null;

            RefSiswa siswaEntity = Context.RefSiswas.Where(e => e.siswa_code.Equals(pCode)).FirstOrDefault();
            if (siswaEntity != null)
            {
                returnModel = new DaftarUlangSiswaModel
                {
                    Code = siswaEntity.siswa_code,
                    Siswa_Id = siswaEntity.siswa_id,
                    IsPindahan = siswaEntity.ispindahan,
                    JKelamin = siswaEntity.jkelamin,
                    Nama = siswaEntity.nama
                };
            }


            return returnModel;
        }
    }
}
