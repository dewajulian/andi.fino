﻿using Fino.Datalib.Entity;
using System;
using Fino.Model;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public interface IRefSiswaRepository : IRepository
    {
        List<SiswaModel> GetSiswa(string p_stringSiswa);
        RefSiswa GetSiswaByCode(string pCode);
        List<SiswaExplorerModel> GetAllRefSiswaForSiswaExplorer();
    }
}
