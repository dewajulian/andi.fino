﻿using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public class RefSiswaRepository : BaseRepository, IRefSiswaRepository
    {
        public List<SiswaModel> GetSiswa(string p_stringSiswa)
        {
            var sqlCmd = new StringBuilder("select a.siswa_id as Id, a.siswa_code as Code, a.nama as Nama, a.jkelamin as JenisKelamin, ")
                .Append("b.kelas_id as IdKelas, e.nama as Kelas, b.tahun_ajaran_id as IdTahunAjaran, ")
                .Append("d.nama as TahunAjaran, ")
                .Append("rk.rekening_code as NoRekening ")
                .Append("from refsiswas a ")
                .Append("left join siswakelas b on b.siswa_id = a.siswa_id ")
                .Append("inner join ")
                .Append("( select siswa_id, ")
                .Append("max(mulai_tanggal) mdtm ")
                .Append("from SiswaKelas ")
                .Append("group by siswa_id) c on a.siswa_id = c.siswa_id and b.mulai_tanggal = c.mdtm ")
                .Append("left join reftahunajarans d on d.tahunajaran_id = b.tahun_ajaran_id ")
                .Append("left join refkelas e on e.kelas_id = b.kelas_id ")
                .Append("left join siswarekenings sr on a.siswa_id = sr.siswa_id ")
                .Append("left join refrekenings rk on sr.rekening_id = rk.rekening_id ")
                .Append("where a.siswa_code like @p0 or a.nama like @p1 ");

            //List<RefSiswa> returnData = null;

            //returnData = (from a in context.RefSiswas
            //              where a.siswa_code.Contains(p_stringSiswa) || a.nama.Contains(p_stringSiswa)
            //              select a).ToList();

            List<SiswaModel> resultSql = Context.Database
                    .SqlQuery<SiswaModel>(
                        sqlCmd.ToString(),
                        string.Format("%{0}%", p_stringSiswa),
                        string.Format("%{0}%", p_stringSiswa)).ToList();

            return resultSql;            
        }

        public RefSiswa GetSiswaByCode(string pCode)
        {
            RefSiswa returnModel = null;

            RefSiswa siswaEntity = Context.RefSiswas.Where(e => e.siswa_code.Equals(pCode)).FirstOrDefault();
            if (siswaEntity != null)
            {
                returnModel = siswaEntity;
            }

            return returnModel;
        }

        public List<SiswaExplorerModel> GetAllRefSiswaForSiswaExplorer()
        {
            List<SiswaExplorerModel> result = null;

            var refSiswa = Context.RefSiswas.ToList();

            if (refSiswa.Count > 0)
            {
                result = new List<SiswaExplorerModel>();
                foreach (var siswa in refSiswa)
                {
                    var siswaKelas = siswa.SiswaKelas.FirstOrDefault();
                    var tunggakan = siswa.PosBiayas.Where(x => x.IsActive && !x.IsPaid).OrderByDescending(x => (DateTime.Today - x.JTempo).TotalDays).Select(x => (DateTime.Today - x.JTempo).TotalDays).FirstOrDefault();

                    if (siswaKelas != null)
                    {
                        string rekeningTabungan = "";

                        SiswaRekening rekSiswa = Context.SiswaRekenings.Where(x => x.siswa_id == siswa.siswa_id)
                            .FirstOrDefault();

                        rekeningTabungan = (rekSiswa != null) ? rekSiswa.Rekening.rekening_code : string.Empty;

                        var model = new SiswaExplorerModel
                        {
                            SiswaCode = siswa.siswa_code,
                            NamaSiswa = siswa.nama,
                            TglMasuk = siswaKelas.mulai_tanggal,
                            Kelas = siswaKelas.Kelas.nama,
                            IsPindahan = siswa.ispindahan,
                            RekeningTabungan = rekeningTabungan,
                            Tunggakan = tunggakan
                        };

                        var listLayanan = siswa.AnggotaLayanans.Where(x => x.siswa_id == siswa.siswa_id && x.hingga_tanggal >= DateTime.Today).ToList();
                        List<string> layananSiswa = new List<string>();

                        layananSiswa = new List<string>();
                        foreach (var layanan in listLayanan)
                        {
                            layananSiswa.Add(layanan.Layanan.code);
                        }
                        model.KodeLayanan = layananSiswa;

                        result.Add(model);
                    }
                }
            }

            return result;
        }
    }
}
