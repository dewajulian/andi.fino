﻿using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public interface IUserManagementRepository : IRepository
    {
        void UpdateUser(UserManagementModel entity);
        void DeleteUser(UserManagementModel entity);
        UserManagementModel GetUserForManagement(int userid);
        List<UserManagementModel> GetAllUserForManagement();
    }
}
