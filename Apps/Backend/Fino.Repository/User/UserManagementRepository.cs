﻿using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Fino.Repository
{
    public class UserManagementRepository : BaseRepository, IUserManagementRepository
    {
        private SysUser GetExistingSysUser(int user_id)
        {
            return Context.Users.Where(x => x.user_id == user_id).FirstOrDefault();
        }

        private List<SysUserContact> GetUserContacts(UserManagementModel entity)
        {
            List<SysUserContact> contacts = new List<SysUserContact>();
            if (!string.IsNullOrEmpty(entity.Phone))
            {
                contacts.Add(new SysUserContact
                {
                    contact = entity.Phone,
                    type = (int)ContactTypeEnum.PHONE
                });
            }

            if (!string.IsNullOrEmpty(entity.Email))
            {
                contacts.Add(new SysUserContact
                {
                    contact = entity.Email,
                    type = (int)ContactTypeEnum.EMAIL
                });
            }

            return contacts;
        }

        public void UpdateUser(UserManagementModel entity)
        {
            var user = GetExistingSysUser(entity.Userid);

            if (user == null)
            {
                Context.Users.Add(new SysUser
                    {
                        fullname = entity.Fullname,
                        name = entity.Username,
                        password = entity.Password,
                        Contacts = GetUserContacts(entity)
                    });
            }
            else 
            {
                user.name = entity.Username;
                user.fullname = entity.Fullname;
                user.Contacts = GetUserContacts(entity);
            }

            Context.SaveChanges();
        }

        public void DeleteUser(UserManagementModel entity)
        {
            var user = GetExistingSysUser(entity.Userid);

            if (user != null)
            {
                Context.Users.Remove(user);
                Context.SaveChanges();
            }
        }

        public UserManagementModel GetUserForManagement(int userid)
        {
            UserManagementModel result = null;

            var user = GetExistingSysUser(userid);

            if (user != null)
            {
                var phone = user.Contacts.Where(x => x.type == (int)ContactTypeEnum.PHONE).FirstOrDefault();
                var email = user.Contacts.Where(x => x.type == (int)ContactTypeEnum.EMAIL).FirstOrDefault();

                result = new UserManagementModel
                {
                    Userid = user.user_id,
                    Fullname = user.fullname,
                    Username = user.name,
                    Password = user.password,
                    Phone = phone == null ? "" : phone.contact,
                    Email = email == null ? "" : email.contact
                };
            }

            return result;
        }

        public List<UserManagementModel> GetAllUserForManagement()
        {
            List<UserManagementModel> result = new List<UserManagementModel>();

            var userList = Context.Users.Where(x => x.name != Constants.SystemUsername).ToList();
            userList.Sort((user1, user2) => user1.fullname.CompareTo(user2.fullname));

            foreach (var user in userList)
            {
                var phone = user.Contacts.Where(x => x.type == (int)ContactTypeEnum.PHONE).FirstOrDefault();
                var email = user.Contacts.Where(x => x.type == (int)ContactTypeEnum.EMAIL).FirstOrDefault();

                result.Add(new UserManagementModel
                {
                    Userid = user.user_id,
                    Fullname = user.fullname,
                    Username = user.name,
                    Password = user.password,
                    Phone = phone == null ? "" : phone.contact,
                    Email = email == null ? "" : email.contact
                });
            }

            return result;
        }
    }
}
