﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public class RefBiayaRepository : IRefBiayaRepository
    {
        public FinoDBContext context { get; set; }

        private void CheckContext()
        {
            if (context == null || context.IsDisposed)
            {
                context = new FinoDBContext();
            }
        }

        public RefBiaya GetRefBiaya(int p_BiayaId)
        {
            CheckContext();
            using (context)
            {
                return context.RefBiayas.Find(p_BiayaId);
            }
        }
    }
}
