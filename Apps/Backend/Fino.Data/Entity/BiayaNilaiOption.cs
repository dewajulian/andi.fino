﻿
namespace Fino.Datalib.Entity
{
    public class BiayaNilaiOption
    {
        public int biayanilaioption_id { get; set; }
        public int biaya_id { get; set; }
        public int option { get; set; }
        public int nilai { get; set; }

        public virtual RefBiaya Biaya { get; set; }
    }


}
