﻿
namespace Fino.Datalib.Entity
{
    public class GroupAccount
    {
        public int groupaccount_id { get; set; }
        public int accgroup_id { get; set; }
        public int account_id { get; set; }

        public virtual RefAccGroup AccountGroup { get; set; }
        public virtual RefAccount Account { get; set; }
    }
}
