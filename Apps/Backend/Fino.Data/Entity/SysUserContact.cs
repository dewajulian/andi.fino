﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class SysUserContact
    {
        public int user_id {get; set;}
        public string contact  {get; set;}
        public int type { get; set; }

        public virtual SysUser User {get;set;}
    }
}
