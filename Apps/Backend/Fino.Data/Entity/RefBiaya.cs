﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class RefBiaya
    {
        public int biaya_id {get;set;}
        public string nama {get;set;}
        public string code {get;set;}
        public int repetisi {get;set;}
        public int jt_tanggal  {get;set;}
        public int jt_bulan {get;set;}
        public bool aktif {get;set;}
        public DateTime mulai_tanggal { get; set; }
        public bool noposbiaya { get; set; }
        public bool bisacicil { get; set; }

        public virtual ICollection<Potongan> Potongans { get; set; }
        public virtual BiayaLayanan BiayaOfLayanan { get; set; }
        public virtual ICollection<GrupBiaya> GrupBiayas { get; set; }
        public virtual ICollection<PosBiaya> PosBiayas { get; set; }
        public virtual ICollection<BiayaNilaiOption> BiayaNilaiOptions { get; set; }
    }
}
