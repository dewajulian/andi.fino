﻿using System.Collections.Generic;

namespace Fino.Datalib.Entity
{
    public class RefGL
    {
        public int gl_id { get; set; }
        public string gl_name { get; set; }
        public int period_id { get; set; }

        public virtual ICollection<GLAccount> GLAccounts { get; set; }
        public virtual RefAccPeriod Period { get; set; }
    }
}
