﻿
namespace Fino.Datalib.Entity
{
    public class JournalPosBiaya
    {
        public int PosBiaya_Id { get; set; }
        public int journal_id { get; set; }

        public virtual PosBiaya PosOfBiaya { get; set; }
        public virtual JournalHeader Journal { get; set; }
    }
}
