﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class PosBiaya
    {
        public int PosBiaya_Id { get; set; }
        public int Siswa_Id { get; set; }
        public int Biaya_Id { get; set; }
        public string Deskripsi { get; set; }
        public DateTime JTempo { get; set; }
        public double Nilai { get; set; }
        public double Potongan { get; set; }
        public bool IsPersen { get; set; }
        public double NilaiPotongan { get; set; }
        public bool IsPaid { get; set; }
        public bool IsActive { get; set; }
        public DateTime? DatePaid { get; set; }
        public int Status_Id { get; set; }


        public virtual RefSiswa Siswa { get; set; }
        public virtual RefBiaya Biaya { get; set; }
		public virtual PosBiayaPaid Paid { get; set; }
		public virtual ICollection<JournalPosBiaya> JournalPosBiayas { get; set; }
        public virtual KonversiCicilan Konversi { get; set; }
        public virtual KonversiCicilanPosBiaya KonversiCicilanBiaya { get; set; }
    }
}
