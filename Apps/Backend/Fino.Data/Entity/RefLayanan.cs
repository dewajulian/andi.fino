﻿using System.Collections.Generic;

namespace Fino.Datalib.Entity
{
    public class RefLayanan
    {
        public int layanan_id {get;set;}
        public string nama {get;set;}
        public string code { get; set; }

        public virtual ICollection<AgtLayanan> AnggotaLayanans { get; set; }
        public virtual BiayaLayanan BiayaOfLayanan { get; set; }
    }
}
