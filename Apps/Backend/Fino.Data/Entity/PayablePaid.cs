﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class PayablePaid
    {
        public int payable_id { get; set; }
        public DateTime datepaid { get; set; }
        public double paynilai { get; set; }
        public string referenceno { get; set; }
        public int status { get; set; }

        public virtual Payable payable { get; set; }
    }
}
