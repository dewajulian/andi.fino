﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class Payable
    {
        public int payable_id { get; set; }
        public int refpayable_id { get; set; }
        public int refvendor_id { get; set; }
        public string deskripsi { get; set; }
        public DateTime jtempo { get; set; }
        public double nilai { get; set; }
        public bool ispaid { get; set; }
        public bool isactive { get; set; }
        public DateTime? datepaid { get; set; }
        public DateTime created { get; set; }
        public int status_id { get; set; }


        public virtual RefPayable refpayable { get; set; }
        public virtual PayablePaid payablepaid { get; set; }
        public virtual RefVendor vendor { get; set; }
    }
}
