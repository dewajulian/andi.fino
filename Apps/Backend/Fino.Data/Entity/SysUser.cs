﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class SysUser
    {
        public int user_id {get;set;}
        public string name {get;set;}
        public string fullname { get; set; }
        public string password { get; set; }
        public virtual ICollection<SysUserContact> Contacts { get; set; }
        public virtual ICollection<NotaPembayaran> NotaPembayarans { get; set; }
    }
}
