﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class RefTahunAjaran
    {
        public int tahunajaran_id {get;set;}
        public string nama { get; set; }
        public int mulai_bulan {get;set;}
        public int mulai_tahun {get;set;}
        public int hingga_bulan {get;set;}
        public int hingga_tahun {get;set;}
        public int semcawu { get; set; }
        public int status { get; set; }

        public virtual ICollection<SiswaKelas> SiswaKelas { get; set; }
    }
}
