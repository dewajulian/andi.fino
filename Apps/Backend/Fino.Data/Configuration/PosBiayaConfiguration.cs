﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class PosBiayaConfiguration
    {
        internal static void ConfigurePosBiaya(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PosBiaya>().HasKey(e => e.PosBiaya_Id);
            modelBuilder.Entity<PosBiaya>().Property(e => e.PosBiaya_Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<PosBiaya>().Property(e => e.Deskripsi).HasMaxLength(70);
            
            modelBuilder.Entity<PosBiaya>()
                .HasRequired(e => e.Biaya)
                .WithMany(e => e.PosBiayas)
                .HasForeignKey(e => e.Biaya_Id);

            modelBuilder.Entity<PosBiaya>()
                .HasRequired(e => e.Siswa)
                .WithMany(e => e.PosBiayas)
                .HasForeignKey(e => e.Siswa_Id);


        }
    }
}
