﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class RefBiayaConfiguration
    {
        internal static void ConfigureBiaya(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefBiaya>().HasKey(e => e.biaya_id);
            modelBuilder.Entity<RefBiaya>().Property(e => e.biaya_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<RefBiaya>().Property(e => e.code).HasMaxLength(10);
            modelBuilder.Entity<RefBiaya>().Property(e => e.nama).HasMaxLength(25);
        }
    }
}
