﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class PayableConfiguration
    {
        internal static void ConfigurePayable(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Payable>().HasKey(e => e.payable_id);
            modelBuilder.Entity<Payable>().Property(e => e.payable_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Payable>().Property(e => e.deskripsi).HasMaxLength(500);


            modelBuilder.Entity<Payable>()
                .HasRequired(e => e.refpayable)
                .WithMany(e => e.Payables)
                .HasForeignKey(e => e.refpayable_id);

            modelBuilder.Entity<Payable>()
                .HasRequired(e => e.vendor)
                .WithMany(e => e.payables)
                .HasForeignKey(e => e.refvendor_id);


        }
    }
}
