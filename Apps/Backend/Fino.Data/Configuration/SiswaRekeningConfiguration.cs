﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class SiswaRekeningConfiguration
    {
        internal static void ConfigureSiswaRekening(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SiswaRekening>().HasKey(e => e.siswa_id);
            modelBuilder.Entity<SiswaRekening>()
                .HasRequired(e => e.Siswa)
                .WithOptional(e => e.RekeningTabungan);

            modelBuilder.Entity<SiswaRekening>().HasKey(e => e.rekening_id);
            modelBuilder.Entity<SiswaRekening>()
                .HasRequired(e => e.Rekening)
                .WithOptional(e => e.RekeningOfSiswa);
        }
    }
}
