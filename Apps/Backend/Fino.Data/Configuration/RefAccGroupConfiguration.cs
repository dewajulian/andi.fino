﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class RefAccGroupConfiguration
    {
        internal static void ConfigureRefAccGroup(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefAccGroup>().HasKey(e => e.accgroup_id);
            modelBuilder.Entity<RefAccGroup>().Property(e => e.accgroup_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<RefAccGroup>().Property(e => e.accgroup_name).HasMaxLength(20);
        }
    }
}
