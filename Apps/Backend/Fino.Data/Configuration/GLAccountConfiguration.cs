﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class GLAccountConfiguration
    {
        internal static void ConfigureGLAccount(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GLAccount>().HasKey(e => e.glaccount_id);
            modelBuilder.Entity<GLAccount>().Property(e => e.glaccount_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<GLAccount>()
                    .HasRequired(e => e.GLHeader)
                    .WithMany(e => e.GLAccounts)
                    .HasForeignKey(e => e.gl_id);

            modelBuilder.Entity<GLAccount>()
                    .HasRequired(e => e.Account)
                    .WithMany(e => e.GLAccounts)
                    .HasForeignKey(e => e.account_id);

        }
    }
}
