﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class AgtLayananConfiguration
    {
        internal static void ConfigureAgtLayanan(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AgtLayanan>().HasKey(e => e.agtlayanan_id);
            modelBuilder.Entity<AgtLayanan>().Property(e => e.agtlayanan_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<AgtLayanan>()
                .HasRequired(e => e.Layanan)
                .WithMany(e => e.AnggotaLayanans)
                .HasForeignKey(e => e.layanan_id);

            modelBuilder.Entity<AgtLayanan>()
                .HasRequired(e => e.Siswa)
                .WithMany(e => e.AnggotaLayanans)
                .HasForeignKey(e => e.siswa_id);

            
        }
    }
}
