﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class AccountClassSettingConfiguration
    {
        internal static void ConfigureAccountClassSetting(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountClassSetting>().HasKey(e => e.account_id);
            modelBuilder.Entity<AccountClassSetting>()
                    .HasRequired(e => e.Account)
                    .WithOptional(e => e.AccountBiayaSetting);
        }
    }
}
