﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class SysUserContactConfiguration
    {
        internal static void ConfigureUserContact(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SysUserContact>().HasKey(e => new { e.user_id, e.contact });
            modelBuilder.Entity<SysUserContact>().Property(e => e.contact).HasMaxLength(100);
            modelBuilder.Entity<SysUserContact>()
                .HasRequired(e => e.User)
                .WithMany(e => e.Contacts)
                .HasForeignKey(e => e.user_id);
        }
    }
}
