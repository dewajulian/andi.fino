﻿using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class KonversiCicilanConfiguration
    {
        internal static void ConfigureKonversiCicilan(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<KonversiCicilan>().HasKey(e => e.PosBiaya_Id);
            modelBuilder.Entity<KonversiCicilan>()
                .HasRequired(e => e.ConvertedPosBiaya)
                .WithOptional(e => e.Konversi);
        }
    }
}
