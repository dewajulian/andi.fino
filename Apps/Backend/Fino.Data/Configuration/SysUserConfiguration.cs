﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class SysUserConfiguration
    {
        internal static void ConfigureSysUser(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SysUser>().HasKey(e => e.user_id);
            modelBuilder.Entity<SysUser>().Property(e => e.user_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<SysUser>().Property(e => e.name).HasMaxLength(15);            
            modelBuilder.Entity<SysUser>().Property(e => e.fullname).HasMaxLength(50);
            modelBuilder.Entity<SysUser>().Property(e => e.password).HasMaxLength(100); 
        }
    }
}
