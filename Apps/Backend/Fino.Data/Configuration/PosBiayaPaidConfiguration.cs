﻿using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class PosBiayaPaidConfiguration
    {
        internal static void ConfigurePosBiayaPaid(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PosBiayaPaid>().HasKey(e => e.PosBiaya_Id);
            modelBuilder.Entity<PosBiayaPaid>()
                .HasRequired(e => e.PosOfBiaya)
                .WithOptional(e => e.Paid);

            modelBuilder.Entity<PosBiayaPaid>()
                .HasOptional(e => e.NotaPembayaran)
                .WithMany(e => e.PosBiayaPaids)
                .HasForeignKey(e => e.nota_pembayaran_id);
        }
    }
}
