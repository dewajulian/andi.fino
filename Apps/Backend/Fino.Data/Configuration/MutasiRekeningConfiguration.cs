﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class MutasiRekeningConfiguration
    {
        internal static void ConfigureMutasiRekening(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MutasiRekening>().HasKey(e => e.mutasi_id);
            modelBuilder.Entity<MutasiRekening>().Property(e => e.mutasi_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<MutasiRekening>()
                .HasRequired(e => e.Rekening)
                .WithMany(e => e.Mutasi);
        }
    }
}
