﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class RefSiswaConfiguration
    {
        internal static void ConfigureRefSiswa(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefSiswa>().HasKey(e => e.siswa_id);
            modelBuilder.Entity<RefSiswa>().Property(e => e.siswa_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<RefSiswa>().Property(e => e.nama).HasMaxLength(25);
            modelBuilder.Entity<RefSiswa>().Property(e => e.siswa_code).HasMaxLength(10);
        }
    }
}
