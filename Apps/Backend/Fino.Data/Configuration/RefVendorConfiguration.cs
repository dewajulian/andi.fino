﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class RefVendorConfiguration
    {
        internal static void ConfigureRefVendor(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RefVendor>().HasKey(e => e.refvendor_id);
            modelBuilder.Entity<RefVendor>().Property(e => e.refvendor_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<RefVendor>().Property(e => e.vendorname).HasMaxLength(50);
            modelBuilder.Entity<RefVendor>().Property(e => e.vendor_addr).HasMaxLength(200);
            modelBuilder.Entity<RefVendor>().Property(e => e.vendor_email).HasMaxLength(100);
            modelBuilder.Entity<RefVendor>().Property(e => e.vendor_phone).HasMaxLength(20);
        }
    }
}
