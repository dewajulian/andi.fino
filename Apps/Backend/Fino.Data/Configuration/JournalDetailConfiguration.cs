﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class JournalDetailConfiguration
    {
        internal static void ConfigureJournalDetail(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JournalDetail>().HasKey(e => e.journaldetail_id);
            modelBuilder.Entity<JournalDetail>().Property(e => e.journaldetail_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<JournalDetail>().Property(e => e.detail_name).HasMaxLength(200);

            modelBuilder.Entity<JournalDetail>()
                    .HasRequired(e => e.Header)
                    .WithMany(e => e.Details)
                    .HasForeignKey(e => e.journal_id);

            modelBuilder.Entity<JournalDetail>()
                    .HasRequired(e => e.Account)
                    .WithMany(e => e.JournalDetails)
                    .HasForeignKey(e => e.account_id);
        }
    }
}
