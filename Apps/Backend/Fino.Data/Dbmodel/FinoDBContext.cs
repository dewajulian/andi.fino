﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Fino.Datalib.Configuration;
using Fino.Datalib.Entity;
using Fino.Lib.Core;

namespace Fino.Datalib.Dbmodel
{
    public class FinoDBContext : DbContext
    {
        public bool IsDisposed { get; private set; }

        public DbSet<SysUser> Users { get; set; }
        public DbSet<SysUserContact> UserContacts { get; set; }
        public DbSet<RefBiaya> RefBiayas { get; set; }
        public DbSet<RefKelas> RefKelases { get; set; }
        public DbSet<RefLayanan> RefLayanans { get; set; }
        public DbSet<RefSiswa> RefSiswas { get; set; }
        public DbSet<RefTahunAjaran> RefTahunAjarans { get; set; }
        public DbSet<SiswaAlamat> SiswaAlamats { get; set; }
        public DbSet<SiswaKelas> SiswaKelases { get; set; }
        public DbSet<AgtLayanan> AgtLayanans { get; set; }
        public DbSet<BiayaLayanan> BiayaLayanans { get; set; }
        public DbSet<Potongan> Potongans { get; set; }
        public DbSet<RefGrupBiaya> RefGrupBiayas { get; set; }
        public DbSet<GrupBiaya> GrupBiayas { get; set; }
        public DbSet<PosBiaya> PosBiayas { get; set; }
        public DbSet<BiayaNilaiOption> BiayaNilaiOptions { get; set; }
        public DbSet<RefRekening> Rekenings { get; set; }
        public DbSet<SiswaRekening> SiswaRekenings { get; set; }
        public DbSet<MutasiRekening> MutasiRekenings { get; set; }
        public DbSet<PosBiayaPaid> PosBiayaPaids { get; set; }
        public DbSet<RefAccount> RefAccounts { get; set; }
        public DbSet<RefAccPeriod> RefAccPeriods { get; set; }
        public DbSet<RefAccGroup> RefAccGroups { get; set; }
        public DbSet<GroupAccount> GroupAccounts { get; set; }
        public DbSet<JournalPosBiaya> JournalPosBiayas { get; set; }
        public DbSet<JournalHeader> JournalHeaders { get; set; }
        public DbSet<JournalDetail> JournalDetails { get; set; }
        public DbSet<JournalGLAccount> JournalGLAccounts { get; set; }
        public DbSet<RefGL> RefGLs { get; set; }
        public DbSet<GLAccount> GLAccounts { get; set; }
        public DbSet<AccountClassSetting> AccClassificationSettings { get; set; }
        public DbSet<NotaPembayaran> NotaPembayarans { get; set; }
        public DbSet<KonversiCicilan> KonversiCicilans { get; set; }
        public DbSet<KonversiCicilanPosBiaya> KonversiCicilanPosBiayas { get; set; }
        public DbSet<RefPayable> RefPayables { get; set; }
        public DbSet<RefVendor> RefVendors { get; set; }
        public DbSet<Payable> Payables { get; set; }
        public DbSet<PayablePaid> PayablePaids { get; set; }

        static FinoDBContext()
        {
            Database.SetInitializer<FinoDBContext>(new FinoDBInitializer());
            using (FinoDBContext db = new FinoDBContext())
            {
                db.Database.Initialize(false);
            }
        }

        public FinoDBContext()
            : base(AppResource.CONN_STRING_NAME)
        { IsDisposed = false; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
            ConfigureModel(modelBuilder);
        }

        private void ConfigureModel(DbModelBuilder modelBuilder)
        {
            SysUserConfiguration.ConfigureSysUser(modelBuilder);
            SysUserContactConfiguration.ConfigureUserContact(modelBuilder);
            RefBiayaConfiguration.ConfigureBiaya(modelBuilder);
            RefKelasConfiguration.ConfigureRefKelas(modelBuilder);
            RefLayananConfiguration.ConfigureRefLayanan(modelBuilder);
            RefSiswaConfiguration.ConfigureRefSiswa(modelBuilder);
            RefTahunAjaranConfiguration.ConfigureRefTahunAjaran(modelBuilder);
            RefGrupBiayaConfiguration.ConfigureRefGrupBiaya(modelBuilder);

            SiswaAlamatConfiguration.ConfigureSiswaAlamat(modelBuilder);
            SiswaKelasConfiguration.ConfigureSiswaKelas(modelBuilder);
            AgtLayananConfiguration.ConfigureAgtLayanan(modelBuilder);
            BiayaLayananConfiguration.ConfigureBiayaLayanan(modelBuilder);
            PotonganConfiguration.ConfigurePotongan(modelBuilder);
            GrupBiayaConfiguration.ConfigureRefGrupBiaya(modelBuilder);
            PosBiayaConfiguration.ConfigurePosBiaya(modelBuilder);
            BiayaNilaiOptionConfiguration.ConfigureBiayaNilaiOption(modelBuilder);

            RefRekeningConfiguration.ConfigureRefRekening(modelBuilder);
            SiswaRekeningConfiguration.ConfigureSiswaRekening(modelBuilder);
            MutasiRekeningConfiguration.ConfigureMutasiRekening(modelBuilder);

            PosBiayaPaidConfiguration.ConfigurePosBiayaPaid(modelBuilder);
            RefAccountConfiguration.ConfigureRefAccount(modelBuilder);
            RefAccPeriodConfiguration.ConfigureRefAccPeriod(modelBuilder);
            RefAccGroupConfiguration.ConfigureRefAccGroup(modelBuilder);
            GroupAccountConfiguration.ConfigureGroupAccount(modelBuilder);
            JournalPosBiayaConfiguration.ConfigureJournalPosBiaya(modelBuilder);
            JournalHeaderConfiguration.ConfigureJournalHeader(modelBuilder);
            JournalDetailConfiguration.ConfigureJournalDetail(modelBuilder);
            JournalGLAccountConfiguration.ConfigureJournalGLAccount(modelBuilder);
            RefGLConfiguration.ConfigureRefGL(modelBuilder);
            GLAccountConfiguration.ConfigureGLAccount(modelBuilder);
            AccountClassSettingConfiguration.ConfigureAccountClassSetting(modelBuilder);
            NotaPembayaranConfiguration.ConfiguraNotaPembayaran(modelBuilder);
            KonversiCicilanConfiguration.ConfigureKonversiCicilan(modelBuilder);
            KonversiCicilanPosBiayaConfiguration.ConfigureKonversiCicilanPosBiaya(modelBuilder);

            RefPayableConfiguration.ConfigureRefPayable(modelBuilder);
            RefVendorConfiguration.ConfigureRefVendor(modelBuilder);
            PayableConfiguration.ConfigurePayable(modelBuilder);
            PayablePaidConfiguration.ConfigurePayablePaid(modelBuilder);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            IsDisposed = true;
        }
    }
}
