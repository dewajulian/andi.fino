﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;
using Fino.Lib.Core;

namespace Fino.Datalib.Dbmodel
{
    public class FinoDBInitializer : CreateDatabaseIfNotExists<FinoDBContext>
    {
        protected override void Seed(FinoDBContext context)
        {

            SeedUser(context);

            var bsb = SeedGrupBiaya(context, "Biaya Siswa Baru", "BSB");
            var bdu = SeedGrupBiaya(context, "Biaya Daftar Ulang", "BDU");

            var spp = SeedBiaya(context, "SPP", "SPP", 0, 10, (int)RepetisiEnum.BULANAN, false);
            var ltk = SeedBiaya(context, "Listrik", "LTK", 0, 10, (int)RepetisiEnum.BULANAN, false);
            var bku = SeedBiaya(context, "Buku", "BKU", 0, 10, (int)RepetisiEnum.NONE, false);
            var bgn = SeedBiaya(context, "Pembangunan", "BGN", 0, 10, (int)RepetisiEnum.NONE, false);
            var kgt = SeedBiaya(context, "Kegiatan", "KGT", 0, 10, (int)RepetisiEnum.NONE, false);
            var sgm = SeedBiaya(context, "Seragam", "SGM", 0, 10, (int)RepetisiEnum.NONE, false);
            var kat = SeedBiaya(context, "Katering", "KAT", 0, 10, (int)RepetisiEnum.NONE, false);
            var jpt = SeedBiaya(context, "Antar Jemput", "JPT", 0, 10, (int)RepetisiEnum.NONE, false);
            var pomg = SeedBiaya(context, "POMG", "POMG", 0, 10, (int)RepetisiEnum.BULANAN, false);
            var zml = SeedBiaya(context, "Zakat Mal", "ZML", 0, 10, (int)RepetisiEnum.NONE, true);
            var zft = SeedBiaya(context, "Zakat Fitrah", "ZFT", 0, 10, (int)RepetisiEnum.NONE, true);
            var frm = SeedBiaya(context, "Form Pendaftaran", "FRM", 0, 10, (int)RepetisiEnum.NONE, false);

            var ljpt = SeedRefLayanan(context, "Antar Jemput", "JPT");
            var lkat = SeedRefLayanan(context, "Katering", "KAT");
            SeedRefKelas(context);
            context.SaveChanges();

            SeedBiayaToGrup(context, frm, bsb);
            SeedBiayaToGrup(context, spp, bsb);
            // SeedBiayaToGrup(context, ltk, bsb); TODO: Confirm to client
            SeedBiayaToGrup(context, bku, bsb);
            SeedBiayaToGrup(context, bgn, bsb);
            SeedBiayaToGrup(context, kgt, bsb);
            SeedBiayaToGrup(context, sgm, bsb);
            SeedBiayaToGrup(context, pomg, bsb);

            SeedBiayaToGrup(context, spp, bdu);
            // SeedBiayaToGrup(context, ltk, bdu); TODO: Confirm to client
            SeedBiayaToGrup(context, bku, bdu);
            SeedBiayaToGrup(context, kgt, bdu);
            SeedBiayaToGrup(context, pomg, bdu);

            SeedRefTahunAjaran(context);


            SeedBiayaLayanan(context, jpt, ljpt);
            SeedBiayaLayanan(context, kat, lkat);

            // For new student (Tingkat 1)
            SeedBiayaNilaiOption(context, frm, 1, 325000);
            SeedBiayaNilaiOption(context, spp, 1, 290000);
            SeedBiayaNilaiOption(context, sgm, 1, 1500000);
            SeedBiayaNilaiOption(context, kgt, 1, 2400000);
            SeedBiayaNilaiOption(context, bku, 1, 1000000);
            SeedBiayaNilaiOption(context, bgn, 1, 3000000);

            // For Tingkat II - VI
            SeedBiayaNilaiOption(context, spp, 2, 280000);
            SeedBiayaNilaiOption(context, spp, 3, 270000);
            SeedBiayaNilaiOption(context, spp, 4, 270000);
            SeedBiayaNilaiOption(context, spp, 5, 250000);
            SeedBiayaNilaiOption(context, spp, 6, 240000);

            SeedBiayaNilaiOption(context, kgt, 2, 1550000);
            SeedBiayaNilaiOption(context, kgt, 3, 1550000);
            SeedBiayaNilaiOption(context, kgt, 4, 1550000);
            SeedBiayaNilaiOption(context, kgt, 5, 1550000);
            SeedBiayaNilaiOption(context, kgt, 6, 1550000);

            SeedBiayaNilaiOption(context, bku, 2, 561000);
            SeedBiayaNilaiOption(context, bku, 3, 549000);
            SeedBiayaNilaiOption(context, bku, 4, 578000);
            SeedBiayaNilaiOption(context, bku, 5, 557000);
            SeedBiayaNilaiOption(context, bku, 6, 557000);

            // For all class
            SeedBiayaNilaiOption(context, ltk, 0, 5000);
            SeedBiayaNilaiOption(context, pomg, 0, 5000);

            // Set accounting data
            // Accounting data
            SeedAccountingPeriod(context);

            var accKas = SeedRefAccount(context, "100.001", "Kas");
            var accRecFrm = SeedRefAccount(context, "120.001", "Piutang Form Pendaftaran");
            var accRecSPP = SeedRefAccount(context, "120.002", "Piutang SPP");
            var accRecPOMG = SeedRefAccount(context, "120.003", "Piutang POMG");
            var accRecBku = SeedRefAccount(context, "120.004", "Piutang Buku");
            var accRecSgm = SeedRefAccount(context, "120.005", "Piutang Seragam");
            var accRecBgn = SeedRefAccount(context, "120.006", "Piutang Pembangunan");
            var accRecKgt = SeedRefAccount(context, "120.007", "Piutang Kegiatan");
            var accRecJpt = SeedRefAccount(context, "120.008", "Piutang Antar Jemput");
            var accRecKat = SeedRefAccount(context, "120.009", "Piutang Katering");
            var accRecLtk = SeedRefAccount(context, "120.010", "Piutang Listrik");
            var accRecZml = SeedRefAccount(context, "120.011", "Piutang Zakat Mal");
            var accRecZft = SeedRefAccount(context, "120.012", "Piutang Zakat Fitrah");
            SeedAccountClassSetting(AccountClassificationEnum.KAS, 1, accKas);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, frm.biaya_id, accRecFrm);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, spp.biaya_id, accRecSPP);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, pomg.biaya_id, accRecPOMG);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, bku.biaya_id, accRecBku);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, sgm.biaya_id, accRecSgm);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, bgn.biaya_id, accRecBgn);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, kgt.biaya_id, accRecKgt);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, jpt.biaya_id, accRecJpt);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, kat.biaya_id, accRecKat);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, ltk.biaya_id, accRecLtk);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, zml.biaya_id, accRecZml);
            SeedAccountClassSetting(AccountClassificationEnum.PIUTANG, zft.biaya_id, accRecZft);
            var accIncFrm = SeedRefAccount(context, "300.001", "Penerimaan Form Pendaftaran");
            var accIncSPP = SeedRefAccount(context, "300.002", "Penerimaan SPP");
            var accIncPOMG = SeedRefAccount(context, "300.003", "Penerimaan POMG");
            var accIncBku = SeedRefAccount(context, "300.004", "Penerimaan Buku");
            var accIncSgm = SeedRefAccount(context, "300.005", "Penerimaan Seragam");
            var accIncBgn = SeedRefAccount(context, "300.006", "Penerimaan Pembangunan");
            var accIncKgt = SeedRefAccount(context, "300.007", "Penerimaan Kegiatan");
            var accIncJpt = SeedRefAccount(context, "300.008", "Penerimaan Antar Jemput");
            var accIncKat = SeedRefAccount(context, "300.009", "Penerimaan Katering");
            var accIncLtk = SeedRefAccount(context, "300.010", "Penerimaan Listrik");
            var accIncZml = SeedRefAccount(context, "300.011", "Penerimaan Zakat Mal");
            var accIncZft = SeedRefAccount(context, "300.012", "Penerimaan Zakat Fitrah");
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, frm.biaya_id, accIncFrm);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, spp.biaya_id, accIncSPP);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, pomg.biaya_id, accIncPOMG);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, bku.biaya_id, accIncBku);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, sgm.biaya_id, accIncSgm);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, bgn.biaya_id, accIncBgn);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, kgt.biaya_id, accIncKgt);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, jpt.biaya_id, accIncJpt);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, kat.biaya_id, accIncKat);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, ltk.biaya_id, accIncLtk);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, zml.biaya_id, accIncZml);
            SeedAccountClassSetting(AccountClassificationEnum.PENDAPATAN, zft.biaya_id, accIncZft);


            // TODO: remove dummy data for release
            #region Dummy data
            //SeedDummyData(context);
            #endregion Dummy data

            context.SaveChanges();
        }

        private void SeedDummyData(FinoDBContext context)
        {
            SeedSiswa(context, "150201", "Chase Meridian", true, false);
            SeedSiswa(context, "150202", "Tessa", false, true);
            SeedSiswa(context, "150203", "Fulan", false, false);
            SeedSiswa(context, "130314", "Alres", true, true);
            SeedSiswa(context, "150201", "Gupta", true, true);
            SeedSiswa(context, "150206", "Eka Hana", true, false);
            SeedSiswa(context, "150207", "Shinta Wijaya", false, false);
            SeedSiswa(context, "150208", "Nasa Lus", false, true);

            SeedSiswaKelas(context, 1, 1, false, DateTime.Parse("2014-09-09"), DateTime.Parse("2014-07-24"), 1);
            SeedSiswaKelas(context, 2, 1, false, DateTime.Parse("2014-09-09"), DateTime.Parse("2014-06-24"), 2);
            SeedSiswaKelas(context, 3, 1, false, DateTime.Parse("2014-09-09"), DateTime.Parse("2014-07-12"), 11);
            SeedSiswaKelas(context, 4, 3, false, DateTime.Parse("2015-09-20"), DateTime.Parse("2015-06-11"), 14);
            SeedSiswaKelas(context, 5, 3, false, DateTime.Parse("2015-09-20"), DateTime.Parse("2015-06-17"), 14);
            SeedSiswaKelas(context, 6, 1, false, DateTime.Parse("2014-09-09"), DateTime.Parse("2014-07-24"), 16);
            SeedSiswaKelas(context, 7, 1, false, DateTime.Parse("2014-09-09"), DateTime.Parse("2014-07-24"), 5);
            SeedSiswaKelas(context, 8, 1, false, DateTime.Parse("2014-09-09"), DateTime.Parse("2014-07-24"), 6);
            context.SaveChanges();

            SeedAgtLayanan(context, 1, 1, DateTime.Parse("2014-10-01"), DateTime.Parse("2015-08-31"), DateTime.Parse("2014-08-14"), 100);
            SeedAgtLayanan(context, 1, 2, DateTime.Parse("2014-10-01"), DateTime.Parse("2015-08-31"), DateTime.Parse("2014-08-14"), 100);
            SeedAgtLayanan(context, 2, 2, DateTime.Parse("2014-10-01"), DateTime.Parse("2015-08-31"), DateTime.Parse("2014-08-14"), 100);

            SeedPosBiaya(context, 1, 1, "SPP", DateTime.Parse("2015-01-12"), 1000000, 0, false, 0, false, true, null, 0);

            var rekening1 = SeedRefRekening(context, "114235678943", 1, DateTime.Parse("2014-10-14"));

            


            try
            {
                context.SaveChanges();

            SeedSiswaRekening(context, 1, rekening1);

            SeedRefAccountDummy(context, "TEST01", "TEST01");
            SeedRefAccountDummy(context, "TEST02", "TEST02");
            SeedRefAccountDummy(context, "TEST03", "TEST03");
            SeedRefAccountDummy(context, "TEST04", "TEST04");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        private void SeedBiayaNilaiOption(FinoDBContext context, RefBiaya pBiaya, int pOption, int pNilai)
        {
            var bNilai = new BiayaNilaiOption
            {
                biaya_id = pBiaya.biaya_id,
                nilai = pNilai,
                option = pOption
            };

            context.BiayaNilaiOptions.Add(bNilai);
        }

        private void SeedRefKelas(FinoDBContext context)
        {
            var sub = new string[] { "A", "B", "C" };
            for (var i = 0; i < 6; i++)
            {
                foreach (var s in sub)
                {
                    var sb = new StringBuilder(string.Empty);
                    var refKelas = new RefKelas
                    {
                        nama = sb.Append((i + 1)).Append(s).ToString(),
                        kelas_code = sb.Append(i).Append(s).ToString(),
                        tingkat = i + 1
                    };
                    context.RefKelases.Add(refKelas);
                }
            }
        }

        private void SeedBiayaLayanan(FinoDBContext context, RefBiaya pBiaya, RefLayanan pLayanan)
        {
            var biayaLayanan = new BiayaLayanan()
            {
                layanan_id = pLayanan.layanan_id,
                biaya_id = pBiaya.biaya_id
            };
            context.BiayaLayanans.Add(biayaLayanan);
        }

        private RefLayanan SeedRefLayanan(FinoDBContext context, string pNama, string pCode)
        {
            var refLayanan = new RefLayanan
            {
                nama = pNama,
                code = pCode
            };
            refLayanan = context.RefLayanans.Add(refLayanan);
            return refLayanan;
        }

        private void SeedRefTahunAjaran(FinoDBContext context)
        {
            int current = DateTime.Now.AddYears(-1).Year;

            for (int i = current; i <= current + 10; i++)
            {
                for (int sem = 0; sem < 2; sem++)
                {
                    var sb = new StringBuilder("Tahun Ajaran ")
                            .Append((i)).Append("/").Append(i + 1)
                            .Append(" Sem. ").Append((sem + 1));
                    var refTh = new RefTahunAjaran
                    {
                        nama = sb.ToString(),
                        mulai_bulan = (sem.Equals(0) ? 7 : 1),
                        mulai_tahun = (sem.Equals(0) ? (i) : i + 1),
                        hingga_bulan = (sem.Equals(0) ? 12 : 6),
                        hingga_tahun = (sem.Equals(0) ? (i) : i + 1),
                        semcawu = sem + 1,
                        status = 0
                    };

                    context.RefTahunAjarans.Add(refTh);
                }
            }
        }

        private void SeedBiayaToGrup(FinoDBContext context, RefBiaya pBiaya, RefGrupBiaya pGrup)
        {
            var grupBiaya = new GrupBiaya
            {
                GrupBiaya_Id = pGrup.Grupbiaya_id,
                Biaya_Id = pBiaya.biaya_id
            };

            context.GrupBiayas.Add(grupBiaya);
        }

        private RefGrupBiaya SeedGrupBiaya(FinoDBContext context, string pNama, string pCode)
        {
            var refGrupBiaya = new RefGrupBiaya
            {
                nama = pNama,
                code = pCode,
                active = true
            };
            refGrupBiaya = context.RefGrupBiayas.Add(refGrupBiaya);
            return refGrupBiaya;
        }

        private RefBiaya SeedBiaya(FinoDBContext context, string pNama, string pCode, int pJtBulan, int pJtTanggal, int pRepetisi, bool pNoposbiaya)
        {
            var refBiaya = new RefBiaya
            {
                nama = pNama,
                code = pCode,
                jt_bulan = pJtBulan,
                jt_tanggal = pJtTanggal,
                mulai_tanggal = DateTime.Now.Date,
                repetisi = pRepetisi,
                aktif = true,
                noposbiaya = pNoposbiaya 
            };
            refBiaya = context.RefBiayas.Add(refBiaya);
            return refBiaya;
        }

        private void SeedUser(FinoDBContext context)
        {
            var ContactList = new List<SysUserContact>();
            ContactList.Add(new SysUserContact
            {
                contact = "fino@nitrocodeus.com",
                type = (int)ContactTypeEnum.EMAIL
            });

            var ContactList2 = new List<SysUserContact>();
            ContactList2.Add(new SysUserContact
            {
                contact = "fathurrohman@nitrocodeus.com",
                type = (int)ContactTypeEnum.EMAIL
            });

            context.Users.Add(new SysUser
            {
                name = "system",
                password = "incrediblehulk",
                fullname = "System Administrator",
                Contacts = ContactList
            });

            context.Users.Add(new SysUser
            {
                name = "Fathurrohman",
                password = "secure",
                Contacts = ContactList2
            });

            var fContactList = new List<SysUserContact>();
            ContactList.Add(new SysUserContact
            {
                contact = "fathrurohman@nitrocodeus.com",
                type = (int)ContactTypeEnum.EMAIL
            });

            context.Users.Add(new SysUser
            {
                name = "Fathurrohman",
                password = "secure",
                fullname = "Fathurrohman",
                Contacts = fContactList
            });
        }

        private void SeedSiswa(FinoDBContext context, string siswa_code, string nama, bool jkelamin, bool ispindahan)
        {
            context.RefSiswas.Add(new RefSiswa
            {
                siswa_code = siswa_code,
                nama = nama,
                jkelamin = jkelamin,
                ispindahan = ispindahan
            });
        }

        private void SeedSiswaKelas(FinoDBContext context, int siswa_id, int tahun_ajaran_id, bool is_siswa_baru, DateTime mulai_tanggal, DateTime tgl_daftar, int kelas_id)
        {
            context.SiswaKelases.Add(new SiswaKelas
            {
                siswa_id = siswa_id,
                tahun_ajaran_id = tahun_ajaran_id,
                is_siswa_baru = is_siswa_baru,
                mulai_tanggal = mulai_tanggal,
                tgl_daftar = tgl_daftar,
                kelas_id = kelas_id
            });
        }

        private void SeedAgtLayanan(FinoDBContext context, int siswa_id, int layanan_id, DateTime mulai_tanggal, DateTime hingga_tanggal, DateTime tgl_daftar, double nilai)
        {
            context.AgtLayanans.Add(new AgtLayanan
            {
                siswa_id = siswa_id,
                layanan_id = layanan_id,
                mulai_tanggal = mulai_tanggal,
                hingga_tanggal = hingga_tanggal,
                tgl_daftar = tgl_daftar,
                nilai = nilai,
            });
        }

        private void SeedPosBiaya(FinoDBContext context, int siswa_id, int biaya_id, string deskripsi, DateTime jTempo, double nilai, double potongan, bool isPersen, double nilaiPotongan, bool isPaid, bool isActive, DateTime? datePaid, int status_id)
        {
            context.PosBiayas.Add(new PosBiaya
            {
                Siswa_Id = siswa_id,
                Biaya_Id = biaya_id,
                Deskripsi = deskripsi,
                JTempo = jTempo,
                Nilai = nilai,
                Potongan = potongan,
                IsPersen = isPersen,
                NilaiPotongan = nilaiPotongan,
                IsPaid = isPaid,
                IsActive = isActive,
                DatePaid = datePaid,
                Status_Id = status_id
            });
        }

        private RefRekening SeedRefRekening(FinoDBContext context, string rekening_code, int status, DateTime tgl_buka)
        {
            RefRekening rekening = new RefRekening
            {
                rekening_code = rekening_code,
                status = status,
                tgl_buka = tgl_buka
            };

            context.Rekenings.Add(rekening);

            return rekening;
        }

        private void SeedSiswaRekening(FinoDBContext context, int siswa_id, RefRekening rekening)
        {
            context.SiswaRekenings.Add(new SiswaRekening
            {
                siswa_id = siswa_id,
                Rekening = rekening
            });
        }

        private void SeedRefAccountDummy(FinoDBContext context, string account_code, string account_name)
        {
            context.RefAccounts.Add(new RefAccount
                {
                    account_code = account_code,
                    account_name = account_name
                });
        }

        private void SeedAccountingPeriod(FinoDBContext context)
        {
            var year = DateTime.Now.Year;
            for (int i = year; i < year + 10; i++)
            {
                for (int month = 1; month <= 12; month++)
                {
                    var periodStart = new DateTime(i, month, 1);
                    var periodEnd = periodStart.AddMonths(1).AddDays(-1);
                    var now = DateTime.Now;
                    var isCurrent = periodStart <= now && periodEnd >= now;
                    var isPast = periodEnd < now;
                    var period = new DateTime(i,month,1);
                    context.RefAccPeriods.Add(new RefAccPeriod
                    {
                        period_name = string.Format("Periode {0}", period.ToString("MMM yyyy")),
                        period_start = periodStart,
                        period_end = periodEnd,
                        status = isCurrent ? (int)AccountingPeriodEnum.ACTIVE : (isPast? (int)AccountingPeriodEnum.CLOSED:(int)AccountingPeriodEnum.NEW)
                    });
                }
            }
        }

        private RefAccount SeedRefAccount(FinoDBContext context, string p_Code, string p_Name)
        {
            var newAcc = new RefAccount
            {
                account_code = p_Code,
                account_name = p_Name
            };
            context.RefAccounts.Add(newAcc);

            return newAcc;
        }

        private void SeedAccountClassSetting(AccountClassificationEnum p_AccountClass, int p_ParamId, RefAccount p_Account)
        {
            var newSetting = new AccountClassSetting
            {
                acc_class_id = (int)p_AccountClass,
                param_id = p_ParamId
            };
            p_Account.AccountBiayaSetting = newSetting;
        }
    }
}
