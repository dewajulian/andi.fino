﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class UserManagementModel : BaseModel
    {
        private const string USERNAME_NOTEMPTY = "VLD_USERNAME_NOTEMPTY";
        private const string USERNAME_LENGTH = "VLD_USERNAME_LENGTH";
        private const string PASSWORD_NOTEMPTY = "VLD_PASSWORD_NOTEMPTY";
        private const string PASSWORD_LENGTH = "VLD_PASSWORD_LENGTH";
        private const string FULLNAME_NOTEMPTY = "VLD_FULLNAME_NOTEMPTY";

        public static readonly string UsernamePropertyName = "Username";
        public static readonly string FullNamePropertyName = "Fullname";
        public static readonly string PasswordPropertyName = "Password";
        public static readonly string PhonePropertyName = "Phone";
        public static readonly string EmailPropertyName = "Email";

        private string _username;
        private string _fullname;
        private string _password;
        private string _phone;
        private string _email;

        public int Userid { get; set; }

        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = FULLNAME_NOTEMPTY)]
        public string Fullname
        {
            get
            {
                return _fullname;
            }
            set
            {
                _fullname = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(FullNamePropertyName));
                ValidateProperty(value, FullNamePropertyName);
            }
        }

        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = USERNAME_NOTEMPTY)]
        [StringLength(15, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = USERNAME_LENGTH)]
        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(UsernamePropertyName));
                ValidateProperty(value, UsernamePropertyName);
            }
        }

        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = PASSWORD_NOTEMPTY)]
        [StringLength(100, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = PASSWORD_LENGTH)]
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(PasswordPropertyName));
                ValidateProperty(value, PasswordPropertyName);
            }
        }

        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                _phone = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(PhonePropertyName));
            }
        }

        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(EmailPropertyName));
            }
        }
    }
}
