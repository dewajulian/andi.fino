﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Fino.Lib.Core;

namespace Fino.Model
{
    public class PotonganBiayaHeaderModel : BaseModel
    {
        public static readonly string SiswaIdPropertyName = "Siswa_Id";
        public static readonly string NamaPropertyName = "Nama";
        public static readonly string CodePropertyName = "Code";
        public static readonly string JKelaminPropertyName = "JKelamin";

        public int Siswa_Id { get; set; }

        private string _Nama;
        [Display(Name = "Nama Siswa")]
        [Required]
        [StringLength(25)]
        public string Nama
        {
            get
            {
                return _Nama;
            }
            set
            {
                _Nama = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NamaPropertyName));
                ValidateProperty(value, NamaPropertyName);
            }
        }

        private string _Code;
        [Display(Name = "No Induk")]
        [Required]
        [StringLength(10)]
        public string Code
        {
            get
            {
                return _Code;
            }
            set
            {
                _Code = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(CodePropertyName));
                ValidateProperty(value, CodePropertyName);
            }
        }

        private bool _JKelamin;
        [Display(Name = "Jenis Kelamin")]
        [Required]
        public bool JKelamin
        {
            get
            {
                return _JKelamin;
            }
            set
            {
                _JKelamin = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(JKelaminPropertyName));
                ValidateProperty(value, JKelaminPropertyName);
            }
        }

        public List<PotonganBiayaDetailModel> PotonganBiayaDetails { get; set; }
    }
}
