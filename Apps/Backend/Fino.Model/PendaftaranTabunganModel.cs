﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class PendaftaranTabunganModel
    {
        public static readonly string RekeningIdPropertyName = "rekening_id";
        public static readonly string RekeningCodePropertyName = "rekening_code";
        public static readonly string TglBukaPropertyName = "tgl_buka";
        public static readonly string StatusPropertyName = "status";
        public static readonly string MutasiNilaiPropertyName = "MutasiNilai";

        public static readonly string SiswaIdPropertyName = "siswa_id";
        public static readonly string SiswaCodePropertyName = "siswa_code";
        public static readonly string SiswaNamaPropertyName = "siswa_nama";
        public static readonly string JKelaminPropertyName = "jkelamin";

        public int rekening_id { get; set; }
        public string rekening_code { get; set; }
        public int siswa_id { get; set; }
        public int status { get; set; }
        public string siswa_code { get; set; }
        public string siswa_nama { get; set; }
        public bool jkelamin { get; set; }

        DateTime _tgl_buka;
        [Display(Name= "Tgl. Pembukaan")]
        [Required]
        public DateTime tgl_buka 
        { 
            get
            {
                return _tgl_buka;
            }

            set
            {
                _tgl_buka = value;
            }
        }

        double _mutasi_nilai;
        [Display(Name = "Nilai Setoran")]
        [Required]
        public double MutasiNilai
        {
            get
            {
                return _mutasi_nilai;
            }

            set
            {
                _mutasi_nilai = value;
            }
        }
        
    }
}
