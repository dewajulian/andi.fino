﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class LaporanMutasiModel
    {
        public string Kelas { get; set; }
        public string NoRekening { get; set; }
        public string NoInduk { get; set; }
        public string Siswa { get; set; }
        public double SaldoAwal { get; set; }
        public DateTime TglMutasi { get; set; }
        public double MutasiNilai { get; set; }
        public double SaldoAkhir { get; set; }
    }
}
