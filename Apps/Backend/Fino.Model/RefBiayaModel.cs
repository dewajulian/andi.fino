﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class RefBiayaModel : BaseModel
    {
        const string NAMA_NOTEMPTY = "VLD_NAMABIAYA_NOTEMPTY";
        const string NAMA_LENGTH = "VLD_NAMABIAYA_LENGTH";
        const string CODE_NOTEMPTY = "VLD_CODEBIAYA_NOTEMPTY";
        const string CODE_LENGTH = "VLD_CODEBIAYA_LENGTH";
        const string REPETISI_NOTEMPTY = "VLD_REPETISI_NOTEMPTY";
        const string TJT_RANGE = "VLD_TANGGALJATUHTEMPO_RANGE";
        const string BJT_RANGE = "VLD_BULANJATUHTEMPO_RANGE";
        const string TANGGALMULAI_NOTEMPTY = "VLD_TANGGALMULAI_NOTEMPTY";
        const string TANGGALMULAI_FORMAT = "VLD_TANGGALMULAI_FORMAT";

        public static readonly string BiayaIdPropertyName = "Biaya_id";
        public static readonly string NamaPropertyName = "Nama";
        public static readonly string CodePropertyName = "Code";
        public static readonly string RepetisiPropertyName = "Repetisi";
        public static readonly string JTTanggalPropertyName = "Jt_tanggal";
        public static readonly string JTBulanPropertyName = "Jt_bulan";
        public static readonly string AktifPropertyName = "Aktif";
        public static readonly string MulaiTanggalPropertyName = "Mulai_tanggal";
        public static readonly string IsTambahanPropertyName = "IsTambahan";
        public static readonly string BisaDiCicilPropertyName = "BisaDiCicil";

        int _biaya_Id;
        string _nama;
        string _code;
        int _repetisi;
        int _jtTanggal;
        int _jtBulan;
        bool _aktif;
        DateTime _mulaiTanggal;
        bool _noPosBiaya;
        bool _bisaDiCicil;

        public int Biaya_id 
        {
            get
            {
                return _biaya_Id;
            }
            set
            {
                _biaya_Id = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(BiayaIdPropertyName));
            }
        }

        [Display(Name = "Nama biaya")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = NAMA_NOTEMPTY)]
        [StringLength(15, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = NAMA_LENGTH)]
        public string Nama 
        {
            get
            {
                return _nama;
            }
            set
            {
                _nama = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NamaPropertyName));
                ValidateProperty(value, NamaPropertyName);
            } 
        }

        [Display(Name = "Kode biaya")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = CODE_NOTEMPTY)]
        [StringLength(10, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = CODE_LENGTH)]
        public string Code 
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(CodePropertyName));
                ValidateProperty(value, CodePropertyName);
            } 
        }

        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = REPETISI_NOTEMPTY)]        
        public int Repetisi 
        {
            get
            {
                return _repetisi;
            }
            set
            {
                _repetisi = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(RepetisiPropertyName));
                ValidateProperty(value, RepetisiPropertyName);
            }
        }

        [Display(Name = "Tanggal jatuh tempo")]
        [Range(1, 31, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = TJT_RANGE)]
        public int Jt_tanggal 
        {
            get
            {
                return _jtTanggal;
            }
            set
            {
                _jtTanggal = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(JTTanggalPropertyName));
                ValidateProperty(value, JTTanggalPropertyName);
            }
        }

        [Display(Name = "Bulan jatuh tempo")]
        [Range(0, 12, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = BJT_RANGE)]
        public int Jt_bulan 
        {
            get
            {
                return _jtBulan;
            }
            set
            {
                _jtBulan = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(JTBulanPropertyName));
                ValidateProperty(value, JTTanggalPropertyName);
            }
        }
        
        public bool Aktif 
        {
            get
            {
                return _aktif;
            }
            set
            {
                _aktif = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(AktifPropertyName));
            }
        }

        [Display(Name = "Tanggal mulai")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = TANGGALMULAI_NOTEMPTY)]
        [DataType(DataType.DateTime, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = TANGGALMULAI_FORMAT)]
        public DateTime Mulai_tanggal 
        {
            get
            {
                return _mulaiTanggal;
            }
            set
            {
                _mulaiTanggal = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(MulaiTanggalPropertyName));
                ValidateProperty(value, MulaiTanggalPropertyName);
            }
        }

        [Display(Name = "Tambahan")]
        public bool IsTambahan
        {
            get
            {
                return _noPosBiaya;
            }
            set
            {
                _noPosBiaya = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(IsTambahanPropertyName));
                ValidateProperty(value, IsTambahanPropertyName);
            }
        }
        [Display(Name = "Bisa dicicil")]
        
        public bool BisaDiCicil
        {
            get
            {
                return _bisaDiCicil;
            }
            set
            {
                _bisaDiCicil = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(BisaDiCicilPropertyName));
                ValidateProperty(value, BisaDiCicilPropertyName);
            }
        }  
    }
}
