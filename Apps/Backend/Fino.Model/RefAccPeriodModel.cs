﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class RefAccPeriodModel  : BaseModel
    {
        public static readonly string PeriodIdPropertyName = "Period_id";
        public static readonly string PeriodNamePropertyName = "Period_name";
        public static readonly string PeriodStartPropertyName = "Period_start";
        public static readonly string PeriodEndPropertyName = "Period_end";
        public static readonly string StatusPropertyName = "Status";

        private int _period_id;
        public int Period_id
        {
            get
            {
                return _period_id;
            }
            set
            {
                _period_id = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(PeriodIdPropertyName));
            }
        }

        private string _period_name;
        [Display(Name="Nama Periode")]
        [Required]
        public string Period_name
        {
            get
            {
                return _period_name;
            }
            set
            {
                _period_name = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(PeriodNamePropertyName));
                ValidateProperty(value, PeriodNamePropertyName);
            }
        }

        private DateTime _period_start;
        [Display(Name = "Awal Periode")]
        [Required]
        public DateTime Period_start
        {
            get
            {
                return _period_start;
            }
            set
            {
                _period_start = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(PeriodStartPropertyName));
                ValidateProperty(value, PeriodStartPropertyName);
            }
        }

        private DateTime _period_end;
        [Display(Name = "Akhir Periode")]
        [Required]
        public DateTime Period_end
        {
            get
            {
                return _period_end;
            }
            set
            {
                _period_end = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(PeriodEndPropertyName));
                ValidateProperty(value, PeriodEndPropertyName);
            }
        }

        private int _status;
        [Display(Name = "Status")]
        [Required]
        public int Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(StatusPropertyName));
                ValidateProperty(value, StatusPropertyName);
            }
        }
    }
}
