﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class PenerimaanHistoryModel
    {
        public int siswa_id { get; set; }
        public string siswa_code { get; set; }
        public string siswa_nama { get; set; }
        public int kelas_id { get; set; }
        public string kelas_nama { get; set; }
        public int posbiaya_id {get;set;}
        public string deskripsi { get; set; }
        public DateTime tgl_bayar { get; set; }
        public double nilai_bayar { get; set; }
        public DateTime jtempo { get; set; }
        public double nilai_biaya { get; set; }
        public double nilai_potongan { get; set; }
        public string nota_pembayaran { get; set; }
    }
}
