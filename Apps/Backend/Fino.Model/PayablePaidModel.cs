﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class PayablePaidModel : BaseModel
    {
        public static readonly string RefVendorIdPropertyName = "RefVendorId";
        public static readonly string RefVendorNamePropertyName = "RefVendorName";
        public static readonly string RefPayableIdPropertyName = "RefPayableId";
        public static readonly string RefPayableNamePropertyName = "RefPayableName";
        public static readonly string DeskripsiPropertyName = "Deskripsi";
        public static readonly string JatuhTempoPropertyName = "JatuhTempo";
        public static readonly string NilaiPropertyName = "Nilai";
        public static readonly string ReferenceNoPropertyName = "ReferenceNo";

        public int PayablePaidId { get; set; }
        public bool IsPaid { get; set; }
        public bool IsActive { get; set; }
        public DateTime DatePaid { get; set; }
        public DateTime Created { get; set; }
        public int StatusId { get; set; }

        private int _refVendorId;
        [Required]
        public int RefVendorId
        {
            get
            {
                return _refVendorId;
            }
            set
            {
                _refVendorId = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(RefVendorIdPropertyName));
            }
        }

        private int _refVendorName;
        [Required]
        public int RefVendorName
        {
            get
            {
                return _refVendorName;
            }
            set
            {
                _refVendorName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(RefVendorNamePropertyName));
            }
        }

        private int _refPayableId;
        [Required]
        public int RefPayableId
        {
            get
            {
                return _refPayableId;
            }
            set
            {
                _refPayableId = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(RefPayableIdPropertyName));
            }
        }

        private string _refPayableName;
        [Required]
        public string RefPayableName
        {
            get
            {
                return _refPayableName;
            }
            set
            {
                _refPayableName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(RefPayableNamePropertyName));
            }
        }

        private string _deskripsi;
        [Required]
        public string Deskripsi
        {
            get
            {
                return _deskripsi;
            }
            set
            {
                _deskripsi = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(DeskripsiPropertyName));
            }
        }

        private DateTime _jatuhTempo;
        [Required]
        public DateTime JatuhTempo
        {
            get
            {
                return _jatuhTempo;
            }
            set
            {
                _jatuhTempo = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(JatuhTempoPropertyName));
            }
        }

        private double _nilai;
        [Required]
        public double Nilai
        {
            get
            {
                return _nilai;
            }
            set
            {
                _nilai = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NilaiPropertyName));
            }
        }

        private string _referenceNo;
        [Required]
        public string  ReferenceNo
        {
            get
            {
                return _referenceNo;
            }
            set
            {
                _referenceNo = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(ReferenceNoPropertyName));
            }
        }
    }
}
