﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class NeracaSaldoModel
    {
        public string AccountClassification { get; set; }
        public int AccountClassificationId { get; set; }
        public string PeriodName { get; set; }
        public string AccountCode { get; set; }
		public string AccountName { get; set; }
        public double DebetValue { get; set; }
        public double CreditValue { get; set; }
    }
}
