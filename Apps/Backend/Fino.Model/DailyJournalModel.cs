﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class DailyJournalModel
    {
        public int period_id { get; set; }
        public string period_name { get; set; }
        public int AccountClassificationId { get; set; }
        public int journal_id { get; set; }
        public DateTime journal_date { get; set; }
        public string journal_name { get; set; }
        public int journaldetail_id { get; set; }
        public int account_id { get; set; }
        public string account_code { get; set; }
        public string detail_name { get; set; }
        public int factor { get; set; }
        public double debet { get; set; }
        public double credit { get; set; }
    }
}
