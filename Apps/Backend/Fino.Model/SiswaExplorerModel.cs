﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class SiswaExplorerModel : BaseModel
    {
        public string SiswaCode { get; set; }
        public string NamaSiswa { get; set; }
        public DateTime TglMasuk { get; set; }
        public string Kelas { get; set; }
        public bool IsPindahan { get; set; }
        public string RekeningTabungan { get; set; }
        public double Tunggakan { get; set; }
        public List<string> KodeLayanan { get; set; }
    }
}
