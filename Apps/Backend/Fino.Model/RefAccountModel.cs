﻿
using Fino.Lib.Core;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Fino.Model
{
    public class RefAccountModel : BaseModel
    {
        const string NAMA_NOTEMPTY = "VLD_NAMAAKUN_NOTEMPTY";
        const string CODE_NOTEMPTY = "VLD_CODEAKUN_NOTEMPTY";

        public static readonly string AccountIdPropertyName = "Account_id";
        public static readonly string NamaPropertyName = "Account_name";
        public static readonly string CodePropertyName = "Account_code";

        private int _account_id;
        public int Account_id
        {
            get
            {
                return _account_id;
            }
            set
            {
                _account_id = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(AccountIdPropertyName));
            }
        }

        private string _account_code;
        [Display(Name = "Kode akun")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = NAMA_NOTEMPTY)]
        public string Account_code
        {
            get
            {
                return _account_code;
            }
            set
            {
                _account_code = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(CodePropertyName));
                ValidateProperty(value, CodePropertyName);
            }
        }

        private string _account_name;
        [Display(Name = "Nama akun")]
        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = NAMA_NOTEMPTY)]
        public string Account_name
        {
            get
            {
                return _account_name;
            }
            set
            {
                _account_name = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(NamaPropertyName));
                ValidateProperty(value, NamaPropertyName);
            }
        }
    }
}
