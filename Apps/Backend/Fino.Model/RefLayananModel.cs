﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class RefLayananModel : BaseModel
    {
        public static readonly string LayananIdPropertyName = "LayananId";
        public static readonly string BiayaIdPropertyName = "BiayaId";
        public static readonly string NamaPropertyName = "Nama";
        public static readonly string CodePropertyName = "Code";

        int _layananId;
        int _biayaId;
        string _code;
        string _nama;

        public int LayananId
        {
            get { return _layananId; }
            set { _layananId = value; }
        }

        public int BiayaId
        {
            get { return _biayaId; }
            set { _biayaId = value; }
        }        

        public string Nama
        {
            get { return _nama; }
            set { _nama = value; }
        }        

        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
    }
}
