﻿using Fino.Lib.Core;
using Fino.Lib.Core.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class UserModel : BaseModel, IUserModel
    {
        const string USERNAME_NOTEMPTY = "VLD_USERNAME_NOTEMPTY";
        const string USERNAME_LENGTH = "VLD_USERNAME_LENGTH";
        const string PASSWORD_NOTEMPTY = "VLD_PASSWORD_NOTEMPTY";
        const string PASSWORD_LENGTH = "VLD_PASSWORD_LENGTH";

        public static readonly string UsernamePropertyName = "Username";
        public static readonly string PasswordPropertyName = "Password";

        int _userId;
        string _username;
        string _password;
        string _fullName;

        public string FullName
        {
            get { return _fullName; }
            set { _fullName = value; }
        }

        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = USERNAME_NOTEMPTY)]
        [StringLength(15, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = USERNAME_LENGTH)]
        public string Username 
        { 
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(UsernamePropertyName));
                ValidateProperty(value, UsernamePropertyName);
            }
        }

        [Required(ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = PASSWORD_NOTEMPTY)]
        [StringLength(100, ErrorMessageResourceType = typeof(AppResource), ErrorMessageResourceName = PASSWORD_LENGTH)]
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(PasswordPropertyName));
                ValidateProperty(value, PasswordPropertyName);
            }
        }
    }
}
