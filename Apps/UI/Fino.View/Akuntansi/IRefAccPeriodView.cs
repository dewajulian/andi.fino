﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IRefAccPeriodView : IView
    {
        event Action AddNewButtonClicked;
        event Action EditButtonClicked;
        event Action OpenPeriodButtonClicked;
        event Action ClosePeriodButtonClicked;
        event Action DeactivateButtonClicked;
        event Action CloseButtonClicked;
        event Action GvAccPeriodDoubleClicked;
        event Action GvAccPeriodSelectionChanged;
        event Action ReloadData;

        void SetData(List<RefAccPeriodModel> p_ListRefAccPeriod);
        void RefreshDataGrid();
        void ShowEditRefAcPeriodView();
        void ShowTambahRefAccPeriodView();
        void ReloadDataEvent();
        void ChangeControlEnabled(bool btnUbahEnabled, bool btnBukaPeriodeEnabled, bool btnTutupPeriodeEnabled, bool btnDeaktivasiEnabled);
        RefAccPeriodModel GetSelectedRefAccPeriod();
    }
}
