﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IPostingJournalView: IView
    {
        event Action PostingButtonClicked;
        event Action SearchTransactionButtonClicked;
        event Action CloseButtonClicked;
        event Action SelectAllClicked;
        event Action UnselectAllClicked;

        DateTime StartDate { get; }
        DateTime EndDate { get; }
        TransaksiToJurnalModel SelectedItem { get; }
        List<TransaksiToJurnalModel> ViewModel { get; set; }
        RefAccPeriodModel AccountPeriod { get; set; }
        void SelectAllItem();
        void UnselectAllItem();

    }
}
