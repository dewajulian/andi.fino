﻿using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IAccountClassSettingView : IView
    {
        event Action GvAccountClassSettingDoubleClick;
        event Action BtnTambahClick;
        event Action BtnHapusClick;
        event Action BtnTutupClick;
        event Action ReloadData;

        AccountClassSettingModel ViewModel { get; set; }
        object AccClassDataObject { get; set; }

        void SetData(List<AccountClassSettingModel> listAccountClassSetting);
        void RefreshDataGrid();
        AccountClassSettingModel GetSelectedAccountClassSetting();

        void ReloadDataEvent();
        void ShowEditAccountClassSettingView();
        void ShowTambahAccountClassSettingView();
        void ShowErrorMessage(string message, string caption);
        void SetBinding();
        void SetDataGridViewParamHeaderText();
    }
}
