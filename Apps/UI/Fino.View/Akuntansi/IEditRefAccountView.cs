﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View
{
    public interface IEditRefAccountView : IView
    {
        event Action BtnBatalClicked;
        event Action BtnSimpanClicked;

        RefAccountModel SelectedRefAccount { get; }
        IRefAccountView RefAccountView { get; set; }

        string NamaAkun { get; set; }
        string KodeAkun { get; set; }

        void SetBinding();
        void ClearData();
    }
}
