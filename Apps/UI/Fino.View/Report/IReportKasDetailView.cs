﻿using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IReportKasDetailView : IView
    {
        event Action BtnOkClicked;
        event Action BtnTutupClicked;
        event Action CmbKelasSelected;

        List<ValueList> Kelas { get; set; }
        List<ValueList> Siswa { get; set; }

        DateTime SelectedBulan { get; }
        int SelectedKelas { get; }
        int SelectedSiswa { get; }

        void ShowRPP(List<LaporanKasDetailModel> p_Model);
    }
}
