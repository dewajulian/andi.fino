﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IUserManagementView : IView
    {
        event Action GvUserManagementDoubleClick;
        event Action BtnTambahClick;
        event Action BtnHapusClick;
        event Action BtnTutupClick;
        event Action ReloadData;

        void SetData(List<UserManagementModel> listUserManagement);
        void RefreshDataGrid();
        UserManagementModel GetSelectedUser();

        void ReloadDataEvent();
        void ShowEditUserView();
        void ShowTambahUserView();
        void ShowErrorMessage(string message, string caption);
    }
}
