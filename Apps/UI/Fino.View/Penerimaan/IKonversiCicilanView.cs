﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IKonversiCicilanView: IView
    {
        event Action SaveButtonClicked;
        event Action NewDataButtonClicked;
        event Action TabViewSelected;
        event Action SearchSiswaButtonClicked;
        event Action CloseButtonClicked;
        event Action BiayaListSelectionChanged;
        event Action LamaRadioButtonSelected;
        event Action NilaiCicilanRadioButtonSelected;
        event Action NilaiCicilanChanged;
        event Action TenorCicilanChanged;
        event Action MulaiTanggalTenorChanged;
        event Action MulaiTanggalNilaiCicilanChanged;
        event Action CariSiswaTextKeypressed;
        event Action SelectingSiswaDone;

        IPencarianSiswaView PencarianSiswaView { get; }

        int SiswaId { get; set; }
        string NoInduk { get; set; }
        string NamaSiswa { get; set; }
        bool JenisKelamin { get; set; }
        bool IsSearchMode { get; set; }

        decimal NilaiCicilan { get; set; }
        decimal TenorCicilan { get; set; }
        decimal TotalBiaya { get; set; }

        int PosBiayaId { get; }
        DateTime MulaiTanggal { get; }
        bool IsHeaderExist { get; set; }
        string PosBiayaNama { get; }
        string Keyword { get; }

        KonversiCicilanSiswaModel ViewModel { get; set; }
        KonversiCicilanHeaderModel CurrentHeader { get; set; }
        PosBiayaModel CurrentPosBiaya { get; set; }
        object BiayaDataSource { get; set; }
        List<KonversiCicilanDetailModel> CicilanDetailDatasource { get; set; }

        void SetBinding();
        void ClearView();
        void ShowErrorMessage(string message, string caption);
        void ShowInformation(string message, string caption);
        void ShowSiswaToEdit(int pSiswaId);
        void SetSearchMode();
        void SetEntryMode();
        void SetViewMode();
        void ShowHeader();
        void SetOptionCicilanView();
        void ShowKonversiHeader(bool p_Show);
        void ShowTenorAndNilai();
    }
}
