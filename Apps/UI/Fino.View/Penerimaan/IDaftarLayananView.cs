using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IDaftarLayananView : IView
    {
        event Action btnSimpanClicked;
        event Action btnTutupClicked;
        event Action SelectingSiswaDone;
        event Action txtSiswaKeyPressed;
        event Action btnClearClicked;
        event Action cmbLayananValueChanged;

        IPencarianSiswaView PencarianSiswaView { get; }
        DaftarLayananModel DaftarLayanan { get; set; }
        List<ValueList> ListLayananItems { get; set; }

        int SiswaId { get; }
        int BiayaId { get; }
        string NamaSiswa { get; set; }
        string TextSearchSiswa { get; }
        string NoInduk { get; set; }

        int LayananId { get; }
        string KodeLayanan { get; set; }
        decimal Nilai { get; set; }
        DateTime MulaiTanggal { get; set; }
        DateTime HinggaTanggal { get; set; }
        
        void SetBinding();
        void ClearBinding();
    }
}
