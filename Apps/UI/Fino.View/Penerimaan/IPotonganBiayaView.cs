﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View
{
    public interface IPotonganBiayaView: IView 
    {
        event Action SaveButtonClicked;
        event Action NewDataButtonClicked;
        event Action DeleteButtonClicked;
        event Action SearchSiswaButtonClicked;
        event Action CloseButtonClicked;
        event Action BiayaListSelectionChanged;
        event Action CariSiswaTextKeypressed;
        event Action SelectingSiswaDone;

        int SiswaId { get; set; }
        string NoInduk { get; set; }
        string NamaSiswa { get; set; }
        bool JenisKelamin { get; set; }
        bool IsSearchMode { get; set; }
        string Keyword { get; }

        int BiayaId { get;  }
        int PotonganId { get; }
        double Nilai { get; }
        bool IsPersen { get; }
        DateTime MulaiTanggal { get; }
        DateTime HinggaTanggal { get; }
        bool IsDetailExist { get; set; }

        IPencarianSiswaView PencarianSiswaView { get; }

        PotonganBiayaHeaderModel ViewModel { get; set; }
        PotonganBiayaDetailModel CurrentDetail { get; set; }
        object BiayaDataSource { get; set; }

        void SetBinding();
        void ClearView();
        void ShowErrorMessage(string message, string caption);
        void ShowInformation(string message, string caption);
        void ShowSiswaToEdit(int pSiswaId);
        void SetSearchMode();
        void SetEntryMode();
        void SetViewMode();
        void ShowPotonganDetail(bool p_Show);
        void ShowHeader();
    }
}
