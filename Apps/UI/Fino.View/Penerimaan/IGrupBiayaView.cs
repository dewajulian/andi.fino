﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;

namespace Fino.View
{
    public interface IGrupBiayaView : IView
    {
        event Action GvGrupBiayaDoubleClick;
        event Action BtnKonfigurasiBiayaClick;
        event Action BtnTambahClick;
        event Action BtnHapusClick;
        event Action BtnTutupClick;
        event Action ReloadData;

        void SetData(List<GrupBiayaModel> p_ListGrupBiaya);
        void RefreshDataGrid();
        void ShowEditGrupBiayaView();
        void ShowTambahGrupBiayaView();
        void ShowKonfigurasiBiayaView();
        void ReloadDataEvent();
        GrupBiayaModel GetSelectedGrupBiaya();
        List<GrupBiayaModel> GetGVGrupBiayaDataSource();
    }
}
