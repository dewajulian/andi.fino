﻿using Fino.Lib.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IManageSiswaView : IView
    {
        //event Action TambahSiswaBaru;
        //event Action UbahDataSiswa;
        //event Action LoadDataSiswa;
        //event Action SiswaSelected;

        void ShowFormUbahDataSiswa();
        void ShowFormTambahSiswa();
        void RefreshData();
    }
}
