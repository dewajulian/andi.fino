﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View
{
    public interface IMutasiTabunganView: IView
    {
        event Action SaveButtonClicked;
        event Action SaveNextButtonClicked;
        event Action NewDataButtonClicked;
        event Action SearchSiswaButtonClicked;
        event Action CloseButtonClicked;
        event Action CariSiswaTextKeypressed;
        event Action SelectingSiswaDone;

        IPencarianSiswaView PencarianSiswaView { get; }
        string Keyword { get; set; }
        int SiswaId { get; set; }
        string NoInduk { get; set; }
        string NamaSiswa { get; set; }
        bool JenisKelamin { get; set; }

        bool IsSearchMode { get; set; }
        object JMutasiDatasource { get; set; }
        string NoRekening { get; set; }

        MutasiTabunganModel ViewModel { get; set; }

        void SetBinding();
        void ClearView();
        void ShowErrorMessage(string message, string caption);
        void ShowInformation(string message, string caption);
        void ShowHeader();
        void SetSearchMode();
        void SetEntryMode();
        void SetViewMode();
    }
}
