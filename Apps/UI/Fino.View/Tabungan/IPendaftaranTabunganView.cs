﻿using System;
using Fino.Model;
using Fino.Lib.Win32;

namespace Fino.View
{
    public interface IPendaftaranTabunganView: IView
    {
        event Action SaveButtonClicked;
        event Action SaveNextButtonClicked;
        event Action NewDataButtonClicked;
        event Action SearchSiswaButtonClicked;
        event Action CloseButtonClicked;
        event Action txtSiswaKeyPressed;
        event Action SelectingSiswaDone;

        int SiswaId { get; set; }
        string TextSearchSiswa { get; }
        string NoInduk { get; set; }
        string NamaSiswa { get; set; }
        bool JenisKelamin { get; set; }

        bool IsSearchMode { get; set; }

        PendaftaranTabunganModel ViewModel { get; set; }
        IPencarianSiswaView PencarianSiswaView { get; }

        void SetBinding();
        void ClearView();
        void ShowErrorMessage(string message, string caption);
        void ShowInformation(string message, string caption);
        void ShowHeader();
        void SetSearchMode();
        void SetEntryMode();
        void SetViewMode();
    }
}
