﻿using Fino.Lib.Win32;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IPayablePaidView : IView
    {
        event Action SaveButtonClicked;
        event Action SaveNextButtonClicked;
        event Action NewDataButtonClicked;
        event Action CloseButtonClicked;
        event Action AddNewVendorButtonClicked;
        event Action ReloadDataEvent;

        string Vendor { get; set; }
        string RefPayableName { get; set; }
        string ReferenceNo { get; set; }
        string Deskripsi { get; set; }
        double Nilai { get; set; }
        DateTime JTempo { get; set; }

        PayablePaidModel ViewModel { get; set; }
        object RefVendorDataSource { get; set; }
        object RefPayableDataSource { get; set; }

        void SetBinding();
        void ClearView();
        void ChangeEditable(bool enabled);
        void ShowErrorMessage(string message, string caption);
        void ShowInformation(string message, string caption);
        void ShowCreateNewRefVendorView();
        void ReloadData();
    }
}
