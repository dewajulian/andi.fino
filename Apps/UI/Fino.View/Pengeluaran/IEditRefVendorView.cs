﻿using Fino.Lib.Win32;
using Fino.Model;
using System;

namespace Fino.View
{
    public interface IEditRefVendorView : IView
    {
        event Action BtnBatalClicked;
        event Action BtnSimpanClicked;

        RefVendorModel ViewModel { get; set; }
        IPayablePaidView PayablePaidView { get; set; }

        string VendorName { get; set; }
        string VendorAddr { get; set; }
        string VendorEmail { get; set; }
        string VendorPhone { get; set; }

        void SetBinding();
    }
}
