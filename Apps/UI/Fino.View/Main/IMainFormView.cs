﻿using Fino.Lib.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IMainFormView : IView
    {
        // event Action ManageSiswaMenuClicked;
        event Action KonfigurasiBiayaClicked;
        event Action PendaftaranSiswaBaruClicked;
        // event Action ShowGrupBiayaClicked;
        event Action ReportRPPClicked;
        event Action SiswaExplorerClicked;
        event Action UbahKataSandiClicked;
        event Action PengaturanPenggunaClicked;

        void ShowManageSiswaView();
        void ShowKonfigurasiBiayaView();
        bool CheckLogonStatus();
        void ShowDaftarSiswaBaruView();
        void ShowGrupBiayaView();
        void ShowReportRPPView();
        void ShowSiswaExplorerView();
        void ShowUbahKataSandiView();
        void ShowPengaturanPenggunaView();
    }
}
