﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Fino.Lib.Core;
using Fino.View;
using Fino.Model;

namespace Fino.Cient.Common
{
    public partial class PosBiayaView : UserControl, IPosBiayaView
    {
        BindingSource _bsPosBiaya;
        List<PosBiayaModel> _source;

        public event Action gvPosBiayaClicked;
        public event Action CariRaised;
        public event Action HideControl;
        public event Action btnTambahkanClicked;
        public event EventHandler<CustomEventArgs<PosBiayaModel>> CellSelectedClicked;

        public int SiswaId { get; set; }
        public bool ShowAllBiaya { get; set; }
        public bool ByPassGridClick { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<PosBiayaModel> gvPosBiayaSource 
        { 
            get
            {
                if (_source == null) _source = new List<PosBiayaModel>();
                return _source;
            }
            set
            {
                _source = value;
                _bsPosBiaya = new BindingSource();
                _bsPosBiaya.DataSource = _source;
                gvPosBiaya.DataSource = _bsPosBiaya;
            }
        }

        public List<PosBiayaModel> SelectedPosBiaya 
        { 
            get
            {
                return gvPosBiayaSource.Where(x => x.Selected).ToList();
            }
        }

        public PosBiayaView()
        {
            InitializeComponent();
            this.Font = new Font(Constants.FontName, Constants.FontSize);
            _bsPosBiaya = new BindingSource();            
        }

        public void Close()
        {
            this.Hide();
        }

        private void gvPosBiaya_CellClick(object sender, DataGridViewCellEventArgs e)
        {     
            if (e.ColumnIndex == selectedDataGridViewCheckBoxColumn.Index)
            {
                gvPosBiaya.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }

            if (gvPosBiayaClicked != null && e.RowIndex > -1 && !ByPassGridClick)
            {
                gvPosBiayaClicked();
            }
        }
        
        public void Cari()
        {
            if (CariRaised != null)
            {
                CariRaised();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            if (HideControl != null)
            {
                HideControl();
            }
        }

        public void RaiseHideControl()
        {
            if (HideControl != null)
            {
                HideControl();
            }
        }

        public void ShowSelectedColumn(bool p_ShowSelectedColumn)
        {
            selectedDataGridViewCheckBoxColumn.Visible = p_ShowSelectedColumn;
            selectedDataGridViewCheckBoxColumn.ReadOnly = !p_ShowSelectedColumn;
        }        

        public void ShowToolbar(bool p_ShowToolbar)
        {
            if (p_ShowToolbar)
            {
                pnlPosBiayaToolbar.Visible = true;
                pnlPosBiayaToolbar.Height = 42;
            }
            else
            {
                pnlPosBiayaToolbar.Visible = false;
                pnlPosBiayaToolbar.Height = 0;
            }
        }

        private void btnTambahkan_Click(object sender, EventArgs e)
        {
            if (btnTambahkanClicked != null)
            {
                btnTambahkanClicked();
            }
        }

        public void ClearSelected()
        {
            if (_bsPosBiaya.Current != null)
            {
                _bsPosBiaya.RemoveCurrent();
            }
        }

        private void gvPosBiaya_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (CellSelectedClicked != null)
            {
                CellSelectedClicked(this, new CustomEventArgs<PosBiayaModel>(_bsPosBiaya.Current as PosBiayaModel));
                //gvPosBiaya.EndEdit(DataGridViewDataErrorContexts.CurrentCellChange);
            }
        }

        private void gvPosBiaya_DataSourceChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in gvPosBiaya.Rows)
            {
                PosBiayaModel pbm = item.DataBoundItem as PosBiayaModel;

                if (pbm != null && pbm.NoPosBiaya.HasValue && pbm.NoPosBiaya.Value)
                {
                    DataGridViewCell cell = gvPosBiaya.Rows[item.Index].Cells[nilaiBiayaDataGridViewTextBoxColumn.Index];
                    cell.Style.BackColor = Color.LightGreen;
                }
            }
        }

        private void gvPosBiaya_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == nilaiBiayaDataGridViewTextBoxColumn.Index && e.RowIndex > -1 && !ShowAllBiaya)
            {
                PosBiayaModel pbm = gvPosBiaya.Rows[e.RowIndex].DataBoundItem as PosBiayaModel;

                if (pbm != null && pbm.NoPosBiaya != null && pbm.NoPosBiaya.Value)
                {
                    DataGridViewCell cell = gvPosBiaya.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    cell.ReadOnly = false;
                }
            }
        }

        private void gvPosBiaya_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewCell cell = (gvPosBiaya.SelectedCells.Count > 0) ? gvPosBiaya.SelectedCells[0] : null;

            if (cell != null && cell.ColumnIndex == nilaiBiayaDataGridViewTextBoxColumn.Index) 
            {
                e.Control.KeyPress  += new KeyPressEventHandler(CheckKey);
             }
        }

        private void CheckKey(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar)
                && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }


        public DialogResult ShowDialog()
        {
            throw new NotImplementedException();
        }

        public DialogResult ShowDialog(IWin32Window owner)
        {
            throw new NotImplementedException();
        }


        public Form MdiParent
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
