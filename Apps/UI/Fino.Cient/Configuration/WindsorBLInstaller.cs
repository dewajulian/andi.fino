﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Cient.Configuration
{
    public class WindsorBLInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyNamed("Fino.BusinessLogic")
                  .Where(c => c.IsClass &&
                                (c.Name.Contains("Process")))
                  .WithService.AllInterfaces()
                  .LifestyleTransient()
                );   
        }
    }
}
