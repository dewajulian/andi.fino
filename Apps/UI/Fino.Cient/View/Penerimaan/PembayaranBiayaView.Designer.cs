﻿namespace Fino.Cient
{
    partial class PembayaranBiayaView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PembayaranBiayaView));
            this.grpInputSiswa = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNamaSiswa = new System.Windows.Forms.TextBox();
            this.txtNoInduk = new System.Windows.Forms.TextBox();
            this.txtSiswa = new System.Windows.Forms.TextBox();
            this.grpBiaya = new System.Windows.Forms.GroupBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ctAddPosBiaya = new Fino.Cient.Common.PosBiayaView();
            this.ctPosBiaya = new Fino.Cient.Common.PosBiayaView();
            this.btnTambahBiaya = new System.Windows.Forms.Button();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.pencarianSiswaControlView1 = new Fino.Cient.Common.PencarianSiswaControlView();
            this.grpInputSiswa.SuspendLayout();
            this.grpBiaya.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpInputSiswa
            // 
            this.grpInputSiswa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpInputSiswa.Controls.Add(this.label3);
            this.grpInputSiswa.Controls.Add(this.label2);
            this.grpInputSiswa.Controls.Add(this.label1);
            this.grpInputSiswa.Controls.Add(this.txtNamaSiswa);
            this.grpInputSiswa.Controls.Add(this.txtNoInduk);
            this.grpInputSiswa.Controls.Add(this.txtSiswa);
            this.grpInputSiswa.Location = new System.Drawing.Point(12, 12);
            this.grpInputSiswa.Name = "grpInputSiswa";
            this.grpInputSiswa.Size = new System.Drawing.Size(524, 114);
            this.grpInputSiswa.TabIndex = 0;
            this.grpInputSiswa.TabStop = false;
            this.grpInputSiswa.Text = "Siswa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Nama Siswa:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "No Induk:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cari Siswa:";
            // 
            // txtNamaSiswa
            // 
            this.txtNamaSiswa.Enabled = false;
            this.txtNamaSiswa.Location = new System.Drawing.Point(140, 81);
            this.txtNamaSiswa.Name = "txtNamaSiswa";
            this.txtNamaSiswa.Size = new System.Drawing.Size(243, 24);
            this.txtNamaSiswa.TabIndex = 2;
            // 
            // txtNoInduk
            // 
            this.txtNoInduk.Enabled = false;
            this.txtNoInduk.Location = new System.Drawing.Point(140, 51);
            this.txtNoInduk.Name = "txtNoInduk";
            this.txtNoInduk.Size = new System.Drawing.Size(243, 24);
            this.txtNoInduk.TabIndex = 1;
            // 
            // txtSiswa
            // 
            this.txtSiswa.Location = new System.Drawing.Point(140, 21);
            this.txtSiswa.Name = "txtSiswa";
            this.txtSiswa.Size = new System.Drawing.Size(243, 24);
            this.txtSiswa.TabIndex = 0;
            this.txtSiswa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSiswa_KeyPress);
            // 
            // grpBiaya
            // 
            this.grpBiaya.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpBiaya.Controls.Add(this.txtTotal);
            this.grpBiaya.Controls.Add(this.label4);
            this.grpBiaya.Controls.Add(this.ctAddPosBiaya);
            this.grpBiaya.Controls.Add(this.ctPosBiaya);
            this.grpBiaya.Controls.Add(this.btnTambahBiaya);
            this.grpBiaya.Location = new System.Drawing.Point(12, 133);
            this.grpBiaya.Name = "grpBiaya";
            this.grpBiaya.Size = new System.Drawing.Size(524, 253);
            this.grpBiaya.TabIndex = 1;
            this.grpBiaya.TabStop = false;
            this.grpBiaya.Text = "Biaya";
            // 
            // txtTotal
            // 
            this.txtTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotal.Enabled = false;
            this.txtTotal.Font = new System.Drawing.Font("Verdana", 14F);
            this.txtTotal.Location = new System.Drawing.Point(332, 214);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(185, 30);
            this.txtTotal.TabIndex = 6;
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label4.Location = new System.Drawing.Point(225, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Total Biaya:";
            // 
            // ctAddPosBiaya
            // 
            this.ctAddPosBiaya.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctAddPosBiaya.ByPassGridClick = false;
            this.ctAddPosBiaya.Font = new System.Drawing.Font("Verdana", 10F);
            this.ctAddPosBiaya.Location = new System.Drawing.Point(7, 24);
            this.ctAddPosBiaya.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ctAddPosBiaya.Name = "ctAddPosBiaya";
            this.ctAddPosBiaya.ShowAllBiaya = false;
            this.ctAddPosBiaya.SiswaId = 0;
            this.ctAddPosBiaya.Size = new System.Drawing.Size(511, 186);
            this.ctAddPosBiaya.TabIndex = 4;
            this.ctAddPosBiaya.Visible = false;
            // 
            // ctPosBiaya
            // 
            this.ctPosBiaya.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctPosBiaya.ByPassGridClick = false;
            this.ctPosBiaya.Font = new System.Drawing.Font("Verdana", 10F);
            this.ctPosBiaya.Location = new System.Drawing.Point(7, 24);
            this.ctPosBiaya.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ctPosBiaya.Name = "ctPosBiaya";
            this.ctPosBiaya.ShowAllBiaya = false;
            this.ctPosBiaya.SiswaId = 0;
            this.ctPosBiaya.Size = new System.Drawing.Size(511, 147);
            this.ctPosBiaya.TabIndex = 3;
            // 
            // btnTambahBiaya
            // 
            this.btnTambahBiaya.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTambahBiaya.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnTambahBiaya.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTambahBiaya.ForeColor = System.Drawing.Color.White;
            this.btnTambahBiaya.Location = new System.Drawing.Point(7, 218);
            this.btnTambahBiaya.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTambahBiaya.Name = "btnTambahBiaya";
            this.btnTambahBiaya.Size = new System.Drawing.Size(145, 28);
            this.btnTambahBiaya.TabIndex = 4;
            this.btnTambahBiaya.Text = "Tambah Biaya";
            this.btnTambahBiaya.UseVisualStyleBackColor = false;
            this.btnTambahBiaya.Click += new System.EventHandler(this.btnTambahBiaya_Click);
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(329, 393);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(100, 28);
            this.btnSimpan.TabIndex = 5;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // btnBatal
            // 
            this.btnBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.Location = new System.Drawing.Point(437, 393);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(100, 28);
            this.btnBatal.TabIndex = 6;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // pencarianSiswaControlView1
            // 
            this.pencarianSiswaControlView1.Font = new System.Drawing.Font("Verdana", 10F);
            this.pencarianSiswaControlView1.Location = new System.Drawing.Point(152, 57);
            this.pencarianSiswaControlView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pencarianSiswaControlView1.Name = "pencarianSiswaControlView1";
            this.pencarianSiswaControlView1.Size = new System.Drawing.Size(329, 320);
            this.pencarianSiswaControlView1.StringSiswa = null;
            this.pencarianSiswaControlView1.TabIndex = 1;
            this.pencarianSiswaControlView1.Visible = false;
            // 
            // PembayaranBiayaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 434);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.pencarianSiswaControlView1);
            this.Controls.Add(this.grpBiaya);
            this.Controls.Add(this.grpInputSiswa);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "PembayaranBiayaView";
            this.Text = "Penerimaan Pelunasan";
            this.grpInputSiswa.ResumeLayout(false);
            this.grpInputSiswa.PerformLayout();
            this.grpBiaya.ResumeLayout(false);
            this.grpBiaya.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpInputSiswa;
        private System.Windows.Forms.TextBox txtSiswa;
        private Common.PencarianSiswaControlView pencarianSiswaControlView1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNamaSiswa;
        private System.Windows.Forms.TextBox txtNoInduk;
        private System.Windows.Forms.GroupBox grpBiaya;
        private Common.PosBiayaView ctPosBiaya;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Button btnTambahBiaya;
        private Common.PosBiayaView ctAddPosBiaya;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label4;
    }
}