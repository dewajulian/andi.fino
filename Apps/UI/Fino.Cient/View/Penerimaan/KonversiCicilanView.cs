﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.Presenter;
using Fino.View;

namespace Fino.Cient
{
    public partial class KonversiCicilanView : BaseForm, IKonversiCicilanView
    {
        public KonversiCicilanView()
        {
            InitializeComponent();

            InitialiseSelectSiswa();

            pencarianSiswaControlView1.HideControl += pencarianSiswaControlView1_HideControl;
        }

        public event Action SaveButtonClicked;
        public event Action NewDataButtonClicked;
        public event Action TabViewSelected;
        public event Action SearchSiswaButtonClicked;
        public event Action CloseButtonClicked;
        public event Action BiayaListSelectionChanged;
        public event Action LamaRadioButtonSelected;
        public event Action NilaiCicilanRadioButtonSelected;
        public event Action NilaiCicilanChanged;
        public event Action TenorCicilanChanged;
        public event Action MulaiTanggalTenorChanged;
        public event Action MulaiTanggalNilaiCicilanChanged;
        public event Action CariSiswaTextKeypressed;
        public event Action SelectingSiswaDone;


        private BindingSource _CicilanDetailBindingSource;

        public string Keyword
        {
            get { return txtCari.Text.Trim(); }
        }
        public IPencarianSiswaView PencarianSiswaView
        {
            get { return pencarianSiswaControlView1; }
        }

        public List<KonversiCicilanDetailModel> CicilanDetailDatasource
        {
            get
            {
                if (_CicilanDetailBindingSource == null || _CicilanDetailBindingSource.DataSource == null)
                {
                    return null;
                }
                else
                {
                    return (List<KonversiCicilanDetailModel>)_CicilanDetailBindingSource.DataSource;
                }
            }
            set
            {
                dgView.AutoGenerateColumns = false;
                if (_CicilanDetailBindingSource == null)
                {
                    _CicilanDetailBindingSource = new BindingSource();
                }
                _CicilanDetailBindingSource.DataSource = value;

                dgView.DataSource = _CicilanDetailBindingSource;

            }
        }


        public int SiswaId { get; set; }

        public string NoInduk
        {
            get
            {
                return txtNoInduk.Text.Trim();
            }
            set
            {
                txtNoInduk.Text = value;
            }
        }

        public string NamaSiswa
        {
            get
            {
                return txtNamaSiswa.Text.Trim();
            }
            set
            {
                txtNamaSiswa.Text = value;
            }
        }

        public bool JenisKelamin
        {
            get
            {
                return rbLaki.Checked;
            }
            set
            {
                rbLaki.Checked = value; ;
            }
        }

        public bool IsSearchMode { get; set; }

        public int PosBiayaId
        {
            get
            {
                if (lstBiaya.SelectedValue == null)
                {
                    return 0;
                }
                else
                {
                    return (int)lstBiaya.SelectedValue;
                }
            }
        }



        public string PosBiayaNama
        {
            get
            {
                if (lstBiaya.SelectedValue == null)
                {
                    return string.Empty;
                }
                else
                {
                    return (string)((ValueList)lstBiaya.SelectedItem).Value;
                }
            }
        }

        public double Nilai
        {
            get
            {
                if (CurrentHeader == null)
                {
                    return CurrentPosBiaya.NilaiBiaya - CurrentPosBiaya.Potongan;
                }
                else
                {
                    return CurrentHeader.NilaiCicilan;
                }
            }
        }

        public DateTime MulaiTanggal
        {
            get
            {
                if (CurrentHeader == null)
                {
                    return CurrentPosBiaya.JTempo;
                }
                else
                {
                    return CurrentHeader.MulaiTanggal;
                }
            }
        }



        public bool IsHeaderExist { get; set; }

        public KonversiCicilanSiswaModel ViewModel { get; set; }

        public KonversiCicilanHeaderModel CurrentHeader { get; set; }
        public PosBiayaModel CurrentPosBiaya { get; set; }

        object _BiayaDataSource;
        public object BiayaDataSource
        {
            get
            {
                return _BiayaDataSource;
            }
            set
            {
                _BiayaDataSource = value;
                lstBiaya.DataSource = _BiayaDataSource;
                lstBiaya.DisplayMember = "Value";
                lstBiaya.ValueMember = "Id";
            }
        }

        public decimal NilaiCicilan
        {
            get
            {
                return nmCicilan.Value;
            }
            set
            {
                nmCicilan.Value = value;
            }
        }

        public decimal TenorCicilan
        {
            get
            {
                return nmLama.Value;
            }
            set
            {
                nmLama.Value = value;
            }
        }

        public decimal TotalBiaya
        {
            get
            {
                return nmNilai.Value;
            }
            set
            {
                nmNilai.Value = value;
            }
        }

        void pencarianSiswaControlView1_HideControl()
        {
            if (SelectingSiswaDone != null && pencarianSiswaControlView1.SelectedSiswa != null)
            {
                SelectingSiswaDone();
            }
        }

        public void SetBinding()
        {

            if (ViewModel != null)
            {
                // TODO: binding siswa information
                txtNoInduk.DataBindings.Clear();
                txtNoInduk.DataBindings.Add("Text", ViewModel, KonversiCicilanSiswaModel.CodePropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                txtNamaSiswa.DataBindings.Clear();
                txtNamaSiswa.DataBindings.Add("Text", ViewModel, KonversiCicilanSiswaModel.NamaPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                rbLaki.DataBindings.Clear();
                rbLaki.DataBindings.Add("Checked", ViewModel, KonversiCicilanSiswaModel.JKelaminPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
            }

            if (CurrentHeader != null)
            {
                // TODO: binding konversi header
                nmNilai.DataBindings.Clear();
                nmNilai.DataBindings.Add("Value", CurrentHeader, KonversiCicilanHeaderModel.TotalNilaiPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                dtpStart.DataBindings.Clear();
                dtpStart.DataBindings.Add("Value", CurrentHeader, KonversiCicilanHeaderModel.MulaiTanggalPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                rbTenor.DataBindings.Clear();
                rbTenor.DataBindings.Add("Checked", CurrentHeader, KonversiCicilanHeaderModel.IsTenorPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                rbNilai.Checked = !CurrentHeader.IsTenor;
                nmLama.DataBindings.Clear();
                nmLama.DataBindings.Add("Value", CurrentHeader, KonversiCicilanHeaderModel.TenorPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                nmCicilan.DataBindings.Clear();
                nmCicilan.DataBindings.Add("Value", CurrentHeader, KonversiCicilanHeaderModel.NilaiCicilanPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
            }
            // TODO: binding konversi detail
        }

        public void ClearView()
        {
            SetBinding();
        }

        public void ShowKonversiHeader(bool p_Show)
        {
            gbCicilanHeader.Visible = p_Show;
            IsHeaderExist = p_Show;
        }

        public void ShowTenorAndNilai()
        {
            nmLama.Enabled = CurrentHeader.IsTenor;
            nmCicilan.Enabled = !CurrentHeader.IsTenor;
        }

        public void ShowErrorMessage(string message, string caption)
        {
            WinApi.ShowErrorMessage(message, caption);
        }

        public void ShowInformation(string message, string caption)
        {
            WinApi.ShowInformationMessage(message, caption);
        }

        public void ShowSiswaToEdit(int pSiswaId)
        {
            throw new NotImplementedException();
        }

        public void SetSearchMode()
        {

            ControlHelper.ChangeEditable(false, gbCicilanHeader);
            ControlHelper.ChangeEditable(false, gbKonversi);
            ControlHelper.ChangeEditable(false, gbSiswa);

            btnSearchSiswa.Enabled = true;
            btnSearchSiswa.Visible = false;
            btnTutup.Enabled = true;
            txtCari.ReadOnly = false;
            txtCari.Focus();
            IsSearchMode = true;

        }

        public void SetEntryMode()
        {
            IsSearchMode = false;

            ControlHelper.ChangeEditable(true, gbKonversi);
            ControlHelper.ChangeEditable(false, gbSiswa);
            ControlHelper.ChangeEditable(true, gbCicilanHeader);

            btnSearchSiswa.Enabled = true;
            btnSearchSiswa.Visible = true;
            nmNilai.ReadOnly = true;
        }

        public void SetViewMode()
        {
            ControlHelper.ChangeEditable(false, gbCicilanHeader);
        }


        public void ShowHeader()
        {
            throw new NotImplementedException();
        }


        public void SetOptionCicilanView()
        {
            nmLama.ReadOnly = rbTenor.Checked;
            nmNilai.ReadOnly = !rbTenor.Checked;
        }


        private void btnSearchSiswa_Click(object sender, EventArgs e)
        {
            if (SearchSiswaButtonClicked != null)
            {
                SearchSiswaButtonClicked();
            }
        }

        private void lstBiaya_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BiayaListSelectionChanged != null)
            {
                BiayaListSelectionChanged();
            }
        }

        private void tabCicilan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabCicilan.SelectedTab.Name.Equals("tbpView"))
            {
                RaiseMulaiTanggalChanged();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveButtonClicked != null)
            {
                SaveButtonClicked();
            }
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            if (CloseButtonClicked != null)
            {
                CloseButtonClicked();
            }
        }

        private void rbTenor_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTenor.Checked && LamaRadioButtonSelected != null)
            {
                LamaRadioButtonSelected();
            }
        }

        private void rbNilai_CheckedChanged(object sender, EventArgs e)
        {
            if (rbNilai.Checked && NilaiCicilanRadioButtonSelected != null)
            {
                NilaiCicilanRadioButtonSelected();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (NewDataButtonClicked != null)
            {
                NewDataButtonClicked();
            }
        }

        private void nmLama_ValueChanged(object sender, EventArgs e)
        {
            if (TenorCicilanChanged != null && CurrentHeader.IsTenor)
            {
                TenorCicilanChanged();
            }
        }

        private void nmLama_Validated(object sender, EventArgs e)
        {
            if (!CurrentHeader.IsSaved && TenorCicilanChanged != null & CurrentHeader.IsTenor)
            {
                TenorCicilanChanged();
            }
        }

        private void nmCicilan_Validated(object sender, EventArgs e)
        {
            if (!CurrentHeader.IsSaved && NilaiCicilanChanged != null & !CurrentHeader.IsTenor)
            {
                NilaiCicilanChanged();
            }
        }

        private void dtpStart_Validated(object sender, EventArgs e)
        {
            Application.DoEvents();

            RaiseMulaiTanggalChanged();
        }

        private void RaiseMulaiTanggalChanged()
        {
            var x = dtpStart.Value;
            if (!CurrentHeader.IsSaved && rbTenor.Checked && MulaiTanggalTenorChanged != null)
            {
                MulaiTanggalTenorChanged();
            }
            else if (!CurrentHeader.IsSaved && rbNilai.Checked && MulaiTanggalNilaiCicilanChanged != null)
            {
                MulaiTanggalNilaiCicilanChanged();
            }
        }

        private void txtCari_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter && CariSiswaTextKeypressed != null)
            {
                CariSiswaTextKeypressed();
            }
        }

        #region Initialise other view
        private void InitialiseSelectSiswa()
        {
            SiswaProcess processSiswa = new SiswaProcess();
            PencarianSiswaPresenter presenterSiswa = new PencarianSiswaPresenter(pencarianSiswaControlView1, processSiswa);
        }

        #endregion Initialise other view
    }
}
