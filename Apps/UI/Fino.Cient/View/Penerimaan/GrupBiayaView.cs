﻿using Fino.Cient.Startup;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class GrupBiayaView : BaseForm, IGrupBiayaView
    {
        BindingSource _bindData;

        public event Action GvGrupBiayaDoubleClick;
        public event Action BtnTambahClick;
        public event Action BtnHapusClick;
        public event Action BtnTutupClick;
        public event Action ReloadData;
        public event Action BtnKonfigurasiBiayaClick;

        public GrupBiayaView()
        {
            InitializeComponent();

            gvGrupBiaya.CellDoubleClick += gvGrupBiaya_CellDoubleClick;
        }

        private void gvGrupBiaya_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (GvGrupBiayaDoubleClick != null)
            {
                GvGrupBiayaDoubleClick();
            }
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            if (BtnTambahClick != null)
            {
                BtnTambahClick();
            }
        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            if (BtnHapusClick != null)
            {
                BtnHapusClick();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (BtnTutupClick != null)
            {
                BtnTutupClick();
            }
        }

        public void SetData(List<GrupBiayaModel> p_ListGrupBiaya)
        {
            _bindData = new BindingSource();
            _bindData.DataSource = p_ListGrupBiaya;

            gvGrupBiaya.DataSource = _bindData;
        }

        public void RefreshDataGrid()
        {
            gvGrupBiaya.Refresh();
        }

        public void ShowEditGrupBiayaView()
        {
            if (gvGrupBiaya.SelectedCells.Count > 0)
            {
                ViewLocator.Instance.EditGrupBiayaView.GrupBiayaView = this;
                ViewLocator.Instance.EditGrupBiayaView.ShowForm(GetSelectedGrupBiaya());
            }
        }

        public void ShowTambahGrupBiayaView()
        {
            ViewLocator.Instance.EditGrupBiayaView.GrupBiayaView = this;
            ViewLocator.Instance.EditGrupBiayaView.ShowForm(null); 
        }

        public void ReloadDataEvent()
        {
            if (ReloadData != null)
            {
                ReloadData();
            }
        }

        public Model.GrupBiayaModel GetSelectedGrupBiaya()
        {
            GrupBiayaModel result = new GrupBiayaModel();

            if (gvGrupBiaya.SelectedCells.Count > 0)
            {
                result = (GrupBiayaModel)gvGrupBiaya.Rows[gvGrupBiaya.SelectedCells[0].RowIndex].DataBoundItem;
            }

            return result;
        }

        public List<GrupBiayaModel> GetGVGrupBiayaDataSource()
        {
            return (_bindData != null) ? _bindData.DataSource as List<GrupBiayaModel> : null;
        }

        public void ShowKonfigurasiBiayaView()
        {
            if (gvGrupBiaya.SelectedCells.Count > 0)
            {
                GrupBiayaModel result = (GrupBiayaModel)gvGrupBiaya.Rows[gvGrupBiaya.SelectedCells[0].RowIndex].DataBoundItem;

                ViewLocator.Instance.KonfigurasiBiayaView.GrupBiayaView = this;
                ViewLocator.Instance.KonfigurasiBiayaView.ShowForm(result);
            }
        }

        private void btnAturPosBiaya_Click(object sender, EventArgs e)
        {
            if (BtnKonfigurasiBiayaClick != null)
            {
                BtnKonfigurasiBiayaClick();
            }
        }
    }
}
