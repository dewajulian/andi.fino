﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;

namespace Fino.Cient
{
    public partial class PengelolaanNilaiBiayaView : BaseForm, IPengelolaanNilaiBiayaView
    {
        #region Events

        public event Action SaveButtonClicked;
        public event Action NewDataButtonClicked;
        public event Action DeleteButtonClicked;
        public event Action CloseButtonClicked;
        public event Action NilaiBiayaSelectionChanged;
        public event Action TingkatSelectionChanged;

        #endregion Events

        #region Private Properties

        private object _BiayaDataSource;
        private object _TingkatDataSource;

        #endregion Private Properties

        #region Public Properties

        public int SelectedBiayaId
        {
            get
            {
                if (lstBiaya.SelectedValue == null)
                {
                    return 0;
                }
                else
                {
                    return (int)lstBiaya.SelectedValue;
                }
            }
        }

        public int SelectedOption
        {
            get
            {
                if (cmbTingkat.SelectedValue == null)
                {
                    return 0;
                }
                else
                {
                    return (int)cmbTingkat.SelectedValue;
                }
            }
        }

        public int BiayaNilaiOption_Id { get; set; }

        public int BiayaId
        {
            get
            {
                return CurrentDetail.Biaya_Id;
            }
        }

        public int Nilai
        {
            get
            {
                return CurrentDetail.Nilai;
            }
        }

        public int Option
        {
            get
            {
                return CurrentDetail.Option;
            }
        }

        public object BiayaDataSource
        {
            get
            {
                return _BiayaDataSource;
            }
            set
            {
                _BiayaDataSource = value;
                lstBiaya.DataSource = _BiayaDataSource;
                lstBiaya.DisplayMember = "Value";
                lstBiaya.ValueMember = "Id";
            }
        }

        public object TingkatDataSource
        {
            get
            {
                return _TingkatDataSource;
            }
            set
            {
                _TingkatDataSource = value;
                cmbTingkat.DataSource = _TingkatDataSource;
                cmbTingkat.DisplayMember = "Value";
                cmbTingkat.ValueMember = "Id";
            }
        }

        public PengelolaanNilaiBiayaModel CurrentDetail { get; set; }

        #endregion Public Properties

        #region Constructors

        public PengelolaanNilaiBiayaView()
        {
            InitializeComponent();

            this.btnAdd.Click += btnAdd_Click;
            this.btnHapus.Click += btnHapus_Click;
            this.btnSave.Click += btnSave_Click;
            this.btnTutup.Click += btnTutup_Click;
            this.lstBiaya.SelectedValueChanged += lstBiaya_SelectedValueChanged;
            this.cmbTingkat.SelectedValueChanged += cmbTingkat_SelectedValueChanged;
        }

        #endregion Constructors

        #region Public Methods

        public void ShowNilaiBiayaDetail(bool p_Show)
        {
            grpNilaiBiaya.Visible = p_Show;
            
            if (CurrentDetail != null)
            {
                SetBinding();
            }
        }

        #endregion

        #region IPengelolaanNilaiBiaya

        public void SetBinding()
        {
            if (CurrentDetail != null)
            {
                BiayaNilaiOption_Id = CurrentDetail.BiayaNilaiOption_Id;
                nmNilai.DataBindings.Clear();
                nmNilai.DataBindings.Add("Value", CurrentDetail, PengelolaanNilaiBiayaModel.NilaiPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
            }
            else
            {
                nmNilai.Value = 0;
            }
        }

        public void ClearView()
        {
            SetBinding();
        }

        public void ShowErrorMessage(string message, string caption)
        {
            WinApi.ShowErrorMessage(message, caption);
        }

        public void ShowInformation(string message, string caption)
        {
            WinApi.ShowInformationMessage(message, caption);
        }

        #endregion IPengelolaanNilaiBiaya

        #region Control Events

        void btnTutup_Click(object sender, EventArgs e)
        {
            if (CloseButtonClicked != null)
            {
                CloseButtonClicked();
            }
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveButtonClicked != null)
            {
                SaveButtonClicked();
            }
        }

        void btnHapus_Click(object sender, EventArgs e)
        {
            if (DeleteButtonClicked != null)
            {
                DeleteButtonClicked();
            }
        }

        void btnAdd_Click(object sender, EventArgs e)
        {
            if (NewDataButtonClicked != null)
            {
                NewDataButtonClicked();
            }
        }

        void cmbTingkat_SelectedValueChanged(object sender, EventArgs e)
        {
            if (TingkatSelectionChanged != null)
            {
                TingkatSelectionChanged();
            }
        }

        void lstBiaya_SelectedValueChanged(object sender, EventArgs e)
        {
            if (NilaiBiayaSelectionChanged != null)
            {
                NilaiBiayaSelectionChanged();
            }
        }

        #endregion Control Events
    }
}
