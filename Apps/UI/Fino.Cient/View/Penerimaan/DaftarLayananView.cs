using Fino.BusinessLogic;
using Fino.Cient.Common;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.Presenter;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class DaftarLayananView : BaseForm, IDaftarLayananView
    {
        private List<ValueList> _listLayananItems;

        public event Action btnSimpanClicked;
        public event Action btnTutupClicked;
        public event Action SelectingSiswaDone;
        public event Action txtSiswaKeyPressed;
        public event Action btnClearClicked;
        public event Action cmbLayananValueChanged;

        public DaftarLayananModel DaftarLayanan { get; set; }

        public List<ValueList> ListLayananItems
        {
            get { return _listLayananItems; }
            set 
            {
                _listLayananItems = value;
                cmbLayanan.DataSource = _listLayananItems;
                cmbLayanan.DisplayMember = "Value";
                cmbLayanan.ValueMember = "Id";
            }
        }

        public int SiswaId
        {
            get
            {
                if (pencarianSiswaControlView1.SelectedSiswa != null)
                {
                    return pencarianSiswaControlView1.SelectedSiswa.Id;
                }

                return 0;
            }
        }

        public string NamaSiswa
        {
            get
            {
                return txtNamaSiswa.Text;
            }
            set
            {
                txtNamaSiswa.Text = value;
            }
        }

        public string NoInduk
        {
            get
            {
                return txtNoInduk.Text;
            }
            set
            {
                txtNoInduk.Text = value;
            }
        }

        public string TextSearchSiswa
        {
            get
            {
                return txtSiswa.Text;
            }
        }

        public IPencarianSiswaView PencarianSiswaView
        {
            get { return pencarianSiswaControlView1; }
        }

        public int LayananId
        {
            get
            {
                int result = 0;

                ValueList vl = (cmbLayanan.SelectedValue != null) ? cmbLayanan.SelectedItem as ValueList : null;

                if (vl != null)
                {
                    RefLayananModel selectedLayanan = vl.Tag as RefLayananModel;

                    if (selectedLayanan != null)
                    {
                        result = selectedLayanan.LayananId;
                    }
                }

                return result;
            }
        }

        public int BiayaId
        {
            get
            {
                int result = 0;

                ValueList vl = (cmbLayanan.SelectedValue != null) ? cmbLayanan.SelectedItem as ValueList : null;

                if (vl != null)
                {
                    RefLayananModel selectedLayanan = vl.Tag as RefLayananModel;

                    if (selectedLayanan != null)
                    {
                        result = selectedLayanan.BiayaId;
                    }
                }

                return result;
            }
        }

        public string KodeLayanan
        {
            get
            {
                return (cmbLayanan.SelectedValue != null ) ? cmbLayanan.SelectedValue.ToString() : null;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    cmbLayanan.SelectedIndex = -1;
                }
                else
                {
                    cmbLayanan.SelectedValue = value;
                }
            }
        }

        public decimal Nilai
        {
            get
            {
                return nmNilai.Value;
            }
            set
            {
                nmNilai.Value = value;
            }
        }

        public DateTime MulaiTanggal
        {
            get
            {
                return dtMulai.Value;
            }
            set
            {
                dtMulai.Value = value;
            }
        }

        public DateTime HinggaTanggal
        {
            get
            {
                return dtAkhir.Value;
            }
            set
            {
                dtAkhir.Value = value;
            }
        }

        public DaftarLayananView()
        {
            InitializeComponent();
            pencarianSiswaControlView1.HideControl += pencarianSiswaControlView1_HideControl;
            cmbLayanan.SelectedValueChanged += cmbLayanan_SelectedValueChanged;

            InitialiseSelectSiswa();
        }

        void cmbLayanan_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbLayananValueChanged != null)
            {
                cmbLayananValueChanged();
            }
        }

        private void pencarianSiswaControlView1_HideControl()
        {
            if (SelectingSiswaDone != null && pencarianSiswaControlView1.SelectedSiswa != null)
            {
                SelectingSiswaDone();
            }
        }

        private void txtSiswa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter && txtSiswaKeyPressed != null)
            {
                txtSiswaKeyPressed();
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            if (btnTutupClicked != null)
            {
                btnTutupClicked();
            }
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (btnSimpanClicked != null)
            {
                btnSimpanClicked();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (btnClearClicked != null)
            {
                btnClearClicked();
            }
        }

        public void SetBinding()
        {
            txtSiswaId.DataBindings.Add("Text", DaftarLayanan, DaftarLayananModel.SiswaIdPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            cmbLayanan.DataBindings.Add("SelectedValue", DaftarLayanan, DaftarLayananModel.KodeLayananPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            nmNilai.DataBindings.Add("Value", DaftarLayanan, DaftarLayananModel.NilaiPropertyName);
            dtMulai.DataBindings.Add("Value", DaftarLayanan, DaftarLayananModel.MulaiTanggalPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            dtAkhir.DataBindings.Add("Value", DaftarLayanan, DaftarLayananModel.HinggaTanggalPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void ClearBinding()
        {
            txtSiswaId.DataBindings.Clear();
            cmbLayanan.DataBindings.Clear();
            nmNilai.DataBindings.Clear();
            dtMulai.DataBindings.Clear();
            dtAkhir.DataBindings.Clear();
        }

        #region Initialise other view
        private void InitialiseSelectSiswa()
        {
            SiswaProcess processSiswa = new SiswaProcess();
            PencarianSiswaPresenter presenterSiswa = new PencarianSiswaPresenter(pencarianSiswaControlView1, processSiswa);
        }
        #endregion
    }
}
