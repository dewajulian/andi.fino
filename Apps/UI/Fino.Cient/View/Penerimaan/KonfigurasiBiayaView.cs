﻿using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class KonfigurasiBiayaView : BaseForm, IKonfigurasiBiayaView
    {
        BindingSource _masterRefBiayaSource;
        BindingSource _selectedRefBiayaSource;

        public event Action BtnTambahRefBiayaClick;
        public event Action BtnHapusRefBiayaClick;
        public event Action BtnTutupClick;
        public event Action BtnSimpanDataClick;
        public event Action BtnHapusSemuaRefBiayaData;
        public event Action BtnTambahSemuaRefBiayaData;
        // public event Action ReloadData;

        public GrupBiayaModel SelectedGrupBiaya { get; private set; }
        public IGrupBiayaView GrupBiayaView { get; set; }

        public KonfigurasiBiayaView()
        {
            InitializeComponent();

            _masterRefBiayaSource = new BindingSource();
            _selectedRefBiayaSource = new BindingSource();

            gvMasterRefBiaya.DataSource = _masterRefBiayaSource;
            gvSelectedRefBiaya.DataSource = _selectedRefBiayaSource;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (BtnSimpanDataClick != null)
            {
                BtnSimpanDataClick();
            }
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            if (BtnTutupClick != null)
            {
                BtnTutupClick();
            }
        }

        private void btnAddPosBiaya_Click(object sender, EventArgs e)
        {
            if (BtnTambahRefBiayaClick != null)
            {
                BtnTambahRefBiayaClick();
            }
        }

        private void btnAddAllPosBiaya_Click(object sender, EventArgs e)
        {
            if (BtnTambahSemuaRefBiayaData != null)
            {
                BtnTambahSemuaRefBiayaData();
            }
        }

        private void btnDeletePosBiaya_Click(object sender, EventArgs e)
        {
            if (BtnHapusRefBiayaClick != null)
            {
                BtnHapusRefBiayaClick();
            }
        }

        private void btnDeleteAllPosBiaya_Click(object sender, EventArgs e)
        {
            if (BtnHapusSemuaRefBiayaData != null)
            {
                BtnHapusSemuaRefBiayaData();
            }
        }

        public void SetRefBiayaMasterGrid(List<RefBiayaModel> p_ListPosBiaya)
        {
            _masterRefBiayaSource.DataSource = null;
            _masterRefBiayaSource.DataSource = p_ListPosBiaya;
        }

        public void SetRefBiayaSelectedGrid(List<RefBiayaModel> p_ListPosBiaya)
        {
            _selectedRefBiayaSource.DataSource = null;
            _selectedRefBiayaSource.DataSource = p_ListPosBiaya;
        }

        public void RefreshRefBiayaMasterDataGrid()
        {
            gvMasterRefBiaya.Refresh();
        }

        public void RefreshRefBiayaSelectedDataGrid()
        {
            gvSelectedRefBiaya.Refresh();
        }

        public void ReloadDataEvent()
        {

        }

        public List<RefBiayaModel> GetGVRefBiayaMasterDataSource()
        {
            return (_masterRefBiayaSource != null) ? _masterRefBiayaSource.DataSource as List<RefBiayaModel> : null;
        }

        public List<RefBiayaModel> GetGVRefBiayaSelectedDataSource()
        {
            return (_selectedRefBiayaSource != null) ? _selectedRefBiayaSource.DataSource as List<RefBiayaModel> : null;
        }
        
        public RefBiayaModel GetSelectedRefBiayaMaster()
        {
            RefBiayaModel result = null;

            if (gvMasterRefBiaya.SelectedCells.Count > 0)
            {
                result = (RefBiayaModel)gvMasterRefBiaya.Rows[gvMasterRefBiaya.SelectedCells[0].RowIndex].DataBoundItem;
            }

            return result;
        }

        public RefBiayaModel GetSelectedRefBiayaSelected()
        {
            RefBiayaModel result = null;

            if (gvSelectedRefBiaya.SelectedCells.Count > 0)
            {
                result = (RefBiayaModel)gvSelectedRefBiaya.Rows[gvSelectedRefBiaya.SelectedCells[0].RowIndex].DataBoundItem;
            }

            return result;
        }

        public void ShowForm(GrupBiayaModel p_SelectedGrupBiaya)
        {
            SelectedGrupBiaya = p_SelectedGrupBiaya;
            this.Show();
        }

        public void RaiseBtnTambahRefBiaya()
        {
            if (BtnTambahRefBiayaClick != null)
            {
                BtnTambahRefBiayaClick();
            }
        }

        public void RaiseBtnTambahSemuaRefBiaya()
        {
            if (BtnTambahSemuaRefBiayaData != null)
            {
                BtnTambahSemuaRefBiayaData();
            }
        }

        public void RaiseBtnHapusRefBiaya()
        {
            if (BtnHapusRefBiayaClick != null)
            {
                BtnHapusRefBiayaClick();
            }
        }

        public void RaiseBtnHapusSemuaRefBiaya()
        {
            if (BtnHapusSemuaRefBiayaData != null)
            {
                BtnHapusSemuaRefBiayaData();
            }
        }

        public void RaiseBtnSimpan()
        {
            if (BtnSimpanDataClick != null)
            {
                BtnSimpanDataClick();
            }
        }
    }
}
