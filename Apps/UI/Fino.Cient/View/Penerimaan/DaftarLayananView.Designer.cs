namespace Fino.Cient
{
    partial class DaftarLayananView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DaftarLayananView));
            this.grpInputSiswa = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNamaSiswa = new System.Windows.Forms.TextBox();
            this.txtNoInduk = new System.Windows.Forms.TextBox();
            this.txtSiswa = new System.Windows.Forms.TextBox();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbLayanan = new System.Windows.Forms.ComboBox();
            this.dtAkhir = new System.Windows.Forms.DateTimePicker();
            this.dtMulai = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nmNilai = new System.Windows.Forms.NumericUpDown();
            this.txtSiswaId = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.pencarianSiswaControlView1 = new Fino.Cient.Common.PencarianSiswaControlView();
            this.grpInputSiswa.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNilai)).BeginInit();
            this.SuspendLayout();
            // 
            // grpInputSiswa
            // 
            this.grpInputSiswa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpInputSiswa.Controls.Add(this.label3);
            this.grpInputSiswa.Controls.Add(this.label2);
            this.grpInputSiswa.Controls.Add(this.label1);
            this.grpInputSiswa.Controls.Add(this.txtNamaSiswa);
            this.grpInputSiswa.Controls.Add(this.txtNoInduk);
            this.grpInputSiswa.Controls.Add(this.txtSiswa);
            this.grpInputSiswa.Location = new System.Drawing.Point(12, 12);
            this.grpInputSiswa.Name = "grpInputSiswa";
            this.grpInputSiswa.Size = new System.Drawing.Size(468, 147);
            this.grpInputSiswa.TabIndex = 0;
            this.grpInputSiswa.TabStop = false;
            this.grpInputSiswa.Text = "Siswa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Nama Siswa:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "No Induk:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cari Siswa:";
            // 
            // txtNamaSiswa
            // 
            this.txtNamaSiswa.Enabled = false;
            this.txtNamaSiswa.Location = new System.Drawing.Point(140, 92);
            this.txtNamaSiswa.Name = "txtNamaSiswa";
            this.txtNamaSiswa.Size = new System.Drawing.Size(300, 24);
            this.txtNamaSiswa.TabIndex = 2;
            // 
            // txtNoInduk
            // 
            this.txtNoInduk.Enabled = false;
            this.txtNoInduk.Location = new System.Drawing.Point(140, 62);
            this.txtNoInduk.Name = "txtNoInduk";
            this.txtNoInduk.Size = new System.Drawing.Size(300, 24);
            this.txtNoInduk.TabIndex = 1;
            // 
            // txtSiswa
            // 
            this.txtSiswa.Location = new System.Drawing.Point(140, 32);
            this.txtSiswa.Name = "txtSiswa";
            this.txtSiswa.Size = new System.Drawing.Size(300, 24);
            this.txtSiswa.TabIndex = 0;
            this.txtSiswa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSiswa_KeyPress);
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(260, 384);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(4);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(100, 28);
            this.btnSimpan.TabIndex = 4;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // btnBatal
            // 
            this.btnBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.Location = new System.Drawing.Point(368, 384);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(4);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(100, 28);
            this.btnBatal.TabIndex = 5;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cmbLayanan);
            this.groupBox1.Controls.Add(this.dtAkhir);
            this.groupBox1.Controls.Add(this.dtMulai);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.nmNilai);
            this.groupBox1.Location = new System.Drawing.Point(12, 165);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(468, 206);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Layanan";
            // 
            // cmbLayanan
            // 
            this.cmbLayanan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLayanan.FormattingEnabled = true;
            this.cmbLayanan.Location = new System.Drawing.Point(164, 35);
            this.cmbLayanan.Name = "cmbLayanan";
            this.cmbLayanan.Size = new System.Drawing.Size(276, 24);
            this.cmbLayanan.TabIndex = 1;
            // 
            // dtAkhir
            // 
            this.dtAkhir.CustomFormat = "dd MMMM yyyy";
            this.dtAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtAkhir.Location = new System.Drawing.Point(164, 109);
            this.dtAkhir.Name = "dtAkhir";
            this.dtAkhir.Size = new System.Drawing.Size(276, 24);
            this.dtAkhir.TabIndex = 5;
            // 
            // dtMulai
            // 
            this.dtMulai.CustomFormat = "dd MMMM yyyy";
            this.dtMulai.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtMulai.Location = new System.Drawing.Point(164, 79);
            this.dtMulai.Name = "dtMulai";
            this.dtMulai.Size = new System.Drawing.Size(276, 24);
            this.dtMulai.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "Sampai Tanggal:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "Mulai Tanggal:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Nilai:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Pilih Layanan:";
            // 
            // nmNilai
            // 
            this.nmNilai.Location = new System.Drawing.Point(164, 135);
            this.nmNilai.Maximum = new decimal(new int[] {
            -1530494976,
            232830,
            0,
            0});
            this.nmNilai.Name = "nmNilai";
            this.nmNilai.Size = new System.Drawing.Size(275, 24);
            this.nmNilai.TabIndex = 7;
            this.nmNilai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nmNilai.ThousandsSeparator = true;
            this.nmNilai.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // txtSiswaId
            // 
            this.txtSiswaId.Location = new System.Drawing.Point(413, 454);
            this.txtSiswaId.Name = "txtSiswaId";
            this.txtSiswaId.Size = new System.Drawing.Size(100, 24);
            this.txtSiswaId.TabIndex = 12;
            this.txtSiswaId.Visible = false;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClear.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(26, 384);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(100, 28);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "Ulang";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // pencarianSiswaControlView1
            // 
            this.pencarianSiswaControlView1.Font = new System.Drawing.Font("Verdana", 10F);
            this.pencarianSiswaControlView1.Location = new System.Drawing.Point(152, 67);
            this.pencarianSiswaControlView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pencarianSiswaControlView1.Name = "pencarianSiswaControlView1";
            this.pencarianSiswaControlView1.Size = new System.Drawing.Size(330, 358);
            this.pencarianSiswaControlView1.StringSiswa = null;
            this.pencarianSiswaControlView1.TabIndex = 1;
            this.pencarianSiswaControlView1.Visible = false;
            // 
            // DaftarLayananView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 425);
            this.Controls.Add(this.pencarianSiswaControlView1);
            this.Controls.Add(this.txtSiswaId);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.grpInputSiswa);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "DaftarLayananView";
            this.Text = "Pendaftaran Layanan";
            this.grpInputSiswa.ResumeLayout(false);
            this.grpInputSiswa.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNilai)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpInputSiswa;
        private System.Windows.Forms.TextBox txtSiswa;
        private Common.PencarianSiswaControlView pencarianSiswaControlView1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNamaSiswa;
        private System.Windows.Forms.TextBox txtNoInduk;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbLayanan;
        private System.Windows.Forms.DateTimePicker dtAkhir;
        private System.Windows.Forms.DateTimePicker dtMulai;
        private System.Windows.Forms.NumericUpDown nmNilai;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSiswaId;
        private System.Windows.Forms.Button btnClear;
    }
}
