﻿using Fino.Cient.Startup;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class RefPayableView : BaseForm, IRefPayableView
    {
        BindingSource _bindData;

        public event Action GvRefPayableDoubleClick;
        public event Action BtnTambahClick;
        public event Action BtnHapusClick;
        public event Action BtnTutupClick;
        public event Action ReloadData;
        public event Action BtnKonfigurasiBiayaClick;

        public RefPayableView()
        {
            InitializeComponent();

            gvRefPayable.CellDoubleClick += gvRefPayable_CellDoubleClick;
        }

        private void gvRefPayable_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (GvRefPayableDoubleClick != null)
            {
                GvRefPayableDoubleClick();
            }
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            if (BtnTambahClick != null)
            {
                BtnTambahClick();
            }
        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            if (BtnHapusClick != null)
            {
                BtnHapusClick();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (BtnTutupClick != null)
            {
                BtnTutupClick();
            }
        }

        public void SetData(List<RefPayableModel> p_ListRefPayable)
        {
            _bindData = new BindingSource();
            _bindData.DataSource = p_ListRefPayable;

            gvRefPayable.DataSource = _bindData;
        }

        public void RefreshDataGrid()
        {
            gvRefPayable.Refresh();
        }

        public void ShowEditRefPayableView()
        {
            if (gvRefPayable.SelectedCells.Count > 0)
            {
                ViewLocator.Instance.EditRefPayableView.RefPayableView = this;
                ViewLocator.Instance.EditRefPayableView.ShowForm(GetSelectedRefPayable());
            }
        }

        public void ShowTambahRefPayableView()
        {
            ViewLocator.Instance.EditRefPayableView.RefPayableView = this;
            ViewLocator.Instance.EditRefPayableView.ShowForm(null); 
        }

        public void ReloadDataEvent()
        {
            if (ReloadData != null)
            {
                ReloadData();
            }
        }

        public RefPayableModel GetSelectedRefPayable()
        {
            RefPayableModel result = new RefPayableModel();

            if (gvRefPayable.SelectedCells.Count > 0)
            {
                result = (RefPayableModel)gvRefPayable.Rows[gvRefPayable.SelectedCells[0].RowIndex].DataBoundItem;
            }

            return result;
        }

        public List<RefPayableModel> GetGVRefPayableDataSource()
        {
            return (_bindData != null) ? _bindData.DataSource as List<RefPayableModel> : null;
        }
    }
}
