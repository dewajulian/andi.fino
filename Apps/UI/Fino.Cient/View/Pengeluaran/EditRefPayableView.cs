﻿using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class EditRefPayableView : BaseForm, IEditRefPayableView
    {
        public event Action BtnBatalClicked;
        public event Action BtnSimpanClicked;

        public EditRefPayableView()
        {
            InitializeComponent();
        }

        public RefPayableModel SelectedRefPayable { get; private set; }

        public IRefPayableView RefPayableView { get; set; }

        public string Nama
        {
            get
            {
                return txtNamaGrupBiaya.Text;
            }
            set
            {
                txtNamaGrupBiaya.Text = value;
            }
        }

        public string Kode
        {
            get
            {
                return txtKodeGrupBiaya.Text;
            }
            set
            {
                txtKodeGrupBiaya.Text = value;
            }
        }

        public int RefPayableId { get; set; }

        public bool Aktif 
        { 
            get
            {
                return chkAktif.Checked;
            }
            set
            {
                chkAktif.Checked = value;
            }
        }

        public void SetBinding()
        {
            txtNamaGrupBiaya.DataBindings.Add("Text", SelectedRefPayable, RefPayableModel.NamePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtKodeGrupBiaya.DataBindings.Add("Text", SelectedRefPayable, RefPayableModel.CodePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            chkAktif.DataBindings.Add("Checked", SelectedRefPayable, RefPayableModel.AktifPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void ClearData()
        {
            throw new NotImplementedException();
        }

        public void ShowForm(RefPayableModel p_RefPayable)
        {
            if (p_RefPayable != null)
            {
                SelectedRefPayable = p_RefPayable;
            }
            else
            {
                SelectedRefPayable = new RefPayableModel();
                SelectedRefPayable.Aktif = true;
            }
            this.Show();
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (BtnSimpanClicked != null)
            {
                BtnSimpanClicked();
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            if (BtnBatalClicked != null)
            {
                BtnBatalClicked();
            }
        }

        public void RaiseBtnSimpanClicked()
        {
            if (BtnSimpanClicked != null)
            {
                BtnSimpanClicked();
            }
        }

        public void RaiseBtnBatalClicked()
        {
            if (BtnBatalClicked != null)
            {
                BtnBatalClicked();
            }
        }
    }
}
