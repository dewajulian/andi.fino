﻿namespace Fino.Cient
{
    partial class EditRefPayableView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditRefPayableView));
            this.lblKodeBiaya = new System.Windows.Forms.Label();
            this.lblNamaGrupBiaya = new System.Windows.Forms.Label();
            this.txtKodeGrupBiaya = new System.Windows.Forms.TextBox();
            this.txtNamaGrupBiaya = new System.Windows.Forms.TextBox();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.chkAktif = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblKodeBiaya
            // 
            this.lblKodeBiaya.AutoSize = true;
            this.lblKodeBiaya.Location = new System.Drawing.Point(49, 22);
            this.lblKodeBiaya.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKodeBiaya.Name = "lblKodeBiaya";
            this.lblKodeBiaya.Size = new System.Drawing.Size(141, 17);
            this.lblKodeBiaya.TabIndex = 6;
            this.lblKodeBiaya.Text = "Kode pengeluaran:";
            // 
            // lblNamaGrupBiaya
            // 
            this.lblNamaGrupBiaya.AutoSize = true;
            this.lblNamaGrupBiaya.Location = new System.Drawing.Point(49, 54);
            this.lblNamaGrupBiaya.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNamaGrupBiaya.Name = "lblNamaGrupBiaya";
            this.lblNamaGrupBiaya.Size = new System.Drawing.Size(144, 17);
            this.lblNamaGrupBiaya.TabIndex = 7;
            this.lblNamaGrupBiaya.Text = "Nama pengeluaran:";
            // 
            // txtKodeGrupBiaya
            // 
            this.txtKodeGrupBiaya.Location = new System.Drawing.Point(195, 19);
            this.txtKodeGrupBiaya.Margin = new System.Windows.Forms.Padding(4);
            this.txtKodeGrupBiaya.Name = "txtKodeGrupBiaya";
            this.txtKodeGrupBiaya.Size = new System.Drawing.Size(144, 24);
            this.txtKodeGrupBiaya.TabIndex = 0;
            // 
            // txtNamaGrupBiaya
            // 
            this.txtNamaGrupBiaya.Location = new System.Drawing.Point(195, 51);
            this.txtNamaGrupBiaya.Margin = new System.Windows.Forms.Padding(4);
            this.txtNamaGrupBiaya.Name = "txtNamaGrupBiaya";
            this.txtNamaGrupBiaya.Size = new System.Drawing.Size(201, 24);
            this.txtNamaGrupBiaya.TabIndex = 1;
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(195, 122);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(4);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(100, 28);
            this.btnSimpan.TabIndex = 2;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // btnBatal
            // 
            this.btnBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.Location = new System.Drawing.Point(304, 122);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(4);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(100, 28);
            this.btnBatal.TabIndex = 3;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // chkAktif
            // 
            this.chkAktif.AutoSize = true;
            this.chkAktif.Location = new System.Drawing.Point(195, 87);
            this.chkAktif.Name = "chkAktif";
            this.chkAktif.Size = new System.Drawing.Size(15, 14);
            this.chkAktif.TabIndex = 8;
            this.chkAktif.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 84);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Aktif:";
            // 
            // EditRefPayableView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 164);
            this.Controls.Add(this.chkAktif);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.lblKodeBiaya);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNamaGrupBiaya);
            this.Controls.Add(this.txtKodeGrupBiaya);
            this.Controls.Add(this.txtNamaGrupBiaya);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "EditRefPayableView";
            this.Text = "Edit Jenis Pengeluaran";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKodeBiaya;
        private System.Windows.Forms.Label lblNamaGrupBiaya;
        private System.Windows.Forms.TextBox txtKodeGrupBiaya;
        private System.Windows.Forms.TextBox txtNamaGrupBiaya;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.CheckBox chkAktif;
        private System.Windows.Forms.Label label1;
    }
}