﻿using Fino.Cient.Startup;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class PayablePaidView : BaseForm, IPayablePaidView
    {
        #region Events

        public event Action SaveButtonClicked;
        public event Action SaveNextButtonClicked;
        public event Action NewDataButtonClicked;
        public event Action CloseButtonClicked;
        public event Action AddNewVendorButtonClicked;
        public event Action ReloadDataEvent;

        #endregion Events

        #region Constructors

        public PayablePaidView()
        {
            InitializeComponent();

            this.btnSave.Click += btnSave_Click;
            this.btnSaveNew.Click += btnSaveNew_Click;
            this.btnNew.Click += btnNew_Click;
            this.btnClose.Click += btnClose_Click;
            this.btnVendorBaru.Click += btnVendorBaru_Click;
        }

        #endregion Constructors

        public PayablePaidModel ViewModel { get; set; }
        public object RefVendorDataSource { get; set; }
        public object RefPayableDataSource { get; set; }

        #region Public Properties

        public string Vendor
        {
            get
            {
                return cboVendor.Text;
            }
            set
            {
                cboVendor.SelectedValue = value;
            }
        }

        public string RefPayableName
        {
            get
            {
                return cboJenisPembayaran.Text;
            }
            set
            {
                cboJenisPembayaran.SelectedValue = value;
            }
        }

        public string ReferenceNo
        {
            get
            {
                return txtNoResi.Text;
            }
            set
            {
                txtNoResi.Text = value;
            }
        }

        public string Deskripsi
        {
            get
            {
                return txtDeskripsi.Text;
            }
            set
            {
                txtDeskripsi.Text = value;
            }
        }

        public double Nilai
        {
            get
            {
                return (double)nmNilai.Value;
            }
            set
            {
                nmNilai.Value = (decimal)value;
            }
        }

        public DateTime JTempo
        {
            get
            {
                return dtpJatuhTempo.Value;
            }
            set
            {
                dtpJatuhTempo.Value = value;
            }
        }

        #endregion Public Properties

        #region Application Events

        private void btnVendorBaru_Click(object sender, EventArgs e)
        {
            if (AddNewVendorButtonClicked != null)
            {
                AddNewVendorButtonClicked();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (CloseButtonClicked != null)
            {
                CloseButtonClicked();
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (NewDataButtonClicked != null)
            {
                NewDataButtonClicked();
            }
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            if (SaveNextButtonClicked != null)
            {
                SaveNextButtonClicked();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveButtonClicked != null)
            {
                SaveButtonClicked();
            }
        }

        #endregion Application Events

        #region IPayablePaidView

        public void SetBinding()
        {
            try
            {
                txtDeskripsi.DataBindings.Clear();
                txtDeskripsi.DataBindings.Add("Text", ViewModel, PayablePaidModel.DeskripsiPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                txtNoResi.DataBindings.Clear();
                txtNoResi.DataBindings.Add("Text", ViewModel, PayablePaidModel.ReferenceNoPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                cboVendor.DataSource = RefVendorDataSource;
                cboVendor.DisplayMember = "Value";
                cboVendor.ValueMember = "Id";
                cboVendor.DataBindings.Clear();
                cboVendor.DataBindings.Add("SelectedValue", ViewModel, PayablePaidModel.RefVendorIdPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                cboVendor.DataBindings.Add("Text", ViewModel, PayablePaidModel.RefVendorNamePropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                dtpJatuhTempo.DataBindings.Clear();
                dtpJatuhTempo.DataBindings.Add("Value", ViewModel, PayablePaidModel.JatuhTempoPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                nmNilai.DataBindings.Clear();
                nmNilai.DataBindings.Add("Value", ViewModel, PayablePaidModel.NilaiPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                
                cboJenisPembayaran.DataSource = RefPayableDataSource;
                cboJenisPembayaran.DisplayMember = "Value";
                cboJenisPembayaran.ValueMember = "Id";
                cboJenisPembayaran.DataBindings.Clear();
                cboJenisPembayaran.DataBindings.Add("SelectedValue", ViewModel, PayablePaidModel.RefPayableIdPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.Message, "Error Binding");
            }
        }

        public void ClearView()
        {
            SetBinding();
        }

        public void ChangeEditable(bool enabled)
        {
            foreach (var c in groupBox2.Controls)
            {
                var ctype = c.GetType();
                if (ctype.Equals(typeof(TextBox)))
                {
                    ((TextBox)c).ReadOnly = !enabled;
                }
                if (ctype.Equals(typeof(ComboBox)))
                {
                    ((ComboBox)c).Enabled = enabled;
                }
                if (ctype.Equals(typeof(DateTimePicker)))
                {
                    ((DateTimePicker)c).Enabled = enabled;
                }
                if (ctype.Equals(typeof(NumericUpDown)))
                {
                    ((NumericUpDown)c).Enabled = enabled;
                }
            }

            btnSaveNew.Enabled = enabled;
            btnSave.Enabled = enabled;
        }

        public void ShowErrorMessage(string message, string caption)
        {
            MessageBox.Show(this, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void ShowInformation(string message, string caption)
        {
            MessageBox.Show(this, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void ShowCreateNewRefVendorView()
        {
            ViewLocator.Instance.EditRefVendorView.PayablePaidView = this;
            ViewLocator.Instance.EditRefVendorView.Show();
        }

        public void ReloadData()
        {
            if (ReloadDataEvent != null)
            {
                ReloadDataEvent();
            }
        }

        #endregion IPayablePaidView
    }
}
