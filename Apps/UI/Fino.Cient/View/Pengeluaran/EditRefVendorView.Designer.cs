﻿namespace Fino.Cient
{
    partial class EditRefVendorView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditRefVendorView));
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.lblNamaBiaya = new System.Windows.Forms.Label();
            this.txtVendorName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtVendorAddr = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtVendorEmail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtVendorPhone = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(217, 155);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(4);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(100, 28);
            this.btnSimpan.TabIndex = 4;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            // 
            // btnBatal
            // 
            this.btnBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.Location = new System.Drawing.Point(326, 155);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(4);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(100, 28);
            this.btnBatal.TabIndex = 5;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            // 
            // lblNamaBiaya
            // 
            this.lblNamaBiaya.AutoSize = true;
            this.lblNamaBiaya.Location = new System.Drawing.Point(101, 25);
            this.lblNamaBiaya.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNamaBiaya.Name = "lblNamaBiaya";
            this.lblNamaBiaya.Size = new System.Drawing.Size(108, 17);
            this.lblNamaBiaya.TabIndex = 23;
            this.lblNamaBiaya.Text = "Nama Vendor:";
            // 
            // txtVendorName
            // 
            this.txtVendorName.Location = new System.Drawing.Point(217, 22);
            this.txtVendorName.Margin = new System.Windows.Forms.Padding(4);
            this.txtVendorName.Name = "txtVendorName";
            this.txtVendorName.Size = new System.Drawing.Size(160, 24);
            this.txtVendorName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(91, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 17);
            this.label1.TabIndex = 27;
            this.label1.Text = "Alamat Vendor:";
            // 
            // txtVendorAddr
            // 
            this.txtVendorAddr.Location = new System.Drawing.Point(217, 54);
            this.txtVendorAddr.Margin = new System.Windows.Forms.Padding(4);
            this.txtVendorAddr.Name = "txtVendorAddr";
            this.txtVendorAddr.Size = new System.Drawing.Size(160, 24);
            this.txtVendorAddr.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(104, 89);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 17);
            this.label2.TabIndex = 29;
            this.label2.Text = "Email Vendor:";
            // 
            // txtVendorEmail
            // 
            this.txtVendorEmail.Location = new System.Drawing.Point(217, 86);
            this.txtVendorEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtVendorEmail.Name = "txtVendorEmail";
            this.txtVendorEmail.Size = new System.Drawing.Size(160, 24);
            this.txtVendorEmail.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(58, 121);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 17);
            this.label3.TabIndex = 31;
            this.label3.Text = "No. Telepon Vendor:";
            // 
            // txtVendorPhone
            // 
            this.txtVendorPhone.Location = new System.Drawing.Point(217, 118);
            this.txtVendorPhone.Margin = new System.Windows.Forms.Padding(4);
            this.txtVendorPhone.Name = "txtVendorPhone";
            this.txtVendorPhone.Size = new System.Drawing.Size(160, 24);
            this.txtVendorPhone.TabIndex = 3;
            // 
            // EditRefVendorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 196);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtVendorPhone);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtVendorEmail);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtVendorAddr);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.lblNamaBiaya);
            this.Controls.Add(this.txtVendorName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditRefVendorView";
            this.Text = "Tambah Vendor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Label lblNamaBiaya;
        private System.Windows.Forms.TextBox txtVendorName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtVendorAddr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtVendorEmail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtVendorPhone;
    }
}