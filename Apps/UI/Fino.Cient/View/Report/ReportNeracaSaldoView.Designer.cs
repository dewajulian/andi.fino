﻿namespace Fino.Cient.Report
{
    partial class ReportNeracaSaldoView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportNeracaSaldoView));
            this.NeracaSaldoModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnTutup = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rptSaldo = new Microsoft.Reporting.WinForms.ReportViewer();
            this.LaporanSaldoModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.cboPeriode = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.NeracaSaldoModelBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LaporanSaldoModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // NeracaSaldoModelBindingSource
            // 
            this.NeracaSaldoModelBindingSource.DataSource = typeof(Fino.Model.NeracaSaldoModel);
            // 
            // btnTutup
            // 
            this.btnTutup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(666, 15);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(138, 35);
            this.btnTutup.TabIndex = 3;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            this.btnTutup.Click += new System.EventHandler(this.btnTutup_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(516, 15);
            this.btnOk.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(138, 35);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Tampilkan";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cboPeriode);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.btnTutup);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(815, 62);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rptSaldo);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 62);
            this.panel2.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(815, 342);
            this.panel2.TabIndex = 4;
            // 
            // rptSaldo
            // 
            this.rptSaldo.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "NeracaSaldo";
            reportDataSource1.Value = this.NeracaSaldoModelBindingSource;
            this.rptSaldo.LocalReport.DataSources.Add(reportDataSource1);
            this.rptSaldo.LocalReport.ReportEmbeddedResource = "Fino.Cient.View.Report.NeracaSaldo.rdlc";
            this.rptSaldo.Location = new System.Drawing.Point(0, 0);
            this.rptSaldo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rptSaldo.Name = "rptSaldo";
            this.rptSaldo.Size = new System.Drawing.Size(815, 342);
            this.rptSaldo.TabIndex = 0;
            // 
            // LaporanSaldoModelBindingSource
            // 
            this.LaporanSaldoModelBindingSource.DataSource = typeof(Fino.Model.NeracaSaldoModel);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Periode Akuntansi";
            // 
            // cboPeriode
            // 
            this.cboPeriode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboPeriode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPeriode.FormattingEnabled = true;
            this.cboPeriode.Location = new System.Drawing.Point(182, 19);
            this.cboPeriode.Name = "cboPeriode";
            this.cboPeriode.Size = new System.Drawing.Size(315, 28);
            this.cboPeriode.TabIndex = 1;
            // 
            // ReportNeracaSaldoView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 404);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "ReportNeracaSaldoView";
            this.Text = "Neraca Saldo";
            this.Load += new System.EventHandler(this.ReportNeracaSaldoView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.NeracaSaldoModelBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LaporanSaldoModelBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTutup;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Microsoft.Reporting.WinForms.ReportViewer rptSaldo;
        private System.Windows.Forms.BindingSource LaporanSaldoModelBindingSource;
        private System.Windows.Forms.BindingSource NeracaSaldoModelBindingSource;
        private System.Windows.Forms.ComboBox cboPeriode;
        private System.Windows.Forms.Label label1;
    }
}