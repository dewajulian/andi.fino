﻿using Fino.View;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Lib.Core.Settings;
using Fino.Model;

namespace Fino.Cient.Report
{
    public partial class ReportPenerimaanView : BaseForm, IReportPenerimaanView
    {
        private List<ValueList> _kelasDataSource;
        private List<ValueList> _siswaDataSource;

        public event Action BtnOkClicked;
        public event Action BtnTutupClicked;
        public event Action CmbKelasSelected;

        public List<ValueList> Kelas
        {
            get
            {
                return _kelasDataSource;
            }
            set
            {
                _kelasDataSource = value;
                if (_kelasDataSource.Count > 0)
                {
                    ValueList temp = new ValueList();
                    temp.Id = 0;
                    temp.Value = "Tampilkan data untuk semua kelas";

                    _kelasDataSource.Insert(0, temp);
                }

                cmbKelas.DataSource = _kelasDataSource;
                cmbKelas.DisplayMember = "Value";
                cmbKelas.ValueMember = "Id";
            }
        }

        public List<ValueList> Siswa
        {
            get
            {
                return _siswaDataSource;
            }
            set
            {
                _siswaDataSource = value;
                if (_siswaDataSource.Count > 0)
                {
                    ValueList temp = new ValueList();
                    temp.Id = 0;
                    temp.Value = "Tampilkan data untuk semua siswa";

                    _siswaDataSource.Insert(0, temp);
                }

                cmbSiswa.DataSource = _siswaDataSource;
                cmbSiswa.DisplayMember = "Value";
                cmbSiswa.ValueMember = "Id";
            }
        }

        public int SelectedKelas
        {
            get 
            {
                ValueList selectedVal = cmbKelas.SelectedItem as ValueList;

                int result = (selectedVal != null) ? (int)selectedVal.Id : 0;

                return result; 
            }
        }

        public int SelectedSiswa
        {
            get
            {
                ValueList selectedVal = cmbSiswa.SelectedItem as ValueList;

                int result = (selectedVal != null) ? (int)selectedVal.Id : 0;

                return result;
            }
        }

        public ReportPenerimaanView()
        {
            InitializeComponent();
            cmbKelas.SelectedIndexChanged += cmbKelas_SelectedIndexChanged;
        }

        void cmbKelas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CmbKelasSelected != null)
            {
                CmbKelasSelected();
            }
        }        

        public DateTime SelectedBulan
        {
            get 
            {
                DateTime result = new DateTime(ToDate.Value.Year, ToDate.Value.Month,
                    DateTime.DaysInMonth(ToDate.Value.Year, ToDate.Value.Month));
                return result;
            }
        }

        public void ShowRPP(List<PenerimaanHistoryModel> p_Model)
        {
            ReportParameter[] parameters = new ReportParameter[2];

            parameters[0] = new ReportParameter("ToDate", SelectedBulan.ToString("dd MMMM yyyy"));
            parameters[1] = new ReportParameter("TahunAjaran", AppVariable.Instance.GetTahunAjaranAktif());
            
            ReportDataSource datasource = new ReportDataSource("PenerimaanDataset", p_Model);
            

            rptKasDetail.LocalReport.DataSources.Clear();
            rptKasDetail.LocalReport.SetParameters(parameters);
            rptKasDetail.LocalReport.DataSources.Add(datasource);
            rptKasDetail.RefreshReport();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (BtnOkClicked != null)
            {
                BtnOkClicked();
            }
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            if (BtnTutupClicked != null)
            {
                BtnTutupClicked();
            }
        }

        private void RepRekapPPView_Load(object sender, EventArgs e)
        {
            //this.rptKasDetail.RefreshReport();
            //this.rptKasDetail.RefreshReport();
        }
    }
}
