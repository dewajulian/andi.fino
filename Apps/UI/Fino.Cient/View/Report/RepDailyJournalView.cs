﻿using Fino.View;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Lib.Core.Settings;
using Fino.Model;

namespace Fino.Cient.Report
{
    public partial class RepDailyJournalView : BaseForm, IRepDailyJournalView
    {
        public event Action BtnOkClicked;
        public event Action BtnTutupClicked;

        public RepDailyJournalView()
        {
            InitializeComponent();
        }        

        public DateTime FromDate
        {
            get
            {
                return dtFrom.Value;
            }
            set
            {
                dtFrom.Value = value;
            }
        }

        public DateTime ToDate
        {
            get
            {
                return dtTo.Value;
            }
            set
            {
                dtTo.Value = value;
            }
        }

        public void ShowRPP(List<DailyJournalModel> p_Model)
        {
            ReportParameter[] parameters = new ReportParameter[2];
            parameters[0] = new ReportParameter("FromDate", FromDate.ToString("dd/MM/yyyy"));            
            parameters[1] = new ReportParameter("ToDate", ToDate.ToString("dd/MM/yyyy"));

            ReportDataSource datasource = new ReportDataSource("DataSetDailyJournal", p_Model);

            rptRekapPP.LocalReport.DataSources.Clear();
            rptRekapPP.LocalReport.SetParameters(parameters);
            rptRekapPP.LocalReport.DataSources.Add(datasource);
            rptRekapPP.RefreshReport();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (BtnOkClicked != null)
            {
                BtnOkClicked();
            }
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            if (BtnTutupClicked != null)
            {
                BtnTutupClicked();
            }
        }

        private void RepRekapPPView_Load(object sender, EventArgs e)
        {

        }        
    }
}
