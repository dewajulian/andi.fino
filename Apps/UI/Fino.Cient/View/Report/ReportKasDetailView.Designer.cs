﻿namespace Fino.Cient.Report
{
    partial class ReportKasDetailView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportKasDetailView));
            this.LaporanKasDetailModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lblDari = new System.Windows.Forms.Label();
            this.btnTutup = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbSiswa = new System.Windows.Forms.ComboBox();
            this.cmbKelas = new System.Windows.Forms.ComboBox();
            this.ToDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rptKasDetail = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.LaporanKasDetailModelBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // LaporanKasDetailModelBindingSource
            // 
            this.LaporanKasDetailModelBindingSource.DataSource = typeof(Fino.Model.LaporanKasDetailModel);
            // 
            // lblDari
            // 
            this.lblDari.AutoSize = true;
            this.lblDari.Location = new System.Drawing.Point(5, 16);
            this.lblDari.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDari.Name = "lblDari";
            this.lblDari.Size = new System.Drawing.Size(53, 17);
            this.lblDari.TabIndex = 0;
            this.lblDari.Text = "Bulan:";
            // 
            // btnTutup
            // 
            this.btnTutup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(484, 12);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(4);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(100, 28);
            this.btnTutup.TabIndex = 2;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            this.btnTutup.Click += new System.EventHandler(this.btnTutup_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(375, 11);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(100, 28);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmbSiswa);
            this.panel1.Controls.Add(this.cmbKelas);
            this.panel1.Controls.Add(this.ToDate);
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblDari);
            this.panel1.Controls.Add(this.btnTutup);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(593, 113);
            this.panel1.TabIndex = 3;
            // 
            // cmbSiswa
            // 
            this.cmbSiswa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSiswa.FormattingEnabled = true;
            this.cmbSiswa.Location = new System.Drawing.Point(65, 74);
            this.cmbSiswa.Name = "cmbSiswa";
            this.cmbSiswa.Size = new System.Drawing.Size(287, 24);
            this.cmbSiswa.TabIndex = 4;
            // 
            // cmbKelas
            // 
            this.cmbKelas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbKelas.FormattingEnabled = true;
            this.cmbKelas.Location = new System.Drawing.Point(65, 44);
            this.cmbKelas.Name = "cmbKelas";
            this.cmbKelas.Size = new System.Drawing.Size(287, 24);
            this.cmbKelas.TabIndex = 4;
            // 
            // ToDate
            // 
            this.ToDate.CustomFormat = "MMMM yyyy";
            this.ToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ToDate.Location = new System.Drawing.Point(65, 14);
            this.ToDate.Name = "ToDate";
            this.ToDate.Size = new System.Drawing.Size(157, 24);
            this.ToDate.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 77);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Siswa:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 47);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kelas:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rptKasDetail);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 113);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(593, 210);
            this.panel2.TabIndex = 4;
            // 
            // rptKasDetail
            // 
            this.rptKasDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "LaporanKasDetail";
            reportDataSource1.Value = this.LaporanKasDetailModelBindingSource;
            this.rptKasDetail.LocalReport.DataSources.Add(reportDataSource1);
            this.rptKasDetail.LocalReport.ReportEmbeddedResource = "Fino.Cient.View.Report.LaporanKasDetail.rdlc";
            this.rptKasDetail.Location = new System.Drawing.Point(0, 0);
            this.rptKasDetail.Name = "rptKasDetail";
            this.rptKasDetail.Size = new System.Drawing.Size(593, 210);
            this.rptKasDetail.TabIndex = 0;
            // 
            // ReportKasDetailView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 323);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ReportKasDetailView";
            this.Text = "Laporan Tunggakan Siswa";
            this.Load += new System.EventHandler(this.RepRekapPPView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LaporanKasDetailModelBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDari;
        private System.Windows.Forms.Button btnTutup;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.BindingSource LaporanKasDetailModelBindingSource;
        private Microsoft.Reporting.WinForms.ReportViewer rptKasDetail;
        private System.Windows.Forms.DateTimePicker ToDate;
        private System.Windows.Forms.ComboBox cmbSiswa;
        private System.Windows.Forms.ComboBox cmbKelas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}