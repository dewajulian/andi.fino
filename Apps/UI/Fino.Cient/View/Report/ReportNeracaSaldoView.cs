﻿using Fino.View;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.Lib.Core.Settings;
namespace Fino.Cient.Report
{
    public partial class ReportNeracaSaldoView : BaseForm, IReportNeracaSaldoView
    {
        public event Action BtnOkClicked;
        public event Action BtnTutupClicked;

        public ReportNeracaSaldoView()
        {
            InitializeComponent();
        }
        public int PeriodId
        {
            get
            {
                if (cboPeriode.SelectedValue == null)
                {
                    return 0;
                }
                else
                {
                    return (int)cboPeriode.SelectedValue;
                }
            }
        }


        object _PeriodDataSource;
        public object PeriodDataSource
        {
            get
            {
                return _PeriodDataSource;
            }
            set
            {
                _PeriodDataSource = value;
                cboPeriode.DataSource = _PeriodDataSource;
                cboPeriode.DisplayMember = "Value";
                cboPeriode.ValueMember = "Id";
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (BtnOkClicked != null)
            {
                BtnOkClicked();
            }
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            if (BtnTutupClicked != null)
            {
                BtnTutupClicked();
            }
        }
        
        public void ShowRPP(List<NeracaSaldoModel> p_Model)
        {
            ReportParameter[] parameters = new ReportParameter[1];

            parameters[0] = new ReportParameter("TahunAjaran", AppVariable.Instance.GetTahunAjaranAktif());

            ReportDataSource datasource = new ReportDataSource("NeracaSaldo", p_Model);
            rptSaldo.LocalReport.DataSources.Clear();
            rptSaldo.LocalReport.SetParameters(parameters);
            rptSaldo.LocalReport.DataSources.Add(datasource);
            rptSaldo.RefreshReport();
        }

        private void ReportNeracaSaldoView_Load(object sender, EventArgs e)
        {

        }

    }
}
