﻿using Fino.View;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.Lib.Core.Settings;

namespace Fino.Cient.Report
{
    public partial class ReportSaldoView : BaseForm, IReportSaldoView
    {
        public event Action BtnOkClicked;
        public event Action BtnTutupClicked;

        public ReportSaldoView()
        {
            InitializeComponent();
        }        

        public DateTime SelectedBulan
        {
            get 
            {
                DateTime result = new DateTime(ToDate.Value.Year, ToDate.Value.Month,
                    DateTime.DaysInMonth(ToDate.Value.Year, ToDate.Value.Month));
                return result;
            }
        }

        public void ShowRPP(List<LaporanSaldoModel> p_Model)
        {
            ReportParameter[] parameters = new ReportParameter[2];

            parameters[0] = new ReportParameter("ToDate", SelectedBulan.ToString("dd MMMM yyyy"));
            parameters[1] = new ReportParameter("TahunAjaran", AppVariable.Instance.GetTahunAjaranAktif());

            ReportDataSource datasource = new ReportDataSource("LaporanSaldo", p_Model);
            rptSaldo.LocalReport.DataSources.Clear();
            rptSaldo.LocalReport.SetParameters(parameters);
            rptSaldo.LocalReport.DataSources.Add(datasource);
            rptSaldo.RefreshReport();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (BtnOkClicked != null)
            {
                BtnOkClicked();
            }
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            if (BtnTutupClicked != null)
            {
                BtnTutupClicked();
            }
        }

        private void RepRekapPPView_Load(object sender, EventArgs e)
        {

            //this.rptSaldo.RefreshReport();
            //this.rptSaldo.RefreshReport();
        }        
    }
}
