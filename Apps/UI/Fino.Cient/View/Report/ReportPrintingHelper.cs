﻿using Fino.Lib.Core;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;

namespace Fino.Lib.Win32
{
    public class ReportPrintingHelper
    {
        private int _currentPageIndex;
        private IList<Stream> _streams;

        #region Singleton
        private static readonly Lazy<ReportPrintingHelper> lazy = new Lazy<ReportPrintingHelper>(() => new ReportPrintingHelper());

        public static ReportPrintingHelper Instance { get { return lazy.Value; } }

        private ReportPrintingHelper() { }
        #endregion

        // Routine to provide to the report renderer, in order to
        //    save an image for each page of the report.
        private Stream CreateStream(string name,
          string fileNameExtension, Encoding encoding,
          string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            _streams.Add(stream);
            return stream;
        }

        // Export the given report as an EMF (Enhanced Metafile) file.
        public void Export(LocalReport report)
        {
            string deviceInfo =
              @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                <PageWidth>8.3in</PageWidth>
                <PageHeight>5.8in</PageHeight>
                <MarginTop>0.25in</MarginTop>
                <MarginLeft>0.25in</MarginLeft>
                <MarginRight>0.25in</MarginRight>
                <MarginBottom>0.25in</MarginBottom>
            </DeviceInfo>";
            Warning[] warnings;
            _streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream,
               out warnings);
            foreach (Stream stream in _streams)
                stream.Position = 0;
        }

        // Handler for PrintPageEvents
        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new
               Metafile(_streams[_currentPageIndex]);

            // Adjust rectangular area with printer margins.
            Rectangle adjustedRect = new Rectangle(
                ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                ev.PageBounds.Width,
                ev.PageBounds.Height);

            // Draw a white background for the report
            ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

            // Draw the report content
            ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            _currentPageIndex++;
            ev.HasMorePages = (_currentPageIndex < _streams.Count);
        }

        public void Print()
        {
            if (_streams == null || _streams.Count == 0)
                throw new NoStreamToPrintException();
            PrintDocument printDoc = new PrintDocument();

            PaperSize pkCustomSize = new System.Drawing.Printing.PaperSize("Custom Paper Size", 850, 550);
            printDoc.DefaultPageSettings.PaperSize = pkCustomSize;
            printDoc.DefaultPageSettings.Margins.Top = 0;
            printDoc.DefaultPageSettings.Margins.Bottom = 0;
            printDoc.DefaultPageSettings.Margins.Left = 0;
            printDoc.DefaultPageSettings.Margins.Right = 0;
            printDoc.DefaultPageSettings.Landscape = false;

            printDoc.PrinterSettings.DefaultPageSettings.PaperSize = printDoc.DefaultPageSettings.PaperSize;
            printDoc.PrinterSettings.DefaultPageSettings.Margins = printDoc.DefaultPageSettings.Margins;

            if (!printDoc.PrinterSettings.IsValid)
            {
                throw new CannotFindDefaultPrinterException();
            }
            else
            {
                printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                _currentPageIndex = 0;
                printDoc.Print();
            }
        }
    }

    public class NoStreamToPrintException : Exception
    {
        public NoStreamToPrintException() : base(AppResource.EX_NOSTREAM_PRINTING)
        {
        }
    }

    public class CannotFindDefaultPrinterException : Exception
    {
        public CannotFindDefaultPrinterException() : base(AppResource.EX_DEFAULTPRINT_PRINTING)
        {
        }
    }
}
