﻿using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Repository;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class SiswaExplorerView : BaseForm, ISiswaExplorerView
    {
        #region Private Porperties

        private DataTable _dataTableSiswa;

        #endregion Private Porperties

        #region Public Properties



        #endregion Public Properties

        #region Constructors

        public SiswaExplorerView()
        {
            InitializeComponent();

            InitializeComboBox();
            InitializeDataSource();
            InitializeDataGridView();

            this.btnCari.Click += btnCari_Click;
            this.btnTutup.Click += btnTutup_Click;
        }

        #endregion Constructors

        #region Private Methods

        private void InitializeDataSource()
        {
            _dataTableSiswa = new DataTable();

            _dataTableSiswa.Columns.Add(new DataColumn("SelectionCol", typeof(bool)) { Caption = "" });
            _dataTableSiswa.Columns.Add(new DataColumn("NoIndukCol", typeof(string)) { Caption = "No Induk" });
            _dataTableSiswa.Columns.Add(new DataColumn("NamaCol", typeof(string)) { Caption = "Nama" });
            _dataTableSiswa.Columns.Add(new DataColumn("TglMasukCol", typeof(DateTime)) { Caption = "Tgl Masuk" });
            _dataTableSiswa.Columns.Add(new DataColumn("KelasCol", typeof(string)) { Caption = "Kelas" });
            _dataTableSiswa.Columns.Add(new DataColumn("SiswaPindahanCol", typeof(bool)) { Caption = "Siswa Pindahan" });
            _dataTableSiswa.Columns.Add(new DataColumn("RekeningTabunganCol", typeof(string)) { Caption = "Rekening Tabungan" });
            _dataTableSiswa.Columns.Add(new DataColumn("TunggakanCol", typeof(string)) { Caption = "Tunggakan" });
            _dataTableSiswa.Columns.Add(new DataColumn("KeteranganCol", typeof(Image)) { Caption = "Keterangan",  });

            IRefSiswaRepository repo = new RefSiswaRepository();
            var allRefSiswa = repo.GetAllRefSiswaForSiswaExplorer();
            if (allRefSiswa != null)
            {
                foreach(var refSiswa in allRefSiswa)
                {
                    _dataTableSiswa.Rows.Add(
                        false,
                        refSiswa.SiswaCode,
                        refSiswa.NamaSiswa,
                        refSiswa.TglMasuk,
                        refSiswa.Kelas,
                        refSiswa.IsPindahan,
                        refSiswa.RekeningTabungan,
                        GetTunggakanString(refSiswa.Tunggakan),
                        GetLayananImage(refSiswa.KodeLayanan));
                }
            }

            gvSiswaExplorer.DataSource = _dataTableSiswa;
            gvSiswaExplorer.Columns["TglMasukCol"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            gvSiswaExplorer.Columns["TglMasukCol"].DefaultCellStyle.Format = "d-MMM-yy";
            gvSiswaExplorer.Columns["KelasCol"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gvSiswaExplorer.Columns["TunggakanCol"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gvSiswaExplorer.Columns["KeteranganCol"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }
        
        private void InitializeDataGridView()
        {
            this.gvSiswaExplorer.CellPainting += gvSiswaExplorer_CellPainting;

            foreach (DataGridViewColumn col in gvSiswaExplorer.Columns)
            {
                if (!string.IsNullOrEmpty(_dataTableSiswa.Columns[col.HeaderText].Caption))
                    col.ReadOnly = true;
                col.HeaderText = _dataTableSiswa.Columns[col.HeaderText].Caption;
            }

            gvSiswaExplorer.Columns[0].Frozen = true;
            gvSiswaExplorer.Columns[0].Width = 25;
            gvSiswaExplorer.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            gvSiswaExplorer.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void InitializeComboBox()
        {
            cmbCariKelas.Items.Add("Semua Kelas");

            RefKelasRepository repo = new RefKelasRepository();
            var allKelas = repo.SelectAllKelas();

            if (allKelas != null)
            {
                foreach (var kelas in allKelas)
                {
                    cmbCariKelas.Items.Add(kelas.nama);
                }
            }

            cmbCariKelas.SelectedIndex = 0;
        }

        private string GetTunggakanString(double tunggakan)
        {
            if (tunggakan > 0)
            {
                if (tunggakan <= 30)
                {
                    return "<30";
                }
                else if (tunggakan <= 60)
                {
                    return "<60";
                }
                else if (tunggakan <= 90)
                {
                    return "<90";
                }
                else if (tunggakan <= 120)
                {
                    return "<120";
                }
                else
                {
                    return ">120";
                }
            }

            return "";
        }

        private Color GetTunggakanColor(string tunggakan)
        {
            if (tunggakan == "<30")
            {
                return Color.LightGreen;
            }
            else if (tunggakan == "<60")
            {
                return Color.Green;
            }
            else if (tunggakan == "<90")
            {
                return Color.Yellow;
            }
            else if (tunggakan == "<120")
            {
                return Color.Orange;
            }
            else if (tunggakan == ">120")
            {
                return Color.Red;
            }

            return Color.White;
        }

        private Image GetLayananImage(List<string> kodeLayanan)
        {
            if (kodeLayanan.Count > 0)
            {
                int layananImageWidth = kodeLayanan.Count * Constants.LayananImageWidth;
                int layananImageSpacing = (kodeLayanan.Count - 1) * Constants.LayananImageSpacing;
                Image layananImage = new Bitmap(layananImageWidth + layananImageSpacing, Constants.LayananImageWidth);

                using (Graphics g = Graphics.FromImage(layananImage))
                {
                    int idx = 0;
                    foreach (var kode in kodeLayanan)
                    {
                        if (Constants.Instance.LayananImage.ContainsKey(kode))
                        {
                            Point pt = new Point((idx * Constants.LayananImageWidth) + (idx * Constants.LayananImageSpacing), idx);
                            g.DrawImage(Constants.Instance.LayananImage[kode], pt);
                        }

                        idx++;
                    }
                }

                return layananImage;
            }

            return new Bitmap(Constants.LayananImageWidth, Constants.LayananImageWidth); ;
        }

        #endregion Private Methods

        #region Control Events

        private void gvSiswaExplorer_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex > -1)
            {
                switch (e.ColumnIndex)
                {
                    case 7:
                        gvSiswaExplorer[e.ColumnIndex, e.RowIndex].Style.BackColor = GetTunggakanColor(gvSiswaExplorer[e.ColumnIndex, e.RowIndex].Value.ToString());
                        break;
                }
            }
        }

        private void btnCari_Click(object sender, EventArgs e)
        {
            string rowFilter = "(NoIndukCol LIKE '%{0}%' ";
            rowFilter += "OR NamaCol LIKE '%{1}%') ";

            if (cmbCariKelas.Text != "Semua Kelas")
                rowFilter += "AND KelasCol = '{2}'";

            _dataTableSiswa.DefaultView.RowFilter = string.Format(rowFilter, txtCariKelas.Text, txtCariKelas.Text, cmbCariKelas.Text);
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion Control Events

    }
}
