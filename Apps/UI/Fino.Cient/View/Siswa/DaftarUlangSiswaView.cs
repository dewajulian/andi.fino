﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fino.BusinessLogic;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.Presenter;
using Fino.View;

namespace Fino.Cient
{
    public partial class DaftarUlangSiswaView : BaseForm, IDaftarUlangSiswaView
    {
        public DaftarUlangSiswaView()
        {
            InitializeComponent();

            InitialiseSelectSiswa();

            pencarianSiswaControlView1.HideControl += pencarianSiswaControlView1_HideControl;
        }

        public event Action SaveButtonClicked;

        public event Action SaveNextButtonClicked;

        public event Action NewDataButtonClicked;

        public event Action PotonganButtonClicked;

        public event Action SearchSiswaButtonClicked;

        public event Action SiswaPindahanCheckedChanged;

        public event Action CloseButtonClicked;

        public object KelasDataSource { get; set; }

        public DaftarUlangSiswaModel ViewModel { get; set; }

        public object OptionDataSource { get; set; }

        public bool IsSearchMode { get; set; }

        public event Action CariSiswaTextKeypressed;

        public event Action SelectingSiswaDone;

        public string Keyword
        {
            get { return txtCari.Text.Trim(); }
        }

        public IPencarianSiswaView PencarianSiswaView
        {
            get { return pencarianSiswaControlView1; }
        }

        public int SiswaId { get; set; }

        public string NoInduk
        {
            get
            {
                return txtNoInduk.Text.Trim();
            }
            set
            {
                txtNoInduk.Text = value.Trim();
            }
        }

        public string NamaSiswa
        {
            get
            {
                return txtNamaSiswa.Text.Trim();
            }
            set
            {
                txtNamaSiswa.Text = value.Trim();
            }
        }

        public bool JenisKelamin
        {
            get
            {
                return rbLaki.Checked;
            }
            set
            {
                rbLaki.Checked = value;
            }
        }

        public int KelasId
        {
            get
            {
                return Int32.Parse(cboKelas.SelectedValue.ToString());
            }
            set
            {
                cboKelas.SelectedValue = value.ToString();
            }
        }

        public DateTime MulaiTanggal
        {
            get
            {
                return dtpMulaiTanggal.Value.Date;
            }
            set
            {
                dtpMulaiTanggal.Value = value;
            }
        }

        public int TahunAjaranId { get; set; }

        public string TahunAjaranName
        {
            get
            {
                return txtTahunAjaran.Text.Trim();
            }
            set
            {
                txtTahunAjaran.Text = value;
            }
        }


        public bool IsPindahan
        {
            get
            {
                return chkPindahan.Checked;
            }
            set
            {
                chkPindahan.Checked = value;

            }
        }

        public int OpsiTingkat { get; set; }

        private void btnPotongan_Click(object sender, EventArgs e)
        {
            if (PotonganButtonClicked != null)
            {
                PotonganButtonClicked();
            }
        }



        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveButtonClicked != null)
            {
                SaveButtonClicked();
            }
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            if (SaveNextButtonClicked != null)
            {
                SaveNextButtonClicked();
            }
        }


        public void ClearView()
        {
            SetBinding();

        }

        public void SetOptionEnable(bool enabled)
        {
            cboOpsi.Enabled = enabled;
        }

        public void ShowErrorMessage(string message, string caption)
        {
            MessageBox.Show(this, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void ShowInformation(string message, string caption)
        {
            MessageBox.Show(this, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        private void pencarianSiswaControlView1_HideControl()
        {
            if (SelectingSiswaDone != null && pencarianSiswaControlView1.SelectedSiswa != null)
            {
                SelectingSiswaDone();
            }
        }

        public void ChangeEditable(bool enabled)
        {
            foreach (var c in groupBox2.Controls)
            {
                var ctype = c.GetType();
                if (ctype.Equals(typeof(TextBox)))
                {
                    if (!((TextBox)c).Name.Equals("txtTahunAjaran"))
                    {
                        ((TextBox)c).ReadOnly = !enabled;
                    }
                }
                if (ctype.Equals(typeof(RadioButton)))
                {
                    ((RadioButton)c).Enabled = enabled;
                }
                if (ctype.Equals(typeof(ComboBox)))
                {
                    ((ComboBox)c).Enabled = enabled;
                }
                if (ctype.Equals(typeof(DateTimePicker)))
                {
                    ((DateTimePicker)c).Enabled = enabled;
                }
                if (ctype.Equals(typeof(CheckBox)))
                {
                    ((CheckBox)c).Enabled = false;
                }
            }

            btnSaveNew.Enabled = enabled;
            btnSave.Enabled = enabled;
        }

        public void SetBinding()
        {
            try
            {
                txtNoInduk.DataBindings.Clear();
                txtNoInduk.DataBindings.Add("Text", ViewModel, DaftarUlangSiswaModel.CodePropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                txtNamaSiswa.DataBindings.Clear();
                txtNamaSiswa.DataBindings.Add("Text", ViewModel, DaftarUlangSiswaModel.NamaPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                cboKelas.DataSource = KelasDataSource;
                cboKelas.DisplayMember = "Value";
                cboKelas.ValueMember = "Id";
                cboKelas.DataBindings.Clear();
                cboKelas.DataBindings.Add("SelectedValue", ViewModel, DaftarUlangSiswaModel.KelasIdPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                rbLaki.DataBindings.Clear();
                rbLaki.DataBindings.Add("Checked", ViewModel, DaftarUlangSiswaModel.JKelaminPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                dtpMulaiTanggal.DataBindings.Clear();
                dtpMulaiTanggal.DataBindings.Add("Value", ViewModel, DaftarUlangSiswaModel.MulaiTanggalPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                TahunAjaranId = ViewModel.TahunAjaran_Id;

                txtTahunAjaran.DataBindings.Clear();
                txtTahunAjaran.DataBindings.Add("Text", ViewModel, DaftarUlangSiswaModel.TahunAjaranNamaPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                chkPindahan.DataBindings.Clear();
                chkPindahan.DataBindings.Add("Checked", ViewModel, DaftarUlangSiswaModel.IsPindahanPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                cboOpsi.DataSource = OptionDataSource;
                cboOpsi.DisplayMember = "Value";
                cboOpsi.ValueMember = "Id";
                cboOpsi.DataBindings.Clear();
                cboOpsi.DataBindings.Add("SelectedValue", ViewModel, DaftarUlangSiswaModel.OptionTingkat_IdPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.Message, "Error Binding");
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (NewDataButtonClicked != null)
            {
                NewDataButtonClicked();
            }
        }

        private void chkPindahan_CheckedChanged(object sender, EventArgs e)
        {
            if (SiswaPindahanCheckedChanged != null)
            {
                SiswaPindahanCheckedChanged();
            }
        }



        public void ShowSiswaToEdit(int pSiswaId)
        {
            throw new NotImplementedException();
        }

        public void SetSearchMode()
        {
            ChangeEditable(false);
            txtCari.ReadOnly = false;
            IsSearchMode = true;
            btnNew.Enabled = !IsSearchMode;
            btnSearchSiswa.Visible = !IsSearchMode;
            txtCari.Focus();
        }

        public void SetEntryMode()
        {
            ChangeEditable(true);
            IsSearchMode = false;
            btnNew.Enabled = !IsSearchMode;
            btnSearchSiswa.Enabled = IsSearchMode;
            btnSearchSiswa.Visible = IsSearchMode;
            txtNoInduk.ReadOnly = true;
            txtNamaSiswa.ReadOnly = true;
            txtCari.ReadOnly = true;
            rbLaki.Enabled = false;
            rbPerempuan.Enabled = false;
        }

        public void SetViewMode()
        {
            ChangeEditable(false);
            btnNew.Enabled = !IsSearchMode;
        }

        private void btnSearchSiswa_Click(object sender, EventArgs e)
        {
            if (SearchSiswaButtonClicked != null)
            {
                SearchSiswaButtonClicked();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (CloseButtonClicked!=null)
            {
                CloseButtonClicked();
            }
        }

        private void txtCari_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter && CariSiswaTextKeypressed != null)
            {
                CariSiswaTextKeypressed();
            }
        }

        #region Initialise other view
        private void InitialiseSelectSiswa()
        {
            SiswaProcess processSiswa = new SiswaProcess();
            PencarianSiswaPresenter presenterSiswa = new PencarianSiswaPresenter(pencarianSiswaControlView1, processSiswa);
        }

        #endregion Initialise other view
        
    }
}
