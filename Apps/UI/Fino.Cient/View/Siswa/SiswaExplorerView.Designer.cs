﻿namespace Fino.Cient
{
    partial class SiswaExplorerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SiswaExplorerView));
            this.gvSiswaExplorer = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbCariKelas = new System.Windows.Forms.ComboBox();
            this.txtCariKelas = new System.Windows.Forms.TextBox();
            this.btnCari = new System.Windows.Forms.Button();
            this.btnTutup = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gvSiswaExplorer)).BeginInit();
            this.SuspendLayout();
            // 
            // gvSiswaExplorer
            // 
            this.gvSiswaExplorer.AllowUserToAddRows = false;
            this.gvSiswaExplorer.AllowUserToDeleteRows = false;
            this.gvSiswaExplorer.AllowUserToResizeRows = false;
            this.gvSiswaExplorer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvSiswaExplorer.BackgroundColor = System.Drawing.Color.White;
            this.gvSiswaExplorer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvSiswaExplorer.Location = new System.Drawing.Point(12, 53);
            this.gvSiswaExplorer.Name = "gvSiswaExplorer";
            this.gvSiswaExplorer.RowHeadersVisible = false;
            this.gvSiswaExplorer.Size = new System.Drawing.Size(619, 324);
            this.gvSiswaExplorer.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cari";
            // 
            // cmbCariKelas
            // 
            this.cmbCariKelas.FormattingEnabled = true;
            this.cmbCariKelas.Location = new System.Drawing.Point(50, 19);
            this.cmbCariKelas.Name = "cmbCariKelas";
            this.cmbCariKelas.Size = new System.Drawing.Size(121, 24);
            this.cmbCariKelas.TabIndex = 2;
            // 
            // txtCariKelas
            // 
            this.txtCariKelas.Location = new System.Drawing.Point(177, 19);
            this.txtCariKelas.Name = "txtCariKelas";
            this.txtCariKelas.Size = new System.Drawing.Size(235, 24);
            this.txtCariKelas.TabIndex = 3;
            // 
            // btnCari
            // 
            this.btnCari.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnCari.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCari.ForeColor = System.Drawing.Color.White;
            this.btnCari.Location = new System.Drawing.Point(418, 19);
            this.btnCari.Name = "btnCari";
            this.btnCari.Size = new System.Drawing.Size(102, 24);
            this.btnCari.TabIndex = 4;
            this.btnCari.Text = "Cari";
            this.btnCari.UseVisualStyleBackColor = false;
            // 
            // btnTutup
            // 
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(530, 19);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(4);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(100, 24);
            this.btnTutup.TabIndex = 6;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            // 
            // SiswaExplorerView
            // 
            this.AcceptButton = this.btnCari;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 389);
            this.Controls.Add(this.btnTutup);
            this.Controls.Add(this.btnCari);
            this.Controls.Add(this.txtCariKelas);
            this.Controls.Add(this.cmbCariKelas);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gvSiswaExplorer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SiswaExplorerView";
            this.Text = "Pencarian Siswa";
            ((System.ComponentModel.ISupportInitialize)(this.gvSiswaExplorer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gvSiswaExplorer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbCariKelas;
        private System.Windows.Forms.TextBox txtCariKelas;
        private System.Windows.Forms.Button btnCari;
        private System.Windows.Forms.Button btnTutup;
    }
}