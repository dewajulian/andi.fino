﻿using Fino.BusinessLogic;
using Fino.Cient.Startup;
using Fino.Lib.Core;
using Fino.Lib.Core.Util;
using Fino.Lib.Win32;
using Fino.Presenter;
using Fino.View;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class MainFormView : BaseForm, IMainFormView
    {
        #region Actions

        // public event Action ManageSiswaMenuClicked;
        public event Action KonfigurasiBiayaClicked;
        public event Action PendaftaranSiswaBaruClicked;
        //public event Action ShowGrupBiayaClicked;
        public event Action ReportRPPClicked;
        public event Action SiswaExplorerClicked;
        public event Action UbahKataSandiClicked;
        public event Action PengaturanPenggunaClicked;

        #endregion Actions

        #region Constructors


        public MainFormView()
        {
            InitializeComponent();

            this.ubahKataSandiToolStripMenuItem.Click += ubahKataSandiToolStripMenuItem_Click;
            this.pengaturanPenggunaToolStripMenuItem.Click += pengaturanPenggunaToolStripMenuItem_Click;

            InitializeMenuListView();
        }

        #endregion Constructors

        #region Private Methods
        
        private void InitializeMenuListView()
        {
            MainMenuHelper.Instance.InitializeMainMenu(menuListView);

            menuListView.MouseClick += menuListView_MouseClick;
        }

        private void menuListView_MouseClick(object sender, MouseEventArgs e)
        {
            ListViewItem clickedItem = menuListView.GetItemAt(e.X, e.Y);

            if (clickedItem != null)
            {
                var menuItem = menuListView.GetItemAt(e.X, e.Y);

                if (menuItem != null)
                {
                    switch ((MainMenuEnum)menuItem.Tag)
                    {
                        case MainMenuEnum.PendaftaranSiswaBaru:
                            ViewLocator.Instance.DaftarSiswaBaruView.Show();
                            break;
                        case MainMenuEnum.JenisPenerimaan:
                            ViewLocator.Instance.RefBiayaView.Show();
                            break;
                        case MainMenuEnum.GrupPenerimaan:
                            ViewLocator.Instance.GrupBiayaView.Show();
                            break;
                        case MainMenuEnum.LaporanPiutang:
                            ViewLocator.Instance.ReportKasDetailView.Show();
                            break;
                        case MainMenuEnum.ReportRPP:
                            ViewLocator.Instance.ReportRekapPPView.Show();
                            break;
                        case MainMenuEnum.DaftarUlang:
                            ViewLocator.Instance.DaftarUlangSiswaView.Show();
                            break;
                        case MainMenuEnum.PenerimaanPelunasan:
                            ViewLocator.Instance.PembayaranBiayaView.Show();
                            break;
                        case MainMenuEnum.PotonganBiaya:
                            ViewLocator.Instance.PotonganBiayaView.Show();
                            break;
                        case MainMenuEnum.DaftarLayanan:
                            ViewLocator.Instance.DaftarLayanView.Show();
                            break;
                        case MainMenuEnum.SiswaExplorer:
                            ViewLocator.Instance.SiswaExplorerView.Show();
                            break;
                        case MainMenuEnum.BukaRekening:
                            ViewLocator.Instance.DaftarTabunganView.Show();
                            break;
                        case MainMenuEnum.SettingNilai:
                            ViewLocator.Instance.PengelolaanNilaiBiayaView.Show();
                            break;
                        case MainMenuEnum.Transaksi:
                            ViewLocator.Instance.MutasiTabunganView.Show();
                            break;
                        case MainMenuEnum.KodeAkun:
                            ViewLocator.Instance.RefAccountView.Show();
                            break;
                        case MainMenuEnum.PeriodeAkun:
                            ViewLocator.Instance.RefAccPeriodView.Show();
                            break;
                        case MainMenuEnum.PostingJurnal:
                            ViewLocator.Instance.PostingJournalView.Show();
                            break;
                        case MainMenuEnum.LaporanTabungan:
                            ViewLocator.Instance.ReportSaldoView.Show();
                            break;
                        case MainMenuEnum.LaporanAkuntansi:
                            ViewLocator.Instance.ReportNeracaSaldoView.Show();
                            break;
                        case MainMenuEnum.LaporanMutasi:
                            ViewLocator.Instance.ReportMutasiView.Show();
                            break;
                        case MainMenuEnum.CicilanBiaya:
                            ViewLocator.Instance.KonversiCicilanView.Show();
                            break;
                        case MainMenuEnum.RekapPengeluaran:
                            ViewLocator.Instance.RepRekapPengeluaranView.Show();
                            break;
                        case MainMenuEnum.JenisPengeluaran:
                            ViewLocator.Instance.RefPayableView.Show();
                            break;
                        case MainMenuEnum.LaporanPengeluaran:
                            ViewLocator.Instance.ReportPengeluaranView.Show();
                            break;
                        case MainMenuEnum.Pembayaran:
                            ViewLocator.Instance.PayablePaidView.Show();
                            break;
                        case MainMenuEnum.JurnalHarian:
                            ViewLocator.Instance.RepDailyJournalView.Show();
                            break;
                        case MainMenuEnum.SettingAkunJurnal:
                            ViewLocator.Instance.AccountClassSettingView.Show();
                            break;
                        case MainMenuEnum.PengelolaanKelas:
                            ViewLocator.Instance.RefKelasView.Show();
                            break;
                        case MainMenuEnum.RiwayatPenerimaan:
                            ViewLocator.Instance.ReportPenerimaanView.Show();
                            break;
                    }
                }
            }
        }

        private void pengaturanPenggunaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (PengaturanPenggunaClicked != null)
            {
                PengaturanPenggunaClicked();
            }
        }

        private void ubahKataSandiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (UbahKataSandiClicked != null)
            {
                UbahKataSandiClicked();
            }
        }

        #endregion Private Methods

        #region Public Methods

        public void ShowManageSiswaView()
        {
            ManageSiswaView msv = new ManageSiswaView();
            msv.MdiParent = this;
            ManageSiswaPresenter manageSiswaPresenter = new ManageSiswaPresenter(msv);

            msv.Show();
        }

        public bool CheckLogonStatus()
        {
            return ViewLocator.Instance.LogonView.LogonResult;
        }

        public void ShowKonfigurasiBiayaView()
        {
            ViewLocator.Instance.PosBiayaView.Show();
        }

        public void ShowGrupBiayaView()
        {
            ViewLocator.Instance.GrupBiayaView.Show();
        }

        public void ShowDaftarSiswaBaruView()
        {

        }

        public void ShowReportRPPView()
        {
            ViewLocator.Instance.ReportRekapPPView.Show();
        }

        public void ShowSiswaExplorerView()
        {
            ViewLocator.Instance.SiswaExplorerView.Show();
        }

        public void ShowUbahKataSandiView()
        {
            ViewLocator.Instance.UbahKataSandiView.IsRestorePassword = false;
            ViewLocator.Instance.UbahKataSandiView.Show();
        }

        public void ShowPengaturanPenggunaView()
        {
            ViewLocator.Instance.UserManagementView.Show();
        }

        #endregion Public Methods

    }
}
