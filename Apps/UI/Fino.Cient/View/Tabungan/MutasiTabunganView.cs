﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fino.BusinessLogic;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.Presenter;
using Fino.View;

namespace Fino.Cient
{
    public partial class MutasiTabunganView : BaseForm, IMutasiTabunganView
    {
        public MutasiTabunganView()
        {
            InitializeComponent();


            InitialiseSelectSiswa();

            pencarianSiswaControlView1.HideControl += pencarianSiswaControlView1_HideControl; ;
        }
        public event Action SaveButtonClicked;
        public event Action SaveNextButtonClicked;
        public event Action NewDataButtonClicked;
        public event Action SearchSiswaButtonClicked;
        public event Action CloseButtonClicked;
        public event Action CariSiswaTextKeypressed;
        public event Action SelectingSiswaDone;

        public MutasiTabunganModel ViewModel { get; set; }
        public bool IsSearchMode { get; set; }
        public int SiswaId { get; set; }


        public IPencarianSiswaView PencarianSiswaView
        {
            get { return pencarianSiswaControlView1; }
        }
        public string Keyword
        {
            get
            {
                return txtCari.Text.Trim();
            }
            set
            {
                txtCari.Text = value.Trim();
            }
        }

        object _JMutasiDatasource;
        public object JMutasiDatasource
        {
            get
            {
                return _JMutasiDatasource;
            }
            set
            {
                _JMutasiDatasource = value;
                cboOpsi.DataSource = _JMutasiDatasource;
                cboOpsi.DisplayMember = "Value";
                cboOpsi.ValueMember = "Id";
            }
        }

        public string NoRekening
        {
            get
            {
                return txtRekno.Text.Trim();
            }

            set
            {
                txtRekno.Text = value;
            }
        }
        public string NoInduk
        {
            get
            {
                return txtNoInduk.Text.Trim();
            }
            set
            {
                txtNoInduk.Text = value.Trim();
            }
        }

        public string NamaSiswa
        {
            get
            {
                return txtNamaSiswa.Text.Trim();
            }
            set
            {
                txtNamaSiswa.Text = value.Trim();
            }
        }

        public bool JenisKelamin
        {
            get
            {
                return rbLaki.Checked;
            }
            set
            {
                rbLaki.Checked = value;
            }
        }

        public double NilaiTransaksi
        {
            get
            {
                return (double)nmNilai.Value;
            }
            set
            {
                nmNilai.Value = (decimal)value;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveButtonClicked != null)
            {
                SaveButtonClicked();
            }
        }

        private void btnSaveNew_Click(object sender, EventArgs e)
        {
            if (SaveNextButtonClicked != null)
            {
                SaveNextButtonClicked();
            }
        }


        public void ClearView()
        {
            SetBinding();

        }



        private void pencarianSiswaControlView1_HideControl()
        {
            if (SelectingSiswaDone != null && pencarianSiswaControlView1.SelectedSiswa != null)
            {
                SelectingSiswaDone();
            }
        }


        public void ShowErrorMessage(string message, string caption)
        {
            MessageBox.Show(this, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void ShowInformation(string message, string caption)
        {
            MessageBox.Show(this, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        public void SetBinding()
        {
            try
            {
                txtRekno.DataBindings.Clear();
                txtRekno.DataBindings.Add("Text", ViewModel, MutasiTabunganModel.RekeningCodePropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                txtNoInduk.DataBindings.Clear();
                txtNoInduk.DataBindings.Add("Text", ViewModel, MutasiTabunganModel.SiswaCodePropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                txtNamaSiswa.DataBindings.Clear();
                txtNamaSiswa.DataBindings.Add("Text", ViewModel, MutasiTabunganModel.SiswaNamaPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                rbLaki.Checked = ViewModel.jkelamin;
                rbPerempuan.Checked = !ViewModel.jkelamin;

                nmNilai.DataBindings.Clear();
                nmNilai.DataBindings.Add("Value", ViewModel, MutasiTabunganModel.MutasiNilaiPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
                
                nmSaldo.DataBindings.Clear();
                nmSaldo.DataBindings.Add("Value", ViewModel, MutasiTabunganModel.SaldoPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);

                cboOpsi.DataBindings.Clear();
                cboOpsi.DataBindings.Add("SelectedValue", ViewModel, MutasiTabunganModel.JMutasiPropertyName, false, DataSourceUpdateMode.OnPropertyChanged);
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.Message, "Error Binding");
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (NewDataButtonClicked != null)
            {
                NewDataButtonClicked();
            }
        }


        public void ShowHeader()
        {
            throw new NotImplementedException();
        }

        public void SetSearchMode()
        {
            txtRekno.ReadOnly = false;
            txtCari.ReadOnly = false;
            ControlHelper.ChangeEditable(false, gbSiswa, new string[] { "btnSearchSiswa", "txtCari"});
            ControlHelper.ChangeEditable(false, gbTabungan);
            ControlHelper.ChangeEditable(false, this, new string[] { "btnClose" });

            IsSearchMode = true;
            btnNew.Enabled = !IsSearchMode;
            btnSearchSiswa.Visible = false;
        }

        public void SetEntryMode()
        {
            ControlHelper.ChangeEditable(false, gbSiswa);
            ControlHelper.ChangeEditable(true, gbTabungan, new string[] { "nmSaldo" });
            ControlHelper.ChangeEditable(true, this);
            IsSearchMode = false;
            btnNew.Enabled = !IsSearchMode;
            btnSearchSiswa.Visible = false;
        }

        public void SetViewMode()
        {
            SetBinding();
            ControlHelper.ChangeEditable(false, gbSiswa);
            ControlHelper.ChangeEditable(false, gbTabungan);
            ControlHelper.ChangeEditable(false, this, new string[] { "btnClose" });
            btnNew.Enabled = !IsSearchMode;
            btnSearchSiswa.Visible = false;
        }

        private void btnSearchSiswa_Click(object sender, EventArgs e)
        {
            if (SearchSiswaButtonClicked != null)
            {
                SearchSiswaButtonClicked();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (CloseButtonClicked != null)
            {
                CloseButtonClicked();
            }
        }

        private void txtCari_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter && CariSiswaTextKeypressed != null)
            {
                CariSiswaTextKeypressed();
            }
        }


        #region Initialise other view
        private void InitialiseSelectSiswa()
        {
            SiswaProcess processSiswa = new SiswaProcess();
            PencarianSiswaPresenter presenterSiswa = new PencarianSiswaPresenter(pencarianSiswaControlView1, processSiswa);
        }

        #endregion Initialise other view

    }
}
