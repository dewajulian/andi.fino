﻿namespace Fino.Cient
{
    partial class MutasiTabunganView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MutasiTabunganView));
            this.gbTabungan = new System.Windows.Forms.GroupBox();
            this.cboOpsi = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.nmNilai = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.nmSaldo = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.gbSiswa = new System.Windows.Forms.GroupBox();
            this.btnSearchSiswa = new System.Windows.Forms.Button();
            this.rbPerempuan = new System.Windows.Forms.RadioButton();
            this.rbLaki = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCari = new System.Windows.Forms.TextBox();
            this.txtNamaSiswa = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRekno = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNoInduk = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSaveNew = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.pencarianSiswaControlView1 = new Fino.Cient.Common.PencarianSiswaControlView();
            this.gbTabungan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNilai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSaldo)).BeginInit();
            this.gbSiswa.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbTabungan
            // 
            this.gbTabungan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTabungan.Controls.Add(this.cboOpsi);
            this.gbTabungan.Controls.Add(this.label7);
            this.gbTabungan.Controls.Add(this.nmNilai);
            this.gbTabungan.Controls.Add(this.label5);
            this.gbTabungan.Controls.Add(this.nmSaldo);
            this.gbTabungan.Controls.Add(this.label4);
            this.gbTabungan.Location = new System.Drawing.Point(13, 253);
            this.gbTabungan.Name = "gbTabungan";
            this.gbTabungan.Size = new System.Drawing.Size(664, 170);
            this.gbTabungan.TabIndex = 2;
            this.gbTabungan.TabStop = false;
            this.gbTabungan.Text = "Transaksi Tabungan";
            // 
            // cboOpsi
            // 
            this.cboOpsi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOpsi.FormattingEnabled = true;
            this.cboOpsi.Location = new System.Drawing.Point(188, 41);
            this.cboOpsi.Margin = new System.Windows.Forms.Padding(4);
            this.cboOpsi.Name = "cboOpsi";
            this.cboOpsi.Size = new System.Drawing.Size(188, 28);
            this.cboOpsi.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 44);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(147, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "Jenis Transaksi:";
            // 
            // nmNilai
            // 
            this.nmNilai.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmNilai.Location = new System.Drawing.Point(188, 110);
            this.nmNilai.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.nmNilai.Name = "nmNilai";
            this.nmNilai.Size = new System.Drawing.Size(188, 28);
            this.nmNilai.TabIndex = 5;
            this.nmNilai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nmNilai.ThousandsSeparator = true;
            this.nmNilai.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(89, 112);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Jumlah:";
            // 
            // nmSaldo
            // 
            this.nmSaldo.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmSaldo.Location = new System.Drawing.Point(188, 76);
            this.nmSaldo.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.nmSaldo.Name = "nmSaldo";
            this.nmSaldo.Size = new System.Drawing.Size(188, 28);
            this.nmSaldo.TabIndex = 3;
            this.nmSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nmSaldo.ThousandsSeparator = true;
            this.nmSaldo.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(103, 78);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Saldo:";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Location = new System.Drawing.Point(537, 494);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(136, 41);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Tutup";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // gbSiswa
            // 
            this.gbSiswa.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSiswa.Controls.Add(this.btnSearchSiswa);
            this.gbSiswa.Controls.Add(this.rbPerempuan);
            this.gbSiswa.Controls.Add(this.rbLaki);
            this.gbSiswa.Controls.Add(this.label3);
            this.gbSiswa.Controls.Add(this.txtCari);
            this.gbSiswa.Controls.Add(this.txtNamaSiswa);
            this.gbSiswa.Controls.Add(this.label2);
            this.gbSiswa.Controls.Add(this.txtRekno);
            this.gbSiswa.Controls.Add(this.label8);
            this.gbSiswa.Controls.Add(this.label6);
            this.gbSiswa.Controls.Add(this.txtNoInduk);
            this.gbSiswa.Controls.Add(this.label1);
            this.gbSiswa.Location = new System.Drawing.Point(13, 13);
            this.gbSiswa.Margin = new System.Windows.Forms.Padding(4);
            this.gbSiswa.Name = "gbSiswa";
            this.gbSiswa.Padding = new System.Windows.Forms.Padding(4);
            this.gbSiswa.Size = new System.Drawing.Size(663, 233);
            this.gbSiswa.TabIndex = 0;
            this.gbSiswa.TabStop = false;
            this.gbSiswa.Text = "Data Siswa";
            // 
            // btnSearchSiswa
            // 
            this.btnSearchSiswa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchSiswa.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchSiswa.Image")));
            this.btnSearchSiswa.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSearchSiswa.Location = new System.Drawing.Point(453, 32);
            this.btnSearchSiswa.Name = "btnSearchSiswa";
            this.btnSearchSiswa.Size = new System.Drawing.Size(63, 62);
            this.btnSearchSiswa.TabIndex = 2;
            this.btnSearchSiswa.Text = "Cari";
            this.btnSearchSiswa.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSearchSiswa.UseVisualStyleBackColor = true;
            this.btnSearchSiswa.Click += new System.EventHandler(this.btnSearchSiswa_Click);
            // 
            // rbPerempuan
            // 
            this.rbPerempuan.AutoSize = true;
            this.rbPerempuan.Location = new System.Drawing.Point(309, 189);
            this.rbPerempuan.Margin = new System.Windows.Forms.Padding(4);
            this.rbPerempuan.Name = "rbPerempuan";
            this.rbPerempuan.Size = new System.Drawing.Size(127, 24);
            this.rbPerempuan.TabIndex = 10;
            this.rbPerempuan.TabStop = true;
            this.rbPerempuan.Text = "Perempuan";
            this.rbPerempuan.UseVisualStyleBackColor = true;
            // 
            // rbLaki
            // 
            this.rbLaki.AutoSize = true;
            this.rbLaki.Location = new System.Drawing.Point(188, 189);
            this.rbLaki.Margin = new System.Windows.Forms.Padding(4);
            this.rbLaki.Name = "rbLaki";
            this.rbLaki.Size = new System.Drawing.Size(102, 24);
            this.rbLaki.TabIndex = 9;
            this.rbLaki.TabStop = true;
            this.rbLaki.Text = "Laki-laki";
            this.rbLaki.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(81, 191);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Kelamin:";
            // 
            // txtCari
            // 
            this.txtCari.Location = new System.Drawing.Point(188, 32);
            this.txtCari.Margin = new System.Windows.Forms.Padding(4);
            this.txtCari.MaxLength = 25;
            this.txtCari.Name = "txtCari";
            this.txtCari.Size = new System.Drawing.Size(252, 28);
            this.txtCari.TabIndex = 1;
            this.txtCari.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCari_KeyPress);
            // 
            // txtNamaSiswa
            // 
            this.txtNamaSiswa.Location = new System.Drawing.Point(188, 151);
            this.txtNamaSiswa.Margin = new System.Windows.Forms.Padding(4);
            this.txtNamaSiswa.MaxLength = 25;
            this.txtNamaSiswa.Name = "txtNamaSiswa";
            this.txtNamaSiswa.Size = new System.Drawing.Size(252, 28);
            this.txtNamaSiswa.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 155);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Nama Siswa:";
            // 
            // txtRekno
            // 
            this.txtRekno.Location = new System.Drawing.Point(188, 78);
            this.txtRekno.Margin = new System.Windows.Forms.Padding(4);
            this.txtRekno.MaxLength = 15;
            this.txtRekno.Name = "txtRekno";
            this.txtRekno.Size = new System.Drawing.Size(169, 28);
            this.txtRekno.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(61, 35);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "Cari Siswa:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 81);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "No Rekening:";
            // 
            // txtNoInduk
            // 
            this.txtNoInduk.Location = new System.Drawing.Point(188, 114);
            this.txtNoInduk.Margin = new System.Windows.Forms.Padding(4);
            this.txtNoInduk.MaxLength = 10;
            this.txtNoInduk.Name = "txtNoInduk";
            this.txtNoInduk.Size = new System.Drawing.Size(169, 28);
            this.txtNoInduk.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 117);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "No Induk:";
            // 
            // btnSaveNew
            // 
            this.btnSaveNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveNew.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSaveNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSaveNew.ForeColor = System.Drawing.Color.White;
            this.btnSaveNew.Location = new System.Drawing.Point(141, 494);
            this.btnSaveNew.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveNew.Name = "btnSaveNew";
            this.btnSaveNew.Size = new System.Drawing.Size(243, 41);
            this.btnSaveNew.TabIndex = 4;
            this.btnSaveNew.Text = "Simpan dan Data Baru";
            this.btnSaveNew.UseVisualStyleBackColor = false;
            this.btnSaveNew.Click += new System.EventHandler(this.btnSaveNew_Click);
            // 
            // btnNew
            // 
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNew.Location = new System.Drawing.Point(393, 494);
            this.btnNew.Margin = new System.Windows.Forms.Padding(4);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(136, 41);
            this.btnNew.TabIndex = 5;
            this.btnNew.Text = "Data Baru";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(18, 494);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(116, 41);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Simpan";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // pencarianSiswaControlView1
            // 
            this.pencarianSiswaControlView1.Font = new System.Drawing.Font("Verdana", 10F);
            this.pencarianSiswaControlView1.Location = new System.Drawing.Point(200, 77);
            this.pencarianSiswaControlView1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.pencarianSiswaControlView1.Name = "pencarianSiswaControlView1";
            this.pencarianSiswaControlView1.Size = new System.Drawing.Size(452, 400);
            this.pencarianSiswaControlView1.StringSiswa = null;
            this.pencarianSiswaControlView1.TabIndex = 1;
            this.pencarianSiswaControlView1.Visible = false;
            // 
            // MutasiTabunganView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 581);
            this.Controls.Add(this.pencarianSiswaControlView1);
            this.Controls.Add(this.gbTabungan);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.gbSiswa);
            this.Controls.Add(this.btnSaveNew);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MutasiTabunganView";
            this.Text = "Mutasi Tabungan";
            this.gbTabungan.ResumeLayout(false);
            this.gbTabungan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNilai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmSaldo)).EndInit();
            this.gbSiswa.ResumeLayout(false);
            this.gbSiswa.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbTabungan;
        private System.Windows.Forms.NumericUpDown nmNilai;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nmSaldo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox gbSiswa;
        private System.Windows.Forms.Button btnSearchSiswa;
        private System.Windows.Forms.RadioButton rbPerempuan;
        private System.Windows.Forms.RadioButton rbLaki;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNamaSiswa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRekno;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNoInduk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSaveNew;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cboOpsi;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCari;
        private System.Windows.Forms.Label label8;
        private Common.PencarianSiswaControlView pencarianSiswaControlView1;
    }
}