﻿namespace Fino.Cient
{
    partial class PendaftaranTabunganView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbSiswa = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSiswa = new System.Windows.Forms.TextBox();
            this.rbPerempuan = new System.Windows.Forms.RadioButton();
            this.rbLaki = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNamaSiswa = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNoInduk = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSaveNew = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbTabungan = new System.Windows.Forms.GroupBox();
            this.nmNilai = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRekNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pencarianSiswa = new Fino.Cient.Common.PencarianSiswaControlView();
            this.gbSiswa.SuspendLayout();
            this.gbTabungan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNilai)).BeginInit();
            this.SuspendLayout();
            // 
            // gbSiswa
            // 
            this.gbSiswa.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSiswa.Controls.Add(this.label6);
            this.gbSiswa.Controls.Add(this.txtSiswa);
            this.gbSiswa.Controls.Add(this.rbPerempuan);
            this.gbSiswa.Controls.Add(this.rbLaki);
            this.gbSiswa.Controls.Add(this.label3);
            this.gbSiswa.Controls.Add(this.txtNamaSiswa);
            this.gbSiswa.Controls.Add(this.label2);
            this.gbSiswa.Controls.Add(this.txtNoInduk);
            this.gbSiswa.Controls.Add(this.label1);
            this.gbSiswa.Location = new System.Drawing.Point(9, 10);
            this.gbSiswa.Name = "gbSiswa";
            this.gbSiswa.Size = new System.Drawing.Size(475, 157);
            this.gbSiswa.TabIndex = 0;
            this.gbSiswa.TabStop = false;
            this.gbSiswa.Text = "Data Siswa";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "Cari Siswa:";
            // 
            // txtSiswa
            // 
            this.txtSiswa.Location = new System.Drawing.Point(137, 27);
            this.txtSiswa.Name = "txtSiswa";
            this.txtSiswa.Size = new System.Drawing.Size(243, 24);
            this.txtSiswa.TabIndex = 8;
            this.txtSiswa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSiswa_KeyPress);
            // 
            // rbPerempuan
            // 
            this.rbPerempuan.AutoSize = true;
            this.rbPerempuan.Location = new System.Drawing.Point(225, 118);
            this.rbPerempuan.Name = "rbPerempuan";
            this.rbPerempuan.Size = new System.Drawing.Size(104, 21);
            this.rbPerempuan.TabIndex = 7;
            this.rbPerempuan.TabStop = true;
            this.rbPerempuan.Text = "Perempuan";
            this.rbPerempuan.UseVisualStyleBackColor = true;
            // 
            // rbLaki
            // 
            this.rbLaki.AutoSize = true;
            this.rbLaki.Location = new System.Drawing.Point(137, 118);
            this.rbLaki.Name = "rbLaki";
            this.rbLaki.Size = new System.Drawing.Size(82, 21);
            this.rbLaki.TabIndex = 6;
            this.rbLaki.TabStop = true;
            this.rbLaki.Text = "Laki-laki";
            this.rbLaki.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Kelamin:";
            // 
            // txtNamaSiswa
            // 
            this.txtNamaSiswa.Enabled = false;
            this.txtNamaSiswa.Location = new System.Drawing.Point(137, 88);
            this.txtNamaSiswa.MaxLength = 25;
            this.txtNamaSiswa.Name = "txtNamaSiswa";
            this.txtNamaSiswa.Size = new System.Drawing.Size(184, 24);
            this.txtNamaSiswa.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nama Siswa:";
            // 
            // txtNoInduk
            // 
            this.txtNoInduk.Enabled = false;
            this.txtNoInduk.Location = new System.Drawing.Point(137, 58);
            this.txtNoInduk.MaxLength = 10;
            this.txtNoInduk.Name = "txtNoInduk";
            this.txtNoInduk.Size = new System.Drawing.Size(183, 24);
            this.txtNoInduk.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "No Induk:";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Location = new System.Drawing.Point(385, 352);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(99, 33);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Tutup";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSaveNew
            // 
            this.btnSaveNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveNew.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSaveNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSaveNew.ForeColor = System.Drawing.Color.White;
            this.btnSaveNew.Location = new System.Drawing.Point(97, 352);
            this.btnSaveNew.Name = "btnSaveNew";
            this.btnSaveNew.Size = new System.Drawing.Size(177, 33);
            this.btnSaveNew.TabIndex = 3;
            this.btnSaveNew.Text = "Simpan dan Data Baru";
            this.btnSaveNew.UseVisualStyleBackColor = false;
            this.btnSaveNew.Click += new System.EventHandler(this.btnSaveNew_Click);
            // 
            // btnNew
            // 
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNew.Location = new System.Drawing.Point(281, 352);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(99, 33);
            this.btnNew.TabIndex = 4;
            this.btnNew.Text = "Data Baru";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(8, 352);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(84, 33);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Simpan";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gbTabungan
            // 
            this.gbTabungan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTabungan.Controls.Add(this.nmNilai);
            this.gbTabungan.Controls.Add(this.label4);
            this.gbTabungan.Controls.Add(this.txtRekNo);
            this.gbTabungan.Controls.Add(this.label5);
            this.gbTabungan.Location = new System.Drawing.Point(9, 172);
            this.gbTabungan.Margin = new System.Windows.Forms.Padding(2);
            this.gbTabungan.Name = "gbTabungan";
            this.gbTabungan.Padding = new System.Windows.Forms.Padding(2);
            this.gbTabungan.Size = new System.Drawing.Size(475, 154);
            this.gbTabungan.TabIndex = 1;
            this.gbTabungan.TabStop = false;
            this.gbTabungan.Text = "Pembukaan Rekening";
            // 
            // nmNilai
            // 
            this.nmNilai.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nmNilai.Location = new System.Drawing.Point(137, 35);
            this.nmNilai.Margin = new System.Windows.Forms.Padding(2);
            this.nmNilai.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.nmNilai.Name = "nmNilai";
            this.nmNilai.Size = new System.Drawing.Size(137, 24);
            this.nmNilai.TabIndex = 2;
            this.nmNilai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nmNilai.ThousandsSeparator = true;
            this.nmNilai.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Setoran Awal:";
            // 
            // txtRekNo
            // 
            this.txtRekNo.Location = new System.Drawing.Point(137, 104);
            this.txtRekNo.MaxLength = 10;
            this.txtRekNo.Name = "txtRekNo";
            this.txtRekNo.ReadOnly = true;
            this.txtRekNo.Size = new System.Drawing.Size(184, 24);
            this.txtRekNo.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "No Rekening:";
            // 
            // pencarianSiswa
            // 
            this.pencarianSiswa.Font = new System.Drawing.Font("Verdana", 10F);
            this.pencarianSiswa.Location = new System.Drawing.Point(146, 61);
            this.pencarianSiswa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pencarianSiswa.Name = "pencarianSiswa";
            this.pencarianSiswa.Size = new System.Drawing.Size(329, 320);
            this.pencarianSiswa.StringSiswa = null;
            this.pencarianSiswa.TabIndex = 10;
            this.pencarianSiswa.Visible = false;
            // 
            // PendaftaranTabunganView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 395);
            this.Controls.Add(this.pencarianSiswa);
            this.Controls.Add(this.gbTabungan);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.gbSiswa);
            this.Controls.Add(this.btnSaveNew);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "PendaftaranTabunganView";
            this.Text = "Pendaftaran Tabungan";
            this.gbSiswa.ResumeLayout(false);
            this.gbSiswa.PerformLayout();
            this.gbTabungan.ResumeLayout(false);
            this.gbTabungan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNilai)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSiswa;
        private System.Windows.Forms.RadioButton rbPerempuan;
        private System.Windows.Forms.RadioButton rbLaki;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNamaSiswa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNoInduk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSaveNew;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox gbTabungan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nmNilai;
        private System.Windows.Forms.TextBox txtRekNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSiswa;
        private Common.PencarianSiswaControlView pencarianSiswa;
    }
}