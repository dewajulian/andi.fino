﻿namespace Fino.Cient
{
    partial class AccountClassSettingView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AccountClassSettingView));
            this.gvAccountClassSetting = new System.Windows.Forms.DataGridView();
            this.btnTambah = new System.Windows.Forms.Button();
            this.btnTutup = new System.Windows.Forms.Button();
            this.btnHapus = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.cboAccountClassSetting = new System.Windows.Forms.ComboBox();
            this.accountClassSettingModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.accountCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accountNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paramNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accClassIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paramIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validationMessageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gvAccountClassSetting)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.accountClassSettingModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gvAccountClassSetting
            // 
            this.gvAccountClassSetting.AllowUserToAddRows = false;
            this.gvAccountClassSetting.AllowUserToDeleteRows = false;
            this.gvAccountClassSetting.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvAccountClassSetting.AutoGenerateColumns = false;
            this.gvAccountClassSetting.BackgroundColor = System.Drawing.Color.White;
            this.gvAccountClassSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvAccountClassSetting.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.accountCodeDataGridViewTextBoxColumn,
            this.accountNameDataGridViewTextBoxColumn,
            this.paramNameDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn1,
            this.accClassIdDataGridViewTextBoxColumn,
            this.paramIdDataGridViewTextBoxColumn,
            this.validationMessageDataGridViewTextBoxColumn});
            this.gvAccountClassSetting.DataSource = this.accountClassSettingModelBindingSource;
            this.gvAccountClassSetting.Location = new System.Drawing.Point(183, 42);
            this.gvAccountClassSetting.Margin = new System.Windows.Forms.Padding(5);
            this.gvAccountClassSetting.Name = "gvAccountClassSetting";
            this.gvAccountClassSetting.ReadOnly = true;
            this.gvAccountClassSetting.RowHeadersVisible = false;
            this.gvAccountClassSetting.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvAccountClassSetting.Size = new System.Drawing.Size(434, 425);
            this.gvAccountClassSetting.TabIndex = 7;
            // 
            // btnTambah
            // 
            this.btnTambah.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnTambah.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTambah.ForeColor = System.Drawing.Color.White;
            this.btnTambah.Location = new System.Drawing.Point(16, 17);
            this.btnTambah.Margin = new System.Windows.Forms.Padding(5);
            this.btnTambah.Name = "btnTambah";
            this.btnTambah.Size = new System.Drawing.Size(133, 34);
            this.btnTambah.TabIndex = 0;
            this.btnTambah.Text = "Tambah";
            this.btnTambah.UseVisualStyleBackColor = false;
            // 
            // btnTutup
            // 
            this.btnTutup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTutup.Location = new System.Drawing.Point(16, 106);
            this.btnTutup.Margin = new System.Windows.Forms.Padding(5);
            this.btnTutup.Name = "btnTutup";
            this.btnTutup.Size = new System.Drawing.Size(133, 34);
            this.btnTutup.TabIndex = 2;
            this.btnTutup.Text = "Tutup";
            this.btnTutup.UseVisualStyleBackColor = true;
            // 
            // btnHapus
            // 
            this.btnHapus.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnHapus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHapus.ForeColor = System.Drawing.Color.White;
            this.btnHapus.Location = new System.Drawing.Point(16, 62);
            this.btnHapus.Margin = new System.Windows.Forms.Padding(5);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(133, 34);
            this.btnHapus.TabIndex = 1;
            this.btnHapus.Text = "Hapus";
            this.btnHapus.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnTambah);
            this.panel1.Controls.Add(this.btnTutup);
            this.panel1.Controls.Add(this.btnHapus);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(165, 480);
            this.panel1.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(179, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Tipe setting";
            // 
            // cboAccountClassSetting
            // 
            this.cboAccountClassSetting.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAccountClassSetting.FormattingEnabled = true;
            this.cboAccountClassSetting.Location = new System.Drawing.Point(269, 7);
            this.cboAccountClassSetting.Margin = new System.Windows.Forms.Padding(4);
            this.cboAccountClassSetting.Name = "cboAccountClassSetting";
            this.cboAccountClassSetting.Size = new System.Drawing.Size(160, 24);
            this.cboAccountClassSetting.TabIndex = 9;
            // 
            // accountClassSettingModelBindingSource
            // 
            this.accountClassSettingModelBindingSource.DataSource = typeof(Fino.Model.AccountClassSettingModel);
            // 
            // accountCodeDataGridViewTextBoxColumn
            // 
            this.accountCodeDataGridViewTextBoxColumn.DataPropertyName = "AccountCode";
            this.accountCodeDataGridViewTextBoxColumn.HeaderText = "Kode Akun";
            this.accountCodeDataGridViewTextBoxColumn.Name = "accountCodeDataGridViewTextBoxColumn";
            this.accountCodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.accountCodeDataGridViewTextBoxColumn.Width = 120;
            // 
            // accountNameDataGridViewTextBoxColumn
            // 
            this.accountNameDataGridViewTextBoxColumn.DataPropertyName = "AccountName";
            this.accountNameDataGridViewTextBoxColumn.HeaderText = "Nama Akun";
            this.accountNameDataGridViewTextBoxColumn.Name = "accountNameDataGridViewTextBoxColumn";
            this.accountNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.accountNameDataGridViewTextBoxColumn.Width = 150;
            // 
            // paramNameDataGridViewTextBoxColumn
            // 
            this.paramNameDataGridViewTextBoxColumn.DataPropertyName = "ParamName";
            this.paramNameDataGridViewTextBoxColumn.HeaderText = "Param";
            this.paramNameDataGridViewTextBoxColumn.Name = "paramNameDataGridViewTextBoxColumn";
            this.paramNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.paramNameDataGridViewTextBoxColumn.Width = 160;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "AccountId";
            this.dataGridViewTextBoxColumn1.HeaderText = "AccountId";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // accClassIdDataGridViewTextBoxColumn
            // 
            this.accClassIdDataGridViewTextBoxColumn.DataPropertyName = "AccClassId";
            this.accClassIdDataGridViewTextBoxColumn.HeaderText = "AccClassId";
            this.accClassIdDataGridViewTextBoxColumn.Name = "accClassIdDataGridViewTextBoxColumn";
            this.accClassIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.accClassIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // paramIdDataGridViewTextBoxColumn
            // 
            this.paramIdDataGridViewTextBoxColumn.DataPropertyName = "ParamId";
            this.paramIdDataGridViewTextBoxColumn.HeaderText = "ParamId";
            this.paramIdDataGridViewTextBoxColumn.Name = "paramIdDataGridViewTextBoxColumn";
            this.paramIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.paramIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // validationMessageDataGridViewTextBoxColumn
            // 
            this.validationMessageDataGridViewTextBoxColumn.DataPropertyName = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.HeaderText = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.Name = "validationMessageDataGridViewTextBoxColumn";
            this.validationMessageDataGridViewTextBoxColumn.ReadOnly = true;
            this.validationMessageDataGridViewTextBoxColumn.Visible = false;
            // 
            // AccountClassSettingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 480);
            this.Controls.Add(this.cboAccountClassSetting);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gvAccountClassSetting);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AccountClassSettingView";
            this.Text = "Setting Akun Jurnal";
            ((System.ComponentModel.ISupportInitialize)(this.gvAccountClassSetting)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.accountClassSettingModelBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gvAccountClassSetting;
        private System.Windows.Forms.BindingSource accountClassSettingModelBindingSource;
        private System.Windows.Forms.Button btnTambah;
        private System.Windows.Forms.Button btnTutup;
        private System.Windows.Forms.Button btnHapus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboAccountClassSetting;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paramNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn accClassIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paramIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn validationMessageDataGridViewTextBoxColumn;
    }
}