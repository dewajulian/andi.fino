﻿namespace Fino.Cient
{
    partial class EditAccountClassSettingView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditAccountClassSettingView));
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.lblParam = new System.Windows.Forms.Label();
            this.lblAccountName = new System.Windows.Forms.Label();
            this.cboAccountId = new System.Windows.Forms.ComboBox();
            this.cboParamId = new System.Windows.Forms.ComboBox();
            this.labelAccClassId = new System.Windows.Forms.Label();
            this.txtAccSettingId = new System.Windows.Forms.TextBox();
            this.txtAccClassSettingName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(161, 110);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(4);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(100, 28);
            this.btnSimpan.TabIndex = 3;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            // 
            // btnBatal
            // 
            this.btnBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.Location = new System.Drawing.Point(270, 110);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(4);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(100, 28);
            this.btnBatal.TabIndex = 4;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            // 
            // lblParam
            // 
            this.lblParam.AutoSize = true;
            this.lblParam.Location = new System.Drawing.Point(129, 76);
            this.lblParam.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblParam.Name = "lblParam";
            this.lblParam.Size = new System.Drawing.Size(57, 17);
            this.lblParam.TabIndex = 24;
            this.lblParam.Text = "Param:";
            this.lblParam.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblAccountName
            // 
            this.lblAccountName.AutoSize = true;
            this.lblAccountName.Location = new System.Drawing.Point(94, 46);
            this.lblAccountName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAccountName.Name = "lblAccountName";
            this.lblAccountName.Size = new System.Drawing.Size(92, 17);
            this.lblAccountName.TabIndex = 25;
            this.lblAccountName.Text = "Nama akun:";
            this.lblAccountName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cboAccountId
            // 
            this.cboAccountId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAccountId.FormattingEnabled = true;
            this.cboAccountId.Location = new System.Drawing.Point(193, 43);
            this.cboAccountId.Name = "cboAccountId";
            this.cboAccountId.Size = new System.Drawing.Size(200, 24);
            this.cboAccountId.TabIndex = 1;
            // 
            // cboParamId
            // 
            this.cboParamId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboParamId.FormattingEnabled = true;
            this.cboParamId.Location = new System.Drawing.Point(193, 73);
            this.cboParamId.Name = "cboParamId";
            this.cboParamId.Size = new System.Drawing.Size(200, 24);
            this.cboParamId.TabIndex = 2;
            // 
            // labelAccClassId
            // 
            this.labelAccClassId.AutoSize = true;
            this.labelAccClassId.Location = new System.Drawing.Point(5, 16);
            this.labelAccClassId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelAccClassId.Name = "labelAccClassId";
            this.labelAccClassId.Size = new System.Drawing.Size(181, 17);
            this.labelAccClassId.TabIndex = 31;
            this.labelAccClassId.Text = "Tipe setting akun jurnal:";
            this.labelAccClassId.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtAccSettingId
            // 
            this.txtAccSettingId.Location = new System.Drawing.Point(193, 13);
            this.txtAccSettingId.Name = "txtAccSettingId";
            this.txtAccSettingId.ReadOnly = true;
            this.txtAccSettingId.Size = new System.Drawing.Size(200, 24);
            this.txtAccSettingId.TabIndex = 32;
            this.txtAccSettingId.TabStop = false;
            // 
            // txtAccClassSettingName
            // 
            this.txtAccClassSettingName.Location = new System.Drawing.Point(193, 13);
            this.txtAccClassSettingName.Name = "txtAccClassSettingName";
            this.txtAccClassSettingName.ReadOnly = true;
            this.txtAccClassSettingName.Size = new System.Drawing.Size(200, 24);
            this.txtAccClassSettingName.TabIndex = 0;
            // 
            // EditAccountClassSettingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 151);
            this.Controls.Add(this.txtAccClassSettingName);
            this.Controls.Add(this.txtAccSettingId);
            this.Controls.Add(this.labelAccClassId);
            this.Controls.Add(this.cboParamId);
            this.Controls.Add(this.cboAccountId);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.lblParam);
            this.Controls.Add(this.lblAccountName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "EditAccountClassSettingView";
            this.Text = "Ubah setting akun jurnal";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Label lblParam;
        private System.Windows.Forms.Label lblAccountName;
        private System.Windows.Forms.ComboBox cboAccountId;
        private System.Windows.Forms.ComboBox cboParamId;
        private System.Windows.Forms.Label labelAccClassId;
        private System.Windows.Forms.TextBox txtAccSettingId;
        private System.Windows.Forms.TextBox txtAccClassSettingName;

    }
}