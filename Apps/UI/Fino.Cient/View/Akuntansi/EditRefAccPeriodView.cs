﻿using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class EditRefAccPeriodView : BaseForm, IEditRefAccPeriodView
    {
        #region Events

        public event Action SaveButtonClicked;
        public event Action CloseButtonClicked;

        #endregion

        public EditRefAccPeriodView()
        {
            InitializeComponent();

            this.btnSimpan.Click += btnSimpan_Click;
            this.btnBatal.Click += btnBatal_Click;
        }

        #region Public Properties

        public RefAccPeriodModel SelectedRefAccPeriod { get; private set; }
        public IRefAccPeriodView RefAccPeriodView { get; set; }

        public string PeriodName
        {
            get
            {
                return txtPeriodName.Text;
            }
            set
            {
                txtPeriodName.Text = value;
            }
        }

        public DateTime PeriodStart
        {
            get
            {
                return dtPeriodStart.Value;
            }
            set
            {
                dtPeriodStart.Value = value;
            }
        }

        public DateTime PeriodEnd
        {
            get
            {
                return dtPeriodEnd.Value;
            }
            set
            {
                dtPeriodEnd.Value = value;
            }
        }

        #endregion Public Properties

        #region Control Events

        private void btnBatal_Click(object sender, EventArgs e)
        {
            if (CloseButtonClicked != null)
            {
                CloseButtonClicked();
            }
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (SaveButtonClicked != null)
            {
                SaveButtonClicked();
            }
        }

        #endregion Control Events

        #region IEditRefAccPeriod

        public void ShowForm(RefAccPeriodModel p_RefAccPeriod)
        {
            if (p_RefAccPeriod != null)
            {
                SelectedRefAccPeriod = p_RefAccPeriod;
                this.Text = "Edit Periode Akun";
            }
            else
            {
                SelectedRefAccPeriod = new RefAccPeriodModel();
                this.Text = "Tambah Periode Akun";
            }
            this.Show();
        }

        public void SetBinding()
        {
            txtPeriodName.DataBindings.Add("Text", SelectedRefAccPeriod, RefAccPeriodModel.PeriodNamePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            dtPeriodStart.DataBindings.Add("Value", SelectedRefAccPeriod, RefAccPeriodModel.PeriodStartPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            dtPeriodEnd.DataBindings.Add("Value", SelectedRefAccPeriod, RefAccPeriodModel.PeriodEndPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void ClearData()
        {

        }

        #endregion IEditRefAccPeriod
    }
}
