﻿namespace Fino.Cient
{
    partial class PostingJournalView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnAll = new System.Windows.Forms.Button();
            this.gvTransaksi = new System.Windows.Forms.DataGridView();
            this.IsSelected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.TransactionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransactionDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransactionValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbTanggal = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPost = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gvTransaksi)).BeginInit();
            this.gbTanggal.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.ForeColor = System.Drawing.Color.Black;
            this.btnReset.Location = new System.Drawing.Point(206, 95);
            this.btnReset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(151, 49);
            this.btnReset.TabIndex = 2;
            this.btnReset.Text = "Kosongkan";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnAll
            // 
            this.btnAll.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAll.ForeColor = System.Drawing.Color.White;
            this.btnAll.Location = new System.Drawing.Point(16, 95);
            this.btnAll.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(183, 49);
            this.btnAll.TabIndex = 2;
            this.btnAll.Text = "Pilih Semua";
            this.btnAll.UseVisualStyleBackColor = false;
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // gvTransaksi
            // 
            this.gvTransaksi.AllowUserToAddRows = false;
            this.gvTransaksi.AllowUserToDeleteRows = false;
            this.gvTransaksi.AllowUserToResizeRows = false;
            this.gvTransaksi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvTransaksi.BackgroundColor = System.Drawing.Color.White;
            this.gvTransaksi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvTransaksi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IsSelected,
            this.TransactionName,
            this.TransactionDate,
            this.TransactionValue});
            this.gvTransaksi.Location = new System.Drawing.Point(12, 151);
            this.gvTransaksi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gvTransaksi.MultiSelect = false;
            this.gvTransaksi.Name = "gvTransaksi";
            this.gvTransaksi.ReadOnly = true;
            this.gvTransaksi.RowHeadersVisible = false;
            this.gvTransaksi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvTransaksi.Size = new System.Drawing.Size(844, 286);
            this.gvTransaksi.TabIndex = 1;
            // 
            // IsSelected
            // 
            this.IsSelected.DataPropertyName = "IsSelected";
            this.IsSelected.FalseValue = "false";
            this.IsSelected.FillWeight = 0.8309568F;
            this.IsSelected.HeaderText = "";
            this.IsSelected.Name = "IsSelected";
            this.IsSelected.ReadOnly = true;
            this.IsSelected.TrueValue = "true";
            this.IsSelected.Width = 20;
            // 
            // TransactionName
            // 
            this.TransactionName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TransactionName.DataPropertyName = "TransactionName";
            this.TransactionName.FillWeight = 369.5432F;
            this.TransactionName.HeaderText = "Deskripsi";
            this.TransactionName.Name = "TransactionName";
            this.TransactionName.ReadOnly = true;
            // 
            // TransactionDate
            // 
            this.TransactionDate.DataPropertyName = "TransactionDate";
            dataGridViewCellStyle1.Format = "dd MMM yyyy";
            dataGridViewCellStyle1.NullValue = null;
            this.TransactionDate.DefaultCellStyle = dataGridViewCellStyle1;
            this.TransactionDate.FillWeight = 11.53631F;
            this.TransactionDate.HeaderText = "Tanggal";
            this.TransactionDate.Name = "TransactionDate";
            this.TransactionDate.ReadOnly = true;
            this.TransactionDate.Width = 130;
            // 
            // TransactionValue
            // 
            this.TransactionValue.DataPropertyName = "TransactionValue";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.TransactionValue.DefaultCellStyle = dataGridViewCellStyle2;
            this.TransactionValue.FillWeight = 18.08958F;
            this.TransactionValue.HeaderText = "Nilai";
            this.TransactionValue.Name = "TransactionValue";
            this.TransactionValue.ReadOnly = true;
            this.TransactionValue.Width = 150;
            // 
            // gbTanggal
            // 
            this.gbTanggal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTanggal.Controls.Add(this.btnClose);
            this.gbTanggal.Controls.Add(this.btnSearch);
            this.gbTanggal.Controls.Add(this.label1);
            this.gbTanggal.Location = new System.Drawing.Point(12, 12);
            this.gbTanggal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbTanggal.Name = "gbTanggal";
            this.gbTanggal.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbTanggal.Size = new System.Drawing.Size(844, 75);
            this.gbTanggal.TabIndex = 0;
            this.gbTanggal.TabStop = false;
            this.gbTanggal.Text = "Informasi Periode Akuntansi";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(710, 28);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(116, 39);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Tutup";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.ForeColor = System.Drawing.Color.White;
            this.btnSearch.Location = new System.Drawing.Point(587, 28);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(116, 39);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Ambil Data";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mulai Tanggal:";
            // 
            // btnPost
            // 
            this.btnPost.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnPost.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPost.ForeColor = System.Drawing.Color.White;
            this.btnPost.Location = new System.Drawing.Point(363, 94);
            this.btnPost.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnPost.Name = "btnPost";
            this.btnPost.Size = new System.Drawing.Size(183, 50);
            this.btnPost.TabIndex = 2;
            this.btnPost.Text = "Post ke Jurnal";
            this.btnPost.UseVisualStyleBackColor = false;
            this.btnPost.Click += new System.EventHandler(this.btnPost_Click);
            // 
            // PostingJournalView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 450);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnPost);
            this.Controls.Add(this.btnAll);
            this.Controls.Add(this.gvTransaksi);
            this.Controls.Add(this.gbTanggal);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "PostingJournalView";
            this.Text = "Posting Transaksi ke Jurnal";
            ((System.ComponentModel.ISupportInitialize)(this.gvTransaksi)).EndInit();
            this.gbTanggal.ResumeLayout(false);
            this.gbTanggal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbTanggal;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.DataGridView gvTransaksi;
        private System.Windows.Forms.Button btnPost;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsSelected;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransactionName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransactionDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransactionValue;
    }
}