﻿using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class EditRefAccountView : BaseForm, IEditRefAccountView
    {

        #region Events

        public event Action BtnBatalClicked;
        public event Action BtnSimpanClicked;

        #endregion Events

        #region Public Properties

        public RefAccountModel SelectedRefAccount { get; private set; }
        public IRefAccountView RefAccountView { get; set; }

        public string NamaAkun
        {
            get
            {
                return txtNamaAkun.Text;
            }
            set
            {
                txtNamaAkun.Text = value;
            }
        }

        public string KodeAkun
        {
            get
            {
                return txtKodeAkun.Text;
            }
            set
            {
                txtKodeAkun.Text = value;
            }
        }

        #endregion Public Properties

        #region Constructors

        public EditRefAccountView()
        {
            InitializeComponent();

            this.btnSimpan.Click += btnSimpan_Click;
            this.btnBatal.Click += btnBatal_Click;
        }

        #endregion Constructors

        #region Public Methods

        public void ShowForm(RefAccountModel refAccount)
        {
            if (refAccount != null)
            {
                SelectedRefAccount = refAccount;
                this.Text = "Edit Akun";
            }
            else
            {
                SelectedRefAccount = new RefAccountModel();
                this.Text = "Tambah Akun";
            }
            this.Show();
        }

        #endregion Public Methods

        #region IEditRefAccountView

        public void SetBinding()
        {
            txtNamaAkun.DataBindings.Add("Text", SelectedRefAccount, RefAccountModel.NamaPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtKodeAkun.DataBindings.Add("Text", SelectedRefAccount, RefAccountModel.CodePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void ClearData()
        {
            
        }

        #endregion IEditRefAccountView

        #region Control Events

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (BtnSimpanClicked != null)
            {
                BtnSimpanClicked();
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            if (BtnBatalClicked != null)
            {
                BtnBatalClicked();
            }
        }

        #endregion ControlEvents
    }
}
