﻿namespace Fino.Cient
{
    partial class EditRefAccountView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditRefAccountView));
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.lblKodeBiaya = new System.Windows.Forms.Label();
            this.lblNamaBiaya = new System.Windows.Forms.Label();
            this.txtKodeAkun = new System.Windows.Forms.TextBox();
            this.txtNamaAkun = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(161, 85);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(4);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(100, 28);
            this.btnSimpan.TabIndex = 20;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            // 
            // btnBatal
            // 
            this.btnBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.Location = new System.Drawing.Point(270, 85);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(4);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(100, 28);
            this.btnBatal.TabIndex = 21;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            // 
            // lblKodeBiaya
            // 
            this.lblKodeBiaya.AutoSize = true;
            this.lblKodeBiaya.Location = new System.Drawing.Point(99, 48);
            this.lblKodeBiaya.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKodeBiaya.Name = "lblKodeBiaya";
            this.lblKodeBiaya.Size = new System.Drawing.Size(89, 17);
            this.lblKodeBiaya.TabIndex = 15;
            this.lblKodeBiaya.Text = "Kode akun:";
            // 
            // lblNamaBiaya
            // 
            this.lblNamaBiaya.AutoSize = true;
            this.lblNamaBiaya.Location = new System.Drawing.Point(94, 16);
            this.lblNamaBiaya.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNamaBiaya.Name = "lblNamaBiaya";
            this.lblNamaBiaya.Size = new System.Drawing.Size(92, 17);
            this.lblNamaBiaya.TabIndex = 16;
            this.lblNamaBiaya.Text = "Nama akun:";
            // 
            // txtKodeAkun
            // 
            this.txtKodeAkun.Location = new System.Drawing.Point(203, 44);
            this.txtKodeAkun.Margin = new System.Windows.Forms.Padding(4);
            this.txtKodeAkun.Name = "txtKodeAkun";
            this.txtKodeAkun.Size = new System.Drawing.Size(160, 24);
            this.txtKodeAkun.TabIndex = 9;
            // 
            // txtNamaAkun
            // 
            this.txtNamaAkun.Location = new System.Drawing.Point(203, 12);
            this.txtNamaAkun.Margin = new System.Windows.Forms.Padding(4);
            this.txtNamaAkun.Name = "txtNamaAkun";
            this.txtNamaAkun.Size = new System.Drawing.Size(160, 24);
            this.txtNamaAkun.TabIndex = 8;
            // 
            // EditRefAccountView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 126);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.lblKodeBiaya);
            this.Controls.Add(this.lblNamaBiaya);
            this.Controls.Add(this.txtKodeAkun);
            this.Controls.Add(this.txtNamaAkun);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditRefAccountView";
            this.Text = "Edit Akun";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Label lblKodeBiaya;
        private System.Windows.Forms.Label lblNamaBiaya;
        private System.Windows.Forms.TextBox txtKodeAkun;
        private System.Windows.Forms.TextBox txtNamaAkun;
    }
}