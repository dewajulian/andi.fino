﻿using Fino.Cient.Startup;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class UbahKataSandiView : BaseForm, IUbahKataSandiView
    {
        #region Events

        public event Action BtnSimpanClicked;
        public event Action BtnTutupClicked;
        public event Action BtnBrowseClicked;

        #endregion Events

        #region Controls

        private Label lblUsername;
        private Label lblFileKunci;
        private TextBox txtFileKunci;
        private Button btnBrowse;

        #endregion Controls

        #region Public Properties

        public ChangePasswordModel CurrentModel { get; set; }
        public RestorePasswordModel RestorePasswordModel { get; set; }
        public TextBox txtUsername { get; set; }
        public bool IsRestorePassword { get; set; }

        public string Username
        {
            get
            {
                return txtUsername.Text;
            }
            set
            {
                txtUsername.Text = value;
            }
        }

        public string OldPassword
        {
            get
            {
                return txtSandiLama.Text;
            }
            set
            {
                txtSandiLama.Text = value;
            }
        }

        public string NewPassword
        {
            get
            {
                return txtSandiBaru.Text;
            }
            set
            {
                txtSandiBaru.Text = value;
            }
        }

        public string FileKunci
        {
            get
            {
                return txtFileKunci.Text;
            }
            set
            {
                txtFileKunci.Text = value;
            }
        }

        #endregion Public Properties

        public UbahKataSandiView()
        {
            InitializeComponent();
            this.IsRestorePassword = false;
            this.txtUsername = new TextBox();
            this.btnSimpan.Click += btnSimpan_Click;
            this.btnTutup.Click += btnTutup_Click;
        }

        private void btnTutup_Click(object sender, EventArgs e)
        {
            if (BtnTutupClicked != null)
            {
                BtnTutupClicked();
            }
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (BtnSimpanClicked != null)
            {
                BtnSimpanClicked();
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (BtnBrowseClicked != null)
            {
                BtnBrowseClicked();
            }
        }

        #region IUbahKataSandiView

        public void SetBinding()
        {
            txtUsername.DataBindings.Add("Text", CurrentModel, ChangePasswordModel.UsernamePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtSandiLama.DataBindings.Add("Text", CurrentModel, ChangePasswordModel.OldPasswordPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtSandiBaru.DataBindings.Add("Text", CurrentModel, ChangePasswordModel.NewPasswordPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtUlangSandiBaru.DataBindings.Add("Text", CurrentModel, ChangePasswordModel.RepeatNewPasswordPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void SetRestorePasswordBinding()
        {
            txtUsername.DataBindings.Add("Text", RestorePasswordModel, RestorePasswordModel.UsernamePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtSandiBaru.DataBindings.Add("Text", RestorePasswordModel, RestorePasswordModel.NewPasswordPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtUlangSandiBaru.DataBindings.Add("Text", RestorePasswordModel, RestorePasswordModel.RepeatNewPasswordPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtFileKunci.DataBindings.Add("Text", RestorePasswordModel, RestorePasswordModel.KeyFilePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        public void LoadData()
        {
            CurrentModel.Username = ViewLocator.Instance.LogonView.Username;
        }

        public void ShowLupaKataSandiForm()
        {
            this.IsRestorePassword = true;

            lblUsername = new Label
            {
                Text = "Nama pengguna",
                Location = lblKataSandiLama.Location
            };

            txtUsername = new TextBox
            {
                Location = txtSandiLama.Location,
                Size = txtSandiLama.Size,
            };

            lblFileKunci = new Label
            {
                Text = "File kunci",
                Location = new Point(lblUlangKataSandiBaru.Left, lblUlangKataSandiBaru.Top + 30)
            };
            
            txtFileKunci = new TextBox
            {
                ReadOnly = true,
                Location = new Point(txtUlangSandiBaru.Left, txtUlangSandiBaru.Top + 30),
                Size = txtUlangSandiBaru.Size
            };

            btnBrowse = new Button
            {
                Text = "Browse",
                Location = new Point(txtUlangSandiBaru.Left + txtUlangSandiBaru.Width + 6, txtUlangSandiBaru.Top + 30),
                Size = new Size(btnSimpan.Width, txtUlangSandiBaru.Height),
                BackColor = btnSimpan.BackColor,
                FlatStyle = btnSimpan.FlatStyle
            };

            btnBrowse.Click += btnBrowse_Click;

            this.Controls.Add(lblUsername);
            this.Controls.Add(txtUsername);
            this.Controls.Add(lblFileKunci);
            this.Controls.Add(txtFileKunci);
            this.Controls.Add(btnBrowse);

            lblKataSandiLama.Hide();
            txtSandiLama.Hide();
            txtUsername.TabIndex = 1;
            txtSandiBaru.TabIndex = 2;
            txtUlangSandiBaru.TabIndex = 3;
            txtFileKunci.TabIndex = 4;
            btnBrowse.TabIndex = 5;
            btnSimpan.TabIndex = 6;
            btnTutup.TabIndex = 7;
            //lblUlangKataSandiBaru.Location = lblKataSandiBaru.Location;
            //lblKataSandiBaru.Location = lblKataSandiLama.Location;

            //txtUlangSandiBaru.Location = txtSandiBaru.Location;
            //txtSandiBaru.Location = txtSandiLama.Location;

            int heightAddition = txtFileKunci.Height + 30;
            btnSimpan.Top += heightAddition;
            btnTutup.Top += heightAddition;

            this.StartPosition = FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.MdiParent = null;
            this.Width += btnBrowse.Width + 12;
            this.Height += heightAddition;
            this.ShowDialog();
        }

        #endregion IUbahKataSandiView
    }
}
