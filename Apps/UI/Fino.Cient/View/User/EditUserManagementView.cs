﻿using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class EditUserManagementView : BaseForm, IEditUserManagementView
    {
        #region Events

        public event Action BtnBatalClicked;
        public event Action BtnSimpanClicked;

        #endregion Events

        #region Public Properties

        public UserManagementModel SelectedUser { get; private set; }
        public IUserManagementView UserManagementView { get; set; }

        public string Fullname
        {
            get
            {
                return txtFullname.Text;
            }
            set
            {
                txtFullname.Text = value;
            }
        }

        public string Username
        {
            get
            {
                return txtUsername.Text;
            }
            set
            {
                txtUsername.Text = value;
            }
        }

        public string Password
        {
            get
            {
                return txtPassword.Text;
            }
            set
            {
                txtPassword.Text = value;
            }
        }

        public string Phone
        {
            get
            {
                return txtPhone.Text;
            }
            set
            {
                txtPhone.Text = value;
            }
        }

        public string Email
        {
            get
            {
                return txtEmail.Text;
            }
            set
            {
                txtEmail.Text = value;
            }
        }

        #endregion Public Properties

        #region Constructors

        public EditUserManagementView()
        {
            InitializeComponent();

            this.btnSimpan.Click += btnSimpan_Click;
            this.btnBatal.Click += btnBatal_Click;
        }

        #endregion Constructors

        #region Public Methods

        public void ShowForm(UserManagementModel user)
        {
            if (user != null)
            {
                SelectedUser = user;
                this.Text = "Edit Pengguna";
            }
            else
            {
                SelectedUser = new UserManagementModel();
                this.Text = "Tambah Pengguna";
            }
            this.Show();
        }

        #endregion Public Methods

        #region Control Events

        private void btnBatal_Click(object sender, EventArgs e)
        {
            if (BtnBatalClicked != null)
            {
                BtnBatalClicked();
            }
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (BtnSimpanClicked != null)
            {
                BtnSimpanClicked();
            }
        }

        #endregion Control Events

        #region IEditUserManagementView

        public void SetBinding()
        {
            txtFullname.DataBindings.Add("Text", SelectedUser, UserManagementModel.FullNamePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtUsername.DataBindings.Add("Text", SelectedUser, UserManagementModel.UsernamePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtPassword.DataBindings.Add("Text", SelectedUser, UserManagementModel.PasswordPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtPhone.DataBindings.Add("Text", SelectedUser, UserManagementModel.PhonePropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
            txtEmail.DataBindings.Add("Text", SelectedUser, UserManagementModel.EmailPropertyName,
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        #endregion IEditUserManagementView
    }
}
