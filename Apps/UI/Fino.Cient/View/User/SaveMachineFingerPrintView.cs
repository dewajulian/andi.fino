﻿using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class SaveMachineFingerPrintView : BaseForm, ISaveMachineFingerPrintView
    {
        public event Action ButtonSaveClicked;

        public SaveMachineFingerPrintView()
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.CenterScreen;
            this.lblMessage.Text = AppResource.MSG_SAVE_MACHINE_FINGERPRINT;
            this.btnSimpan.Click += btnSimpan_Click;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (ButtonSaveClicked != null)
            {
                ButtonSaveClicked();
            }
        }

        public void UpdateFirstRun(bool isFirstRun)
        {
            Properties.Settings.Default.IsFirstRun = isFirstRun;
            Properties.Settings.Default.Save();
        }
    }
}
