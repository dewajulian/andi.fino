﻿using Fino.Cient.Configuration;
using Fino.Cient.Properties;
using Fino.Cient.Startup;
using Fino.Lib.Core.Settings;
using Fino.Lib.Win32;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace Fino.Cient
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (!SingleInstance.Start())
            {
                SingleInstance.ShowFirstInstance();
                return;
            }
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            AppVariable.Instance.AppGlobal = new Dictionary<string, object>();
            
            MapperConfiguration.Instance.SetupMapping();

            FirstRun();
            ViewLocator.Instance.Initialise();
            ViewLocator.Instance.LogonView.ShowDialog();

            if (ViewLocator.Instance.LogonView.LogonResult)
            {
                ViewLocator.Instance.ResetUbahKataSandiView();
                Application.Run(ViewLocator.Instance.MainFormView);
            }            

            SingleInstance.Stop();
        }

        private static void FirstRun()
        {
            Settings.Default.Upgrade();

            if (Settings.Default.IsFirstRun)
            {
                ViewLocator.Instance.SaveMachineFingerPrintView.ShowDialog();
            }
        }
    }
}
