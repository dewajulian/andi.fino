﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Lib.Win32
{
    public partial class TaskProgress : Form
    {
        public static ProcessResult GetDataResult(Task<ProcessResult> p_Task, string p_Message)
        {
            TaskProgress dlgSearchProcessing = new TaskProgress(p_Task, p_Message);

            dlgSearchProcessing.ShowDialog();

            return dlgSearchProcessing.Result;
        }

        private Task<ProcessResult> _LogicToRun = null;

        public string LoadingMessage
        {
            get { return this.lblMessage.Text; }
            set { this.lblMessage.Text = value; }
        }

        public ProcessResult Result { get; private set; }

        public TaskProgress(Task<ProcessResult> p_LogicToRun, string p_WaitingMessage)
        {
            InitializeComponent();

            _LogicToRun = p_LogicToRun;
            LoadingMessage = p_WaitingMessage;
        }

        private void TaskProgress_Shown(object sender, EventArgs e)
        {
            if (_LogicToRun != null)
            {
                _LogicToRun.Start();
                
                _LogicToRun.ContinueWith(task =>
                {
                    Result = task.Result;
                    DialogResult = Result.IsSucess ? DialogResult.OK : DialogResult.Cancel;
                });
            }
        }

    }
}
