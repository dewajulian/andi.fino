﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Lib.Win32
{
    public class ControlHelper
    {
        public static void ChangeEditable(bool p_Enabled, Control p_Owner)
        {
            ChangeEditable(p_Enabled, p_Owner, new string[] { });
        }

        public static void ChangeEditable(bool p_Enabled, Control p_Owner, string[] p_ExceptControlNames)
        {
            foreach (Control c in p_Owner.Controls)
            {
                var ctype = c.GetType();
                var impacted = !p_ExceptControlNames.Any(e => e.Equals(c.Name));

                if (impacted)
                {
                    if (ctype.Equals(typeof(TextBox)))
                    {
                        ((TextBox)c).ReadOnly = !p_Enabled;
                    }
                    if (ctype.Equals(typeof(RadioButton)))
                    {
                        ((RadioButton)c).Enabled = p_Enabled;
                    }
                    if (ctype.Equals(typeof(ComboBox)))
                    {
                        ((ComboBox)c).Enabled = p_Enabled;
                    }
                    if (ctype.Equals(typeof(DateTimePicker)))
                    {
                        ((DateTimePicker)c).Enabled = p_Enabled;
                    }
                    if (ctype.Equals(typeof(CheckBox)))
                    {
                        ((CheckBox)c).Enabled = p_Enabled;
                    }
                    if (ctype.Equals(typeof(ListView)))
                    {
                        ((ListView)c).Enabled = p_Enabled;
                    }
                    if (ctype.Equals(typeof(DateTimePicker)))
                    {
                        ((DateTimePicker)c).Enabled = p_Enabled;
                    }
                    if (ctype.Equals(typeof(Button)))
                    {
                        ((Button)c).Enabled = p_Enabled;
                    }
                    if (ctype.Equals(typeof(ListBox)))
                    {
                        ((ListBox)c).Enabled = p_Enabled;
                    }
                    if (ctype.Equals(typeof(NumericUpDown)))
                    {
                        ((NumericUpDown)c).ReadOnly = !p_Enabled;
                    }
                }
            }
        }
    }
}
