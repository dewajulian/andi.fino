﻿using System;

namespace Fino.Lib.Core.Interface
{
    public interface IUserModel
    {
        int UserId { get; set; }
        string Password { get; set; }
        string Username { get; set; }
        string FullName { get; set; }
    }
}
