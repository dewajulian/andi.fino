﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core
{
    public class BaseModel : INotifyPropertyChanged, IEditableObject, ICloneable
    {
        object _backup;
        bool _inEdit;

        public string ValidationMessage { get; set; }
        public event EventHandler<CustomEventArgs<string>> ErrorValidation;

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void InvokePropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, e);
        }

        public void ValidateProperty(object p_Value, string p_NamaProperty)
        {
            try
            {
                Validator.ValidateProperty(p_Value,
                    new ValidationContext(this, null, null) { MemberName = p_NamaProperty });
                ValidationMessage = "";
            }
            catch (ValidationException ve)
            {
                ValidationMessage = ve.Message;

                if (ErrorValidation != null)
                {
                    ErrorValidation(this, new CustomEventArgs<string>(ValidationMessage));
                }
            }
        }

        #endregion

        public void BeginEdit()
        {
            if (_inEdit) return;
            _backup = Clone();
            _inEdit = true;
        }

        public void CancelEdit()
        {
            if (!_inEdit) return;            
            foreach (var info in this.GetType().GetProperties())
            {
                if (!info.CanRead || !info.CanWrite) continue;
                var oldValue = info.GetValue(_backup, null);
                this.GetType().GetProperty(info.Name).SetValue(this, oldValue, null);
            }
            _inEdit = false;
        }

        public void EndEdit()
        {
            if (!_inEdit) return;
            _backup = null;
            _inEdit = false;
        }

        public object Clone()
        {
            return this.Copy();
        }
    }
}
