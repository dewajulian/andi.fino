﻿
namespace Fino.Lib.Core
{
    public enum RefAccPeriodStatusEnum
    {
        New,
        Opened,
        Closed,
        Deactivated
    }
}
