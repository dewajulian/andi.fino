﻿
namespace Fino.Lib.Core
{
    public enum RepetisiEnum
    {
        NONE = 0,
        MINGGUAN = 1,
        BULANAN = 2,
        TAHUNAN = 3,
        TAHUNAJARAN = 4,
        SEMESTER = 5,
        CAWU = 6
    }
}
