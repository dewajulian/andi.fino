﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core.Messaging
{
    public enum MessageTypeEnum
    {
        BroadCast = 1,
        WithKey = 2
    }
}
