﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core.Messaging
{
    public class MessageData
    {
        public MessageTypeEnum MessageType { get; private set; }
        public object Data { get; private set; }

        public MessageData(MessageTypeEnum p_MessageType, object p_MessageData)
        {
            MessageType = p_MessageType;
            Data = p_MessageData;
        }    
    }
}
