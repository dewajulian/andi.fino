﻿
namespace Fino.Lib.Core
{
    public enum AccountClassificationEnum
    {
        KAS = 1,
        PIUTANG = 2,
        PENDAPATAN = 3,
        HUTANG = 4,
        CANCELED_PIUTANG = 5,
        BEBAN = 6
    }
}
