﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core
{
    public enum MainMenuEnum
    {
        PendaftaranSiswaBaru,
        DaftarUlang,
        PotonganBiaya,
        Jemputan,
        Katering,
        JenisPenerimaan,
        GrupPenerimaan,
        BukaRekening,
        Transaksi,
        LaporanTabungan,
        KodeAkun,
        PostingJurnal,
        JurnalHarian,
        PostingBukuBesar,
        LaporanAkuntansi,
        LaporanPiutang,
        ReportRPP,
        PenerimaanPelunasan,
        DaftarLayanan,
        SiswaExplorer,
        SettingNilai,
        PeriodeAkun,
        LaporanMutasi,
        UserManagement,
        CicilanBiaya,
        JenisPengeluaran,
        PenerimaanTagihan,
        Pembayaran,
        RekapPengeluaran,
        LaporanPengeluaran,
        SettingAkunJurnal,
        PengelolaanKelas,
        RiwayatPenerimaan
    }
}
