﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core
{
    public enum TransactionStatusEnum
    {
        OPEN = 0,
        POSTED = 1,
        CANCELED = 2,
        POST_CANCELED = 3
    }
}
