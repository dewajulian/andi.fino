﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Security.Cryptography;
using System.Text;

namespace Fino.Lib.Core.Util
{
    public class EnvironmentHelper
    {
        #region Singleton

        private static readonly Lazy<EnvironmentHelper> lazy = new Lazy<EnvironmentHelper>(() => new EnvironmentHelper());

        public static EnvironmentHelper Instance { get { return lazy.Value; } }

        private EnvironmentHelper() { }

        #endregion Singleton

        #region Private Methods

        private string GetManufacturer()
        {
            var res = string.Empty;

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from Win32_BIOS");
            foreach (ManagementObject share in searcher.Get())
            {
                foreach (PropertyData PC in share.Properties)
                {
                    if (PC.Name.Equals("Manufacturer"))
                    {
                        res = PC.Value == null ? string.Empty : PC.Value.ToString();
                    }
                }
            }

            return res;
        }

        private static string GetMotherboard()
        {
            var res = string.Empty;

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from Win32_BaseBoard");
            foreach (ManagementObject share in searcher.Get())
            {
                foreach (PropertyData PC in share.Properties)
                {
                    if (PC.Name.Equals("Manufacturer"))
                    {
                        res = PC.Value == null ? string.Empty : PC.Value.ToString();
                    }
                }
            }

            return res;
        }

        private string GetBiosSerialNumber()
        {
            var res = string.Empty;

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from Win32_BIOS");
            foreach (ManagementObject share in searcher.Get())
            {
                foreach (PropertyData PC in share.Properties)
                {
                    if (PC.Name.Equals("SerialNumber"))
                    {
                        res = PC.Value == null ? string.Empty : PC.Value.ToString();
                    }
                }
            }

            return res;
        }

        private string GenerateKey(EnvironmentInfo info)
        {
            MD5 sec = new MD5CryptoServiceProvider();
            ASCIIEncoding asciiEncoding = new ASCIIEncoding();
            byte[] byteArr = asciiEncoding.GetBytes(GenerateMachineFingerPrint(info));
            return GetHexString(sec.ComputeHash(byteArr));
        }

        private string GenerateMachineFingerPrint(EnvironmentInfo info)
        {
            return string.Format("BIOS >> {0}\nBASE >> {1}\nMANUFACTURER >> {2}", info.BIOSSerialNumber, info.Motherboard, info.Manufacturer);
        }

        private static string GetHexString(byte[] bt)
        {
            string s = string.Empty;
            for (int i = 0; i < bt.Length; i++)
            {
                byte b = bt[i];
                int n, n1, n2;
                n = (int)b;
                n1 = n & 15;
                n2 = (n >> 4) & 15;
                if (n2 > 9)
                    s += ((char)(n2 - 10 + (int)'A')).ToString();
                else
                    s += n2.ToString();
                if (n1 > 9)
                    s += ((char)(n1 - 10 + (int)'A')).ToString();
                else
                    s += n1.ToString();
                if ((i + 1) != bt.Length && (i + 1) % 2 == 0) s += "-";
            }
            return s;
        }

        #endregion Private Methods

        #region Public Methods

        public EnvironmentInfo GetEnvironmentInfo()
        {
            var info = new EnvironmentInfo();

            info.Manufacturer = GetManufacturer();
            info.Motherboard = GetMotherboard();
            info.BIOSSerialNumber = GetBiosSerialNumber();

            return info;
        }

        public string GetEnvironmentHashID()
        {
            var info = GetEnvironmentInfo();
            var key = GenerateKey(info);
            return key;
        }

        #endregion Public Methods
    }
}
