﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fino.Lib.Core.Util
{
    public class MainMenuHelper
    {
        #region Singleton

        private static readonly Lazy<MainMenuHelper> lazy = new Lazy<MainMenuHelper>(() => new MainMenuHelper());

        public static MainMenuHelper Instance { get { return lazy.Value; } }

        private MainMenuHelper() { }

        #endregion Singleton

        #region Public Methods

        public void InitializeMainMenu(ListView listViewMainMenu)
        {
            foreach (var header in Constants.Instance.MenuGroupHeader)
            {
                listViewMainMenu.Groups.Add(header, header);
            }

            ImageList imgList = new ImageList();
            foreach (MainMenuEnum menuEnum in Enum.GetValues(typeof(MainMenuEnum)))
            {
                string imageFilepath = Path.Combine(Constants.MENU_IMAGE_DIRECTORY, menuEnum.ToString() + ".png");

                if (File.Exists(imageFilepath))
                {
                    Image img = Image.FromFile(imageFilepath);
                    imgList.Images.Add(menuEnum.ToString(), img);
                }
            }

            listViewMainMenu.LargeImageList = imgList;
            listViewMainMenu.SmallImageList = imgList;

            InitializeSiswaMenu(listViewMainMenu);
            InitializePenerimaanMenu(listViewMainMenu);
            InitializePengeluaranMenu(listViewMainMenu);
            InitializeAkuntansiMainMenu(listViewMainMenu);
            InitializeTabunganMainMenu(listViewMainMenu);
        }

        #endregion Public Methods

        #region Private Methods

        private void InitializeSiswaMenu(ListView listViewMainMenu)
        {
            ListViewItem siswaExplorerItem = new ListViewItem("Pencarian Siswa", MainMenuEnum.SiswaExplorer.ToString());
            siswaExplorerItem.Tag = MainMenuEnum.SiswaExplorer;
            ListViewItem pendaftaranSiswaBaruItem = new ListViewItem("Pendaftaran Siswa Baru", MainMenuEnum.PendaftaranSiswaBaru.ToString());
            pendaftaranSiswaBaruItem.Tag = MainMenuEnum.PendaftaranSiswaBaru;
            ListViewItem daftarUlangItem = new ListViewItem("Daftar Ulang", MainMenuEnum.DaftarUlang.ToString());
            daftarUlangItem.Tag = MainMenuEnum.DaftarUlang;
            ListViewItem potonganBiayaItem = new ListViewItem("Potongan Biaya", MainMenuEnum.PotonganBiaya.ToString());
            potonganBiayaItem.Tag = MainMenuEnum.PotonganBiaya;
            ListViewItem penerimaanPelunasanItem = new ListViewItem("Penerimaan Pelunasan", MainMenuEnum.PenerimaanPelunasan.ToString());
            penerimaanPelunasanItem.Tag = MainMenuEnum.PenerimaanPelunasan;
            ListViewItem layananItem = new ListViewItem("Daftar Layanan", MainMenuEnum.DaftarLayanan.ToString());
            layananItem.Tag = MainMenuEnum.DaftarLayanan;
            ListViewItem cicilanBiaya = new ListViewItem("Cicilan Biaya", MainMenuEnum.CicilanBiaya.ToString());
            cicilanBiaya.Tag = MainMenuEnum.CicilanBiaya;
            ListViewItem pengelolaanKelasItem = new ListViewItem("Pengelolaan Kelas", MainMenuEnum.PengelolaanKelas.ToString());
            pengelolaanKelasItem.Tag = MainMenuEnum.PengelolaanKelas;

            listViewMainMenu.Items.Add(siswaExplorerItem);
            listViewMainMenu.Items.Add(pendaftaranSiswaBaruItem);
            listViewMainMenu.Items.Add(daftarUlangItem);
            listViewMainMenu.Items.Add(potonganBiayaItem);
            listViewMainMenu.Items.Add(penerimaanPelunasanItem);
            listViewMainMenu.Items.Add(layananItem);
            listViewMainMenu.Items.Add(cicilanBiaya);
            listViewMainMenu.Items.Add(pengelolaanKelasItem);

            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(siswaExplorerItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(pendaftaranSiswaBaruItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(daftarUlangItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(penerimaanPelunasanItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(potonganBiayaItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(layananItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(cicilanBiaya);
            listViewMainMenu.Groups[Constants.MAIN_MENU_SISWA_GROUP_NAME].Items.Add(pengelolaanKelasItem);
        }

        private void InitializePenerimaanMenu(ListView listViewMainMenu)
        {
            ListViewItem jenisPenerimaanItem = new ListViewItem("Jenis Penerimaan", MainMenuEnum.JenisPenerimaan.ToString());
            jenisPenerimaanItem.Tag = MainMenuEnum.JenisPenerimaan;
            ListViewItem grupPenerimaanItem = new ListViewItem("Grup Penerimaan", MainMenuEnum.GrupPenerimaan.ToString());
            grupPenerimaanItem.Tag = MainMenuEnum.GrupPenerimaan;
            ListViewItem settingNilaiBiayaItem = new ListViewItem("Nilai Biaya", MainMenuEnum.SettingNilai.ToString());
            settingNilaiBiayaItem.Tag = MainMenuEnum.SettingNilai;
            ListViewItem ReportRPPItem = new ListViewItem("Laporan Penerimaan dan Piutang", MainMenuEnum.ReportRPP.ToString());
            ReportRPPItem.Tag = MainMenuEnum.ReportRPP;
            ListViewItem LaporanPiutangItem = new ListViewItem("Laporan Piutang", MainMenuEnum.LaporanPiutang.ToString());
            LaporanPiutangItem.Tag = MainMenuEnum.LaporanPiutang;
            ListViewItem RiwayatPenerimaanItem = new ListViewItem("Riwayat Penerimaan", MainMenuEnum.RiwayatPenerimaan.ToString());
            RiwayatPenerimaanItem.Tag = MainMenuEnum.RiwayatPenerimaan;

            listViewMainMenu.Items.Add(jenisPenerimaanItem);
            listViewMainMenu.Items.Add(grupPenerimaanItem);
            listViewMainMenu.Items.Add(settingNilaiBiayaItem);
            listViewMainMenu.Items.Add(ReportRPPItem);
            listViewMainMenu.Items.Add(LaporanPiutangItem);
            listViewMainMenu.Items.Add(RiwayatPenerimaanItem);

            listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(jenisPenerimaanItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(grupPenerimaanItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(settingNilaiBiayaItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(ReportRPPItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(LaporanPiutangItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENERIMAAN_GROUP_NAME].Items.Add(RiwayatPenerimaanItem);
        }

        private void InitializePengeluaranMenu(ListView listViewMainMenu)
        {
            ListViewItem jenisPengeluaranItem = new ListViewItem("Jenis Pengeluaran", MainMenuEnum.JenisPengeluaran.ToString());
            jenisPengeluaranItem.Tag = MainMenuEnum.JenisPengeluaran;
            ListViewItem pembayaranItem = new ListViewItem("Pembayaran", MainMenuEnum.Pembayaran.ToString());
            pembayaranItem.Tag = MainMenuEnum.Pembayaran;
            ListViewItem rekapPengeluaranItem = new ListViewItem("Rekap Pengeluaran", MainMenuEnum.RekapPengeluaran.ToString());
            rekapPengeluaranItem.Tag = MainMenuEnum.RekapPengeluaran;
            ListViewItem laporanPengeluaranItem = new ListViewItem("Laporan Pengeluaran", MainMenuEnum.LaporanPengeluaran.ToString());
            laporanPengeluaranItem.Tag = MainMenuEnum.LaporanPengeluaran;

            listViewMainMenu.Items.Add(jenisPengeluaranItem);
            listViewMainMenu.Items.Add(pembayaranItem);
            listViewMainMenu.Items.Add(rekapPengeluaranItem);
            listViewMainMenu.Items.Add(laporanPengeluaranItem);

            listViewMainMenu.Groups[Constants.MAIN_MENU_PENGELUARAN_GROUP_NAME].Items.Add(jenisPengeluaranItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENGELUARAN_GROUP_NAME].Items.Add(pembayaranItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENGELUARAN_GROUP_NAME].Items.Add(rekapPengeluaranItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_PENGELUARAN_GROUP_NAME].Items.Add(laporanPengeluaranItem);
        }

        private void InitializeAkuntansiMainMenu(ListView listViewMainMenu)
        {
            ListViewItem kodeAkunItem = new ListViewItem("Kode Akun", MainMenuEnum.KodeAkun.ToString());
            kodeAkunItem.Tag = MainMenuEnum.KodeAkun;
            ListViewItem periodeAkunItem = new ListViewItem("Periode Akun", MainMenuEnum.PeriodeAkun.ToString());
            periodeAkunItem.Tag = MainMenuEnum.PeriodeAkun;
            ListViewItem settingAkunJurnalItem = new ListViewItem("Setting Akun Jurnal", MainMenuEnum.SettingAkunJurnal.ToString());
            settingAkunJurnalItem.Tag = MainMenuEnum.SettingAkunJurnal;
            ListViewItem postingJurnalItem = new ListViewItem("Posting Transaksi", MainMenuEnum.PostingJurnal.ToString());
            postingJurnalItem.Tag = MainMenuEnum.PostingJurnal;
            ListViewItem jurnalHarianItem = new ListViewItem("Jurnal Harian", MainMenuEnum.JurnalHarian.ToString());
            jurnalHarianItem.Tag = MainMenuEnum.JurnalHarian;
            ListViewItem laporanAkuntansiItem = new ListViewItem("Neraca Saldo", MainMenuEnum.LaporanAkuntansi.ToString());
            laporanAkuntansiItem.Tag = MainMenuEnum.LaporanAkuntansi;

            listViewMainMenu.Items.Add(kodeAkunItem);
            listViewMainMenu.Items.Add(periodeAkunItem);
            listViewMainMenu.Items.Add(settingAkunJurnalItem);
            listViewMainMenu.Items.Add(postingJurnalItem);
            listViewMainMenu.Items.Add(jurnalHarianItem);
            listViewMainMenu.Items.Add(laporanAkuntansiItem);

            listViewMainMenu.Groups[Constants.MAIN_MENU_AKUNTANSI_GROUP_NAME].Items.Add(kodeAkunItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_AKUNTANSI_GROUP_NAME].Items.Add(periodeAkunItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_AKUNTANSI_GROUP_NAME].Items.Add(settingAkunJurnalItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_AKUNTANSI_GROUP_NAME].Items.Add(postingJurnalItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_AKUNTANSI_GROUP_NAME].Items.Add(jurnalHarianItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_AKUNTANSI_GROUP_NAME].Items.Add(laporanAkuntansiItem);
        }

        private void InitializeTabunganMainMenu(ListView listViewMainMenu)
        {
            ListViewItem rekeningItem = new ListViewItem("Buka Rekening", MainMenuEnum.BukaRekening.ToString());
            rekeningItem.Tag = MainMenuEnum.BukaRekening;
            ListViewItem transaksiItem = new ListViewItem("Transaksi", MainMenuEnum.Transaksi.ToString());
            transaksiItem.Tag = MainMenuEnum.Transaksi;
            ListViewItem laporanTabunganItem = new ListViewItem("Laporan Saldo ", MainMenuEnum.LaporanTabungan.ToString());
            laporanTabunganItem.Tag = MainMenuEnum.LaporanTabungan;
            ListViewItem laporanMutasiItem = new ListViewItem("Laporan Mutasi ", MainMenuEnum.LaporanMutasi.ToString());
            laporanMutasiItem.Tag = MainMenuEnum.LaporanMutasi;

            listViewMainMenu.Items.Add(rekeningItem);
            listViewMainMenu.Items.Add(transaksiItem);
            listViewMainMenu.Items.Add(laporanTabunganItem);
            listViewMainMenu.Items.Add(laporanMutasiItem);

            listViewMainMenu.Groups[Constants.MAIN_MENU_TABUNGAN_GROUP_NAME].Items.Add(rekeningItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_TABUNGAN_GROUP_NAME].Items.Add(transaksiItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_TABUNGAN_GROUP_NAME].Items.Add(laporanTabunganItem);
            listViewMainMenu.Groups[Constants.MAIN_MENU_TABUNGAN_GROUP_NAME].Items.Add(laporanMutasiItem);
        }

        #endregion Private Methods
    }
}
