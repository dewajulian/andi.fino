﻿using Fino.BusinessLogic;
using Fino.Cient;
using Fino.Cient.Configuration;
using Fino.Presenter;
using Fino.View;
using System;
using TechTalk.SpecFlow;
using Fino.Datalib.Entity;
using Fino.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Fino.Model;

namespace Fino.Spec.Master.KonfigurasiBiaya
{
    [Binding]
    public class ShowGrupBiayaSteps
    {
        IGrupBiayaView _grupBiayaView = new GrupBiayaView();
        IKonfigurasiBiayaProcess _process = new KonfigurasiBiayaProcess();
        GrupBiayaPresenter _presenter;

        [Given(@"there are grup biaya datas already saved")]
        public void GivenThereAreGrupBiayaDatasAlreadySaved()
        {
            MapperConfiguration.Instance.SetupMapping();

            _presenter = new GrupBiayaPresenter(_grupBiayaView, _process);

            SaveSomeDataToTheDB();
        }
        
        [When(@"the group biaya form is load")]
        public void WhenTheGroupBiayaFormIsLoad()
        {
            _grupBiayaView.Show();
        }
        
        [Then(@"all group biaya data will be loaded to the datagrid")]
        public void ThenAllGroupBiayaDataWillBeLoadedToTheDatagrid()
        {
            List<GrupBiayaModel> expectedData = _grupBiayaView.GetGVGrupBiayaDataSource();

            Assert.IsNotNull(expectedData);
            Assert.IsTrue(expectedData.Count > 0);
        }

        private void SaveSomeDataToTheDB()
        {
            KonfigurasiBiayaRepository repo = new KonfigurasiBiayaRepository();

            for (int i = 0; i < 5; i++)
            {
                RefGrupBiaya grupBiaya = new RefGrupBiaya();

                grupBiaya.nama = "NamaTest " + i;
                grupBiaya.code = "CodeTest " + i;

                repo.AddGrupBiayaData(grupBiaya);
            }

            repo.SaveChanges();
        }
    }
}
