﻿using System;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;
using Fino.Lib.Core;

namespace Test.Fino.Backend
{
    [TestClass]
    public class Datalib_TestUnit
    {
        private FinoDBContext testDBContext;

        [TestInitialize]
        public void InitiateContext()
        {
            testDBContext = new FinoDBContext();
        }

        /*
        public DbSet<RefKelas> RefKelas { get; set; }
        public DbSet<RefTahunAjaran> RefTahunAjarans { get; set; }
        public DbSet<SiswaKelas> SiswaKelases { get; set; }
        public DbSet<AgtLayanan> AgtLayanans { get; set; }
        public DbSet<Potongan> Potongans { get; set; }
        public DbSet<RefGrupBiaya> RefGrupBiayas { get; set; }
        public DbSet<GrupBiaya> GrupBiayas { get; set; }
         */
        [TestMethod]
        public void CRUD_RefGrupBiaya()
        {
            var result = true;

            var biayaSPP = new RefBiaya
            {
                nama = "SPP1",
                code = "SPP1",
                aktif = true,
                jt_tanggal = 10,
                mulai_tanggal = DateTime.Today,
                repetisi = (int)RepetisiEnum.BULANAN
            };

            testDBContext.RefBiayas.Add(biayaSPP);
            result = result && (testDBContext.SaveChanges() > 0);

            var grupBiayaList = new List<GrupBiaya>();
            var item = testDBContext.RefBiayas.Where(e => e.code.Equals("SPP1")).FirstOrDefault();
            if (item != null)
            {
                grupBiayaList.Add(new GrupBiaya { Biaya_Id = item.biaya_id });
            }

            var biayaSiswaBaru = new RefGrupBiaya
            {
                nama = "Biaya Siswa Baru 1",
                code = "REG01",
                active = true
            };
            testDBContext.RefGrupBiayas.Add(biayaSiswaBaru);
            result = result && (testDBContext.SaveChanges() > 0);

            Assert.IsTrue(result);

        }

        [TestMethod]
        public void CRUD_PosBiaya()
        {
            var result = true;

            var biayaSPP = new RefBiaya
            {
                nama = "SPP1",
                code = "SPP1",
                aktif = true,
                jt_tanggal = 10,
                mulai_tanggal = DateTime.Today,
                repetisi = (int)RepetisiEnum.BULANAN
            };

            biayaSPP = testDBContext.RefBiayas.Add(biayaSPP);
            result = result && (testDBContext.SaveChanges() > 0);

            var male = true;
            var fulan = new RefSiswa
            {
                nama = "Olla Rakiman",
                jkelamin = male,
                Alamat = new SiswaAlamat
                {
                    alamat = "Jl. Kecak Lor No 190",
                    kecamatan = "Kricak",
                    kota = "Ujungpandang",
                    propinsi = "Papua Tenggara"
                },
                siswa_code = "1235"
            };

            fulan = testDBContext.RefSiswas.Add(fulan);
            result = result && (testDBContext.SaveChanges() > 0);


            var pos = new PosBiaya
            {
                Biaya_Id = biayaSPP.biaya_id,
                Siswa_Id = fulan.siswa_id,
                Deskripsi = "SPP Bulan 10",
                IsActive = true,
                IsPaid = false,
                JTempo = DateTime.Now.Date.AddDays(3),
                Nilai = 10000,
                Status_Id = 0,
                DatePaid = null
            };
            pos = testDBContext.PosBiayas.Add(pos);
            result = result && (testDBContext.SaveChanges() > 0);
            Assert.IsTrue(result);

        }


        [TestMethod]
        public void CRUD_RefBiaya()
        {
            var result = true;

            var biayaSPP = new RefBiaya
            {
                nama = "SPP1",
                code = "SPP1",
                aktif = true,
                jt_tanggal = 10,
                mulai_tanggal = DateTime.Today,
                repetisi = (int)RepetisiEnum.BULANAN
            };

            testDBContext.RefBiayas.Add(biayaSPP);
            result = result && (testDBContext.SaveChanges() > 0);

            var item = testDBContext.RefBiayas.Where(e => e.code.Equals("SPP1")).FirstOrDefault();
            if (item != null)
            {
                testDBContext.RefBiayas.Remove(item);
                result = result && (testDBContext.SaveChanges() > 0);
            }


            Assert.IsTrue(result);

        }

        [TestMethod]
        public void CRUD_RefSiswa()
        {
            var result = true;
            var male = true;

            var fulan = new RefSiswa
            {
                nama = "Fulan Gurindo",
                jkelamin = male,
                Alamat = new SiswaAlamat
                {
                    alamat = "Jl. Kecak Lor No 190",
                    kecamatan = "Kricak",
                    kota = "Ujungpandang",
                    propinsi = "Jawa Timur"
                },
                siswa_code = "1234"
            };

            testDBContext.RefSiswas.Add(fulan);
            result = result && (testDBContext.SaveChanges() > 0);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CRUD_RefLayanan()
        {
            var result = true;

            var biayaJemputan = new RefBiaya
            {
                nama = "Fasilitas Jemputan",
                code = "Jemputan",
                aktif = true,
                jt_tanggal = 10,
                mulai_tanggal = DateTime.Today,
                repetisi = (int)RepetisiEnum.BULANAN
            };

            testDBContext.RefBiayas.Add(biayaJemputan);
            result = result && (testDBContext.SaveChanges() > 0);

            // Get ID
            var item = testDBContext.RefBiayas.Where(e => e.code.Equals("Jemputan")).FirstOrDefault();
            if (item != null)
            {
                var biayaLayananList = new List<BiayaLayanan>();
                biayaLayananList.Add(new BiayaLayanan { biaya_id = item.biaya_id });
                var layananJemputan = new RefLayanan
                {
                    nama = "Antar Jemput Siswa",
                    code = "F001"
                };
                testDBContext.RefLayanans.Add(layananJemputan);
                result = result && (testDBContext.SaveChanges() > 0);
            }

            Assert.IsTrue(result);

        }
    }
}
