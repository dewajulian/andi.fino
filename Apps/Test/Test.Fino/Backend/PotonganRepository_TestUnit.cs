﻿using System;
using System.Collections.Generic;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Model;
using Fino.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Fino.Backend
{
    [TestClass]
    public class PotonganRepository_TestUnit
    {


        [TestInitialize]
        public void InitiateContext()
        {
            IDaftarSiswaBaruRepository siswaBaruRepo = new DaftarSiswaBaruRepository();
            var model = new DaftarSiswaBaruModel
            {
                Code = "3001",
                JKelamin = true,
                Kelas_Id = 3,
                Mulai_Tanggal = DateTime.Now.Date,
                Nama = "Coba laksana",
                Siswa_Id = 0,
                TahunAjaran_Id = 3,
                TahunAjaran_Nama = "Nama tahun ajaran"
            };
            siswaBaruRepo.TambahSiswaBaru(model);
        }

        [TestMethod]
        public void UpdatePotongan_Valid()
        {
            IPotonganRepository repo = new PotonganRepository();
            var potonganList = new List<Potongan>();
            for (int i = 0; i < 2; i++)
            {
                potonganList.Add(new Potongan
                {
                    biaya_id = i + 1,
                    hingga_tanggal = DateTime.Now.Date,
                    mulai_tanggal = DateTime.Now.Date.AddYears(1),
                    nilai = 10000,
                    persen = false,
                    siswa_id = 1
                });
            }
            try
            {
                potonganList = repo.UpdatePotongan(potonganList);

                potonganList[0].potongan_id = potonganList[0].potongan_id * (-1);
                potonganList = repo.UpdatePotongan(potonganList);
                
            }
            catch(Exception ex)
            {
                throw ex; 
            }
            Assert.AreEqual(potonganList.Count, 1);
        }
    }
}
